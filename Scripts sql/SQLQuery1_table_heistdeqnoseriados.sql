USE catalogo
GO
 
/****** Object:  Table [catalogo].[producto]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE heistdeqnoseriados(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	nombrematerial [nvarchar](80) NOT NULL,
	CodigoMaterial [nvarchar](12) NOT NULL,
	Origen [nvarchar](240) NULL,
	ODSEntrada [nvarchar](15) NULL,
	Destino [nvarchar](240) NULL,
	ODSSalida [nvarchar](15) NULL,
	Stock int NOT NULL
	)

GO