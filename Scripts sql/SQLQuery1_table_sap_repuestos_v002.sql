USE catalogo
GO
 
/****** Object:  Table [catalogo].[disBotParte]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE sap_repuestos_v002(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	codesap [nvarchar](20) NOT NULL,
	codeax [nvarchar](20) NULL DEFAULT '0',
	tipo [nvarchar](12) NULL,
	name [nvarchar](255) NOT NULL,
	price [float] NULL DEFAULT 0,
	)

GO