USE catalogo
GO
 
/****** Object:  Table [catalogo].[producto]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE repuestos(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	referencia [nvarchar](20) NOT NULL,
	nombre [nvarchar](120) NOT NULL,
	descripcion [nvarchar](240) NULL,
	precio [float] NULL DEFAULT 0,
	stock [int] NULL DEFAULT 0,
	familia [nvarchar](60) NULL,
    proveedor [nvarchar](60) NULL,
	imagen [nvarchar](240) NULL
	)

GO