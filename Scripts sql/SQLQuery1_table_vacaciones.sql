USE catalogo
GO
 
/****** Object:  Table [catalogo].[vacaciones]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE personalvacaciones(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	nombre [nvarchar](60) NOT NULL,
	abono [int] NOT NULL DEFAULT 0,
	cargo [int] NOT NULL DEFAULT 0,
	saldo [int] NOT NULL,
	detalle [nvarchar](240) NULL,
	fecha int NOT NULL
	)

GO