USE [catalogo]
GO

SELECT [id]
      ,[codesap]
      ,[codeax]
      ,[name]
      ,[price]
  FROM [dbo].[sap_repuestos_general]
  order by codesap asc
GO



USE [catalogo]
GO
SELECT  ROW_NUMBER() OVER(PARTITION BY ph.codesap ORDER BY PH.price DESC) Position,
        ph.codesap,
        PH.price 
    FROM [dbo].[sap_repuestos_general] AS PH

	DELETE FROM sap_repuestos_general
    WHERE id IN 
            (
                SELECT X.id
                    FROM (
                            SELECT  ROW_NUMBER() OVER(PARTITION BY ph.codesap ORDER BY PH.price DESC) Position,
                                    PH.id
                                FROM dbo.sap_repuestos_general AS PH
                        ) X
                    WHERE X.Position >1
                )
go

