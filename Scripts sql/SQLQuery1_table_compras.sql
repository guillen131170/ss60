USE catalogo
GO
 
/****** Object:  Table [catalogo].[producto]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE comprasentrada(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
    proveedor [nvarchar](60) NOT NULL,
	nalbaran [nvarchar](120) NOT NULL,
	referencia [nvarchar](20) NULL,
	descripcion [nvarchar](240) NULL,
	fecha [int] NULL DEFAULT 20010101
	)
GO

CREATE TABLE comprassalida(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
    proveedor [nvarchar](60) NOT NULL,
	nalbaran [nvarchar](120) NOT NULL,
	referencia [nvarchar](20) NULL,
	descripcion [nvarchar](240) NULL,
	fecha [int] NULL DEFAULT 20010101
	)
GO

CREATE TABLE comprascompras(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
    proveedor [nvarchar](60) NOT NULL,
	nalbaran [nvarchar](120) NOT NULL,
	referencia [nvarchar](20) NULL,
	descripcion [nvarchar](240) NULL,
	fecha [int] NULL DEFAULT 20010101
	)
GO