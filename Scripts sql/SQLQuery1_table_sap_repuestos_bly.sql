﻿USE catalogo
GO
 
/****** Object:  Table [catalogo].[disBotParte]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE sap_repuestos_bly(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	codesap [nvarchar](20) NOT NULL,
	codeax [nvarchar](20) NULL DEFAULT '0',
	tipo [nvarchar](12) NULL,
	name [nvarchar](255) NOT NULL,
	price [float] NULL DEFAULT 0,
	)

GO


INSERT INTO [dbo].sap_repuestos_bly
           ([codesap]
           ,[name]
           ,[tipo])
     VALUES

('2000150930','Regleta/Fuente alimentación','0€-10€'),
('200015094','Clavija/Cable alimentación','10€-20€'),
('200015095','Bandejas','0€-20€'),
('200015096','Bisagra','0€-30€'),
('200015097','Kit Bisagra','30€-55€'),
('200015154','Burlete genérico','0€-20€'),
('200015155','Burlete específico','20€-40€'),
('200015098','Condensador >6μF','0€-20€'),
('200015099','Contactor','0€-40€'),
('200015100','Contactor potencia','41€-65€'),
('200015101','Cristal','N/A'),
('200015102','Iluminación','0€-7€'),
('200015103','Kit iluminación','7€-20€'),
('200015104','Inversor giro','0€-65€'),
('200015105','Inversor-programador','65€-90€'),
('200015106','Mandos','0€-5€'),
('200015107','Pomo/maneta puerta','0€-20€'),
('200015108','Kit tirador','20€-40€'),
('200015109','Micro puerta','0€-65€'),
('200015110','Kit micro puerta','65€-90€'),
('200015111','Patas','0€-5€'),
('200015112','Kit patas','5€-15€'),
('200014543','PEQUEÑO MATERIAL','0€-4€'),
('200015113','Piloto','0€-5€'),
('200015114','Kit piloto','5€-10€'),
('200015115','placa/centralita horno','N/A'),
('200015116','puerta cristal completa','N/A'),
('200015117','Resistencia','0€-40€'),
('200015118','Resistencia alta potencia','40€-60€'),
('200005976','Rulina','N/A'),
('200015119','Mando/Temporizador/Termostato','0€-20€'),
('200015120','Temporizador','21€-45€'),
('200015121','Kit temporizador','46€-70€'),
('200015122','Termostato seguridad/trabajo','21€-45€'),
('200015123','Kit termostato','46€-70€'),
('200015124','Ventilador/Motor/Turbina','0€-35€'),
('200015125','Ventilador/Motor/Turbina alta potencia','35€-60€'),
('200015127','Zumbador','0€-20€')

GO
