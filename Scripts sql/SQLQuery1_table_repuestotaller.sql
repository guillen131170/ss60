USE catalogo
GO
 
/****** Object:  Table [catalogo].[repuestotaller]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE repuestotaller(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	material [nvarchar](72) NOT NULL,
	codmaterial [nvarchar](12) NOT NULL,
	cantidad [int] NOT NULL,
	ordentaller [int] NOT NULL,
	)

GO