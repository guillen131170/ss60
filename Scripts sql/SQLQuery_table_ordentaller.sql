USE catalogo
GO
 
/****** Object:  Table [catalogo].[ordentaller]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE ordentaller(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	tecnico [nvarchar](36) NULL DEFAULT '',
	cod_tecnico [nvarchar](12) NULL DEFAULT '',
	proyecto [nvarchar](36) NULL DEFAULT '',
	cod_proyecto [nvarchar](6) NULL DEFAULT '',
	cliente [nvarchar](72) NULL DEFAULT '',
	pds [nvarchar](12) NULL DEFAULT '',
	ods [nvarchar](24) NULL DEFAULT '',
	provincia [nvarchar](36) NULL DEFAULT '',
	actividad [nvarchar](24) NULL DEFAULT '',
	tipo [nvarchar](24) NULL DEFAULT '',
	estado [nvarchar](12) NULL DEFAULT 'ABIERTO',
	resultado [nvarchar](12) NULL DEFAULT 'TRATAMIENTO',
	f_trabajo [int] NULL DEFAULT 0,
	f_repa [int] NULL DEFAULT 0,
	equipo [nvarchar](36) NULL DEFAULT '',
	material [nvarchar](72)  NULL DEFAULT '',
	codmaterial [nvarchar](12) NULL DEFAULT '',
	ax [nvarchar](24) NULL DEFAULT '',
	sap [nvarchar](24) NULL DEFAULT '',
	orden [nvarchar](24) NULL DEFAULT '',
	pedido [nvarchar](12) NULL DEFAULT '',
	oferta [float] NULL DEFAULT 0,
	repuesto [float] NULL DEFAULT 0,
	despl [float] NULL DEFAULT 0,
	mo [float] NULL DEFAULT 0,
	descripcion [nvarchar](1024) NULL DEFAULT '',
	fichero [nvarchar](96) NULL DEFAULT '',
	)

GO