USE catalogo
GO
 
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE vehiculos(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
    matricula [nvarchar](10) NOT NULL,
	modelo [nvarchar](60) NOT NULL,
	conductor [nvarchar](60) NOT NULL,
	kms [int] NULL,
	itvpasada [int] NULL,
	itvproxima [int] NULL,
	tipo [nvarchar](20) NOT NULL,
	detalle [nvarchar](60) NULL,
	importe [float] NULL,	
	fecha [int] NOT NULL DEFAULT 20010101
	)
GO