USE catalogo
GO
 
/****** Object:  Table [catalogo].[producto]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE personalvestuario(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
    proveedor [nvarchar](60) NULL,
	nalbaran [nvarchar](120) NULL,
	destinatario [nvarchar](60) NOT NULL,
	descripcion [nvarchar](240) NULL,
	fecha [int] NOT NULL DEFAULT 20010101
	)
GO