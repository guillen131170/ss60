USE catalogo
GO
 
/****** Object:  Table [catalogo].[producto]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE itc(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	nserie [nvarchar](30) NOT NULL,
	tag [nvarchar](30) NULL,
	iip [nvarchar](30) NULL,
	dns [nvarchar](80) NULL,
	fecha [int] NOT NULL DEFAULT 20210101,
	autor [nvarchar](60) NOT NULL,
    estado [nvarchar](10) NOT NULL DEFAULT 'ACTIVO'
	)

GO


USE catalogo
GO
 
/****** Object:  Table [catalogo].[producto]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE itcubicaciones(
	id [int] IDENTITY NOT NULL PRIMARY KEY,
	nserie [nvarchar](30) NOT NULL,
	nompds [nvarchar](60) NOT NULL DEFAULT 'ALMACEN',
	codpds [nvarchar](20) NULL,
	direcccionpds [nvarchar](80) NULL,
    telefonopds [nvarchar](10) NULL,
	contactopds [nvarchar](60) NULL,
	stc [nvarchar](60) NULL,
	telefonostc [nvarchar](10) NULL,
	contactostc [nvarchar](60) NULL,
    poblacion [nvarchar](30) NULL,
	fecha [int] NOT NULL DEFAULT 20210101,
	)

GO

CREATE TABLE itcactividades(
	id [int] IDENTITY NOT NULL PRIMARY KEY,
	nserie [nvarchar](30) NOT NULL,
	servicio [nvarchar](60) NOT NULL,
	problema [nvarchar](240) NOT NULL,
	solucion [nvarchar](240) NOT NULL,
    autor [nvarchar](60) NULL,
	fecha [int] NOT NULL DEFAULT 20210101,
	)

GO