USE catalogo
GO
 
/****** Object:  Table [catalogo].[producto]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE repuestospedido(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	pedido [nvarchar](20) NOT NULL,
	referencia [nvarchar](20) NOT NULL,
	nombre [nvarchar](120) NOT NULL,
	precio [float] NOT NULL,
	unidades [int] NOT NULL,
	fecha [int] NOT NULL,
	familia [nvarchar](60) NOT NULL,
    proveedor [nvarchar](60) NOT NULL
	)

GO