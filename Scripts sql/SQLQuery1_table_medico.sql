USE catalogo
GO
 
/****** Object:  Table [catalogo].[producto]    Script Date: 14/01/20120:48:12 PM ******/
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE personalmedico(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
    centro [nvarchar](60) NOT NULL,
	paciente [nvarchar](120) NOT NULL,
	referencia [nvarchar](60) NULL,
	descripcion [nvarchar](240) NOT NULL,
	fecha [int] NOT NULL
	)
GO