﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeS60.CORE
{
    /// <summary>
    /// 
    /// </summary>
    public class OBSOLETO_OTaller
    {
        #region Atributes
        private int id;
        private string tecnico;
        private string estado;
        private string resultado;
        private string proyecto;
        private string cod_proyecto;
        private string pds;
        private int f_trabajo;
        private int f_repa;
        private string cliente;
        private string provincia;
        private string oaveria;
        private string omontaje;
        private string odesmontaje;
        private string otaller;
        private string material;
        private string codmaterial;
        private string ax;
        private string sap;
        private string norden;
        private float oferta;
        private float despl;
        private float mo;
        private string repuesto;
        private string cmi;
        private string tipo;
        #endregion


        #region Properties
        [Key]
        [Required]
        public int Id { get; set; }
        public string Tecnico { get; set; }
        [Required]
        public string Estado { get; set; }
        [Required]
        public string Resultado { get; set; }
        [Required]
        public string Proyecto { get; set; }
        public string Cod_proyecto { get; set; }
        public string Pds { get; set; }
        public int F_trabajo { get; set; }
        [Required]
        public int F_repa { get; set; }
        public string Cliente { get; set; }
        public string Provincia { get; set; }
        public string Oaveria { get; set; }
        public string Omontaje { get; set; }
        public string Odesmontaje { get; set; }
        public string Otaller { get; set; }
        [Required]
        public string Material { get; set; }
        [Required]
        public string Codmaterial { get; set; }
        public string Ax { get; set; }
        public string Sap { get; set; }
        [Required]
        public string Norden { get; set; }
        [Required]
        public float Oferta { get; set; }
        public float Despl { get; set; }
        public float Mo { get; set; }
        [Required]
        public string Repuesto { get; set; }
        [Required]
        public string Cmi { get; set; }
        public string Tipo { get; set; }
        #endregion
    }
}
