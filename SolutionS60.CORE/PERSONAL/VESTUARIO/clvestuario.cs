﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.PERSONAL.VESTUARIO
{
    public class clvestuario
    {
        /// <summary>
        /// 
        /// </summary>
        #region Atributes
        private int id;
        private string proveedor;
        private string nalbaran;
        private string destinatario;
        private string descripcion;
        private int fecha;
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region Properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Proveedor
        {
            get { return proveedor; }
            set { proveedor = value; }
        }

        public string Nalbaran
        {
            get { return nalbaran; }
            set { nalbaran = value; }
        }

        public string Destinatario
        {
            get { return destinatario; }
            set { destinatario = value; }
        }


        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public int Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        #endregion
    }
}
