﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.PERSONAL.VACACIONES
{
    public class clvacaciones
    {
			private int id;
			private string nombre;
			private int abono;
			private int cargo;
			private int saldo;
			private string detalle;
			private int fecha;

			public int Id { get; set; }
			public string Nombre { get; set; }
			public int Abono { get; set; }
			public int Cargo { get; set; }
			public int Saldo { get; set; }
			public string Detalle { get; set; }
			public int Fecha { get; set; }
	}
}
