﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.PERSONAL.VEHICULOS
{
    public class clmulta
    {
        public clmulta() { Tipo = "MULTA"; }


        public string Tipo { get; set; }
        public string Infraccion { get; set; }
        public float Importe { get; set; }
        public int Fecha { get; set; }
    }
}
