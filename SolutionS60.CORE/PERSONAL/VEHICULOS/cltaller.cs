﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.PERSONAL.VEHICULOS
{
    public class cltaller
    {
        public cltaller() { Tipo = "TALLER"; }


        public string Tipo { get; set; }
        public string Centro { get; set; }
        public float Importe { get; set; }
        public int Fecha { get; set; }
    }
}
