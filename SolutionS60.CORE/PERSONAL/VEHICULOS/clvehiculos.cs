﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.PERSONAL.VEHICULOS
{
    public class clvehiculos
    {
        int id;
        string matricula;
        string modelo;
        string conductor;
        int kms;
        int itvpasada;
        int itvproxima;
        string tipo;
        string detalle;
        float importe;
        int fecha;

        public int Id { get; set; }
        public string Matricula { get; set; }
        public string Modelo { get; set; }
        public string Conductor { get; set; }
        public int Kms { get; set; }
        public int Itvpasada { get; set; }
        public int Itvproxima { get; set; }
        public string Tipo { get; set; }
        public string Detalle { get; set; }
        public float Importe { get; set; }
        public int Fecha { get; set; }
    }
}
