﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.PERSONAL.MEDICO
{
    public class clmedico
    {
        /// <summary>
        /// 
        /// </summary>
        #region Atributes
        private int id;
        private string centro;
        private string paciente;
        private string referencia;
        private string descripcion;
        private int fecha;
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region Properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Centro
        {
            get { return centro; }
            set { centro = value; }
        }

        public string Paciente
        {
            get { return paciente; }
            set { paciente = value; }
        }

        public string Referencia
        {
            get { return referencia; }
            set { referencia = value; }
        }


        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public int Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        #endregion
    }
}
