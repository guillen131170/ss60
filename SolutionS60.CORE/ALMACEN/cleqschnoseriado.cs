﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE
{
    public class cleqschnoseriado
    {
        #region Constructores
        public cleqschnoseriado() { }
        #endregion

        #region Propiedades
        public int Id { get; set; }
        public string NombreMaterial { get; set; }
        public string CodigoMaterial { get; set; }
        public string Origen { get; set; }
        public string ODSEntrada { get; set; }
        public string Destino { get; set; }
        public string ODSSalida { get; set; }
        public int Stock { get; set; }
        public int Fecha { get; set; }
        #endregion
    }
}
