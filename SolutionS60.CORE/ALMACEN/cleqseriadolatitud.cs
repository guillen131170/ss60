﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE
{
    public class cleqseriadolatitud
    {
        #region Constructores
        public cleqseriadolatitud() { }
        #endregion

        #region Propiedades
        public int Id { get; set; }
        public string NombreMaterial { get; set; }
        public string CodigoMaterial { get; set; }
        public string CodSAP { get; set; }
        public string CodAX { get; set; }
        public string Origen { get; set; }
        public string ODSEntrada { get; set; }
        public string Ubicacion { get; set; }
        public string Destino { get; set; }
        public string ODSSalida { get; set; }
        public string Estado { get; set; }
        public int Stock { get; set; }
        #endregion
    }
}
