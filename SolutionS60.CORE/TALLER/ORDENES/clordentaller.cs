﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace SolutionS60.CORE
{
    public class clordentaller
    {
        #region Atributes
        private int id;
        private string tecnico;
        private string cod_tecnico;

        private string proyecto;
        private string cod_proyecto;

        private string cliente;
        private string pds;

        private string ods;
        private string provincia;
        private string actividad;
        private string tipo;
        private string estado;
        private string resultado;
        private int f_trabajo;
        private int f_repa;

        private string equipo;
        private string material;
        private string codmaterial;
        private string ax;
        private string sap;

        private string orden;
        private string pedido;
        private float oferta;
        private float repuesto;
        private float despl;
        private float mo;

        private string descripcion;
        private string fichero;
        #endregion
        


        #region Properties
        public int Id { get; set; }
        public string Tecnico { get; set; }
        public string Cod_Tecnico { get; set; }

        public string Proyecto { get; set; }
        public string Cod_proyecto { get; set; }

        public string Cliente { get; set; }
        public string Pds { get; set; }

        public string Ods { get; set; }
        public string Provincia { get; set; }
        public string Actividad { get; set; }
        public string Tipo { get; set; }
        public string Estado { get; set; }
        public string Resultado { get; set; }
        public int F_trabajo { get; set; }
        public int F_repa { get; set; }

        public string Equipo { get; set; }
        public string Material { get; set; }
        public string Codmaterial { get; set; }
        public string Ax { get; set; }
        public string Sap { get; set; }

        public string Orden { get; set; }
        public string Pedido { get; set; }
        public float Oferta { get; set; }
        public float Repuestos { get; set; }
        public float Despl { get; set; }
        public float Mo { get; set; }

        public string Descripcion { get; set; }
        public string Fichero { get; set; }
        #endregion
    }
}
