﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.COMPRAS
{
    public class clalbaranes
    {
        /// <summary>
        /// 
        /// </summary>
        #region Atributes
        private int id;
        private string proveedor;
        private string nalbaran;
        private string referencia;
        private string descripcion;
        private int fecha;
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region Properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Proveedor
        {
            get { return proveedor; }
            set { proveedor = value; }
        }

        public string Nalbaran
        {
            get { return nalbaran; }
            set { nalbaran = value; }
        }

        public string Referencia
        {
            get { return referencia; }
            set { referencia = value; }
        }


        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public int Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        #endregion
    }
}
