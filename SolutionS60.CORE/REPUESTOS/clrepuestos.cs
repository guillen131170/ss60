﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE
{
    public class clrepuestos
    {
        /// <summary>
        /// 
        /// </summary>
        #region Atributes
        private int id;
        private string referencia;
        private string nombre;
        private string descripcion;
        private float precio;
        private int stock;
        private string familia;
        private string proveedor;
        private string imagen;
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region Properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Referencia
        {
            get { return referencia; }
            set { referencia = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        public int Stock
        {
            get { return stock; }
            set { stock = value; }
        }

        public string Familia
        {
            get { return familia; }
            set { familia = value; }
        }

        public string Proveedor
        {
            get { return proveedor; }
            set { proveedor = value; }
        }

        public string Imagen
        {
            get { return imagen; }
            set { imagen = value; }
        }
        #endregion
    }
}
