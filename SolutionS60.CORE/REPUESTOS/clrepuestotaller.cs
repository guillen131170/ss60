﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE
{
    public class clrepuestotaller
    {
        #region Atributes
        private int id;
        private string material;
        private string codmaterial;
        private int cantidad;
        private string ordentaller;
        #endregion


        #region Properties
        public int Id { get; set; }
        public string Material { get; set; }
        public string Cod_pmaterial{ get; set; }
        public int Cantidad { get; set; }
        public string Orden { get; set; }
        #endregion
    }
}
