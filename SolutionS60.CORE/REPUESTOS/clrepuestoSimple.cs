﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.REPUESTOS
{
    public class clrepuestoSimple
    {
        /// <summary>
        /// 
        /// </summary>
        #region Atributes
        private int id;
        private string codigo;
        private string nombre;
        private float precio;
        #endregion


        /// <summary>
        /// 
        /// </summary>
        #region Properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        #endregion
    }
}
