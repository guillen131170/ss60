﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE
{
    public class clrepuestospedido
    {
        #region Atributes
        private int id;
        private string pedido;
        private string referencia;
        private string nombre;
        private float precio;
        private int unidades;
        private int fecha;
        private string familia;
        private string proveedor;
        #endregion


        #region Properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Referencia
        {
            get { return referencia; }
            set { referencia = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Pedido
        {
            get { return pedido; }
            set { pedido = value; }
        }

        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        public int Unidades
        {
            get { return unidades; }
            set { unidades = value; }
        }

        public int Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }

        public string Familia
        {
            get { return familia; }
            set { familia = value; }
        }

        public string Proveedor
        {
            get { return proveedor; }
            set { proveedor = value; }
        }
        #endregion
    }
}
