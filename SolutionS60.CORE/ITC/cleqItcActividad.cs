﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.ITC
{
    public class cleqItcActividad
    {
        #region Atributes
        private int id;

        private string nserie;

        private string servicio;
        private string problema;
        private string solucion;

        private string autor;
        private int fecha;
        #endregion


        #region Properties
        public int Id { get; set; }

        public string Nserie { get; set; }

        public string Servicio { get; set; }
        public string Problema { get; set; }
        public string Solucion { get; set; }

        public string Autor { get; set; }
        public int Fecha { get; set; }
        #endregion
    }
}
