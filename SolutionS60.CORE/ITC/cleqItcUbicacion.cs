﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.ITC
{
    public class cleqItcUbicacion
    {
        #region Atributes
        private int id;

        private string nserie;

        private int nompds;
        private string codpds;

        private string direccionpds;
        private string telefonopds;
        private string contactopds;

        private string stc;
        private string telefonostc;
        private string contactostc;

        private string poblacion;
        private int fecha;
        #endregion


        #region Properties
        public int Id { get; set; }

        public string Nserie { get; set; }

        public string Nompds { get; set; }
        public string Codpds { get; set; }

        public string DireccionPds { get; set; }
        public string TelefonoPds { get; set; }
        public string ContactoPds { get; set; }

        public string Stc { get; set; }
        public string TelefonoStc { get; set; }
        public string ContactoStc { get; set; }

        public string Poblacion { get; set; }
        public int Fecha { get; set; }
        #endregion
    }
}
