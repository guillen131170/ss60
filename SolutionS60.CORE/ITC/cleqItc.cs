﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.CORE.ITC
{
    public class cleqItc
    {
        #region Atributes
        private int id;

        private string nserie;
        private string tag;
        private string ip;
        private string dns;

        private int fecha;
        private string autor;

        private string estado;
        #endregion


        #region Properties
        public int Id { get; set; }
        public string Nserie { get; set; }
        public string Tag { get; set; }
        public string IP { get; set; }
        public string DNS { get; set; }

        public int Fecha { get; set; }
        public string Autor { get; set; }

        public string Estado { get; set; }
        #endregion
    }
}
