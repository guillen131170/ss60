﻿using SolutionS60.CORE.PERSONAL.VACACIONES;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL.PERSONAL.VACACIONES
{
    public class SqlVacaciones
    {
        private string connectionString;
        private SqlConnection connection;

        public SqlVacaciones()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);          
        }


        #region TODAS LAS CONSULTAS VACACIONES
        #region INSERTAR REGISTRO
        /// <summary>
        /// Insertar un nuevo producto es la base de datos
        /// <param name="repuestos">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int inserta(clvacaciones producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into personalvacaciones " +
                "values(@Nombre,@Abono,@Cargo,@Saldo,@Detalle,@Fecha); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Nombre", producto.Nombre));
            command.Parameters.Add(new SqlParameter("Abono", producto.Abono));
            command.Parameters.Add(new SqlParameter("Cargo", producto.Cargo));
            command.Parameters.Add(new SqlParameter("Saldo", producto.Saldo));
            command.Parameters.Add(new SqlParameter("Detalle", producto.Detalle));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region LISTAR REGISTROS TODOS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clvacaciones> obtenerTodos()
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvacaciones> lista = new List<clvacaciones>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from personalvacaciones";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    clvacaciones producto = new clvacaciones()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Nombre = Convert.ToString(dr[1]),
                        Abono = Convert.ToInt32(dr[2]),
                        Cargo = Convert.ToInt32(dr[3]),
                        Saldo = Convert.ToInt32(dr[4]),
                        Detalle = Convert.ToString(dr[5]),
                        Fecha = Convert.ToInt32(dr[6]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region LISTAR REGISTROS POR NOMBRE
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clvacaciones> obtenerTodos(string nombre)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvacaciones> lista = new List<clvacaciones>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from personalvacaciones where nombre like '%" + nombre + "%' ORDER BY fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            //command.Parameters.Add(new SqlParameter("Nombre", nombre));

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    clvacaciones producto = new clvacaciones()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Nombre = Convert.ToString(dr[1]),
                        Abono = Convert.ToInt32(dr[2]),
                        Cargo = Convert.ToInt32(dr[3]),
                        Saldo = Convert.ToInt32(dr[4]),
                        Detalle = Convert.ToString(dr[5]),
                        Fecha = Convert.ToInt32(dr[6]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region LISTAR REGISTROS POR NOMBRE Y ENTRE FECHAS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clvacaciones> obtenerTodos(int i, int f, string nombre)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvacaciones> lista = new List<clvacaciones>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from personalvacaciones WHERE fecha >= " + i + " AND fecha <= " + f +
                " and nombre like '%" + nombre + "%' ORDER BY fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    clvacaciones producto = new clvacaciones()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Nombre = Convert.ToString(dr[1]),
                        Abono = Convert.ToInt32(dr[2]),
                        Cargo = Convert.ToInt32(dr[3]),
                        Saldo = Convert.ToInt32(dr[4]),
                        Detalle = Convert.ToString(dr[5]),
                        Fecha = Convert.ToInt32(dr[6]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region LISTAR REGISTROS ENTRE FECHAS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clvacaciones> obtenerTodos(int i, int f)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvacaciones> lista = new List<clvacaciones>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from personalvacaciones WHERE fecha >= " + i + 
                " AND fecha <= " + f + " order by fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    clvacaciones producto = new clvacaciones()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Nombre = Convert.ToString(dr[1]),
                        Abono = Convert.ToInt32(dr[2]),
                        Cargo = Convert.ToInt32(dr[3]),
                        Saldo = Convert.ToInt32(dr[4]),
                        Detalle = Convert.ToString(dr[5]),
                        Fecha = Convert.ToInt32(dr[6]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region BUSCAR UN REGISTRO POR SU ID
        /// <summary>
        /// Devuelve un Objeto Pedido
        /// </summary>
        public clvacaciones obtenerRegistroId(int id)
        {
            clvacaciones registro = new clvacaciones();
            registro = null;
            string query = "select * from personalvacaciones where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", id));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                registro = new clvacaciones()
                {
                    Id = Convert.ToInt32(dr[0]),
                    Nombre = Convert.ToString(dr[1]),
                    Abono = Convert.ToInt32(dr[2]),
                    Cargo = Convert.ToInt32(dr[3]),
                    Saldo = Convert.ToInt32(dr[4]),
                    Detalle = Convert.ToString(dr[5]),
                    Fecha = Convert.ToInt32(dr[6])
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion

        #region BUSCAR UN REGISTRO POR SU NOMBRE
        /// <summary>
        /// Devuelve un Objeto Pedido
        /// </summary>
        public clvacaciones obtenerRegistroNombre(string nombre)
        {
            clvacaciones registro = new clvacaciones();
            registro = null;
            string query = "select * from personalvacaciones where nombre like '%" + nombre + "%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    registro = new clvacaciones()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Nombre = Convert.ToString(dr[1]),
                        Abono = Convert.ToInt32(dr[2]),
                        Cargo = Convert.ToInt32(dr[3]),
                        Saldo = Convert.ToInt32(dr[4]),
                        Detalle = Convert.ToString(dr[5]),
                        Fecha = Convert.ToInt32(dr[6])
                    };
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
