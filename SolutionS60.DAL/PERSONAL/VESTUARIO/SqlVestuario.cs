﻿using SolutionS60.CORE.PERSONAL.VESTUARIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL.PERSONAL.VESTUARIO
{
    public class SqlVestuario
    {
        private string connectionString;
        private SqlConnection connection;

        public SqlVestuario()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }


        #region TODAS LAS CONSULTAS VESTUARIO
        #region INSERTAR REGISTRO
        /// <summary>
        /// Insertar un nuevo producto es la base de datos
        /// <param name="repuestos">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int inserta(clvestuario producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into personalvestuario " +
                "values(@Proveedor,@Nalbaran,@Destinatario,@Descripcion,@Fecha); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            command.Parameters.Add(new SqlParameter("Nalbaran", producto.Nalbaran));
            command.Parameters.Add(new SqlParameter("Destinatario", producto.Destinatario));
            command.Parameters.Add(new SqlParameter("Descripcion", producto.Descripcion));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region COMPROBAR REGISTROS DUPLICADOS
        /// <summary>
        /// Devuelve el resultado si 2 códigos o nombres de un producto son iguales
        /// </summary>  
        /// modo: 0 - ok
        ///       1 - duplicado
        public bool duplicado(string referencia)
        {
            bool resultado = false;

            //Cadena para la consulta sql a la base de datos
            string query = "select * from personalvestuario where nalbaran LIKE @Referencia";

            try
            {
                //Abrimos la conexion a la Base de datos
                Connection.Open();
                /*Ejecuta la consulta sql*/
                SqlCommand command = new SqlCommand(query, Connection);
                command.Parameters.Add(new SqlParameter("Referencia", referencia));

                //command.CommandType = CommandType.StoredProcedure;
                if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                {
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion

        #region LISTAR REGISTROS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clvestuario> obtenerTodos()
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvestuario> lista = new List<clvestuario>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from personalvestuario order by fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clvestuario producto = new clvestuario()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Destinatario = item["Destinatario"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region LISTAR REGISTROS POR NOMBRE
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clvestuario> obtenerTodosNombre(string nombre)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvestuario> lista = new List<clvestuario>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from personalvestuario where destinatario like '%" + nombre + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clvestuario producto = new clvestuario()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Destinatario = item["Destinatario"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region LISTAR REGISTROS POR NOMBRE Y ENTRE FECHAS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clvestuario> obtenerTodos(int i, int f, string nombre)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvestuario> lista = new List<clvestuario>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from personalvestuario WHERE fecha >= " + i + " AND fecha <= " + f +
                " and destinatario like '%" + nombre + "%' order by fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clvestuario producto = new clvestuario()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Destinatario = item["Destinatario"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region LISTAR REGISTROS ENTRE FECHAS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clvestuario> obtenerTodos(int i, int f)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvestuario> lista = new List<clvestuario>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from personalvestuario WHERE fecha >= " + i + " AND fecha <= " + f + " order by fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clvestuario producto = new clvestuario()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Destinatario = item["Destinatario"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region BUSCAR UN REGISTRO POR SU ID
        /// <summary>
        /// Devuelve un Objeto Pedido
        /// </summary>
        public clvestuario obtenerRegistroId(int id)
        {
            clvestuario registro = new clvestuario();
            registro = null;
            string query = "select * from personalvestuario where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", id));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow item = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                registro = new clvestuario()
                {
                    Id = Convert.ToInt32(item["Id"]),
                    Proveedor = item["Proveedor"].ToString(),
                    Nalbaran = item["Nalbaran"].ToString(),
                    Destinatario = item["Destinatario"].ToString(),
                    Descripcion = item["Descripcion"].ToString(),
                    Fecha = Convert.ToInt32(item["Fecha"]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion

        #region MODIFICAR UN REGISTRO
        /// <summary>
        /// Modificar un producto es la base de datos
        /// <param name="producto">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int modifica(int id, clvestuario producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update personalvestuario " +
                "set proveedor=@Proveedor, nalbaran=@Nalbaran, destinatario=@Destinatario, descripcion=@Descripcion, fecha=@Fecha, " +
                " where id=@id; ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            command.Parameters.Add(new SqlParameter("Nalbaran", producto.Nalbaran));
            command.Parameters.Add(new SqlParameter("Destinatario", producto.Destinatario));
            command.Parameters.Add(new SqlParameter("Descripcion", producto.Descripcion));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
