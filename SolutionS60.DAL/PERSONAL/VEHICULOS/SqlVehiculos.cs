﻿using SolutionS60.CORE.PERSONAL.VEHICULOS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL.PERSONAL.VEHICULOS
{
    public class SqlVehiculos
    {
        private string connectionString;
        private SqlConnection connection;

        public SqlVehiculos()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }


        #region TODAS LAS CONSULTAS VEHICULOS
        #region INSERTAR REGISTRO VEHICULOS
        /// <summary>
        /// Insertar un nuevo producto es la base de datos
        /// <param name="repuestos">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int inserta(clvehiculos producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into vehiculos " +
                "values(@Matricula,@Modelo,@Conductor,@Kms,@Itvpasada,@Itvproxima," +
                "@Tipo,@Detalle,@Importe,@Fecha); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Matricula", producto.Matricula));
            command.Parameters.Add(new SqlParameter("Modelo", producto.Modelo));
            command.Parameters.Add(new SqlParameter("Conductor", producto.Conductor));
            command.Parameters.Add(new SqlParameter("Kms", producto.Kms));
            command.Parameters.Add(new SqlParameter("Itvpasada", producto.Itvpasada));
            command.Parameters.Add(new SqlParameter("Itvproxima", producto.Itvproxima));
            command.Parameters.Add(new SqlParameter("Tipo", producto.Tipo));
            command.Parameters.Add(new SqlParameter("Detalle", producto.Detalle));
            command.Parameters.Add(new SqlParameter("Importe", producto.Importe));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region LISTAR REGISTROS TODOS VEHICULOS POR TIPO
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clvehiculos> obtenerTodos(string tipo)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvehiculos> lista = new List<clvehiculos>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "SELECT * FROM vehiculos WHERE tipo LIKE '%" + tipo + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    clvehiculos producto = new clvehiculos()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Matricula = Convert.ToString(dr[1]),
                        Modelo = Convert.ToString(dr[2]),
                        Conductor = Convert.ToString(dr[3]),
                        Kms = Convert.ToInt32(dr[4]),
                        Itvpasada = Convert.ToInt32(dr[5]),
                        Itvproxima = Convert.ToInt32(dr[6]),
                        Tipo = Convert.ToString(dr[7]),
                        Detalle = Convert.ToString(dr[8]),
                        Importe = (float)Convert.ToDouble(dr[9]),
                        Fecha = Convert.ToInt32(dr[10]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region LISTAR REGISTROS VEHICULOS POR TIPO Y POR CONDUCTOR
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clvehiculos> obtenerTodos(string tipo, string conductor)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvehiculos> lista = new List<clvehiculos>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "SELECT * FROM vehiculos WHERE tipo LIKE '%" + tipo + "%' AND " +
                "conductor LIKE '%" + conductor + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    clvehiculos producto = new clvehiculos()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Matricula = Convert.ToString(dr[1]),
                        Modelo = Convert.ToString(dr[2]),
                        Conductor = Convert.ToString(dr[3]),
                        Kms = Convert.ToInt32(dr[4]),
                        Itvpasada = Convert.ToInt32(dr[5]),
                        Itvproxima = Convert.ToInt32(dr[6]),
                        Tipo = Convert.ToString(dr[7]),
                        Detalle = Convert.ToString(dr[8]),
                        Importe = (float)Convert.ToDouble(dr[9]),
                        Fecha = Convert.ToInt32(dr[10]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region LISTAR REGISTROS VEHICULOS POR TIPO Y POR CONDUCTOR Y ENTRE FECHAS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado int i, int f,
        ///       1 - catalogado 
        ///       3 o nada - todos
        ///       string query = "select * from vehiculos WHERE fecha >= " + i + " AND fecha <= " + f +
        ///        " and nombre like '%" + nombre + "%' ORDER BY fecha DESC";
        public List<clvehiculos> obtenerTodos(int i, int f, string tipo, string conductor)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clvehiculos> lista = new List<clvehiculos>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "SELECT * FROM vehiculos WHERE fecha >= " + i + " AND fecha <= " + f + " AND " +
                " tipo LIKE '%" + tipo + "%' AND " +
                " conductor LIKE '%" + conductor + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    clvehiculos producto = new clvehiculos()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Matricula = Convert.ToString(dr[1]),
                        Modelo = Convert.ToString(dr[2]),
                        Conductor = Convert.ToString(dr[3]),
                        Kms = Convert.ToInt32(dr[4]),
                        Itvpasada = Convert.ToInt32(dr[5]),
                        Itvproxima = Convert.ToInt32(dr[6]),
                        Tipo = Convert.ToString(dr[7]),
                        Detalle = Convert.ToString(dr[8]),
                        Importe = (float)Convert.ToDouble(dr[9]),
                        Fecha = Convert.ToInt32(dr[10]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region BUSCAR UN REGISTRO VEHICULOS POR SU ID
        /// <summary>
        /// Devuelve un Objeto Pedido
        /// </summary>
        public clvehiculos obtenerRegistroId(int id)
        {
            clvehiculos registro = new clvehiculos();
            registro = null;
            string query = "select * from vehiculos where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", id));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow dr = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                registro = new clvehiculos()
                {
                    Id = Convert.ToInt32(dr[0]),
                    Matricula = Convert.ToString(dr[1]),
                    Modelo = Convert.ToString(dr[2]),
                    Conductor = Convert.ToString(dr[3]),
                    Kms = Convert.ToInt32(dr[4]),
                    Itvpasada = Convert.ToInt32(dr[5]),
                    Itvproxima = Convert.ToInt32(dr[6]),
                    Tipo = Convert.ToString(dr[7]),
                    Detalle = Convert.ToString(dr[8]),
                    Importe = (float)Convert.ToDouble(dr[9]),
                    Fecha = Convert.ToInt32(dr[10]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion


        #region BUSCAR ULTIMO REGISTRO VEHICULOS POR CONDUCTOR
        /// <summary>
        /// Devuelve un Objeto Pedido
        /// </summary>
        public clvehiculos obtenerRegistroNombre(string nombre)
        {
            clvehiculos registro = new clvehiculos();
            registro = null;
            string query = "select * from vehiculos where conductor like '%" + nombre + "%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    registro = new clvehiculos()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Matricula = Convert.ToString(dr[1]),
                        Modelo = Convert.ToString(dr[2]),
                        Conductor = Convert.ToString(dr[3]),
                        Kms = Convert.ToInt32(dr[4]),
                        Itvpasada = Convert.ToInt32(dr[5]),
                        Itvproxima = Convert.ToInt32(dr[6]),
                        Tipo = Convert.ToString(dr[7]),
                        Detalle = Convert.ToString(dr[8]),
                        Importe = (float)Convert.ToDouble(dr[9]),
                        Fecha = Convert.ToInt32(dr[10]),
                    };
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion


        #region BUSCAR ULTIMO REGISTRO VEHICULOS POR MATRICULA
        /// <summary>
        /// Devuelve un Objeto Pedido
        /// </summary>
        public clvehiculos obtenerRegistroMatricula(string matricula)
        {
            clvehiculos registro = new clvehiculos();
            registro = null;
            string query = "select * from vehiculos where matricula like '%" + matricula + "%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    registro = new clvehiculos()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Matricula = Convert.ToString(dr[1]),
                        Modelo = Convert.ToString(dr[2]),
                        Conductor = Convert.ToString(dr[3]),
                        Kms = Convert.ToInt32(dr[4]),
                        Itvpasada = Convert.ToInt32(dr[5]),
                        Itvproxima = Convert.ToInt32(dr[6]),
                        Tipo = Convert.ToString(dr[7]),
                        Detalle = Convert.ToString(dr[8]),
                        Importe = (float)Convert.ToDouble(dr[9]),
                        Fecha = Convert.ToInt32(dr[10]),
                    };
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
