﻿using SolutionS60.CORE.USUARIOS;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL.USUARIOS
{
    public class SqlUsuarios
    {
        private string connectionString;
        private SqlConnection connection;
        private string connectionStringCopia;
        private SqlConnection connectionCopia;


        public SqlUsuarios()
        {
            ConnectionString = Properties.Resource1.CadenaConexion1;
            Connection = new SqlConnection(ConnectionString);

            //PARA COPIA
            ConnectionStringCopia = Properties.Resource1.CadenaConexion2;
            ConnectionCopia = new SqlConnection(ConnectionStringCopia);

        }



        #region INSERTAR UN REGISTRO - USUARIOS
        public int inserta(clusuarios registro)
        {
            int resultado = 0;
            string query =
                "insert into usuarios " +
                "values(@Mail,@Pass);";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Mail", registro.Mail));
            command.Parameters.Add(new SqlParameter("Pass", registro.Pass));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region COMPROBAR REGISTROS CON CAMPO MAIL DUPLICADO
        public bool duplicado(string mail)
        {
            bool resultado = false;

            //Cadena para la consulta sql a la base de datos
            string query = "select * from usuarios where mail like '%" + mail + "%'";

            try
            {
                //Abrimos la conexion a la Base de datos
                Connection.Open();
                /*Ejecuta la consulta sql*/
                SqlCommand command = new SqlCommand(query, Connection);

                //command.CommandType = CommandType.StoredProcedure;
                if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                {
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion

        #region AUTENTIFICACION
        public bool autentificacion(string mail, string pass)
        {
            bool resultado = false;

            //Cadena para la consulta sql a la base de datos
            string query = "SELECT * FROM usuarios WHERE (mail LIKE '" + mail + "') AND (pass LIKE '" + pass + "')";

            try
            {
                //Abrimos la conexion a la Base de datos
                Connection.Open();
                /*Ejecuta la consulta sql*/
                SqlCommand command = new SqlCommand(query, Connection);

                //command.CommandType = CommandType.StoredProcedure;
                if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                {
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion



        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }

        public string ConnectionStringCopia
        { get; set; }

        public SqlConnection ConnectionCopia
        { get; set; }
        #endregion
    }
}
