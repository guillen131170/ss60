﻿using SolutionS60.CORE.COMPRAS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL.COMPRAS
{
    public class SqlCompras
    {
        private string connectionString;
        private SqlConnection connection;

        public SqlCompras()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }


        #region LISTAR REGISTROS POR NUMERO DE ALBARAN
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clalbaranes> obtenerTodosAlbaran(string albaran)
        {
            List<clalbaranes> lista = new List<clalbaranes>();
            string query1 = "select * from comprascompras where nalbaran LIKE '%" + albaran + "%' order by fecha DESC";
            string query2 = "select * from comprasentrada where nalbaran LIKE '%" + albaran + "%' order by fecha DESC";
            string query3 = "select * from comprassalida where nalbaran LIKE '%" + albaran + "%' order by fecha DESC";

            Connection.Open();
            SqlCommand command1 = new SqlCommand(query1, Connection);
            SqlCommand command2 = new SqlCommand(query2, Connection);
            SqlCommand command3 = new SqlCommand(query3, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command1;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
                da = new SqlDataAdapter();
                da.SelectCommand = command2;
                ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
                da = new SqlDataAdapter();
                da.SelectCommand = command3;
                ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            return lista;
        }
        #endregion


        #region LISTAR REGISTROS POR NUMERO DE ALBARAN
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clalbaranes> obtenerTodosReferencia(string referencia)
        {
            List<clalbaranes> lista = new List<clalbaranes>();
            string query1 = "select * from comprascompras where referencia LIKE '%" + referencia + "%' order by fecha DESC";
            string query2 = "select * from comprasentrada where referencia LIKE '%" + referencia + "%' order by fecha DESC";
            string query3 = "select * from comprassalida where referencia LIKE '%" + referencia + "%' order by fecha DESC";

            Connection.Open();
            SqlCommand command1 = new SqlCommand(query1, Connection);
            SqlCommand command2 = new SqlCommand(query2, Connection);
            SqlCommand command3 = new SqlCommand(query3, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command1;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
                da = new SqlDataAdapter();
                da.SelectCommand = command2;
                ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
                da = new SqlDataAdapter();
                da.SelectCommand = command3;
                ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            return lista;
        }
        #endregion


        #region TODAS LAS CONSULTAS ENTRADAS
        #region INSERTAR REGISTRO
        /// <summary>
        /// Insertar un nuevo producto es la base de datos
        /// <param name="repuestos">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int insertaE(clalbaranes producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into comprasentrada " +
                "values(@Proveedor,@Nalbaran,@Referencia,@Descripcion,@Fecha); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            command.Parameters.Add(new SqlParameter("Nalbaran", producto.Nalbaran));
            command.Parameters.Add(new SqlParameter("Referencia", producto.Referencia));
            command.Parameters.Add(new SqlParameter("Descripcion", producto.Descripcion));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region COMPROBAR REGISTROS DUPLICADOS
        /// <summary>
        /// Devuelve el resultado si 2 códigos o nombres de un producto son iguales
        /// </summary>  
        /// modo: 0 - ok
        ///       1 - duplicado
        public bool duplicadoE(string referencia)
        {
            bool resultado = false;

            //Cadena para la consulta sql a la base de datos
            string query = "select * from comprasentrada where nalbaran LIKE @Referencia";

            try
            {
                //Abrimos la conexion a la Base de datos
                Connection.Open();
                /*Ejecuta la consulta sql*/
                SqlCommand command = new SqlCommand(query, Connection);
                command.Parameters.Add(new SqlParameter("Referencia", referencia));

                //command.CommandType = CommandType.StoredProcedure;
                if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                {
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion

        #region LISTAR REGISTROS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clalbaranes> obtenerTodosE()
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clalbaranes> lista = new List<clalbaranes>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from comprasentrada order by fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region LISTAR REGISTROS ENTRE FECHAS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clalbaranes> obtenerTodosE(int i, int f)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clalbaranes> lista = new List<clalbaranes>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from comprasentrada WHERE fecha >= " + i + " AND fecha <= " + f + " order by fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region BUSCAR UN REGISTRO POR SU ID
        /// <summary>
        /// Devuelve un Objeto Pedido
        /// </summary>
        public clalbaranes obtenerRegistroIdE(int id)
        {
            clalbaranes registro = new clalbaranes();
            registro = null;
            string query = "select * from comprasentrada where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", id));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow item = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                registro = new clalbaranes()
                {
                    Id = Convert.ToInt32(item["Id"]),
                    Proveedor = item["Proveedor"].ToString(),
                    Nalbaran = item["Nalbaran"].ToString(),
                    Referencia = item["Referencia"].ToString(),
                    Descripcion = item["Descripcion"].ToString(),
                    Fecha = Convert.ToInt32(item["Fecha"]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion

        #region MODIFICAR UN REGISTRO
        /// <summary>
        /// Modificar un producto es la base de datos
        /// <param name="producto">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int modificaE(int id, clalbaranes producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update comprasentrada " +
                "set proveedor=@Proveedor, nalbaran=@Nalbaran, referencia=@Referencia, descripcion=@Descripcion, fecha=@Fecha, " +
                " where id=@id; ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            command.Parameters.Add(new SqlParameter("Nalbaran", producto.Nalbaran));
            command.Parameters.Add(new SqlParameter("Referencia", producto.Referencia));
            command.Parameters.Add(new SqlParameter("Descripcion", producto.Descripcion));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion
        #endregion


        #region TODAS LAS CONSULTAS SALIDAS
        #region INSERTAR REGISTRO
        /// <summary>
        /// Insertar un nuevo producto es la base de datos
        /// <param name="repuestos">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int insertaS(clalbaranes producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into comprassalida " +
                "values(@Proveedor,@Nalbaran,@Referencia,@Descripcion,@Fecha); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            command.Parameters.Add(new SqlParameter("Nalbaran", producto.Nalbaran));
            command.Parameters.Add(new SqlParameter("Referencia", producto.Referencia));
            command.Parameters.Add(new SqlParameter("Descripcion", producto.Descripcion));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region COMPROBAR REGISTROS DUPLICADOS
        /// <summary>
        /// Devuelve el resultado si 2 códigos o nombres de un producto son iguales
        /// </summary>  
        /// modo: 0 - ok
        ///       1 - duplicado
        public bool duplicadoS(string referencia)
        {
            bool resultado = false;

            //Cadena para la consulta sql a la base de datos
            string query = "select * from comprassalida where nalbaran LIKE @Referencia";

            try
            {
                //Abrimos la conexion a la Base de datos
                Connection.Open();
                /*Ejecuta la consulta sql*/
                SqlCommand command = new SqlCommand(query, Connection);
                command.Parameters.Add(new SqlParameter("Referencia", referencia));

                //command.CommandType = CommandType.StoredProcedure;
                if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                {
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion

        #region LISTAR REGISTROS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clalbaranes> obtenerTodosS()
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clalbaranes> lista = new List<clalbaranes>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from comprassalida order by fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region LISTAR REGISTROS ENTRE FECHAS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clalbaranes> obtenerTodosS(int i, int f)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clalbaranes> lista = new List<clalbaranes>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from comprassalida WHERE fecha >= " + i + " AND fecha <= " + f + " order by fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region BUSCAR UN REGISTRO POR SU ID
        /// <summary>
        /// Devuelve un Objeto Pedido
        /// </summary>
        public clalbaranes obtenerRegistroIdS(int id)
        {
            clalbaranes registro = new clalbaranes();
            registro = null;
            string query = "select * from comprassalida where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", id));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow item = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                registro = new clalbaranes()
                {
                    Id = Convert.ToInt32(item["Id"]),
                    Proveedor = item["Proveedor"].ToString(),
                    Nalbaran = item["Nalbaran"].ToString(),
                    Referencia = item["Referencia"].ToString(),
                    Descripcion = item["Descripcion"].ToString(),
                    Fecha = Convert.ToInt32(item["Fecha"]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion

        #region MODIFICAR UN REGISTRO
        /// <summary>
        /// Modificar un producto es la base de datos
        /// <param name="producto">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int modificaS(int id, clalbaranes producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update comprassalida " +
                "set proveedor=@Proveedor, nalbaran=@Nalbaran, referencia=@Referencia, descripcion=@Descripcion, fecha=@Fecha, " +
                " where id=@id; ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            command.Parameters.Add(new SqlParameter("Nalbaran", producto.Nalbaran));
            command.Parameters.Add(new SqlParameter("Referencia", producto.Referencia));
            command.Parameters.Add(new SqlParameter("Descripcion", producto.Descripcion));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion
        #endregion


        #region TODAS LAS CONSULTAS COMPRAS
        #region INSERTAR REGISTRO
        /// <summary>
        /// Insertar un nuevo producto es la base de datos
        /// <param name="repuestos">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int insertaC(clalbaranes producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into comprascompras " +
                "values(@Proveedor,@Nalbaran,@Referencia,@Descripcion,@Fecha); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            command.Parameters.Add(new SqlParameter("Nalbaran", producto.Nalbaran));
            command.Parameters.Add(new SqlParameter("Referencia", producto.Referencia));
            command.Parameters.Add(new SqlParameter("Descripcion", producto.Descripcion));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region COMPROBAR REGISTROS DUPLICADOS
        /// <summary>
        /// Devuelve el resultado si 2 códigos o nombres de un producto son iguales
        /// </summary>  
        /// modo: 0 - ok
        ///       1 - duplicado
        public bool duplicadoC(string referencia)
        {
            bool resultado = false;

            //Cadena para la consulta sql a la base de datos
            string query = "select * from comprascompras where nalbaran LIKE @Referencia";

            try
            {
                //Abrimos la conexion a la Base de datos
                Connection.Open();
                /*Ejecuta la consulta sql*/
                SqlCommand command = new SqlCommand(query, Connection);
                command.Parameters.Add(new SqlParameter("Referencia", referencia));

                //command.CommandType = CommandType.StoredProcedure;
                if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                {
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion

        #region LISTAR REGISTROS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clalbaranes> obtenerTodosC()
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clalbaranes> lista = new List<clalbaranes>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from comprascompras order by fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region LISTAR REGISTROS ENTRE FECHAS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clalbaranes> obtenerTodosC(int i, int f)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clalbaranes> lista = new List<clalbaranes>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from comprascompras WHERE fecha >= " + i + " AND fecha <= " + f + " order by fecha DESC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clalbaranes producto = new clalbaranes()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Proveedor = item["Proveedor"].ToString(),
                        Nalbaran = item["Nalbaran"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion

        #region BUSCAR UN REGISTRO POR SU ID
        /// <summary>
        /// Devuelve un Objeto Pedido
        /// </summary>
        public clalbaranes obtenerRegistroIdC(int id)
        {
            clalbaranes registro = new clalbaranes();
            registro = null;
            string query = "select * from comprascompras where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", id));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow item = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                registro = new clalbaranes()
                {
                    Id = Convert.ToInt32(item["Id"]),
                    Proveedor = item["Proveedor"].ToString(),
                    Nalbaran = item["Nalbaran"].ToString(),
                    Referencia = item["Referencia"].ToString(),
                    Descripcion = item["Descripcion"].ToString(),
                    Fecha = Convert.ToInt32(item["Fecha"]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion

        #region MODIFICAR UN REGISTRO
        /// <summary>
        /// Modificar un producto es la base de datos
        /// <param name="producto">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int modificaC(int id, clalbaranes producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update comprascompras " +
                "set proveedor=@Proveedor, nalbaran=@Nalbaran, referencia=@Referencia, descripcion=@Descripcion, fecha=@Fecha, " +
                " where id=@id; ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            command.Parameters.Add(new SqlParameter("Nalbaran", producto.Nalbaran));
            command.Parameters.Add(new SqlParameter("Referencia", producto.Referencia));
            command.Parameters.Add(new SqlParameter("Descripcion", producto.Descripcion));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
