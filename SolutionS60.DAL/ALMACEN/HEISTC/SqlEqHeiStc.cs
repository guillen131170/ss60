﻿using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL
{
    public class SqlEqHeiStc
    {
        private string connectionString;
        private SqlConnection connection;

        public SqlEqHeiStc()
        {
            ConnectionString = Properties.Resource1.CadenaConexion1;
            Connection = new SqlConnection(ConnectionString);
        }


        #region METODOS EQUIPOS NO SERIADOS
        #region REALIZAR UNA MOVIMIENTO DE SALIDA
        public int sacar(cleqnoseriadoheistd producto)
        {
            int resultado = 0;
            string query =
                "insert into heistcmoveqnoseriados " +
                "values(@NombreMaterial,@CodigoMaterial,@Origen,@ODSEntrada,@Destino,@ODSSalida,@Stock,@Fecha);";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("NombreMaterial", producto.NombreMaterial));
            command.Parameters.Add(new SqlParameter("CodigoMaterial", producto.CodigoMaterial));
            command.Parameters.Add(new SqlParameter("Origen", producto.Origen));
            command.Parameters.Add(new SqlParameter("ODSEntrada", producto.ODSEntrada));
            command.Parameters.Add(new SqlParameter("Destino", producto.Destino));
            command.Parameters.Add(new SqlParameter("ODSSalida", producto.ODSSalida));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }

        public int sacar(int idorden, string ods, string destino, int numero)
        {
            int resultado = 0;
            string query = "update heistcmoveqnoseriados set Destino = '" + destino + "', " +
                           "ODSSalida = '" + ods + "', " +
                           "Stock = Stock- " + numero +
                           " where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }

        public int sacar(int idorden, int numero)
        {
            int resultado = 0;
            string query = "update heistcmoveqnoseriados set " +
                           "Stock = Stock- " + numero +
                           " where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            //command.Parameters.Add(new SqlParameter("ODS", ods));
            //command.Parameters.Add(new SqlParameter("DESTINO", destino));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region REALIZAR UNA SALIDA
        public int salida(int id, int numero)
        {
            int resultado = 0;
            string query =
                "update heistdeqnoseriados set " +
                "Stock = Stock- " + numero +
                " where id = " + id + ";";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region INSERTAR NUEVO REGISTRO
        /// <summary>
        /// Insertar un nuevo equipo en la base de datos
        /// <param name="producto">Tiene la información del equipo a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(cleqnoseriadoheistd producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into heistdeqnoseriados " +
                "values(@NombreMaterial,@CodigoMaterial,'','','','',@Stock);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("NombreMaterial", producto.NombreMaterial));
            command.Parameters.Add(new SqlParameter("CodigoMaterial", producto.CodigoMaterial));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion


        #region REALIZAR UNA ENTRADA
        /// <summary>
        /// Insertar un nuevo equipo en la base de datos
        /// <param name="producto">Tiene la información del equipo a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int entrada(int id, int numero)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update heistdeqnoseriados set " +
                "Stock = Stock+ " + numero +
                " where id = " + id + ";";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion


        #region REALIZAR UNA MOVIMIENTO DE ENTRADA
        /// <summary>
        /// Insertar un nuevo equipo en la base de datos
        /// <param name="producto">Tiene la información del equipo a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int entradaMovimiento(cleqnoseriadoheistd producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into heistcmoveqnoseriados " +
                "values(@NombreMaterial,@CodigoMaterial,@Origen,@ODSEntrada,@Destino,@ODSSalida,@Stock,@Fecha);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("NombreMaterial", producto.NombreMaterial));
            command.Parameters.Add(new SqlParameter("CodigoMaterial", producto.CodigoMaterial));
            command.Parameters.Add(new SqlParameter("Origen", producto.Origen));
            command.Parameters.Add(new SqlParameter("ODSEntrada", producto.ODSEntrada));
            command.Parameters.Add(new SqlParameter("Destino", producto.Destino));
            command.Parameters.Add(new SqlParameter("ODSSalida", producto.ODSSalida));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR NOMBRE MATERIAL
        /// <summary>
        /// Devuelve último estado de un equipo
        /// </summary>  
        public List<cleqnoseriadoheistd> obtenerMovimientosMaterial(string material)
        {
            List<cleqnoseriadoheistd> lista = new List<cleqnoseriadoheistd>();

            string query = "select * from heistcmoveqnoseriados where nombrematerial LIKE '%' + @MATERIAL + '%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@MATERIAL", material);

            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    cleqnoseriadoheistd equipo = new cleqnoseriadoheistd()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        NombreMaterial = Convert.ToString(dr[1]),
                        CodigoMaterial = Convert.ToString(dr[2]),
                        Origen = Convert.ToString(dr[3]),
                        ODSEntrada = Convert.ToString(dr[4]),
                        Destino = Convert.ToString(dr[5]),
                        ODSSalida = Convert.ToString(dr[6]),
                        Stock = Convert.ToInt32(dr[7]),
                        Fecha = Convert.ToInt32(dr[8]),
                    };
                    lista.Add(equipo);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            return lista;
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR CODIGO MATERIAL
        /// <summary>
        /// Devuelve último estado de un equipo
        /// </summary>  
        public List<cleqnoseriadoheistd> obtenerMovimientosCodigo(string material)
        {
            List<cleqnoseriadoheistd> lista = new List<cleqnoseriadoheistd>();

            string query = "select * from heistcmoveqnoseriados where CodigoMaterial LIKE '%' + @MATERIAL + '%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@MATERIAL", material);

            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    cleqnoseriadoheistd equipo = new cleqnoseriadoheistd()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        NombreMaterial = Convert.ToString(dr[1]),
                        CodigoMaterial = Convert.ToString(dr[2]),
                        Origen = Convert.ToString(dr[3]),
                        ODSEntrada = Convert.ToString(dr[4]),
                        Destino = Convert.ToString(dr[5]),
                        ODSSalida = Convert.ToString(dr[6]),
                        Stock = Convert.ToInt32(dr[7]),
                        Fecha = Convert.ToInt32(dr[8]),
                    };
                    lista.Add(equipo);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            return lista;
        }
        #endregion


        #region BUSCAR UN EQUIPO POR SU NOMBRE MATERIAL
        /// <summary>
        /// Busca un registro
        /// </summary>
        /// <returns>retorna 0 si no existe, otro valor => registro encontrado</returns>
        public int existeMaterial(string material)
        {
            int resultado = 0;
            string query = "select COUNT(*) from heistdeqnoseriados where nombrematerial like '%" + material + "%'";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region BUSCAR UN EQUIPO POR SU CODIGO MATERIAL
        /// <summary>
        /// Busca un registro
        /// </summary>
        /// <returns>retorna 0 si no existe, otro valor => registro encontrado</returns>
        public int existeCodigo(string material)
        {
            int resultado = 0;
            string query =
                "select COUNT(*) from heistdeqnoseriados where CodigoMaterial like '%" + material + "%'";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region COMPROBAR SI EXISTEN EQUIPOS DE UN MATERIAL CONCRETO
        /// Busca un registro
        /// </summary>
        /// <returns>retorna 0 si no existe, otro valor => registro encontrado</returns>
        public int existeMaterialCodigo(string material)
        {
            int resultado = 0;
            string query =
                "select COUNT(*) from heistdeqnoseriados where CodigoMaterial like '%" + material + "%'";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region COMPROBAR SI EXISTEN EQUIPOS DE UN MATERIAL CONCRETO
        /// Busca un registro
        /// </summary>
        /// <returns>retorna 0 si no existe, otro valor => registro encontrado</returns>
        public int existeNombreMaterial(string material)
        {
            int resultado = 0;
            string query =
                "select COUNT(*) from heistdeqnoseriados where nombrematerial like '%" + material + "%'";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = Convert.ToInt32(r);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region MODiFICA UN REGISTRO
        /// Cambiar a cerrada
        public int modificar(int idObjeto, cleqnoseriadoheistd objeto)
        {
            int resultado = 0;
            //Declaramos nuestra consulta de Acción Sql
            string query = "update heistdeqnoseriados set nombrematerial = '" + objeto.NombreMaterial + "', " +
                                                    "CodigoMaterial = '" + objeto.CodigoMaterial + "', " +
                                                    "Origen = '" + objeto.Origen + "', " +
                                                    "ODSEntrada = '" + objeto.ODSEntrada + "', " +
                                                    "Destino = '" + objeto.Destino + "', " +
                                                    "ODSSalida = '" + objeto.ODSSalida + "', " +
                                                    " where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idObjeto));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region MODIFICAR LOS DATOS DE UN EQUIPO
        /// <summary>
        /// Modifica un nuevo equipo en la base de datos
        /// <param name="producto">Tiene la información del equipo a modificar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int modificar2(cleqnoseriadoheistd producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update heistdeqnoseriados " +
                " set nombrematerial = @NombreMaterial, CodigoMaterial = @CodigoMaterial," +
                "     Origen = @Origen, ODSEntrada = @ODSEntrada," +
                "     Destino = @Destino, ODSSalida = @ODSSalida," +
                "     Stock = @Stock " +
                " where id = Id;";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Id", producto.Id));
            command.Parameters.Add(new SqlParameter("NombreMaterial", producto.NombreMaterial));
            command.Parameters.Add(new SqlParameter("CodigoMaterial", producto.CodigoMaterial));
            command.Parameters.Add(new SqlParameter("Origen", producto.Origen));
            command.Parameters.Add(new SqlParameter("ODSEntrada", producto.ODSEntrada));
            command.Parameters.Add(new SqlParameter("Destino", producto.Destino));
            command.Parameters.Add(new SqlParameter("ODSSalida", producto.ODSSalida));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            command.Parameters.Add(new SqlParameter("Fecha", Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")).ToString()));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion


        #region LISTAR TODOS LOS MATERIALES
        /// <summary>
        /// Busca todos los registros
        /// </summary>
        /// <returns>Lista de registros</returns>
        public List<cleqnoseriadoheistd> busca()
        {
            List<cleqnoseriadoheistd> lista = new List<cleqnoseriadoheistd>();
            string query = "select * from heistdeqnoseriados";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    cleqnoseriadoheistd producto = new cleqnoseriadoheistd()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        NombreMaterial = Convert.ToString(dr[1]),
                        CodigoMaterial = Convert.ToString(dr[2]),
                        Origen = Convert.ToString(dr[3]),
                        ODSEntrada = Convert.ToString(dr[4]),
                        Destino = Convert.ToString(dr[5]),
                        ODSSalida = Convert.ToString(dr[6]),
                        Stock = Convert.ToInt32(dr[7]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region LISTAR TODOS LOS MOVIMIENTOS
        /// <summary>
        /// Busca todos los registros
        /// </summary>
        /// <returns>Lista de registros</returns>
        public List<cleqnoseriadoheistd> buscaMovimientos()
        {
            List<cleqnoseriadoheistd> lista = new List<cleqnoseriadoheistd>();
            string query = "select * from heistcmoveqnoseriados";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    cleqnoseriadoheistd producto = new cleqnoseriadoheistd()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        NombreMaterial = Convert.ToString(dr[1]),
                        CodigoMaterial = Convert.ToString(dr[2]),
                        Origen = Convert.ToString(dr[3]),
                        ODSEntrada = Convert.ToString(dr[4]),
                        Destino = Convert.ToString(dr[5]),
                        ODSSalida = Convert.ToString(dr[6]),
                        Stock = Convert.ToInt32(dr[7]),
                        Fecha = Convert.ToInt32(dr[8]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
