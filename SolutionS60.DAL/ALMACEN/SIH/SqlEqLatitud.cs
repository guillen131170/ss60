﻿using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL
{
    public class SqlEqLatitud
    {
        private string connectionString;
        private SqlConnection connection;


        #region CONSTRUCTOR
        public SqlEqLatitud()
        {
            ConnectionString = Properties.Resource1.CadenaConexion1;
            Connection = new SqlConnection(ConnectionString);
        }
        #endregion


        #region SACAR EQUIPO DEL SISTEMA
        public void sacar(int idorden, string ods, string destino)
        {
            //Declaramos nuestra consulta de Acción Sql
            string query = "update latitudequipos set Destino = '" + destino + "', " +
                           "ODSSalida = '" + ods + "', " +
                           "Estado = 'MONTADO', " +
                           "Stock = 0 " +
                           " where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            //command.Parameters.Add(new SqlParameter("ODS", ods));
            //command.Parameters.Add(new SqlParameter("DESTINO", destino));
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }
        #endregion


        #region INSERTAR NUEVO REGISTRO
        /// <summary>
        /// Insertar un nuevo equipo en la base de datos
        /// <param name="producto">Tiene la información del equipo a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(cleqseriadolatitud producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into latitudequipos " +
                "values(@NombreMaterial,@CodigoMaterial,@CodSAP,@CodAX," +
                "       @Origen,@ODSEntrada,@Ubicacion,@Destino,@ODSSalida," +
                "       @Estado,@Stock);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("NombreMaterial", producto.NombreMaterial));
            command.Parameters.Add(new SqlParameter("CodigoMaterial", producto.CodigoMaterial));
            command.Parameters.Add(new SqlParameter("CodSAP", producto.CodSAP));
            command.Parameters.Add(new SqlParameter("CodAX", producto.CodAX));
            command.Parameters.Add(new SqlParameter("Origen", producto.Origen));
            command.Parameters.Add(new SqlParameter("ODSEntrada", producto.ODSEntrada));
            command.Parameters.Add(new SqlParameter("Ubicacion", producto.Ubicacion));
            command.Parameters.Add(new SqlParameter("Destino", producto.Destino));
            command.Parameters.Add(new SqlParameter("ODSSalida", producto.ODSSalida));
            command.Parameters.Add(new SqlParameter("Estado", producto.Estado));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion


        #region MODiFICA UN REGISTRO
        /// Cambiar a cerrada
        public int modificar(int idObjeto, cleqseriadolatitud objeto)
        {
            int resultado = 0;
            //Declaramos nuestra consulta de Acción Sql
            string query = "update latitudequipos set nombrematerial = '" + objeto.NombreMaterial + "', " +
                                                    "CodigoMaterial = '" + objeto.CodigoMaterial + "', " +
                                                    "CodSAP = '" + objeto.CodSAP + "', " +
                                                    "CodAX = '" + objeto.CodAX + "', " +
                                                    "Origen = '" + objeto.Origen + "', " +
                                                    "ODSEntrada = '" + objeto.ODSEntrada + "', " +
                                                    "Ubicacion = '" + objeto.Ubicacion + "', " +
                                                    "Destino = '" + objeto.Destino + "', " +
                                                    "ODSSalida = '" + objeto.ODSSalida + "', " +
                                                    "Estado = '" + objeto.Estado + "' " +
                                                    " where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idObjeto));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region BUSCA EQUIPOS PREPARADOS PARA SALIR - EQUIPOS EN ALMACEN
        /// <summary>
        /// Busca todos los registros
        /// </summary>
        /// <returns>Lista de registros</returns>
        public List<cleqseriadolatitud> busca()
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<cleqseriadolatitud> lista = new List<cleqseriadolatitud>();
            string query = "select * from latitudequipos where Stock>0";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    cleqseriadolatitud producto = new cleqseriadolatitud()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        NombreMaterial = Convert.ToString(dr[1]),
                        CodigoMaterial = Convert.ToString(dr[2]),
                        CodSAP = Convert.ToString(dr[3]),
                        CodAX = Convert.ToString(dr[4]),
                        Origen = Convert.ToString(dr[5]),
                        ODSEntrada = Convert.ToString(dr[6]),
                        Ubicacion = Convert.ToString(dr[7]),
                        Destino = Convert.ToString(dr[8]),
                        ODSSalida = Convert.ToString(dr[9]),
                        Estado = Convert.ToString(dr[10]),
                        Stock = Convert.ToInt32(dr[11]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region BUSCA EQUIPOS PREPARADOS PARA SALIR - EQUIPOS EN ALMACEN
        /// <summary>
        /// Busca todos los registros
        /// </summary>
        /// <returns>Lista de registros</returns>
        public List<cleqseriadolatitud> buscaModelo(string modelo)
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<cleqseriadolatitud> lista = new List<cleqseriadolatitud>();
            string query = "SELECT * FROM latitudequipos WHERE Stock>0 AND nombrematerial LIKE '%" + modelo + "%'";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    cleqseriadolatitud producto = new cleqseriadolatitud()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        NombreMaterial = Convert.ToString(dr[1]),
                        CodigoMaterial = Convert.ToString(dr[2]),
                        CodSAP = Convert.ToString(dr[3]),
                        CodAX = Convert.ToString(dr[4]),
                        Origen = Convert.ToString(dr[5]),
                        ODSEntrada = Convert.ToString(dr[6]),
                        Ubicacion = Convert.ToString(dr[7]),
                        Destino = Convert.ToString(dr[8]),
                        ODSSalida = Convert.ToString(dr[9]),
                        Estado = Convert.ToString(dr[10]),
                        Stock = Convert.ToInt32(dr[11]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region BUSCAR ÚLTIMO ESTADO DE UN EQUIPO POR SU NS AX
        /// <summary>
        /// Devuelve último estado de un equipo
        /// </summary>  
        public cleqseriadolatitud obtenerUltimoEstadoEquipo(string ax)
        {
            cleqseriadolatitud equipo = new cleqseriadolatitud();

            string query = "select * from latitudequipos where CodAX LIKE '%' + @AX + '%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@AX", ax);

            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    equipo = new cleqseriadolatitud()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        NombreMaterial = Convert.ToString(dr[1]),
                        CodigoMaterial = Convert.ToString(dr[2]),
                        CodSAP = Convert.ToString(dr[3]),
                        CodAX = Convert.ToString(dr[4]),
                        Origen = Convert.ToString(dr[5]),
                        ODSEntrada = Convert.ToString(dr[6]),
                        Ubicacion = Convert.ToString(dr[7]),
                        Destino = Convert.ToString(dr[8]),
                        ODSSalida = Convert.ToString(dr[9]),
                        Estado = Convert.ToString(dr[10]),
                        Stock = Convert.ToInt32(dr[11]),
                    };
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            // Retorna la lista de productos   
            return equipo;
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR NS AX
        /// <summary>
        /// Devuelve último estado de un equipo
        /// </summary>  
        public List<cleqseriadolatitud> obtenerMovimientosAx(string ax)
        {
            List < cleqseriadolatitud> lista = new List<cleqseriadolatitud>();

            string query = "select * from latitudequipos where CodAX LIKE '%' + @AX + '%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@AX", ax);

            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    cleqseriadolatitud equipo = new cleqseriadolatitud()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        NombreMaterial = Convert.ToString(dr[1]),
                        CodigoMaterial = Convert.ToString(dr[2]),
                        CodSAP = Convert.ToString(dr[3]),
                        CodAX = Convert.ToString(dr[4]),
                        Origen = Convert.ToString(dr[5]),
                        ODSEntrada = Convert.ToString(dr[6]),
                        Ubicacion = Convert.ToString(dr[7]),
                        Destino = Convert.ToString(dr[8]),
                        ODSSalida = Convert.ToString(dr[9]),
                        Estado = Convert.ToString(dr[10]),
                        Stock = Convert.ToInt32(dr[11]),
                    };
                    lista.Add(equipo);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR MODELO
        /// <summary>
        /// Devuelve último estado de un equipo
        /// </summary>  
        public List<cleqseriadolatitud> obtenerMovimientosModelo(string modelo)
        {
            List<cleqseriadolatitud> lista = new List<cleqseriadolatitud>();

            string query = "select * from latitudequipos where nombrematerial LIKE '%' + @Modelo + '%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@Modelo", modelo);

            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    cleqseriadolatitud equipo = new cleqseriadolatitud()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        NombreMaterial = Convert.ToString(dr[1]),
                        CodigoMaterial = Convert.ToString(dr[2]),
                        CodSAP = Convert.ToString(dr[3]),
                        CodAX = Convert.ToString(dr[4]),
                        Origen = Convert.ToString(dr[5]),
                        ODSEntrada = Convert.ToString(dr[6]),
                        Ubicacion = Convert.ToString(dr[7]),
                        Destino = Convert.ToString(dr[8]),
                        ODSSalida = Convert.ToString(dr[9]),
                        Estado = Convert.ToString(dr[10]),
                        Stock = Convert.ToInt32(dr[11]),
                    };
                    lista.Add(equipo);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
