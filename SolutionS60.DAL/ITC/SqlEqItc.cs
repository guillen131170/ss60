﻿using SolutionS60.CORE.ITC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL.ITC
{
    public class SqlEqItc
    {
        private string connectionString;
        private SqlConnection connection;


        #region CONSTRUCTOR
        public SqlEqItc()
        {
            ConnectionString = Properties.Resource1.CadenaConexion1;
            Connection = new SqlConnection(ConnectionString);
        }
        #endregion


        #region COMPROBAR REGISTROS DUPLICADOS
        /// <summary>
        /// Devuelve el resultado si 2 códigos o nombres de un producto son iguales
        /// </summary>  
        /// modo: 0 - ok
        ///       1 - duplicado
        public bool duplicado(string referencia)
        {
            bool resultado = false;

            //Cadena para la consulta sql a la base de datos
            string query = "select * from itc where nserie LIKE '%" + referencia + "%'";

            try
            {
                //Abrimos la conexion a la Base de datos
                Connection.Open();
                /*Ejecuta la consulta sql*/
                SqlCommand command = new SqlCommand(query, Connection);
                //command.Parameters.Add(new SqlParameter("Referencia", referencia));

                //command.CommandType = CommandType.StoredProcedure;
                if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                {
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region BUSCA REGISTRO POR ID - ORDEN TALLER
        /// <summary>
        /// Devuelve un Objeto OTaller
        /// </summary>
        public cleqItc obtenerRegistroNS(string ns)
        {
            cleqItc registro = new cleqItc();
            registro = null;
            string query = "select * from itc where nserie like '%" + ns + "%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            //command.Parameters.Add(new SqlParameter("NS", ns));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow item = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                registro = new cleqItc()
                {
                    Id = Convert.ToInt32(item[0]),
                    Nserie = Convert.ToString(item[1]),
                    Tag = Convert.ToString(item[2]),
                    IP = Convert.ToString(item[3]),
                    DNS = Convert.ToString(item[4]),
                    Fecha = Convert.ToInt32(item[5]),
                    Autor = Convert.ToString(item[6]),
                    Estado = Convert.ToString(item[7]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion


        #region INSERTAR NUEVO REGISTRO ITC
        /// <summary>
        /// Insertar un nuevo equipo en la base de datos
        /// <param name="producto">Tiene la información del equipo a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(cleqItc eq)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into itc " +
                "values(@Nserie,@Tag,@Ip,@Dns,@Fecha,@Autor,@Estado);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Nserie", eq.Nserie));
            command.Parameters.Add(new SqlParameter("Tag", eq.Tag));
            command.Parameters.Add(new SqlParameter("Ip", eq.IP));
            command.Parameters.Add(new SqlParameter("Dns", eq.DNS));
            command.Parameters.Add(new SqlParameter("Fecha", eq.Fecha));
            command.Parameters.Add(new SqlParameter("Autor", eq.Autor));
            command.Parameters.Add(new SqlParameter("Estado", eq.Estado));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion

        #region INSERTAR NUEVO REGISTRO ITC ACTIVIDAD
        /// <summary>
        /// Insertar un nuevo equipo en la base de datos
        /// <param name="producto">Tiene la información del equipo a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(cleqItcActividad eq)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into itcactividades " +
                "values(@Nserie,@Servicio,@Problema,@Solucion,@Autor,@Fecha);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Nserie", eq.Nserie));
            command.Parameters.Add(new SqlParameter("Servicio", eq.Servicio));
            command.Parameters.Add(new SqlParameter("Problema", eq.Problema));
            command.Parameters.Add(new SqlParameter("Solucion", eq.Solucion));
            command.Parameters.Add(new SqlParameter("Autor", eq.Autor));
            command.Parameters.Add(new SqlParameter("Fecha", eq.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion

        #region INSERTAR NUEVO REGISTRO ITC UBICACION
        /// <summary>
        /// Insertar un nuevo equipo en la base de datos
        /// <param name="producto">Tiene la información del equipo a registrar</param>
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(cleqItcUbicacion eq)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into itcubicaciones " +
                "values(@Nserie,@Nompds,@Codpds,@Direccionpds,@Telefonopds,@Contactopds," +
                "@Stc, @Telefonostc, @Contactostc," +
                "@Poblacion,@Fecha);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Nserie", eq.Nserie));
            command.Parameters.Add(new SqlParameter("Nompds", eq.Nompds));
            command.Parameters.Add(new SqlParameter("Codpds", eq.Codpds));
            command.Parameters.Add(new SqlParameter("Direccionpds", eq.DireccionPds));
            command.Parameters.Add(new SqlParameter("Telefonopds", eq.TelefonoPds));
            command.Parameters.Add(new SqlParameter("Contactopds", eq.ContactoPds));
            command.Parameters.Add(new SqlParameter("Stc", eq.Stc));
            command.Parameters.Add(new SqlParameter("Telefonostc", eq.TelefonoStc));
            command.Parameters.Add(new SqlParameter("Contactostc", eq.ContactoStc));
            command.Parameters.Add(new SqlParameter("Poblacion", eq.Poblacion));
            command.Parameters.Add(new SqlParameter("Fecha", eq.Fecha));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion


        #region MODiFICA UN REGISTRO ITC
        /// Cambiar a cerrada
        public int modificar(string ns, cleqItc objeto)
        {
            int resultado = 0;
            //Declaramos nuestra consulta de Acción Sql
            string query = "update itc set id = '" + objeto.Id + "', " +
                                                    "nserie = '" + objeto.Nserie + "', " +
                                                    "tag = '" + objeto.Tag + "', " +
                                                    "ip = '" + objeto.IP + "', " +
                                                    "dns = '" + objeto.DNS + "', " +
                                                    "fecha = " + objeto.Fecha + ", " +
                                                    "autor = '" + objeto.Autor + "', " +
                                                    "estado = '" + objeto.Estado + "' " +
                                                    " where nserie like '%" + ns + "%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //command.Parameters.Add(new SqlParameter("ID", ns));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region MODiFICA UN REGISTRO ITC ACTIVIDAD
        /// Cambiar a cerrada
        public int modificar(int idObjeto, cleqItcActividad objeto)
        {
            int resultado = 0;
            //Declaramos nuestra consulta de Acción Sql
            string query = "update itcactividades set nserie = '" + objeto.Nserie + "', " +
                                                    "servicio = '" + objeto.Servicio + "', " +
                                                    "problema = " + objeto.Problema + ", " +
                                                    "solucion = '" + objeto.Solucion + "', " +
                                                    "autor = '" + objeto.Autor + "' " +
                                                    "fecha = " + objeto.Fecha + " " +
                                                    " where nserie=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idObjeto));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion

        #region MODiFICA UN REGISTRO
        /// Cambiar a cerrada
        public int modificar(string ns, cleqItcUbicacion objeto)
        {//@Nserie,@Nompds,@Codpds,@Direccion,@Telefono,@Contacto,@Poblacion@Fecha
            int resultado = 0;
            //Declaramos nuestra consulta de Acción Sql
            string query = "update itcubicaciones set nserie = '" + objeto.Nserie + "', " +
                                                    "nompds = '" + objeto.Nompds + "', " +
                                                    "codpds = " + objeto.Codpds + ", " +
                                                    "direccionpds = '" + objeto.DireccionPds + "', " +
                                                    "telefonopds = '" + objeto.TelefonoPds + "' " +
                                                    "contactopds = '" + objeto.ContactoPds + "' " +
                                                    "stc = '" + objeto.Stc + "', " +
                                                    "telefonostc = '" + objeto.TelefonoStc + "' " +
                                                    "contactostc = '" + objeto.ContactoStc + "' " +
                                                    "poblacion = '" + objeto.Poblacion + "' " +
                                                    "fecha = '" + objeto.Fecha + "' " +
                                                    " where nserie like '%" + ns + "%'";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //command.Parameters.Add(new SqlParameter("ID", idObjeto));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region DEVUELVE LISTA DE REGISTROS
        /// <summary>
        /// Busca todos los registros
        /// </summary>
        /// <returns>Lista de registros</returns>
        public List<cleqItc> lista()
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<cleqItc> lista = new List<cleqItc>();
            string query = "select * from itc";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    cleqItc producto = new cleqItc()
                    {
                        Id = Convert.ToInt32(item[0]),
                        Nserie = Convert.ToString(item[1]),
                        Tag = Convert.ToString(item[2]),
                        IP = Convert.ToString(item[3]),
                        DNS = Convert.ToString(item[4]),
                        Fecha = Convert.ToInt32(item[5]),
                        Autor = Convert.ToString(item[6]),
                        Estado = Convert.ToString(item[7]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion

        #region DEVUELVE LISTA DE REGISTROS
        /// <summary>
        /// Busca todos los registros
        /// </summary>
        /// <returns>Lista de registros</returns>
        public List<cleqItcActividad> listaActividad()
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<cleqItcActividad> lista = new List<cleqItcActividad>();
            string query = "select * from itcactividades";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    cleqItcActividad producto = new cleqItcActividad()
                    {//Nserie,@Servicio,@Problema,@Solucion,@Autor,@Fecha
                        Nserie = Convert.ToString(dr[0]),
                        Servicio = Convert.ToString(dr[1]),
                        Problema = Convert.ToString(dr[2]),
                        Solucion = Convert.ToString(dr[3]),
                        Autor = Convert.ToString(dr[4]),
                        Fecha = Convert.ToInt32(dr[5]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion

        #region DEVUELVE LISTA DE REGISTROS
        /// <summary>
        /// Busca todos los registros
        /// </summary>
        /// <returns>Lista de registros</returns>
        public List<cleqItcUbicacion> listaUbicacion()
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<cleqItcUbicacion> lista = new List<cleqItcUbicacion>();
            string query = "select * from itcubicaciones";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    cleqItcUbicacion producto = new cleqItcUbicacion()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Nserie = Convert.ToString(dr[1]),
                        Nompds = Convert.ToString(dr[2]),
                        Codpds = Convert.ToString(dr[3]),
                        DireccionPds = Convert.ToString(dr[4]),
                        TelefonoPds = Convert.ToString(dr[5]),
                        ContactoPds = Convert.ToString(dr[6]),
                        Stc = Convert.ToString(dr[7]),
                        TelefonoStc = Convert.ToString(dr[8]),
                        ContactoStc = Convert.ToString(dr[9]),
                        Poblacion = Convert.ToString(dr[10]),
                        Fecha = Convert.ToInt32(dr[11]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
