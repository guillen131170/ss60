﻿using CafeS60.CORE;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL
{
    public class SqlOrderTaller
    {
        private string connectionString;
        private SqlConnection connection;
        private string connectionStringCopia;
        private SqlConnection connectionCopia;


        public SqlOrderTaller()
        {
            ConnectionString = Properties.Resource1.CadenaConexion1;
            Connection = new SqlConnection(ConnectionString);

            //PARA COPIA
            ConnectionStringCopia = Properties.Resource1.CadenaConexion2;
            ConnectionCopia = new SqlConnection(ConnectionStringCopia);

        }


        #region COMPROBAR REGISTROS DUPLICADOS
        /// <summary>
        /// Devuelve el resultado si 2 códigos o nombres de un producto son iguales
        /// </summary>  
        /// modo: 0 - ok
        ///       1 - duplicado
        public bool duplicado(int id)
        {
            bool resultado = false;

            //Cadena para la consulta sql a la base de datos
            string query = "select * from ordentaller where id = @ID";

            try
            {
                //Abrimos la conexion a la Base de datos
                Connection.Open();
                /*Ejecuta la consulta sql*/
                SqlCommand command = new SqlCommand(query, Connection);
                command.Parameters.Add(new SqlParameter("ID", id));

                //command.CommandType = CommandType.StoredProcedure;
                if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                {
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region INSERTAR UN REGISTRO - ORDEN DE TALLER
        /// <summary>
        /// Insertar un nuevo registro en la base de datos
        /// <param name="registro">Tiene la información del registrar</param>
        /// </summary>
        /// <param name="registro"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(clordentaller registro)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into ordentaller " +
                "values(@Tecnico,@Cod_Tecnico,@Proyecto,@Cod_proyecto,@Cliente,@Pds,@Ods,@Provincia,@Actividad," +
                "       @Tipo,@Estado,@Resultado,@F_trabajo,@F_repa,@Equipo,@Material,@Codmaterial,@Ax,@Sap," +
                "       @Orden,@Pedido,@Oferta,@Repuestos,@Despl,@Mo,@Descripcion,@Fichero);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Tecnico", registro.Tecnico));
            command.Parameters.Add(new SqlParameter("Cod_Tecnico", registro.Cod_Tecnico));
            command.Parameters.Add(new SqlParameter("Proyecto", registro.Proyecto));
            command.Parameters.Add(new SqlParameter("Cod_proyecto", registro.Cod_proyecto));
            command.Parameters.Add(new SqlParameter("Cliente", registro.Cliente));
            command.Parameters.Add(new SqlParameter("Pds", registro.Pds));
            command.Parameters.Add(new SqlParameter("Ods", registro.Ods));
            command.Parameters.Add(new SqlParameter("Provincia", registro.Provincia));
            command.Parameters.Add(new SqlParameter("Actividad", registro.Actividad));
            command.Parameters.Add(new SqlParameter("Tipo", registro.Tipo));
            command.Parameters.Add(new SqlParameter("Estado", registro.Estado));
            command.Parameters.Add(new SqlParameter("Resultado", registro.Resultado));
            command.Parameters.Add(new SqlParameter("F_trabajo", registro.F_trabajo));
            command.Parameters.Add(new SqlParameter("F_repa", registro.F_repa));
            command.Parameters.Add(new SqlParameter("Equipo", registro.Equipo));
            command.Parameters.Add(new SqlParameter("Material", registro.Material));
            command.Parameters.Add(new SqlParameter("Codmaterial", registro.Codmaterial));
            command.Parameters.Add(new SqlParameter("Ax", registro.Ax));
            command.Parameters.Add(new SqlParameter("Sap", registro.Sap));
            command.Parameters.Add(new SqlParameter("Orden", registro.Orden));
            command.Parameters.Add(new SqlParameter("Pedido", registro.Pedido));
            command.Parameters.Add(new SqlParameter("Oferta", registro.Oferta));
            command.Parameters.Add(new SqlParameter("Repuestos", registro.Repuestos));
            command.Parameters.Add(new SqlParameter("Despl", registro.Despl));
            command.Parameters.Add(new SqlParameter("Mo", registro.Mo));
            command.Parameters.Add(new SqlParameter("Descripcion", registro.Descripcion));
            command.Parameters.Add(new SqlParameter("Fichero", registro.Fichero));
            try
            {
                //Si resultado != 0 operación OK
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region BUSCA EL ULTIMO REGISTRO - ORDEN TALLER
        /// <summary>
        /// Devuelve el último registro de ORDEN TALLER
        /// </summary>  
        public clordentaller obtenerUltimoRegistro()
        {
            clordentaller registro = new clordentaller();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from ordentaller";
            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    registro = new clordentaller()
                    {
                        Id = Convert.ToInt32(item[0]),
                        Tecnico = Convert.ToString(item[1]),
                        Cod_Tecnico = Convert.ToString(item[2]),
                        Proyecto = Convert.ToString(item[3]),
                        Cod_proyecto = Convert.ToString(item[4]),
                        Cliente = Convert.ToString(item[5]),
                        Pds = Convert.ToString(item[6]),
                        Ods = Convert.ToString(item[7]),
                        Provincia = Convert.ToString(item[8]),
                        Actividad = Convert.ToString(item[9]),
                        Tipo = Convert.ToString(item[10]),
                        Estado = Convert.ToString(item[11]),
                        Resultado = Convert.ToString(item[12]),
                        F_trabajo = Convert.ToInt32(item[13]),
                        F_repa = Convert.ToInt32(item[14]),
                        Equipo = Convert.ToString(item[15]),
                        Material = Convert.ToString(item[16]),
                        Codmaterial = Convert.ToString(item[17]),
                        Ax = Convert.ToString(item[18]),
                        Sap = Convert.ToString(item[19]),
                        Orden = Convert.ToString(item[20]),
                        Pedido = Convert.ToString(item[21]),
                        Oferta = (float)Convert.ToDouble(item[22]),
                        Repuestos = (float)Convert.ToDouble(item[23]),
                        Despl = (float)Convert.ToDouble(item[24]),
                        Mo = (float)Convert.ToDouble(item[25]),
                        Descripcion = Convert.ToString(item[26]),
                        Fichero = Convert.ToString(item[27]),
                    };
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return registro;
        }
        #endregion


        #region BUSCA REGISTRO POR ID - ORDEN TALLER
        /// <summary>
        /// Devuelve un Objeto OTaller
        /// </summary>
        public clordentaller obtenerRegistroId(int id)
        {
            clordentaller registro = new clordentaller();
            registro = null;
            string query = "select * from ordentaller where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", id));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow item = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                registro = new clordentaller()
                {
                    Id = Convert.ToInt32(item[0]),
                        Tecnico = Convert.ToString(item[1]),
                        Cod_Tecnico = Convert.ToString(item[2]),
                        Proyecto = Convert.ToString(item[3]),
                        Cod_proyecto = Convert.ToString(item[4]),
                        Cliente = Convert.ToString(item[5]),
                        Pds = Convert.ToString(item[6]),
                        Ods = Convert.ToString(item[7]),
                        Provincia = Convert.ToString(item[8]),
                        Actividad = Convert.ToString(item[9]),
                        Tipo = Convert.ToString(item[10]),
                        Estado = Convert.ToString(item[11]),
                        Resultado = Convert.ToString(item[12]),
                        F_trabajo = Convert.ToInt32(item[13]),
                        F_repa = Convert.ToInt32(item[14]),
                        Equipo = Convert.ToString(item[15]),
                        Material = Convert.ToString(item[16]),
                        Codmaterial = Convert.ToString(item[17]),
                        Ax = Convert.ToString(item[18]),
                        Sap = Convert.ToString(item[19]),
                        Orden = Convert.ToString(item[20]),
                        Pedido = Convert.ToString(item[21]),
                        Oferta = (float)Convert.ToDouble(item[22]),
                        Repuestos = (float)Convert.ToDouble(item[23]),
                        Despl = (float)Convert.ToDouble(item[24]),
                        Mo = (float)Convert.ToDouble(item[25]),
                        Descripcion = Convert.ToString(item[26]),
                        Fichero = Convert.ToString(item[27]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion


        #region OBTENER TOTAL REGISTROS REPUESTOS USADOS POR FECHA Y REPUESTO - GRAFICO 2
        /// <summary>
        /// Busca todos los registros por estado y fecha inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public int obtenerTotalRepuestoGrafico2(int i, int f, string[] repuesto)
        {
            int resultado = 0;
            string query = 
                "SELECT count(*) FROM ordentaller WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND (";
            /*
            int total = repuesto.Length;
            foreach (string material in repuesto)
            {
                query += " (descripcion LIKE '%" + material + "%') ";
                if (total>1)
                {
                    total--;
                    query += " OR ";
                }
            }
            */
            for (int x=0; x<repuesto.Length; x++)
            {
                query += " (descripcion LIKE '%" + repuesto[x] + "%') ";
                if (x < (repuesto.Length-1)) query += " OR ";
            }

            query += ") AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                var r = command.ExecuteScalar();
                if (r != DBNull.Value) resultado = Convert.ToInt32(r);
            }
            catch { throw; }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion


        #region LISTAR REGISTROS POR FECHA Y ESTADO - ORDEN TALLER - DESPLAZAMIENTOS
        public List<clordentaller> listaDesplazamientos(int i, int f, string proyecto)
        {
            List<clordentaller> lista = new List<clordentaller>();
            string query;
            query = "SELECT * FROM ordentaller WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) ";

            if (proyecto.Equals("CAFENTO"))
            {
                query += " AND proyecto LIKE '%CAFENT%'";
            }
            else if (proyecto.Equals("CANDELAS"))
            {
                query += " AND proyecto LIKE '%CANDEL%'";
            }
            else if (proyecto.Equals("LATITUD"))
            {
                query += " AND (proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%') ";
            }
            else if (proyecto.Equals("DELTA"))
            {
                query += " AND proyecto LIKE '%DELT%'";
            }
            else if (proyecto.Equals("BLACKZI"))
            {
                query += " AND proyecto LIKE '%BLACKZ%'";
            }
            else if (proyecto.Equals("DUNKIN"))
            {
                query += " AND proyecto LIKE '%DUNKI%'";
            }
            else if (proyecto.Equals("PROINDE"))
            {
                query += " AND proyecto LIKE '%PROIND%'";
            }
            else if (proyecto.Equals("STARBUCKS CAFE"))
            {
                query += " AND proyecto LIKE '%STARBUCk%CAF%'";
            }
            else if (proyecto.Equals("STARBUCKS FRIO"))
            {
                query += " AND proyecto LIKE '%STARBUCk%FR%'";
            }
            else if (proyecto.Equals("IKEA"))
            {
                query += " AND proyecto LIKE '%IKEA%'";
            }
            else if (proyecto.Equals("BERLYS"))
            {
                query += " AND proyecto LIKE '%BERLY%'";
            }
            else if (proyecto.Equals("DANONE WATERS"))
            {
                query += " AND proyecto LIKE '%DANO%WATE%'";
            }
            else if (proyecto.Equals("DANONE VENDING"))
            {
                query += " AND proyecto LIKE '%DANO%VEND%'";
            }
            else if (proyecto.Equals("PEPSI"))
            {
                query += " AND proyecto LIKE '%PEPSI%'";
            }
            else if (proyecto.Equals("SCHWEPPES"))
            {
                query += " AND proyecto LIKE '%SCH%'";
            }
            else if (proyecto.Equals("HEINEKEN STC"))
            {
                query += " AND proyecto LIKE '%HEI%ST%'";
            }
            else if (proyecto.Equals("HEINEKEN EVENTO"))
            {
                query += " AND proyecto LIKE '%HEI%EV%'";
            }
            else if (proyecto.Equals("HEINEKEN ALIMENTACIÓN"))
            {
                query += " AND proyecto LIKE '%HEI%ALI%'";
            }
            else if (proyecto.Equals("EDEN SPRING"))
            {
                query += " AND proyecto LIKE '%EDEN%'";
            }
            else if (proyecto.Equals("SPECTANK"))
            {
                query += " AND proyecto LIKE '%SPEC%'";
            }
            else if (proyecto.Equals("GINOS FRIO"))
            {
                query += " AND proyecto LIKE '%GIN%FRI%'";
            }
            else if (proyecto.Equals("LACREM"))
            {
                query += " AND proyecto LIKE '%LACRE%'";
            }
            else if (proyecto.Equals("OTROS"))
            {
                query += " AND (proyecto LIKE '%STARBUCk%CAF%' OR proyecto LIKE '%STARBUCk%FR%' OR " +
                         " proyecto LIKE '%IKEA%' OR proyecto LIKE '%BERLY%' OR " +
                         " proyecto LIKE '%DANONE%' OR proyecto LIKE '%PEPSI%' OR " +
                         " proyecto LIKE '%SCH%' OR proyecto LIKE '%HEI%' OR " +
                         " proyecto LIKE '%SPECT%' OR proyecto LIKE '%GINO%' OR " +
                         " proyecto LIKE '%EDEN%')";
            }
            else if (proyecto.Equals("CAFE TODO"))
            {
                query += " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";
            }
            else
            {
                query += " AND proyecto LIKE '%%' ";
            }
            /*****************************************/
            query += " AND (provincia NOT LIKE '%VALLA%' AND oferta > 0 AND despl <=0 AND actividad NOT LIKE '%REPARA%')";
            /*****************************************/

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clordentaller y llena 
                      los registros con los datos de la orden*/
                    clordentaller registro = new clordentaller()
                    {
                        Id = Convert.ToInt32(item[0]),
                        Tecnico = Convert.ToString(item[1]),
                        Cod_Tecnico = Convert.ToString(item[2]),
                        Proyecto = Convert.ToString(item[3]),
                        Cod_proyecto = Convert.ToString(item[4]),
                        Cliente = Convert.ToString(item[5]),
                        Pds = Convert.ToString(item[6]),
                        Ods = Convert.ToString(item[7]),
                        Provincia = Convert.ToString(item[8]),
                        Actividad = Convert.ToString(item[9]),
                        Tipo = Convert.ToString(item[10]),
                        Estado = Convert.ToString(item[11]),
                        Resultado = Convert.ToString(item[12]),
                        F_trabajo = Convert.ToInt32(item[13]),
                        F_repa = Convert.ToInt32(item[14]),
                        Equipo = Convert.ToString(item[15]),
                        Material = Convert.ToString(item[16]),
                        Codmaterial = Convert.ToString(item[17]),
                        Ax = Convert.ToString(item[18]),
                        Sap = Convert.ToString(item[19]),
                        Orden = Convert.ToString(item[20]),
                        Pedido = Convert.ToString(item[21]),
                        Oferta = (float)Convert.ToDouble(item[22]),
                        Repuestos = (float)Convert.ToDouble(item[23]),
                        Despl = (float)Convert.ToDouble(item[24]),
                        Mo = (float)Convert.ToDouble(item[25]),
                        Descripcion = Convert.ToString(item[26]),
                        Fichero = Convert.ToString(item[27]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(registro);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region OBTENER TOTAL REGISTROS REPUESTOS USADOS POR FECHA Y REPUESTO - POR TECNICO
        /// <summary>
        /// Busca todos los registros por estado y fecha inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public int obtenerTotalRepuestoTecnico(int i, int f, string[] repuesto, string tecnico)
        {
            int resultado = 0;
            string query =
                "SELECT count(*) FROM ordentaller WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND (";

            for (int x = 0; x < repuesto.Length; x++)
            {
                query += " (descripcion LIKE '%" + repuesto[x] + "%') ";
                if (x < (repuesto.Length - 1)) query += " OR ";
            }

            query += ") AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";

            query += " AND (cod_tecnico LIKE '%" + tecnico + "%')";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                var r = command.ExecuteScalar();
                if (r != DBNull.Value) resultado = Convert.ToInt32(r);
            }
            catch { throw; }
            finally { Connection.Close(); }
            return resultado;
        }
        #endregion


        #region LISTAR REGISTROS POR FECHA Y REPUESTO USADO - ORDEN TALLER
        /// <summary>
        /// Busca todos los registros por estado y fecha inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<clordentaller> listarPorRepuesto(int i, int f, string repuesto, string proyecto)
        {
            List<clordentaller> lista = new List<clordentaller>();
            string query;
            query = "SELECT * FROM ordentaller WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND (descripcion LIKE '%" + repuesto + "%') ";
            /*****************************************/

            /*****************************************/
            if (proyecto.Equals("CAFENTO"))
            {
                query += " AND proyecto LIKE '%CAFENT%'";
            }
            else if (proyecto.Equals("CANDELAS"))
            {
                query += " AND proyecto LIKE '%CANDEL%'";
            }
            else if (proyecto.Equals("LATITUD"))
            {
                query += " AND (proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%') ";
            }
            else if (proyecto.Equals("DELTA"))
            {
                query += " AND proyecto LIKE '%DELT%'";
            }
            else if (proyecto.Equals("BLACKZI"))
            {
                query += " AND proyecto LIKE '%BLACKZ%'";
            }
            else if (proyecto.Equals("DUNKIN"))
            {
                query += " AND proyecto LIKE '%DUNKI%'";
            }
            else if (proyecto.Equals("PROINDE"))
            {
                query += " AND proyecto LIKE '%PROIND%'";
            }
            else if (proyecto.Equals("STARBUCKS CAFE"))
            {
                query += " AND proyecto LIKE '%STARBUCk%CAF%'";
            }
            else if (proyecto.Equals("STARBUCKS FRIO"))
            {
                query += " AND proyecto LIKE '%STARBUCk%FR%'";
            }
            else if (proyecto.Equals("IKEA"))
            {
                query += " AND proyecto LIKE '%IKEA%'";
            }
            else if (proyecto.Equals("BERLYS"))
            {
                query += " AND proyecto LIKE '%BERLY%'";
            }
            else if (proyecto.Equals("DANONE WATERS"))
            {
                query += " AND proyecto LIKE '%DANO%WATE%'";
            }
            else if (proyecto.Equals("DANONE VENDING"))
            {
                query += " AND proyecto LIKE '%DANO%VEND%'";
            }
            else if (proyecto.Equals("PEPSI"))
            {
                query += " AND proyecto LIKE '%PEPSI%'";
            }
            else if (proyecto.Equals("SCHWEPPES"))
            {
                query += " AND proyecto LIKE '%SCH%'";
            }
            else if (proyecto.Equals("HEINEKEN STC"))
            {
                query += " AND proyecto LIKE '%HEI%ST%'";
            }
            else if (proyecto.Equals("HEINEKEN EVENTO"))
            {
                query += " AND proyecto LIKE '%HEI%EV%'";
            }
            else if (proyecto.Equals("HEINEKEN ALIMENTACIÓN"))
            {
                query += " AND proyecto LIKE '%HEI%ALI%'";
            }
            else if (proyecto.Equals("EDEN SPRING"))
            {
                query += " AND proyecto LIKE '%EDEN%'";
            }
            else if (proyecto.Equals("SPECTANK"))
            {
                query += " AND proyecto LIKE '%SPEC%'";
            }
            else if (proyecto.Equals("GINOS FRIO"))
            {
                query += " AND proyecto LIKE '%GIN%FRI%'";
            }
            else if (proyecto.Equals("LACREM"))
            {
                query += " AND proyecto LIKE '%LACRE%'";
            }
            else if (proyecto.Equals("OTROS"))
            {
                query += " AND (proyecto LIKE '%STARBUCk%CAF%' OR proyecto LIKE '%STARBUCk%FR%' OR " +
                         " proyecto LIKE '%IKEA%' OR proyecto LIKE '%BERLY%' OR " +
                         " proyecto LIKE '%DANONE%' OR proyecto LIKE '%PEPSI%' OR " +
                         " proyecto LIKE '%SCH%' OR proyecto LIKE '%HEI%' OR " +
                         " proyecto LIKE '%SPECT%' OR proyecto LIKE '%GINO%' OR " +
                         " proyecto LIKE '%EDEN%')";
            }
            else if (proyecto.Equals("CAFE TODO"))
            {
                query += " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";
            }
            else
            {
                query += " AND proyecto LIKE '%%' ";
            }
            /*****************************************/
            query += " ORDER BY id DESC";
            /*****************************************/

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clordentaller y llena 
                      los registros con los datos de la orden*/
                    clordentaller registro = new clordentaller()
                    {
                        Id = Convert.ToInt32(item[0]),
                        Tecnico = Convert.ToString(item[1]),
                        Cod_Tecnico = Convert.ToString(item[2]),
                        Proyecto = Convert.ToString(item[3]),
                        Cod_proyecto = Convert.ToString(item[4]),
                        Cliente = Convert.ToString(item[5]),
                        Pds = Convert.ToString(item[6]),
                        Ods = Convert.ToString(item[7]),
                        Provincia = Convert.ToString(item[8]),
                        Actividad = Convert.ToString(item[9]),
                        Tipo = Convert.ToString(item[10]),
                        Estado = Convert.ToString(item[11]),
                        Resultado = Convert.ToString(item[12]),
                        F_trabajo = Convert.ToInt32(item[13]),
                        F_repa = Convert.ToInt32(item[14]),
                        Equipo = Convert.ToString(item[15]),
                        Material = Convert.ToString(item[16]),
                        Codmaterial = Convert.ToString(item[17]),
                        Ax = Convert.ToString(item[18]),
                        Sap = Convert.ToString(item[19]),
                        Orden = Convert.ToString(item[20]),
                        Pedido = Convert.ToString(item[21]),
                        Oferta = (float)Convert.ToDouble(item[22]),
                        Repuestos = (float)Convert.ToDouble(item[23]),
                        Despl = (float)Convert.ToDouble(item[24]),
                        Mo = (float)Convert.ToDouble(item[25]),
                        Descripcion = Convert.ToString(item[26]),
                        Fichero = Convert.ToString(item[27]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(registro);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region LISTAR REGISTROS POR FECHA Y ESTADO - ORDEN TALLER
        /// <summary>
        /// Busca todos los registros por estado y fecha inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<clordentaller> lista(int i, int f, string estado, string proyecto)
        {
            List<clordentaller> lista = new List<clordentaller>();
            string query;
            query = "SELECT * FROM ordentaller WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND ";
            /*****************************************/
            if (estado.Equals("ABIERTAS"))
            {
                query += "estado LIKE '%ABIERT%'";
            }
            else if (estado.Equals("CERRADAS"))
            {
                query += "estado LIKE '%CERRAD%'";
            }
            else
            {
                query += " estado LIKE '%%' ";
            }
            /*****************************************/
            if (proyecto.Equals("CAFENTO"))
            {
                query += " AND proyecto LIKE '%CAFENT%'";
            }
            else if (proyecto.Equals("CANDELAS"))
            {
                query += " AND proyecto LIKE '%CANDEL%'";
            }
            else if (proyecto.Equals("LATITUD"))
            {
                query += " AND (proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%') ";
            }
            else if (proyecto.Equals("DELTA"))
            {
                query += " AND proyecto LIKE '%DELT%'";
            }
            else if (proyecto.Equals("BLACKZI"))
            {
                query += " AND proyecto LIKE '%BLACKZ%'";
            }
            else if (proyecto.Equals("DUNKIN"))
            {
                query += " AND proyecto LIKE '%DUNKI%'";
            }
            else if (proyecto.Equals("PROINDE"))
            {
                query += " AND proyecto LIKE '%PROIND%'";
            }
            else if (proyecto.Equals("STARBUCKS CAFE"))
            {
                query += " AND proyecto LIKE '%STARBUCk%CAF%'";
            }
            else if (proyecto.Equals("STARBUCKS FRIO"))
            {
                query += " AND proyecto LIKE '%STARBUCk%FR%'";
            }
            else if (proyecto.Equals("IKEA"))
            {
                query += " AND proyecto LIKE '%IKEA%'";
            }
            else if (proyecto.Equals("BERLYS"))
            {
                query += " AND proyecto LIKE '%BERLY%'";
            }
            else if (proyecto.Equals("DANONE WATERS"))
            {
                query += " AND proyecto LIKE '%DANO%WATE%'";
            }
            else if (proyecto.Equals("DANONE VENDING"))
            {
                query += " AND proyecto LIKE '%DANO%VEND%'";
            }
            else if (proyecto.Equals("PEPSI"))
            {
                query += " AND proyecto LIKE '%PEPSI%'";
            }
            else if (proyecto.Equals("SCHWEPPES"))
            {
                query += " AND proyecto LIKE '%SCH%'";
            }
            else if (proyecto.Equals("HEINEKEN STC"))
            {
                query += " AND proyecto LIKE '%HEI%ST%'";
            }
            else if (proyecto.Equals("HEINEKEN EVENTO"))
            {
                query += " AND proyecto LIKE '%HEI%EV%'";
            }
            else if (proyecto.Equals("HEINEKEN ALIMENTACIÓN"))
            {
                query += " AND proyecto LIKE '%HEI%ALI%'";
            }
            else if (proyecto.Equals("EDEN SPRING"))
            {
                query += " AND proyecto LIKE '%EDEN%'";
            }
            else if (proyecto.Equals("SPECTANK"))
            {
                query += " AND proyecto LIKE '%SPEC%'";
            }
            else if (proyecto.Equals("GINOS FRIO"))
            {
                query += " AND proyecto LIKE '%GIN%FRI%'";
            }
            else if (proyecto.Equals("LACREM"))
            {
                query += " AND proyecto LIKE '%LACRE%'";
            }
            else if (proyecto.Equals("OTROS"))
            {
                query += " AND (proyecto LIKE '%STARBUCk%CAF%' OR proyecto LIKE '%STARBUCk%FR%' OR " +
                         " proyecto LIKE '%IKEA%' OR proyecto LIKE '%BERLY%' OR " +
                         " proyecto LIKE '%DANONE%' OR proyecto LIKE '%PEPSI%' OR " +
                         " proyecto LIKE '%SCH%' OR proyecto LIKE '%HEI%' OR " +
                         " proyecto LIKE '%SPECT%' OR proyecto LIKE '%GINO%' OR " +
                         " proyecto LIKE '%EDEN%')";
            }
            else if (proyecto.Equals("CAFE TODO"))
            {
                query += " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";
            }
            else
            {
                query += " AND proyecto LIKE '%%' ";
            }
            /*****************************************/
            query += " ORDER BY id DESC";
            /*****************************************/

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clordentaller y llena 
                      los registros con los datos de la orden*/
                    clordentaller registro = new clordentaller()
                    {
                        Id = Convert.ToInt32(item[0]),
                        Tecnico = Convert.ToString(item[1]),
                        Cod_Tecnico = Convert.ToString(item[2]),
                        Proyecto = Convert.ToString(item[3]),
                        Cod_proyecto = Convert.ToString(item[4]),
                        Cliente = Convert.ToString(item[5]),
                        Pds = Convert.ToString(item[6]),
                        Ods = Convert.ToString(item[7]),
                        Provincia = Convert.ToString(item[8]),
                        Actividad = Convert.ToString(item[9]),
                        Tipo = Convert.ToString(item[10]),
                        Estado = Convert.ToString(item[11]),
                        Resultado = Convert.ToString(item[12]),
                        F_trabajo = Convert.ToInt32(item[13]),
                        F_repa = Convert.ToInt32(item[14]),
                        Equipo = Convert.ToString(item[15]),
                        Material = Convert.ToString(item[16]),
                        Codmaterial = Convert.ToString(item[17]),
                        Ax = Convert.ToString(item[18]),
                        Sap = Convert.ToString(item[19]),
                        Orden = Convert.ToString(item[20]),
                        Pedido = Convert.ToString(item[21]),
                        Oferta = (float)Convert.ToDouble(item[22]),
                        Repuestos = (float)Convert.ToDouble(item[23]),
                        Despl = (float)Convert.ToDouble(item[24]),
                        Mo = (float)Convert.ToDouble(item[25]),
                        Descripcion = Convert.ToString(item[26]),
                        Fichero = Convert.ToString(item[27]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(registro);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region LISTAR REGISTROS POR FECHA Y ESTADO - ORDEN TALLER
        /// <summary>
        /// Busca todos los registros por estado y fecha inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<clordentaller> lista(int i, int f, string estado, string proyecto, float importe)
        {
            List<clordentaller> lista = new List<clordentaller>();
            string query;
            query = "SELECT * FROM ordentaller WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND ";
            /*****************************************/
            if (estado.Equals("ABIERTAS"))
            {
                query += "estado LIKE '%ABIERT%'";
            }
            else if (estado.Equals("CERRADAS"))
            {
                query += "estado LIKE '%CERRAD%'";
            }
            else
            {
                query += " estado LIKE '%%' ";
            }
            /*****************************************/
            if (proyecto.Equals("CAFENTO"))
            {
                query += " AND proyecto LIKE '%CAFENT%'";
            }
            else if (proyecto.Equals("CANDELAS"))
            {
                query += " AND proyecto LIKE '%CANDEL%'";
            }
            else if (proyecto.Equals("LATITUD"))
            {
                query += " AND (proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%') ";
            }
            else if (proyecto.Equals("DELTA"))
            {
                query += " AND proyecto LIKE '%DELT%'";
            }
            else if (proyecto.Equals("BLACKZI"))
            {
                query += " AND proyecto LIKE '%BLACKZ%'";
            }
            else if (proyecto.Equals("DUNKIN"))
            {
                query += " AND proyecto LIKE '%DUNKI%'";
            }
            else if (proyecto.Equals("PROINDE"))
            {
                query += " AND proyecto LIKE '%PROIND%'";
            }
            else if (proyecto.Equals("STARBUCKS CAFE"))
            {
                query += " AND proyecto LIKE '%STARBUCk%CAF%'";
            }
            else if (proyecto.Equals("STARBUCKS FRIO"))
            {
                query += " AND proyecto LIKE '%STARBUCk%FR%'";
            }
            else if (proyecto.Equals("IKEA"))
            {
                query += " AND proyecto LIKE '%IKEA%'";
            }
            else if (proyecto.Equals("BERLYS"))
            {
                query += " AND proyecto LIKE '%BERLY%'";
            }
            else if (proyecto.Equals("DANONE WATERS"))
            {
                query += " AND proyecto LIKE '%DANO%WATE%'";
            }
            else if (proyecto.Equals("DANONE VENDING"))
            {
                query += " AND proyecto LIKE '%DANO%VEND%'";
            }
            else if (proyecto.Equals("PEPSI"))
            {
                query += " AND proyecto LIKE '%PEPSI%'";
            }
            else if (proyecto.Equals("SCHWEPPES"))
            {
                query += " AND proyecto LIKE '%SCH%'";
            }
            else if (proyecto.Equals("HEINEKEN STC"))
            {
                query += " AND proyecto LIKE '%HEI%ST%'";
            }
            else if (proyecto.Equals("HEINEKEN EVENTO"))
            {
                query += " AND proyecto LIKE '%HEI%EV%'";
            }
            else if (proyecto.Equals("HEINEKEN ALIMENTACIÓN"))
            {
                query += " AND proyecto LIKE '%HEI%ALI%'";
            }
            else if (proyecto.Equals("EDEN SPRING"))
            {
                query += " AND proyecto LIKE '%EDEN%'";
            }
            else if (proyecto.Equals("SPECTANK"))
            {
                query += " AND proyecto LIKE '%SPEC%'";
            }
            else if (proyecto.Equals("GINOS FRIO"))
            {
                query += " AND proyecto LIKE '%GIN%FRI%'";
            }
            else if (proyecto.Equals("LACREM"))
            {
                query += " AND proyecto LIKE '%LACRE%'";
            }
            else if (proyecto.Equals("OTROS"))
            {
                query += " AND (proyecto LIKE '%STARBUCk%CAF%' OR proyecto LIKE '%STARBUCk%FR%' OR " +
                         " proyecto LIKE '%IKEA%' OR proyecto LIKE '%BERLY%' OR " +
                         " proyecto LIKE '%DANONE%' OR proyecto LIKE '%PEPSI%' OR " +
                         " proyecto LIKE '%SCH%' OR proyecto LIKE '%HEI%' OR " +
                         " proyecto LIKE '%SPECT%' OR proyecto LIKE '%GINO%' OR " +
                         " proyecto LIKE '%EDEN%')";
            }
            else if (proyecto.Equals("CAFE TODO"))
            {
                query += " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";
            }
            else
            {
                query += " AND proyecto LIKE '%%' ";
            }
            /*****************************************/
            query += " AND resultado LIKE '%AUTORIZAD%' AND oferta >= @Importe ORDER BY oferta DESC";
            /*****************************************/

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));
            command.Parameters.Add(new SqlParameter("Importe", importe));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clordentaller y llena 
                      los registros con los datos de la orden*/
                    clordentaller registro = new clordentaller()
                    {
                        Id = Convert.ToInt32(item[0]),
                        Tecnico = Convert.ToString(item[1]),
                        Cod_Tecnico = Convert.ToString(item[2]),
                        Proyecto = Convert.ToString(item[3]),
                        Cod_proyecto = Convert.ToString(item[4]),
                        Cliente = Convert.ToString(item[5]),
                        Pds = Convert.ToString(item[6]),
                        Ods = Convert.ToString(item[7]),
                        Provincia = Convert.ToString(item[8]),
                        Actividad = Convert.ToString(item[9]),
                        Tipo = Convert.ToString(item[10]),
                        Estado = Convert.ToString(item[11]),
                        Resultado = Convert.ToString(item[12]),
                        F_trabajo = Convert.ToInt32(item[13]),
                        F_repa = Convert.ToInt32(item[14]),
                        Equipo = Convert.ToString(item[15]),
                        Material = Convert.ToString(item[16]),
                        Codmaterial = Convert.ToString(item[17]),
                        Ax = Convert.ToString(item[18]),
                        Sap = Convert.ToString(item[19]),
                        Orden = Convert.ToString(item[20]),
                        Pedido = Convert.ToString(item[21]),
                        Oferta = (float)Convert.ToDouble(item[22]),
                        Repuestos = (float)Convert.ToDouble(item[23]),
                        Despl = (float)Convert.ToDouble(item[24]),
                        Mo = (float)Convert.ToDouble(item[25]),
                        Descripcion = Convert.ToString(item[26]),
                        Fichero = Convert.ToString(item[27]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(registro);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region LISTAR REGISTROS POR FECHA, ESTADO, PROYECTO Y ODS - ORDEN TALLER
        /// <summary>
        /// Busca todos los registros por estado y fecha inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<clordentaller> listaOds(int i, int f, string estado, string proyecto, string ods)
        {
            List<clordentaller> lista = new List<clordentaller>();
            string query;
            query = "SELECT * FROM ordentaller WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND ";
            /*****************************************/
            if (estado.Equals("ABIERTAS"))
            {
                query += "estado LIKE '%ABIERT%'";
            }
            else if (estado.Equals("CERRADAS"))
            {
                query += "estado LIKE '%CERRAD%'";
            }
            else
            {
                query += " estado LIKE '%%' ";
            }
            /*****************************************/
            if (proyecto.Equals("CAFENTO"))
            {
                query += " AND proyecto LIKE '%CAFENT%'";
            }
            else if (proyecto.Equals("CANDELAS"))
            {
                query += " AND proyecto LIKE '%CANDEL%'";
            }
            else if (proyecto.Equals("LATITUD"))
            {
                query += " AND (proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%') ";
            }
            else if (proyecto.Equals("DELTA"))
            {
                query += " AND proyecto LIKE '%DELT%'";
            }
            else if (proyecto.Equals("BLACKZI"))
            {
                query += " AND proyecto LIKE '%BLACKZ%'";
            }
            else if (proyecto.Equals("DUNKIN"))
            {
                query += " AND proyecto LIKE '%DUNKI%'";
            }
            else if (proyecto.Equals("PROINDE"))
            {
                query += " AND proyecto LIKE '%PROIND%'";
            }
            else if (proyecto.Equals("STARBUCKS CAFE"))
            {
                query += " AND proyecto LIKE '%STARBUCk%CAF%'";
            }
            else if (proyecto.Equals("STARBUCKS FRIO"))
            {
                query += " AND proyecto LIKE '%STARBUCk%FR%'";
            }
            else if (proyecto.Equals("IKEA"))
            {
                query += " AND proyecto LIKE '%IKEA%'";
            }
            else if (proyecto.Equals("BERLYS"))
            {
                query += " AND proyecto LIKE '%BERLY%'";
            }
            else if (proyecto.Equals("DANONE WATERS"))
            {
                query += " AND proyecto LIKE '%DANO%WATE%'";
            }
            else if (proyecto.Equals("DANONE VENDING"))
            {
                query += " AND proyecto LIKE '%DANO%VEND%'";
            }
            else if (proyecto.Equals("PEPSI"))
            {
                query += " AND proyecto LIKE '%PEPSI%'";
            }
            else if (proyecto.Equals("SCHWEPPES"))
            {
                query += " AND proyecto LIKE '%SCH%'";
            }
            else if (proyecto.Equals("HEINEKEN STC"))
            {
                query += " AND proyecto LIKE '%HEI%ST%'";
            }
            else if (proyecto.Equals("HEINEKEN EVENTO"))
            {
                query += " AND proyecto LIKE '%HEI%EV%'";
            }
            else if (proyecto.Equals("HEINEKEN ALIMENTACIÓN"))
            {
                query += " AND proyecto LIKE '%HEI%ALI%'";
            }
            else if (proyecto.Equals("EDEN SPRING"))
            {
                query += " AND proyecto LIKE '%EDEN%'";
            }
            else if (proyecto.Equals("SPECTANK"))
            {
                query += " AND proyecto LIKE '%SPEC%'";
            }
            else if (proyecto.Equals("GINOS FRIO"))
            {
                query += " AND proyecto LIKE '%GIN%FRI%'";
            }
            else if (proyecto.Equals("LACREM"))
            {
                query += " AND proyecto LIKE '%LACRE%'";
            }
            else if (proyecto.Equals("OTROS"))
            {
                query += " AND (proyecto LIKE '%STARBUCk%CAF%' OR proyecto LIKE '%STARBUCk%FR%' OR " +
                         " proyecto LIKE '%IKEA%' OR proyecto LIKE '%BERLY%' OR " +
                         " proyecto LIKE '%DANONE%' OR proyecto LIKE '%PEPSI%' OR " +
                         " proyecto LIKE '%SCH%' OR proyecto LIKE '%HEI%' OR " +
                         " proyecto LIKE '%SPECT%' OR proyecto LIKE '%GINO%' OR " +
                         " proyecto LIKE '%EDEN%')";
            }
            else if (proyecto.Equals("CAFE TODO"))
            {
                query += " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";
            }
            else
            {
                query += " AND proyecto LIKE '%%' ";
            }
            /*****************************************/
            query += " AND ods LIKE '%" + ods + "%'";
            query += " ORDER BY id ASC";
            /*****************************************/

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));
            //command.Parameters.Add(new SqlParameter("ODS", ods));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clordentaller y llena 
                      los registros con los datos de la orden*/
                    clordentaller registro = new clordentaller()
                    {
                        Id = Convert.ToInt32(item[0]),
                        Tecnico = Convert.ToString(item[1]),
                        Cod_Tecnico = Convert.ToString(item[2]),
                        Proyecto = Convert.ToString(item[3]),
                        Cod_proyecto = Convert.ToString(item[4]),
                        Cliente = Convert.ToString(item[5]),
                        Pds = Convert.ToString(item[6]),
                        Ods = Convert.ToString(item[7]),
                        Provincia = Convert.ToString(item[8]),
                        Actividad = Convert.ToString(item[9]),
                        Tipo = Convert.ToString(item[10]),
                        Estado = Convert.ToString(item[11]),
                        Resultado = Convert.ToString(item[12]),
                        F_trabajo = Convert.ToInt32(item[13]),
                        F_repa = Convert.ToInt32(item[14]),
                        Equipo = Convert.ToString(item[15]),
                        Material = Convert.ToString(item[16]),
                        Codmaterial = Convert.ToString(item[17]),
                        Ax = Convert.ToString(item[18]),
                        Sap = Convert.ToString(item[19]),
                        Orden = Convert.ToString(item[20]),
                        Pedido = Convert.ToString(item[21]),
                        Oferta = (float)Convert.ToDouble(item[22]),
                        Repuestos = (float)Convert.ToDouble(item[23]),
                        Despl = (float)Convert.ToDouble(item[24]),
                        Mo = (float)Convert.ToDouble(item[25]),
                        Descripcion = Convert.ToString(item[26]),
                        Fichero = Convert.ToString(item[27]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(registro);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region LISTAR REGISTROS POR FECHA, ESTADO, PROYECTO Y PDS - ORDEN TALLER
        /// <summary>
        /// Busca todos los registros por estado y fecha inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<clordentaller> listaPds(int i, int f, string estado, string proyecto, string pds)
        {
            List<clordentaller> lista = new List<clordentaller>();
            string query;
            query = "SELECT * FROM ordentaller WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND ";
            /*****************************************/
            if (estado.Equals("ABIERTAS"))
            {
                query += "estado LIKE '%ABIERT%'";
            }
            else if (estado.Equals("CERRADAS"))
            {
                query += "estado LIKE '%CERRAD%'";
            }
            else
            {
                query += " estado LIKE '%%' ";
            }
            /*****************************************/
            if (proyecto.Equals("CAFENTO"))
            {
                query += " AND proyecto LIKE '%CAFENT%'";
            }
            else if (proyecto.Equals("CANDELAS"))
            {
                query += " AND proyecto LIKE '%CANDEL%'";
            }
            else if (proyecto.Equals("LATITUD"))
            {
                query += " AND (proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%') ";
            }
            else if (proyecto.Equals("DELTA"))
            {
                query += " AND proyecto LIKE '%DELT%'";
            }
            else if (proyecto.Equals("BLACKZI"))
            {
                query += " AND proyecto LIKE '%BLACKZ%'";
            }
            else if (proyecto.Equals("DUNKIN"))
            {
                query += " AND proyecto LIKE '%DUNKI%'";
            }
            else if (proyecto.Equals("PROINDE"))
            {
                query += " AND proyecto LIKE '%PROIND%'";
            }
            else if (proyecto.Equals("STARBUCKS CAFE"))
            {
                query += " AND proyecto LIKE '%STARBUCk%CAF%'";
            }
            else if (proyecto.Equals("STARBUCKS FRIO"))
            {
                query += " AND proyecto LIKE '%STARBUCk%FR%'";
            }
            else if (proyecto.Equals("IKEA"))
            {
                query += " AND proyecto LIKE '%IKEA%'";
            }
            else if (proyecto.Equals("BERLYS"))
            {
                query += " AND proyecto LIKE '%BERLY%'";
            }
            else if (proyecto.Equals("DANONE WATERS"))
            {
                query += " AND proyecto LIKE '%DANO%WATE%'";
            }
            else if (proyecto.Equals("DANONE VENDING"))
            {
                query += " AND proyecto LIKE '%DANO%VEND%'";
            }
            else if (proyecto.Equals("PEPSI"))
            {
                query += " AND proyecto LIKE '%PEPSI%'";
            }
            else if (proyecto.Equals("SCHWEPPES"))
            {
                query += " AND proyecto LIKE '%SCH%'";
            }
            else if (proyecto.Equals("HEINEKEN STC"))
            {
                query += " AND proyecto LIKE '%HEI%ST%'";
            }
            else if (proyecto.Equals("HEINEKEN EVENTO"))
            {
                query += " AND proyecto LIKE '%HEI%EV%'";
            }
            else if (proyecto.Equals("HEINEKEN ALIMENTACIÓN"))
            {
                query += " AND proyecto LIKE '%HEI%ALI%'";
            }
            else if (proyecto.Equals("EDEN SPRING"))
            {
                query += " AND proyecto LIKE '%EDEN%'";
            }
            else if (proyecto.Equals("SPECTANK"))
            {
                query += " AND proyecto LIKE '%SPEC%'";
            }
            else if (proyecto.Equals("GINOS FRIO"))
            {
                query += " AND proyecto LIKE '%GIN%FRI%'";
            }
            else if (proyecto.Equals("LACREM"))
            {
                query += " AND proyecto LIKE '%LACRE%'";
            }
            else if (proyecto.Equals("OTROS"))
            {
                query += " AND (proyecto LIKE '%STARBUCk%CAF%' OR proyecto LIKE '%STARBUCk%FR%' OR " +
                         " proyecto LIKE '%IKEA%' OR proyecto LIKE '%BERLY%' OR " +
                         " proyecto LIKE '%DANONE%' OR proyecto LIKE '%PEPSI%' OR " +
                         " proyecto LIKE '%SCH%' OR proyecto LIKE '%HEI%' OR " +
                         " proyecto LIKE '%SPECT%' OR proyecto LIKE '%GINO%' OR " +
                         " proyecto LIKE '%EDEN%')";
            }
            else if (proyecto.Equals("CAFE TODO"))
            {
                query += " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";
            }
            else
            {
                query += " AND proyecto LIKE '%%' ";
            }
            /*****************************************/
            query += " AND pds LIKE '%" + pds + "%'";
            query += " ORDER BY id ASC";
            /*****************************************/

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));
            //command.Parameters.Add(new SqlParameter("PDS", pds));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clordentaller y llena 
                      los registros con los datos de la orden*/
                    clordentaller registro = new clordentaller()
                    {
                        Id = Convert.ToInt32(item[0]),
                        Tecnico = Convert.ToString(item[1]),
                        Cod_Tecnico = Convert.ToString(item[2]),
                        Proyecto = Convert.ToString(item[3]),
                        Cod_proyecto = Convert.ToString(item[4]),
                        Cliente = Convert.ToString(item[5]),
                        Pds = Convert.ToString(item[6]),
                        Ods = Convert.ToString(item[7]),
                        Provincia = Convert.ToString(item[8]),
                        Actividad = Convert.ToString(item[9]),
                        Tipo = Convert.ToString(item[10]),
                        Estado = Convert.ToString(item[11]),
                        Resultado = Convert.ToString(item[12]),
                        F_trabajo = Convert.ToInt32(item[13]),
                        F_repa = Convert.ToInt32(item[14]),
                        Equipo = Convert.ToString(item[15]),
                        Material = Convert.ToString(item[16]),
                        Codmaterial = Convert.ToString(item[17]),
                        Ax = Convert.ToString(item[18]),
                        Sap = Convert.ToString(item[19]),
                        Orden = Convert.ToString(item[20]),
                        Pedido = Convert.ToString(item[21]),
                        Oferta = (float)Convert.ToDouble(item[22]),
                        Repuestos = (float)Convert.ToDouble(item[23]),
                        Despl = (float)Convert.ToDouble(item[24]),
                        Mo = (float)Convert.ToDouble(item[25]),
                        Descripcion = Convert.ToString(item[26]),
                        Fichero = Convert.ToString(item[27]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(registro);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region LISTAR REGISTROS POR FECHA, ESTADO, PROYECTO Y PDS - ORDEN TALLER
        /// <summary>
        /// Busca todos los registros por estado y fecha inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<clordentaller> listaNombre(int i, int f, string estado, string proyecto, string nombre)
        {
            List<clordentaller> lista = new List<clordentaller>();
            string query;
            query = "SELECT * FROM ordentaller WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND ";
            /*****************************************/
            if (estado.Equals("ABIERTAS"))
            {
                query += "estado LIKE '%ABIERT%'";
            }
            else if (estado.Equals("CERRADAS"))
            {
                query += "estado LIKE '%CERRAD%'";
            }
            else
            {
                query += " estado LIKE '%%' ";
            }
            /*****************************************/
            if (proyecto.Equals("CAFENTO"))
            {
                query += " AND proyecto LIKE '%CAFENT%'";
            }
            else if (proyecto.Equals("CANDELAS"))
            {
                query += " AND proyecto LIKE '%CANDEL%'";
            }
            else if (proyecto.Equals("LATITUD"))
            {
                query += " AND (proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%') ";
            }
            else if (proyecto.Equals("DELTA"))
            {
                query += " AND proyecto LIKE '%DELT%'";
            }
            else if (proyecto.Equals("BLACKZI"))
            {
                query += " AND proyecto LIKE '%BLACKZ%'";
            }
            else if (proyecto.Equals("DUNKIN"))
            {
                query += " AND proyecto LIKE '%DUNKI%'";
            }
            else if (proyecto.Equals("PROINDE"))
            {
                query += " AND proyecto LIKE '%PROIND%'";
            }
            else if (proyecto.Equals("STARBUCKS CAFE"))
            {
                query += " AND proyecto LIKE '%STARBUCk%CAF%'";
            }
            else if (proyecto.Equals("STARBUCKS FRIO"))
            {
                query += " AND proyecto LIKE '%STARBUCk%FR%'";
            }
            else if (proyecto.Equals("IKEA"))
            {
                query += " AND proyecto LIKE '%IKEA%'";
            }
            else if (proyecto.Equals("BERLYS"))
            {
                query += " AND proyecto LIKE '%BERLY%'";
            }
            else if (proyecto.Equals("DANONE WATERS"))
            {
                query += " AND proyecto LIKE '%DANO%WATE%'";
            }
            else if (proyecto.Equals("DANONE VENDING"))
            {
                query += " AND proyecto LIKE '%DANO%VEND%'";
            }
            else if (proyecto.Equals("PEPSI"))
            {
                query += " AND proyecto LIKE '%PEPSI%'";
            }
            else if (proyecto.Equals("SCHWEPPES"))
            {
                query += " AND proyecto LIKE '%SCH%'";
            }
            else if (proyecto.Equals("HEINEKEN STC"))
            {
                query += " AND proyecto LIKE '%HEI%ST%'";
            }
            else if (proyecto.Equals("HEINEKEN EVENTO"))
            {
                query += " AND proyecto LIKE '%HEI%EV%'";
            }
            else if (proyecto.Equals("HEINEKEN ALIMENTACIÓN"))
            {
                query += " AND proyecto LIKE '%HEI%ALI%'";
            }
            else if (proyecto.Equals("EDEN SPRING"))
            {
                query += " AND proyecto LIKE '%EDEN%'";
            }
            else if (proyecto.Equals("SPECTANK"))
            {
                query += " AND proyecto LIKE '%SPEC%'";
            }
            else if (proyecto.Equals("GINOS FRIO"))
            {
                query += " AND proyecto LIKE '%GIN%FRI%'";
            }
            else if (proyecto.Equals("LACREM"))
            {
                query += " AND proyecto LIKE '%LACRE%'";
            }
            else if (proyecto.Equals("OTROS"))
            {
                query += " AND (proyecto LIKE '%STARBUCk%CAF%' OR proyecto LIKE '%STARBUCk%FR%' OR " +
                         " proyecto LIKE '%IKEA%' OR proyecto LIKE '%BERLY%' OR " +
                         " proyecto LIKE '%DANONE%' OR proyecto LIKE '%PEPSI%' OR " +
                         " proyecto LIKE '%SCH%' OR proyecto LIKE '%HEI%' OR " +
                         " proyecto LIKE '%SPECT%' OR proyecto LIKE '%GINO%' OR " +
                         " proyecto LIKE '%EDEN%')";
            }
            else if (proyecto.Equals("CAFE TODO"))
            {
                query += " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";
            }
            else
            {
                query += " AND proyecto LIKE '%%' ";
            }
            /*****************************************/
            query += " AND cliente LIKE '%" + nombre + "%'";
            query += " ORDER BY id ASC";
            /*****************************************/

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));
            //command.Parameters.Add(new SqlParameter("NOMBRE", nombre));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clordentaller y llena 
                      los registros con los datos de la orden*/
                    clordentaller registro = new clordentaller()
                    {
                        Id = Convert.ToInt32(item[0]),
                        Tecnico = Convert.ToString(item[1]),
                        Cod_Tecnico = Convert.ToString(item[2]),
                        Proyecto = Convert.ToString(item[3]),
                        Cod_proyecto = Convert.ToString(item[4]),
                        Cliente = Convert.ToString(item[5]),
                        Pds = Convert.ToString(item[6]),
                        Ods = Convert.ToString(item[7]),
                        Provincia = Convert.ToString(item[8]),
                        Actividad = Convert.ToString(item[9]),
                        Tipo = Convert.ToString(item[10]),
                        Estado = Convert.ToString(item[11]),
                        Resultado = Convert.ToString(item[12]),
                        F_trabajo = Convert.ToInt32(item[13]),
                        F_repa = Convert.ToInt32(item[14]),
                        Equipo = Convert.ToString(item[15]),
                        Material = Convert.ToString(item[16]),
                        Codmaterial = Convert.ToString(item[17]),
                        Ax = Convert.ToString(item[18]),
                        Sap = Convert.ToString(item[19]),
                        Orden = Convert.ToString(item[20]),
                        Pedido = Convert.ToString(item[21]),
                        Oferta = (float)Convert.ToDouble(item[22]),
                        Repuestos = (float)Convert.ToDouble(item[23]),
                        Despl = (float)Convert.ToDouble(item[24]),
                        Mo = (float)Convert.ToDouble(item[25]),
                        Descripcion = Convert.ToString(item[26]),
                        Fichero = Convert.ToString(item[27]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(registro);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region BUSCAR IMPORTE TOTAL ENTRE DOS FECHAS - OFERTA - MO - REPUESTO - DESPLAZAMIENTO - IMPORTE
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public float valorTotal(int i, int f, string estado, string objeto, string proyecto, float importe)
        {
            float resultado = 0;
            string query = "";

            if (objeto.Equals("REPUESTO"))
            {
                query += "SELECT SUM(repuesto) FROM ordentaller ";
            }
            else if (objeto.Equals("DESPLAZAMIENTO"))
            {
                query += "SELECT SUM(despl) FROM ordentaller ";
            }
            else if (objeto.Equals("MANO DE OBRA"))
            {
                query += "SELECT SUM(mo) FROM ordentaller ";
            }
            else
            {
                query += "SELECT SUM(oferta) FROM ordentaller ";
            }
            /*****************************************/
            query += " WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND oferta >= @Importe AND ";
            /*****************************************/
            if (estado.Equals("ABIERTAS"))
            {
                query += "estado LIKE '%aABIERT%'";
            }
            else if (estado.Equals("CERRADAS"))
            {
                query += "estado LIKE '%CERRAD%'";
            }
            else
            {
                query += " estado LIKE '%%' ";
            }
            /*****************************************/
            if (proyecto.Equals("CAFENTO"))
            {
                query += " AND proyecto LIKE '%CAFENT%'";
            }
            else if (proyecto.Equals("CANDELAS"))
            {
                query += " AND proyecto LIKE '%CANDEL%'";
            }
            else if (proyecto.Equals("LATITUD"))
            {
                query += " AND (proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%') ";
            }
            else if (proyecto.Equals("DELTA"))
            {
                query += " AND proyecto LIKE '%DELT%'";
            }
            else if (proyecto.Equals("BLACKZI"))
            {
                query += " AND proyecto LIKE '%BLACKZ%'";
            }
            else if (proyecto.Equals("DUNKIN"))
            {
                query += " AND proyecto LIKE '%DUNKI%'";
            }
            else if (proyecto.Equals("PROINDE"))
            {
                query += " AND proyecto LIKE '%PROIND%'";
            }
            else if (proyecto.Equals("STARBUCKS CAFE"))
            {
                query += " AND proyecto LIKE '%STARBUCk%CAF%'";
            }
            else if (proyecto.Equals("STARBUCKS FRIO"))
            {
                query += " AND proyecto LIKE '%STARBUCk%FR%'";
            }
            else if (proyecto.Equals("IKEA"))
            {
                query += " AND proyecto LIKE '%IKEA%'";
            }
            else if (proyecto.Equals("BERLYS"))
            {
                query += " AND proyecto LIKE '%BERLY%'";
            }
            else if (proyecto.Equals("DANONE WATERS"))
            {
                query += " AND proyecto LIKE '%DANO%WATE%'";
            }
            else if (proyecto.Equals("DANONE VENDING"))
            {
                query += " AND proyecto LIKE '%DANO%VEND%'";
            }
            else if (proyecto.Equals("PEPSI"))
            {
                query += " AND proyecto LIKE '%PEPSI%'";
            }
            else if (proyecto.Equals("SCHWEPPES"))
            {
                query += " AND proyecto LIKE '%SCH%'";
            }
            else if (proyecto.Equals("HEINEKEN STC"))
            {
                query += " AND proyecto LIKE '%HEI%ST%'";
            }
            else if (proyecto.Equals("HEINEKEN EVENTO"))
            {
                query += " AND proyecto LIKE '%HEI%EV%'";
            }
            else if (proyecto.Equals("HEINEKEN ALIMENTACIÓN"))
            {
                query += " AND proyecto LIKE '%HEI%ALI%'";
            }
            else if (proyecto.Equals("EDEN SPRING"))
            {
                query += " AND proyecto LIKE '%EDEN%'";
            }
            else if (proyecto.Equals("SPECTANK"))
            {
                query += " AND proyecto LIKE '%SPEC%'";
            }
            else if (proyecto.Equals("GINOS FRIO"))
            {
                query += " AND proyecto LIKE '%GIN%FRI%'";
            }
            else if (proyecto.Equals("LACREM"))
            {
                query += " AND proyecto LIKE '%LACRE%'";
            }
            else if (proyecto.Equals("OTROS"))
            {
                query += " AND (proyecto LIKE '%STARBUCk%CAF%' OR proyecto LIKE '%STARBUCk%FR%' OR " +
                         " proyecto LIKE '%IKEA%' OR proyecto LIKE '%BERLY%' OR " +
                         " proyecto LIKE '%DANONE%' OR proyecto LIKE '%PEPSI%' OR " +
                         " proyecto LIKE '%SCH%' OR proyecto LIKE '%HEI%' OR " +
                         " proyecto LIKE '%SPECT%' OR proyecto LIKE '%GINO%' OR " +
                         " proyecto LIKE '%EDEN%')";
            }
            else if (proyecto.Equals("CAFE TODO"))
            {
                query += " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";
            }
            else
            {
                query += " AND proyecto LIKE '%%' ";
            }
            /*****************************************/


            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));
            command.Parameters.Add(new SqlParameter("Importe", importe));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = (float)Convert.ToDouble(r);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region BUSCAR IMPORTE TOTAL ENTRE DOS FECHAS - OFERTA - MO - REPUESTO - DESPLAZAMIENTO
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public float valorTotal(int i, int f, string estado, string objeto, string proyecto)
        {
            float resultado = 0;
            string query = "";

            if (objeto.Equals("REPUESTO"))
            {
                query += "SELECT SUM(repuesto) FROM ordentaller ";
            }
            else if (objeto.Equals("DESPLAZAMIENTO"))
            {
                query += "SELECT SUM(despl) FROM ordentaller ";
            }
            else if (objeto.Equals("MANO DE OBRA"))
            {
                query += "SELECT SUM(mo) FROM ordentaller ";
            }
            else
            {
                query += "SELECT SUM(oferta) FROM ordentaller ";
            }
            /*****************************************/
            query += " WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND ";
            /*****************************************/
            if (estado.Equals("ABIERTAS"))
            {
                query += "estado LIKE '%aABIERT%'";
            }
            else if (estado.Equals("CERRADAS"))
            {
                query += "estado LIKE '%CERRAD%'";
            }
            else
            {
                query += " estado LIKE '%%' ";
            }
            /*****************************************/
            if (proyecto.Equals("CAFENTO"))
            {
                query += " AND proyecto LIKE '%CAFENT%'";
            }
            else if (proyecto.Equals("CANDELAS"))
            {
                query += " AND proyecto LIKE '%CANDEL%'";
            }
            else if (proyecto.Equals("LATITUD"))
            {
                query += " AND (proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%') ";
            }
            else if (proyecto.Equals("DELTA"))
            {
                query += " AND proyecto LIKE '%DELT%'";
            }
            else if (proyecto.Equals("BLACKZI"))
            {
                query += " AND proyecto LIKE '%BLACKZ%'";
            }
            else if (proyecto.Equals("DUNKIN"))
            {
                query += " AND proyecto LIKE '%DUNKI%'";
            }
            else if (proyecto.Equals("PROINDE"))
            {
                query += " AND proyecto LIKE '%PROIND%'";
            }
            else if (proyecto.Equals("STARBUCKS CAFE"))
            {
                query += " AND proyecto LIKE '%STARBUCk%CAF%'";
            }
            else if (proyecto.Equals("STARBUCKS FRIO"))
            {
                query += " AND proyecto LIKE '%STARBUCk%FR%'";
            }
            else if (proyecto.Equals("IKEA"))
            {
                query += " AND proyecto LIKE '%IKEA%'";
            }
            else if (proyecto.Equals("BERLYS"))
            {
                query += " AND proyecto LIKE '%BERLY%'";
            }
            else if (proyecto.Equals("DANONE WATERS"))
            {
                query += " AND proyecto LIKE '%DANO%WATE%'";
            }
            else if (proyecto.Equals("DANONE VENDING"))
            {
                query += " AND proyecto LIKE '%DANO%VEND%'";
            }
            else if (proyecto.Equals("PEPSI"))
            {
                query += " AND proyecto LIKE '%PEPSI%'";
            }
            else if (proyecto.Equals("SCHWEPPES"))
            {
                query += " AND proyecto LIKE '%SCH%'";
            }
            else if (proyecto.Equals("HEINEKEN STC"))
            {
                query += " AND proyecto LIKE '%HEI%ST%'";
            }
            else if (proyecto.Equals("HEINEKEN EVENTO"))
            {
                query += " AND proyecto LIKE '%HEI%EV%'";
            }
            else if (proyecto.Equals("HEINEKEN ALIMENTACIÓN"))
            {
                query += " AND proyecto LIKE '%HEI%ALI%'";
            }
            else if (proyecto.Equals("EDEN SPRING"))
            {
                query += " AND proyecto LIKE '%EDEN%'";
            }
            else if (proyecto.Equals("SPECTANK"))
            {
                query += " AND proyecto LIKE '%SPEC%'";
            }
            else if (proyecto.Equals("GINOS FRIO"))
            {
                query += " AND proyecto LIKE '%GIN%FRI%'";
            }
            else if (proyecto.Equals("LACREM"))
            {
                query += " AND proyecto LIKE '%LACRE%'";
            }
            else if (proyecto.Equals("OTROS"))
            {
                query += " AND (proyecto LIKE '%STARBUCk%CAF%' OR proyecto LIKE '%STARBUCk%FR%' OR " +
                         " proyecto LIKE '%IKEA%' OR proyecto LIKE '%BERLY%' OR " +
                         " proyecto LIKE '%DANONE%' OR proyecto LIKE '%PEPSI%' OR " +
                         " proyecto LIKE '%SCH%' OR proyecto LIKE '%HEI%' OR " +
                         " proyecto LIKE '%SPECT%' OR proyecto LIKE '%GINO%' OR " +
                         " proyecto LIKE '%EDEN%')";
            }
            else if (proyecto.Equals("CAFE TODO"))
            {
                query += " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";
            }
            else
            {
                query += " AND proyecto LIKE '%%' ";
            }
            /*****************************************/


            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = (float)Convert.ToDouble(r);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region BUSCAR IMPORTE TOTAL ENTRE DOS FECHAS - OFERTA - MO - REPUESTO - DESPLAZAMIENTO TECNICO
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public float valorTotal(int i, int f, string estado, string objeto, string proyecto, string tecnico)
        {
            float resultado = 0;
            string query = "";

            if (objeto.Equals("REPUESTO"))
            {
                query += "SELECT SUM(repuesto) FROM ordentaller ";
            }
            else if (objeto.Equals("DESPLAZAMIENTO"))
            {
                query += "SELECT SUM(despl) FROM ordentaller ";
            }
            else if (objeto.Equals("MANO DE OBRA"))
            {
                query += "SELECT SUM(mo) FROM ordentaller ";
            }
            else
            {
                query += "SELECT SUM(oferta) FROM ordentaller ";
            }
            /*****************************************/
            query += " WHERE (f_repa >= @F_inicio AND f_repa <= @F_fin) AND (cod_tecnico like '%" + tecnico + "%') AND ";
            /*****************************************/
            if (estado.Equals("ABIERTAS"))
            {
                query += "estado LIKE '%aABIERT%'";
            }
            else if (estado.Equals("CERRADAS"))
            {
                query += "estado LIKE '%CERRAD%'";
            }
            else
            {
                query += " estado LIKE '%%' ";
            }
            /*****************************************/
            if (proyecto.Equals("CAFENTO"))
            {
                query += " AND proyecto LIKE '%CAFENT%'";
            }
            else if (proyecto.Equals("CANDELAS"))
            {
                query += " AND proyecto LIKE '%CANDEL%'";
            }
            else if (proyecto.Equals("LATITUD"))
            {
                query += " AND (proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%') ";
            }
            else if (proyecto.Equals("DELTA"))
            {
                query += " AND proyecto LIKE '%DELT%'";
            }
            else if (proyecto.Equals("BLACKZI"))
            {
                query += " AND proyecto LIKE '%BLACKZ%'";
            }
            else if (proyecto.Equals("DUNKIN"))
            {
                query += " AND proyecto LIKE '%DUNKI%'";
            }
            else if (proyecto.Equals("PROINDE"))
            {
                query += " AND proyecto LIKE '%PROIND%'";
            }
            else if (proyecto.Equals("STARBUCKS CAFE"))
            {
                query += " AND proyecto LIKE '%STARBUCk%CAF%'";
            }
            else if (proyecto.Equals("STARBUCKS FRIO"))
            {
                query += " AND proyecto LIKE '%STARBUCk%FR%'";
            }
            else if (proyecto.Equals("IKEA"))
            {
                query += " AND proyecto LIKE '%IKEA%'";
            }
            else if (proyecto.Equals("BERLYS"))
            {
                query += " AND proyecto LIKE '%BERLY%'";
            }
            else if (proyecto.Equals("DANONE WATERS"))
            {
                query += " AND proyecto LIKE '%DANO%WATE%'";
            }
            else if (proyecto.Equals("DANONE VENDING"))
            {
                query += " AND proyecto LIKE '%DANO%VEND%'";
            }
            else if (proyecto.Equals("PEPSI"))
            {
                query += " AND proyecto LIKE '%PEPSI%'";
            }
            else if (proyecto.Equals("SCHWEPPES"))
            {
                query += " AND proyecto LIKE '%SCH%'";
            }
            else if (proyecto.Equals("HEINEKEN STC"))
            {
                query += " AND proyecto LIKE '%HEI%ST%'";
            }
            else if (proyecto.Equals("HEINEKEN EVENTO"))
            {
                query += " AND proyecto LIKE '%HEI%EV%'";
            }
            else if (proyecto.Equals("HEINEKEN ALIMENTACIÓN"))
            {
                query += " AND proyecto LIKE '%HEI%ALI%'";
            }
            else if (proyecto.Equals("EDEN SPRING"))
            {
                query += " AND proyecto LIKE '%EDEN%'";
            }
            else if (proyecto.Equals("SPECTANK"))
            {
                query += " AND proyecto LIKE '%SPEC%'";
            }
            else if (proyecto.Equals("GINOS FRIO"))
            {
                query += " AND proyecto LIKE '%GIN%FRI%'";
            }
            else if (proyecto.Equals("LACREM"))
            {
                query += " AND proyecto LIKE '%LACRE%'";
            }
            else if (proyecto.Equals("OTROS"))
            {
                query += " AND (proyecto LIKE '%STARBUCk%CAF%' OR proyecto LIKE '%STARBUCk%FR%' OR " +
                         " proyecto LIKE '%IKEA%' OR proyecto LIKE '%BERLY%' OR " +
                         " proyecto LIKE '%DANONE%' OR proyecto LIKE '%PEPSI%' OR " +
                         " proyecto LIKE '%SCH%' OR proyecto LIKE '%HEI%' OR " +
                         " proyecto LIKE '%SPECT%' OR proyecto LIKE '%GINO%' OR " +
                         " proyecto LIKE '%EDEN%')";
            }
            else if (proyecto.Equals("CAFE TODO"))
            {
                query += " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";
            }
            else
            {
                query += " AND proyecto LIKE '%%' ";
            }
            /*****************************************/


            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("F_inicio", i));
            command.Parameters.Add(new SqlParameter("F_fin", f));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value) 
                    resultado = (float)Convert.ToDouble(r);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region CAMBIA EL ESTADO DE UNA ORDEN DE TALLER
        /// Cambiar a cerrada
        public void concluir(int idorden, string estado, string resultado)
        {
            //Declaramos nuestra consulta de Acción Sql
            string query = "update ordentaller set estado = '" + estado + "', resultado = '" + resultado + "' where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }
        #endregion


        #region TRASPASO DE REGISTROS
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<OBSOLETO_OTaller> busca()
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<OBSOLETO_OTaller> lista = new List<OBSOLETO_OTaller>();
            string query = "select * from otaller";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    OBSOLETO_OTaller producto = new OBSOLETO_OTaller()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Tecnico = Convert.ToString(dr[1]),
                        Estado = Convert.ToString(dr[2]),
                        Resultado = Convert.ToString(dr[3]),
                        Proyecto = Convert.ToString(dr[4]),
                        Pds = Convert.ToString(dr[5]),
                        F_trabajo = Convert.ToInt32(dr[6]),
                        F_repa = Convert.ToInt32(dr[7]),
                        Cliente = Convert.ToString(dr[8]),
                        Provincia = Convert.ToString(dr[9]),
                        Oaveria = Convert.ToString(dr[10]),
                        Omontaje = Convert.ToString(dr[11]),
                        Odesmontaje = Convert.ToString(dr[12]),
                        Otaller = Convert.ToString(dr[13]),
                        Material = Convert.ToString(dr[14]),
                        Codmaterial = Convert.ToString(dr[15]),
                        Ax = Convert.ToString(dr[16]),
                        Sap = Convert.ToString(dr[17]),
                        Norden = Convert.ToString(dr[18]),
                        Oferta = (float)Convert.ToDouble(dr[19]),
                        Despl = (float)Convert.ToDouble(dr[20]),
                        Mo = (float)Convert.ToDouble(dr[21]),
                        Repuesto = Convert.ToString(dr[22]),
                        Cmi = Convert.ToString(dr[23]),
                        Tipo = Convert.ToString(dr[24]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region TRASPASO DE REGISTROS
        /// <summary>
        /// Busca todos los registros por tipo: fecha de orden o fecha de trabajo,
        /// entre las fechas inicial y final
        /// </summary>
        /// <param name="i">fechas inicial</param>
        /// <param name="f">fechas final</param>
        /// <param name="tipo">tipo: fecha de orden o fecha de trabajo</param>
        /// <returns>Lista de registros</returns>
        public List<clordentaller> buscaParaCopia()
        {
            /*Declaramos una lista de objetos de tipo clsProducto que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clordentaller> lista = new List<clordentaller>();
            string query = "select * from ordentaller";

            ConnectionCopia.Open();
            SqlCommand commandCopia = new SqlCommand(query, ConnectionCopia);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = commandCopia;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    clordentaller producto = new clordentaller()
                    {
                        Id = Convert.ToInt32(item[0]),
                        Tecnico = Convert.ToString(item[1]),
                        Cod_Tecnico = Convert.ToString(item[2]),
                        Proyecto = Convert.ToString(item[3]),
                        Cod_proyecto = Convert.ToString(item[4]),
                        Cliente = Convert.ToString(item[5]),
                        Pds = Convert.ToString(item[6]),
                        Ods = Convert.ToString(item[7]),
                        Provincia = Convert.ToString(item[8]),
                        Actividad = Convert.ToString(item[9]),
                        Tipo = Convert.ToString(item[10]),
                        Estado = Convert.ToString(item[11]),
                        Resultado = Convert.ToString(item[12]),
                        F_trabajo = Convert.ToInt32(item[13]),
                        F_repa = Convert.ToInt32(item[14]),
                        Equipo = Convert.ToString(item[15]),
                        Material = Convert.ToString(item[16]),
                        Codmaterial = Convert.ToString(item[17]),
                        Ax = Convert.ToString(item[18]),
                        Sap = Convert.ToString(item[19]),
                        Orden = Convert.ToString(item[20]),
                        Pedido = Convert.ToString(item[21]),
                        Oferta = (float)Convert.ToDouble(item[22]),
                        Repuestos = (float)Convert.ToDouble(item[23]),
                        Despl = (float)Convert.ToDouble(item[24]),
                        Mo = (float)Convert.ToDouble(item[25]),
                        Descripcion = Convert.ToString(item[26]),
                        Fichero = Convert.ToString(item[27]),
                    };

                    /*Agrega un producto a la lista de productos*/
                    lista.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                ConnectionCopia.Close();
            }
            //retorna la lista de productos   
            return lista;
        }
        #endregion


        #region CAMBIA LAS FECHAS DE UNA ORDEN
        /// Cambiar a cerrada
        public void modificarFechas(int idorden, int f1, int f2)
        {
            //Declaramos nuestra consulta de Acción Sql
            string query = "update ordentaller set f_trabajo = " + f1 + ", f_repa = " + f2 + " where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idorden));
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }

        }
        #endregion


        #region MODiFICA UN REGISTRO
        /// Cambiar a cerrada
        public int modificar(int idObjeto, clordentaller objeto)
        {
            int resultado = 0;
            //Declaramos nuestra consulta de Acción Sql
            string query = "update ordentaller set tecnico  = '" + objeto.Tecnico + "', " +
                                                    "cod_tecnico  = '" + objeto.Cod_Tecnico + "', " +
                                                    "proyecto  = '" + objeto.Proyecto + "', " +
                                                    "cod_proyecto  = '" + objeto.Cod_proyecto + "', " +
                                                    "cliente  = '" + objeto.Cliente + "', " +
                                                    "pds  = '" + objeto.Pds + "', " +
                                                    "ods  = '" + objeto.Ods + "', " +
                                                    "provincia  = '" + objeto.Provincia + "', " +
                                                    "actividad  = '" + objeto.Actividad + "', " +
                                                    "tipo  = '" + objeto.Tipo + "' " +
                                                    "estado = '" + objeto.Estado + "', " +
                                                    "resultado  = '" + objeto.Resultado + "', " +
                                                    "f_trabajo  = '" + objeto.F_trabajo + "', " +
                                                    "f_repa  = '" + objeto.F_repa + "', " +
                                                    "equipo  = '" + objeto.Equipo + "', " +
                                                    "material  = '" + objeto.Material + "', " +
                                                    "codmaterial  = '" + objeto.Codmaterial + "', " +
                                                    "ax  = '" + objeto.Ax + "', " +
                                                    "sap  = '" + objeto.Sap + "', " +
                                                    "orden  = '" + objeto.Orden + "' " +
                                                    "pedido  = '" + objeto.Pedido + "' " +
                                                    "oferta = '" + objeto.Oferta + "', " +
                                                    "repuesto  = '" + objeto.Repuestos + "', " +
                                                    "despl  = '" + objeto.Despl + "', " +
                                                    "mo  = '" + objeto.Mo + "', " +
                                                    "descripcion  = '" + objeto.Descripcion + "', " +
                                                    "fichero  = '" + objeto.Fichero + "', " +
                                                    " where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("ID", idObjeto));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region OBTENER TOTAL REGISTROS DE ORDENES TALLER CAFE POR FECHA - GRAFICO 1
        public int obtenerTotalOrdenesGrafico1(int fecha)
        {
            int resultado = 0;
            string query;
            query = "SELECT count(*) FROM ordentaller WHERE (f_trabajo = @Fecha) " +
            " AND (proyecto LIKE '%CAFENT%' OR proyecto LIKE '%CANDEL%' OR " +
                         " proyecto LIKE '%LATIT%' OR proyecto LIKE '%SIH%' OR " +
                         " proyecto LIKE '%DELT%' OR proyecto LIKE '%BLACKZ%' OR " +
                         " proyecto LIKE '%DUNKI%' OR proyecto LIKE '%PROIND%' OR " +
                         " proyecto LIKE '%STARBUCk%CAF%')";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("Fecha", fecha));
            //command.Parameters.Add(new SqlParameter("ODS", ods));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                    var r = command.ExecuteScalar();
                    if (r != DBNull.Value)
                        resultado = Convert.ToInt32(r);
            }
            catch
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }

        public string ConnectionStringCopia
        { get; set; }

        public SqlConnection ConnectionCopia
        { get; set; }
        #endregion
    }
}
