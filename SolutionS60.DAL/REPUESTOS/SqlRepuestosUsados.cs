﻿using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL
{
    public class SqlRepuestosUsados
    {
        private string connectionString;
        private SqlConnection connection;


        public SqlRepuestosUsados()
        {
            ConnectionString = Properties.Resource1.CadenaConexion1;
            Connection = new SqlConnection(ConnectionString);
        }


        #region INSERTAR UN REGISTRO - REPUESTOS DE ORDEN DE TALLER
        /// <summary>
        /// Insertar un nuevo registro en la base de datos
        /// <param name="registro">Tiene la información del registrar</param>
        /// </summary>
        /// <param name="registro"></param>
        /// <returns>Si 'resultado' es distinto de 0 -> OK, 
        /// sino, Si 'resultado'==0 -> ERROR</returns>
        public int inserta(clrepuestotaller registro)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into repuestotaller " +
                "values(@Material,@Cod_pmaterial,@Cantidad,@Id);";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Material", registro.Material));
            command.Parameters.Add(new SqlParameter("Cod_pmaterial", registro.Cod_pmaterial));
            command.Parameters.Add(new SqlParameter("Cantidad", registro.Cantidad));
            command.Parameters.Add(new SqlParameter("Id", registro.Id));
            try
            {
                //Si resultado != 0 operación OK
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region listar todos los registros de REPUESTOS GENERALES NUEVA LISTA BOOM CAFE V002 con un LIKE NAME - NOMBRE MATERIAL SAP
        /// <summary>
        /// Devuelve la lista de todos los registros coincidentes por nombre de material
        /// </summary>  
        public List<SAP_Repuestos_v001> obtenerRepuestosV001PorNombre(string nombreMaterial)
        {
            /*Declaramos una lista de objetos de tipo SAP_Repuestos_General que guardará
            los REPUESTOS de la base de datos que coincidan con el NOMBRE dado en argumantos,
            y posteriormente será devuelta cuando finalize la función*/
            List<SAP_Repuestos_v001> lista = new List<SAP_Repuestos_v001>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_repuestos_v002 " +
                "where name LIKE '%" + nombreMaterial + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_v001 producto = new SAP_Repuestos_v001()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codesap"].ToString(),
                        Code_ax = item["codeax"].ToString(),
                        Tipo = item["tipo"].ToString(),
                        Name = item["name"].ToString(),
                        Price = (float)Convert.ToDouble(item["price"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region listar todos los registros de REPUESTOS GENERALES NUEVA LISTA BOOM CAFE V002 con un LIKE CODE SAP - CÓDIGO MATERIAL SAP
        /// <summary>
        /// Devuelve la lista de todos los registros coincidentes por código de material
        /// </summary>  
        public List<SAP_Repuestos_v001> obtenerRepuestosV001PorCodigo(string codigoMaterial)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<SAP_Repuestos_v001> lista = new List<SAP_Repuestos_v001>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_repuestos_v002 " +
                "where codesap LIKE '%" + codigoMaterial + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_v001 producto = new SAP_Repuestos_v001()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codesap"].ToString(),
                        Code_ax = item["codeax"].ToString(),
                        Tipo = item["tipo"].ToString(),
                        Name = item["name"].ToString(),
                        Price = (float)Convert.ToDouble(item["price"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region listar todos los registros de REPUESTOS GENERALES NUEVA LISTA BOOM BLY con un LIKE NAME - NOMBRE MATERIAL SAP
        /// <summary>
        /// Devuelve la lista de todos los registros coincidentes por nombre de material
        /// </summary>  
        public List<SAP_Repuestos_v001> obtenerRepuestosBlyPorNombre(string nombreMaterial)
        {
            /*Declaramos una lista de objetos de tipo SAP_Repuestos_General que guardará
            los REPUESTOS de la base de datos que coincidan con el NOMBRE dado en argumantos,
            y posteriormente será devuelta cuando finalize la función*/
            List<SAP_Repuestos_v001> lista = new List<SAP_Repuestos_v001>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_repuestos_bly " +
                "where name LIKE '%" + nombreMaterial + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_v001 producto = new SAP_Repuestos_v001()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codesap"].ToString(),
                        Code_ax = "",
                        Tipo = item["tipo"].ToString(),
                        Name = item["name"].ToString(),
                        Price = (float)0,
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region listar todos los registros de REPUESTOS GENERALES NUEVA LISTA BOOM BLY con un LIKE CODE SAP - CÓDIGO MATERIAL SAP
        /// <summary>
        /// Devuelve la lista de todos los registros coincidentes por código de material
        /// </summary>  
        public List<SAP_Repuestos_v001> obtenerRepuestosBlyPorCodigo(string codigoMaterial)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<SAP_Repuestos_v001> lista = new List<SAP_Repuestos_v001>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_repuestos_bly " +
                "where codesap LIKE '%" + codigoMaterial + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_v001 producto = new SAP_Repuestos_v001()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codesap"].ToString(),
                        Code_ax = "",
                        Tipo = item["tipo"].ToString(),
                        Name = item["name"].ToString(),
                        Price = (float)0,
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region listar todos los registros de REPUESTOS GENERALES NUEVA LISTA BOOM STURBUCKS con un LIKE NAME - NOMBRE MATERIAL SAP
        /// <summary>
        /// Devuelve la lista de todos los registros coincidentes por nombre de material
        /// </summary>  
        public List<SAP_Repuestos_v001> obtenerRepuestosStarbucksPorNombre(string nombreMaterial)
        {
            /*Declaramos una lista de objetos de tipo SAP_Repuestos_General que guardará
            los REPUESTOS de la base de datos que coincidan con el NOMBRE dado en argumantos,
            y posteriormente será devuelta cuando finalize la función*/
            List<SAP_Repuestos_v001> lista = new List<SAP_Repuestos_v001>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_listaboonstarbucks " +
                "where nombre LIKE '%" + nombreMaterial + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_v001 producto = new SAP_Repuestos_v001()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codigo"].ToString(),
                        Code_ax = "",
                        Tipo = "",
                        Name = item["nombre"].ToString(),
                        Price = (float)0,
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region listar todos los registros de REPUESTOS GENERALES NUEVA LISTA BOOM STURBUCKS con un LIKE CODE SAP - CÓDIGO MATERIAL SAP
        /// <summary>
        /// Devuelve la lista de todos los registros coincidentes por código de material
        /// </summary>  
        public List<SAP_Repuestos_v001> obtenerRepuestosStarbucksPorCodigo(string codigoMaterial)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<SAP_Repuestos_v001> lista = new List<SAP_Repuestos_v001>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_listaboonstarbucks " +
                "where codigo LIKE '%" + codigoMaterial + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_v001 producto = new SAP_Repuestos_v001()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codesap"].ToString(),
                        Code_ax = "",
                        Tipo = "",
                        Name = item["name"].ToString(),
                        Price = (float)0,
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region listar todos los registros de REPUESTOS GENERALES con un LIKE NAME - NOMBRE MATERIAL SAP
        /// <summary>
        /// Devuelve la lista de todos los registros coincidentes por nombre de material
        /// </summary>  
        public List<SAP_Repuestos_General> obtenerRepuestosPorNombre(string nombreMaterial)
        {
            /*Declaramos una lista de objetos de tipo SAP_Repuestos_General que guardará
            los REPUESTOS de la base de datos que coincidan con el NOMBRE dado en argumantos,
            y posteriormente será devuelta cuando finalize la función*/
            List<SAP_Repuestos_General> lista = new List<SAP_Repuestos_General>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_repuestos_general " +
                "where name LIKE '%" + nombreMaterial + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_General producto = new SAP_Repuestos_General()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codesap"].ToString(),
                        Code_ax = item["codeax"].ToString(),
                        Name = item["name"].ToString(),
                        Price = (float)Convert.ToDouble(item["price"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region listar todos los registros de REPUESTOS GENERALES con un LIKE CODE SAP - CÓDIGO MATERIAL SAP
        /// <summary>
        /// Devuelve la lista de todos los registros coincidentes por código de material
        /// </summary>  
        public List<SAP_Repuestos_General> obtenerRepuestosPorCodigo(string codigoMaterial)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<SAP_Repuestos_General> lista = new List<SAP_Repuestos_General>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from sap_repuestos_general " +
                "where codesap LIKE '%" + codigoMaterial + "%'";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SAP_Repuestos_General producto = new SAP_Repuestos_General()
                    {
                        Id = Convert.ToInt32(item["id"]),
                        Code_sap = item["codesap"].ToString(),
                        Code_ax = item["codeax"].ToString(),
                        Name = item["name"].ToString(),
                        Price = (float)Convert.ToDouble(item["price"]),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region TRASPASO DE REGISTROS
        public List<SAP_Repuestos_v001> traspasoOrigen()
        {
            List<SAP_Repuestos_v001> lista = new List<SAP_Repuestos_v001>();
            string query = "select * from listaboonoriginal";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    SAP_Repuestos_v001 origen = new SAP_Repuestos_v001()
                    {
                        Code_sap = item["codigo"].ToString(),
                        Code_ax = "",
                        Tipo = "",
                        Name = item["articulo"].ToString(),
                        Price = (float)Convert.ToDouble(item["precio"]),
                    };
                    lista.Add(origen);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }

        public void traspasoDestino(List<SAP_Repuestos_v001> lista)
        {
            Connection.Open();
            foreach (SAP_Repuestos_v001 origen in lista)
            {
                string query =
                    "insert into sap_repuestos_v002 " +
                    "values(@sap,@ax,@tipo,@nombre,@precio);";
                
                SqlCommand command = new SqlCommand(query, Connection);
                command.Parameters.Add(new SqlParameter("sap", origen.Code_sap));
                command.Parameters.Add(new SqlParameter("ax", origen.Code_ax));
                command.Parameters.Add(new SqlParameter("tipo", origen.Tipo));
                command.Parameters.Add(new SqlParameter("nombre", origen.Name));
                command.Parameters.Add(new SqlParameter("precio", origen.Price));
                command.ExecuteNonQuery();
            }
            Connection.Close();

        }

        public List<SAP_Repuestos_v001> traspasoOrigen2()
        {
            List<SAP_Repuestos_v001> lista = new List<SAP_Repuestos_v001>();
            string query = "select * from [dbo].[0BORRARlistaboonstarbucks];";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    /*Instancia un objeto clsProducto y llena 
                      los registros con los datos de los productos*/
                    SAP_Repuestos_v001 origen = new SAP_Repuestos_v001()
                    {
                        Code_sap = item["F1"].ToString(),
                        Code_ax = "",
                        Tipo = "",
                        Name = item["F2"].ToString(),
                        Price = (float)0,
                    };
                    lista.Add(origen);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }

        public void traspasoDestino2(List<SAP_Repuestos_v001> lista)
        {
            Connection.Open();
            foreach (SAP_Repuestos_v001 origen in lista)
            {
                string query =
                    "insert into sap_listaboonstarbucks " +
                    "values(@sap,@nombre);";

                SqlCommand command = new SqlCommand(query, Connection);
                command.Parameters.Add(new SqlParameter("sap", origen.Code_sap));
                command.Parameters.Add(new SqlParameter("nombre", origen.Name));
                command.ExecuteNonQuery();
            }
            Connection.Close();

        }
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
