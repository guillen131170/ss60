﻿using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.DAL
{
    public class SqlRepuestos
    {
        private string connectionString;
        private SqlConnection connection;

        public SqlRepuestos()
        {
            /*En CafeS60.DAL - Settings.settings*/
            ConnectionString = Properties.Resource1.CadenaConexion1;
            //@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=catalogo;Integrated Security=True";
            Connection = new SqlConnection(ConnectionString);
        }


        #region TODAS LAS CONSULTAS
        #region INSERTAR REGISTRO DE REPUESTO NUEVO
        /// <summary>
        /// Insertar un nuevo producto es la base de datos
        /// <param name="repuestos">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int inserta(clrepuestos producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into repuestos " +
                "values(@Referencia,@Nombre,@Descripcion,@Precio,@Stock," +
                "       @Familia, @Proveedor, @Imagen); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            command.Parameters.Add(new SqlParameter("Referencia", producto.Referencia));
            command.Parameters.Add(new SqlParameter("Nombre", producto.Nombre));
            command.Parameters.Add(new SqlParameter("Descripcion", producto.Descripcion));
            command.Parameters.Add(new SqlParameter("Precio", producto.Precio));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            command.Parameters.Add(new SqlParameter("Familia", producto.Familia));
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            command.Parameters.Add(new SqlParameter("Imagen", producto.Imagen));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region INSERTAR REGISTRO PEDIDO DE REPUESTOS
        /// <summary>
        /// Insertar un nuevo producto es la base de datos
        /// <param name="repuestos">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int insertaPedido(clrepuestospedido producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "insert into repuestospedido " +
                "values(@Pedido, @Referencia, @Nombre, @Precio, @Unidades," +
                "       @Fecha, @Familia, @Proveedor); ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Pedido", producto.Pedido));
            command.Parameters.Add(new SqlParameter("Referencia", producto.Referencia));
            command.Parameters.Add(new SqlParameter("Nombre", producto.Nombre));
            command.Parameters.Add(new SqlParameter("Precio", producto.Precio));
            command.Parameters.Add(new SqlParameter("Unidades", producto.Unidades));
            command.Parameters.Add(new SqlParameter("Fecha", producto.Fecha));
            command.Parameters.Add(new SqlParameter("Familia", producto.Familia));
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region COMPROBAR REGISTROS DUPLICADOS
        /// <summary>
        /// Devuelve el resultado si 2 códigos o nombres de un producto son iguales
        /// </summary>  
        /// modo: 0 - ok
        ///       1 - duplicado
        public bool duplicado(string referencia)
        {
            bool resultado = false;

            //Cadena para la consulta sql a la base de datos
            string query = "select * from repuestos where referencia LIKE @Referencia";

            try
            {
                //Abrimos la conexion a la Base de datos
                Connection.Open();
                /*Ejecuta la consulta sql*/
                SqlCommand command = new SqlCommand(query, Connection);
                command.Parameters.Add(new SqlParameter("Referencia", referencia));

                //command.CommandType = CommandType.StoredProcedure;
                if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                {
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region LISTAR CATALOGO DE REPUESTOS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clrepuestos> obtenerTodos()
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clrepuestos> lista = new List<clrepuestos>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from repuestos order by nombre ASC";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clrepuestos producto = new clrepuestos()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Referencia = item["Referencia"].ToString(),
                        Nombre = item["Nombre"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Precio = (float)Convert.ToDouble(item["Precio"]),
                        Stock = Convert.ToInt32(item["Stock"]),
                        Familia = item["Familia"].ToString(),
                        Proveedor = item["Proveedor"].ToString(),
                        Imagen = item["Imagen"].ToString(),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region BUSCAR IMPORTE TOTAL DEL PEDIDO
        public float importe(string referencia)
        {
            float resultado = 0;
            string query = "SELECT SUM(precio) FROM repuestospedido " +
                           " WHERE pedido LIKE @Referencia";

            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("Referencia", referencia));

            /*Guarda los datos en un DataSet*/
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {

                var r = command.ExecuteScalar();
                if (r != DBNull.Value)
                    resultado = (float)Convert.ToDouble(r);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            //retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region LISTAR PEDIDOS
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clrepuestospedido> obtenerTodosPedidos()
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clrepuestospedido> lista = new List<clrepuestospedido>();
            string coincidencia = "";
            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            //string query = "select DISTINCT pedido, precio, fecha, proveedor from repuestospedido order by fecha ASC";
            string query = "SELECT * FROM repuestospedido ORDER BY id DESC";
            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clrepuestospedido producto = new clrepuestospedido()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Pedido = item["Pedido"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Nombre = item["Nombre"].ToString(),
                        Precio = (float)Convert.ToDouble(item["Precio"]),
                        Unidades = Convert.ToInt32(item["Unidades"]),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                        Familia = item["Familia"].ToString(),
                        Proveedor = item["Proveedor"].ToString(),                       
                    };
                    if (!coincidencia.Equals(item["Pedido"].ToString()))
                    {
                        coincidencia = item["Pedido"].ToString();                       
                        lista.Add(producto);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region DETALLE PEDIDO
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clrepuestospedido> detallePedido(string referencia)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clrepuestospedido> lista = new List<clrepuestospedido>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from repuestospedido WHERE pedido LIKE @Referencia";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Referencia", referencia));

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clrepuestospedido producto = new clrepuestospedido()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Pedido = item["Pedido"].ToString(),
                        Referencia = item["Referencia"].ToString(),
                        Nombre = item["Nombre"].ToString(),
                        Precio = (float)Convert.ToDouble(item["Precio"]),
                        Unidades = Convert.ToInt32(item["Unidades"]),
                        Fecha = Convert.ToInt32(item["Fecha"]),
                        Familia = item["Familia"].ToString(),
                        Proveedor = item["Proveedor"].ToString(),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region BUSCAR UN PEDIDO POR SU ID
        /// <summary>
        /// Devuelve un Objeto Pedido
        /// </summary>
        public clrepuestospedido obtenerRegistroId(int id)
        {
            clrepuestospedido registro = new clrepuestospedido();
            registro = null;
            string query = "select * from repuestospedido where id=@ID";
            Connection.Open();
            SqlCommand command = new SqlCommand(query, Connection);
            //
            //Utilizamos el valor del parámetro idTarjeta para enviarlo al parámetro declarado en la consulta
            //de selección SQL
            command.Parameters.Add(new SqlParameter("ID", id));
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow item = ds.Tables[0].Rows[0];
                //Instanciamos al objeto cls_Tarjeta para llenar sus propiedades
                registro = new clrepuestospedido()
                {
                    Id = Convert.ToInt32(item[0]),
                    Pedido = Convert.ToString(item[1]),
                    Referencia = Convert.ToString(item[2]),
                    Nombre = Convert.ToString(item[3]),
                    Precio = (float)Convert.ToDouble(item[4]),
                    Unidades = Convert.ToInt32(item[5]),
                    Fecha = Convert.ToInt32(item[6]),
                    Familia = Convert.ToString(item[7]),
                    Proveedor = Convert.ToString(item[8]),
                };

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Connection.Close();
            }

            return registro;
        }
        #endregion


        #region listar todos los registros con filtro
        /// <summary>
        /// Devuelve la lista de todos los productos
        /// </summary>  
        /// modo: 0 - descatalogado
        ///       1 - catalogado 
        ///       3 o nada - todos
        public List<clrepuestos> obtenerTodosFiltrado(string familia)
        {
            /*Declaramos una lista de objetos de tipo Product que guardará
            los productos de la base de datos, y posteriormente será devuelta
            cuando finalize la función*/
            List<clrepuestos> lista = new List<clrepuestos>();

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select * from repuestos  where familia LIKE '%' + @Familia + '%'";
            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@Familia", familia);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = command;
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    clrepuestos producto = new clrepuestos()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Referencia = item["Referencia"].ToString(),
                        Nombre = item["Nombre"].ToString(),
                        Descripcion = item["Descripcion"].ToString(),
                        Precio = (float)Convert.ToDouble(item["Precio"]),
                        Stock = Convert.ToInt32(item["Stock"]),
                        Familia = item["Familia"].ToString(),
                        Proveedor = item["Proveedor"].ToString(),
                        Imagen = item["Imagen"].ToString(),
                    };
                    lista.Add(producto);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return lista;
        }
        #endregion


        #region devuelve el stock por su id
        /// <summary>
        /// Devuelve el stock por su id
        /// </summary>  
        public int obtenerStock(int id)
        {
            /**/
            int resultado = 0;

            //Creamos una variable que contendra la consulta a ejecutar
            //Cadena para la consulta sql a la base de datos
            string query = "select stock from repuestos where id LIKE @ID";

            //Abrimos la conexion a la Base de datos
            Connection.Open();
            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@ID", id);

            // Guarda los datos en un DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                resultado = (Int32)command.ExecuteScalar();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            // Retorna la lista de productos   
            return resultado;
        }
        #endregion


        #region modificar un registro
        /// <summary>
        /// Modificar un producto es la base de datos
        /// <param name="producto">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int modifica(int id, clrepuestos producto)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update repuestos " +
                "set referencia=@Referencia, nombre=@Nombre, descripcion=@Descripcion, precio=@Precio, stock=@Stock," +
                "    familia=@Familia, proveedor=@Proveedor, imagen=@Imagen " +
                "where id=@id; ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Referencia", producto.Referencia));
            command.Parameters.Add(new SqlParameter("Nombre", producto.Nombre));
            command.Parameters.Add(new SqlParameter("Descripcion", producto.Descripcion));
            command.Parameters.Add(new SqlParameter("Precio", producto.Precio));
            command.Parameters.Add(new SqlParameter("Stock", producto.Stock));
            command.Parameters.Add(new SqlParameter("Familia", producto.Familia));
            command.Parameters.Add(new SqlParameter("Proveedor", producto.Proveedor));
            command.Parameters.Add(new SqlParameter("Imagen", producto.Imagen));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion


        #region modificar un stock
        /// <summary>
        /// Modificar un producto es la base de datos
        /// <param name="producto">Tiene la información del producto a registrar</param>
        /// </summary> 
        public int stock(int id, string referencia, int stock)
        {
            int resultado = 0;
            /*Cadena para la consulta sql a la base de datos*/
            string query =
                "update repuestos " +
                "set stock=@Stock " +
                " where id=@Id or referencia=@Referencia; ";

            /*Abre una conexión*/
            Connection.Open();

            /*Ejecuta la consulta sql*/
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.Add(new SqlParameter("Id", id));
            command.Parameters.Add(new SqlParameter("Referencia", referencia));
            command.Parameters.Add(new SqlParameter("Stock", stock));
            try
            {
                resultado = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // Cierra la Conexion
                Connection.Close();
            }
            return resultado;
        }
        #endregion
        #endregion


        #region DESCRIPTORES DE ACCESO
        public string ConnectionString
        { get; set; }

        public SqlConnection Connection
        { get; set; }
        #endregion
    }
}
