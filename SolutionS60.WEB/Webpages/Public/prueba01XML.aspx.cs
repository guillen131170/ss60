﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using SolutionS60.CORE.REPUESTOS;
using System.Data;
using System.Diagnostics;

namespace SolutionS60.WEB.Webpages.Public
{
    public partial class prueba01XML : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //List<clrepuestoSimple> lista = new List<clrepuestoSimple>();
            //lista = lectura();
        }


        private void lectura(string valor)
        {
            List<clrepuestoSimple> lista = new List<clrepuestoSimple>();
            clrepuestoSimple item = new clrepuestoSimple();
            string ruta = Properties.Resource1.RutaDatosXML2;
            XmlTextReader m_xmlr;
            m_xmlr = new XmlTextReader(ruta + valor);
            m_xmlr.WhitespaceHandling = WhitespaceHandling.None;
            m_xmlr.Read();
            m_xmlr.Read();

          
            /// Lee los registros y los guarda en una lista
            while (!m_xmlr.EOF)
            {
                m_xmlr.Read();
                if (!m_xmlr.IsStartElement())
                {
                    break;
                }

                object mRegistro = m_xmlr.GetAttribute("Registro");
                m_xmlr.Read();
                object mCodigo = m_xmlr.ReadElementString("codigo");
                object mNombre = m_xmlr.ReadElementString("nombre");

                item = new clrepuestoSimple()
                {
                    Codigo = mCodigo.ToString(),
                    Nombre = mNombre.ToString(),
                };
                lista.Add(item);               
            }
            // Cerramos la lactura del archivo
            m_xmlr.Close();

            lista = lista.OrderBy(o => o.Nombre).ToList();
            DataTable dt = new DataTable();
            dt.Columns.Add("Codigo");
            dt.Columns.Add("Nombre");

            foreach (clrepuestoSimple i in lista)
            {
                DataRow dr = dt.NewRow();
                dr[0] = i.Codigo.ToString();
                dr[1] = i.Nombre.ToString();
                dt.Rows.Add(dr);
            }
            /*
            while (!m_xmlr.EOF)
            {
                m_xmlr.Read();
                if (!m_xmlr.IsStartElement())
                {
                    break;
                }

                object mRegistro = m_xmlr.GetAttribute("Registro");
                m_xmlr.Read();
                object mCodigo = m_xmlr.ReadElementString("codigo");
                object mNombre = m_xmlr.ReadElementString("nombre");

                DataRow dr = dt.NewRow();
                dr[0] = mCodigo.ToString();
                dr[1] = mNombre.ToString();
                dt.Rows.Add(dr);
            }
            */
        
            tablaRegistros.Visible = true;
            tablaRegistros.DataSource = dt;
            tablaRegistros.DataBind();
        }


        private void lectura2()
        {
            string msg = "";
            string ruta = Properties.Resource1.RutaDatosXML2;
            XmlTextReader m_xmlr;
            // Creamos el XML Reader
            //m_xmlr = new XmlTextReader("D:/Usuario/Desktop/O.TALLER/000_LISTAS XML/DUNKIN3.XML");
            m_xmlr = new XmlTextReader(ruta + "dunkin.xml");
            // Desabilitamos las lineas en blanco,
            // ya no las necesitamos
            m_xmlr.WhitespaceHandling = WhitespaceHandling.None;
            // Leemos el archivo y avanzamos al tag de usuarios
            m_xmlr.Read();
            // Leemos el tag usuarios
            m_xmlr.Read();
            // Creamos la secuancia que nos permite
            // leer el archivo
            while (!m_xmlr.EOF)
            {
                // Avanzamos al siguiente tag
                m_xmlr.Read();
                // si no tenemos el elemento inicial
                // debemos salir del ciclo
                if (!m_xmlr.IsStartElement())
                {
                    break;
                }

                // Obtenemos el elemento codigo
                object mCodigo = m_xmlr.GetAttribute("Registro");
                // Read elements firstname and lastname
                m_xmlr.Read();
                // Obtenemos el elemento del Nombre del Usuario
                object mNombre = m_xmlr.ReadElementString("codigo");
                // Obtenemos el elemento del Apellido del Usuario
                object mApellido = m_xmlr.ReadElementString("nombre");
                // Escribimos el resultado en la consola,
                // pero tambien podriamos utilizarlos en
                // donde deseemos
                /*
                Console.WriteLine("Codigo usuario: " + mCodigo
                                  + " Nombre: " + mNombre
                                  + " Apellido: " + mApellido);
                Console.Write("\r\n");
                */
                msg += "Codigo usuario: " + mCodigo
                                  + " Nombre: " + mNombre
                                  + " Apellido: " + mApellido + " --- ";
            }
            // Cerramos la lactura del archivo
            m_xmlr.Close();
            status.Text = msg;
            statusdiv.Attributes["class"] = "alert alert-danger";
        }


        #region APLICAR FILTRO
        protected void actualizar_Click(object sender, EventArgs e)
        {
            lectura(clase.SelectedItem.ToString() + ".xml");
        }

        public void mostrarLista()
        {
            lectura("dunkin.xml");
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "Ver_Click":
                        break;

                    default:
                        throw new Exception("Operación desconocida");
                }
                lectura("dunkin.xml");
            }
            catch
            {
                // recoge errores hacia el status text
                status.Text = "ARCHIVO NO ENCONTRADO";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }
        #endregion


        #region PEPSI
        protected void pepsicooler_Click(object sender, EventArgs e)
        {
            string ruta = Properties.Resource1.RutaDatosXML2;
            OpenMicrosoftExcel(ruta + "MAESTROFOTOSCOOLER.xlsx");
        }

        protected void pepsivending_Click(object sender, EventArgs e)
        {
            string ruta = Properties.Resource1.RutaDatosXML2;
            OpenMicrosoftExcel(ruta + "MAESTROFOTOSVENDINGCOMPLETO.xlsx");
        }

        protected void pesigeneral_Click(object sender, EventArgs e)
        {
            string ruta = Properties.Resource1.RutaDatosXML2;
            OpenMicrosoftExcel(ruta + "MAESTROFOTOREPUESTOSPEPSI.xlsx");
        }
        #endregion


        #region ZONAS Y TELEFONOS
        protected void zonas_Click(object sender, EventArgs e)
        {
            string ruta = Properties.Resource1.RutaDatosXML2;
            Process p = new Process();
            p.StartInfo.FileName = ruta + "ZONAS.jpeg";
            p.Start();
        } 
        #endregion


        #region ABRIR EXCEL
        /// <summary>
        /// Open specified excel document.
        /// </summary>
        static void OpenMicrosoftExcel(string archivo)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "EXCEL.EXE";
            startInfo.Arguments = archivo;
            Process.Start(startInfo);
        }
        #endregion
    }
}