﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="prueba01XML.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Public.prueba01XML" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>
    <div class="form-horizontal-inline" role="form">
    <br />
    <br />
             <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:dropdownlist runat="server" id="clase" class="caja-gr" >
                                        <asp:listitem text="BERLYS_MO" value="1"></asp:listitem>
                                        <asp:listitem text="DUNKIN" value="2"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="actualizar" runat="server" Text="APLICAR FILTRO" class="btn btn-success btn-lg btn-block" OnClick="actualizar_Click" />
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="imprimir" runat="server" Text="PDF" class="btn btn-success btn-lg btn-block" />
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:Button ID="pepsicooler" runat="server" Text="PEPSI COOLER" class="btn btn-success btn-lg btn-block" OnClick="pepsicooler_Click" />
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="pepsivending" runat="server" Text="PEPSI VENDING" class="btn btn-success btn-lg btn-block" OnClick="pepsivending_Click" />
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="pesigeneral" runat="server" Text="PEPSI GENERAL" class="btn btn-success btn-lg btn-block" OnClick="pesigeneral_Click" />
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:Button ID="zonas" runat="server" Text="ZONAS Y TELÉFONOS" class="btn btn-success btn-lg btn-block" OnClick="zonas_Click" />
                                </div>
                                <div class="col-xs-4"></div>
                                <div class="col-xs-4"></div>
                            </div><br />
                        </div>
                    </div>
                </div>
            </div>


    <div class="form-horizontal-inline" role="form">
    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">LISTA DE REPUESTOS PARA ORDENES DE TALLER</h3></div>
        <div class="panel-body">     
            <div class="table-responsive" style="margin-top: 30px;">      
                <asp:GridView runat="server" ID="tablaRegistros" class="table table-striped" Visible="false" 
                    AutoGenerateColumns="true" GridLines="None" BorderWidth="0" OnRowCommand="tablaRegistros_RowCommand">
                    <Columns>
                        <asp:ButtonField ButtonType="Link" HeaderText="Detalle" Text="ver" CommandName="Ver_Click"  />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</div>
</div>
</asp:Content>
