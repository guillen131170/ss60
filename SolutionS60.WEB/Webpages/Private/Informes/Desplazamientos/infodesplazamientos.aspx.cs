﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Informes.Desplazamientos
{
    public partial class infodesplazamientos : System.Web.UI.Page
    {
        private int inicio;
        private int fin;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fin = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
                inicio = fin - (fin % 100) + 1;
                fechainicio.Value = inicio.ToString();
                fechafin.Value = fin.ToString();
            }
        }
        #endregion


        protected void ObtenerDatos()
        {
            int dd = ((Convert.ToInt32(inicio)) % 100);
            int mm = (((Convert.ToInt32(inicio)) / 100) % 100);
            int aa = ((Convert.ToInt32(inicio)) / 10000);
            int ddd = ((Convert.ToInt32(fin)) % 100);
            int mmm = (((Convert.ToInt32(fin)) / 100) % 100);
            int aaa = ((Convert.ToInt32(fin)) / 10000);
            titulo.Text = "PERIODO DE " + dd.ToString() + "/" + mm.ToString() + "/" + aa.ToString() + " A " +
                ddd.ToString() + "/" + mmm.ToString() + "/" + aaa.ToString();
            divtitulo.Attributes["class"] = "alert alert-sucess";
        }


        #region PULSAR BOTÓN ACTUALIZAR LISTA APLICANDO FILTRO
        protected void actualizar_Click(object sender, EventArgs e)
        {
            procesosOrdenTaller proceso = new procesosOrdenTaller();
            List<clordentaller> lista = new List<clordentaller>();

            lista = proceso.obtenerListaDesplazamientos(Convert.ToInt32(fechainicio.Value),
                                                     Convert.ToInt32(fechafin.Value), nombreProyecto.SelectedItem.ToString());

            if ((!inicio.Equals("") && !fin.Equals("")) &&
                (Convert.ToInt32(inicio) <= Convert.ToInt32(fin)) &&
                (lista.Count > 0))
            {
                try
                {                  
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Id");
                    dt.Columns.Add("Proyecto");
                    dt.Columns.Add("Cliente");
                    dt.Columns.Add("Actividad");
                    dt.Columns.Add("Ods");
                    dt.Columns.Add("Provincia");
                    dt.Columns.Add("Orden");
                    dt.Columns.Add("Oferta");
                    foreach (clordentaller registro in lista)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = registro.Id;
                        if (registro.Proyecto.Length > 8)
                        {
                            dr[1] = registro.Proyecto.Remove(8);
                        }
                        else dr[1] = registro.Proyecto;
                        if (registro.Cliente.Length > 25)
                        {
                            dr[2] = registro.Cliente.Remove(25);
                        }
                        else dr[2] = registro.Cliente;
                        dr[3] = registro.Actividad;
                        dr[4] = registro.Ods;
                        dr[5] = registro.Provincia;
                        dr[6] = registro.Orden;
                        dr[7] = registro.Oferta;
                        if (!registro.Cod_proyecto.Equals("007")) dt.Rows.Add(dr);
                    }
                    tablaRegistros.Visible = true;
                    tablaRegistros.DataSource = dt;
                    tablaRegistros.DataBind();
                }
                catch (Exception ex)
                {
                    // las excepciones al status y cual es el error
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }

            if (lista.Count <= 0)
            {
                status.Text = "No se encontraron registros";
                statusdiv.Attributes["class"] = "alert alert-sucess";
            }
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        #region IMPRIMIR ORDEN DE TALLER EN DETALLE
        void imprimirDocumento(clordentaller i)//clordentaller registro)
        {
            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));

            // Le colocamos el título y el autor
            // **Nota: Esto no será visible en el documento
            doc.AddTitle("FACTURACION");
            doc.AddCreator("Fco Guillén");
            // Abrimos el archivo
            doc.Open();

            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph("ID " + i.Id));
            doc.Add(new Paragraph("TECNICO " + i.Tecnico + " " + i.Cod_Tecnico));
            doc.Add(new Paragraph("PROYECTO " + i.Proyecto + " " + i.Cod_proyecto));
            doc.Add(new Paragraph("CLIENTE " + i.Cliente + " " + i.Pds + " " + i.Provincia));
            doc.Add(new Paragraph("ODS " + i.Ods));
            doc.Add(new Paragraph("ACTIVIDAD " + i.Actividad + " " + i.Tipo));
            doc.Add(new Paragraph("ESTADO " + i.Estado + " " + i.Resultado));
            doc.Add(new Paragraph("FECHA " + i.F_trabajo + " " + i.F_repa));
            doc.Add(new Paragraph("EQUIPO " + i.Equipo));
            doc.Add(new Paragraph("MATERIAL " + i.Material + " " + i.Codmaterial));
            doc.Add(new Paragraph("NS " + i.Ax + " " + i.Sap));
            doc.Add(new Paragraph("ORDEN " + i.Orden + " " + i.Pedido));
            doc.Add(new Paragraph("OFERTA " + i.Oferta));
            doc.Add(new Paragraph("REPUESTO " + i.Repuestos + " DESPLAZAMIENTO " + i.Despl + " MO " + i.Mo));
            doc.Add(new Paragraph("DESCRIPCION " + i.Descripcion));
            doc.Add(new Paragraph("FICHERO " + i.Fichero));
            doc.Close();
            writer.Close();

            //string pdfPath = Path.Combine(Application.StartupPath, "archivo.pdf");
            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        }
        #endregion
        #endregion


        #region IMPRIMIR LISTA
        protected void imprimir_Click(object sender, EventArgs e)
        {
            List<clordentaller> lista = new List<clordentaller>();     

            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
            doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            doc.Open();
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá el nombre, apellido y país
            // de nuestros visitante.
            //5, 5, 5, 10, 4, 4, 4, 59
            float[] columnWidths = { 10, 42, 18, 15, 10, 12, 8 };
            PdfPTable tblPrueba = new PdfPTable(columnWidths);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 0;
            tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            tblPrueba.AddCell("Proyecto");
            tblPrueba.AddCell("Cliente");
            tblPrueba.AddCell("Actividad");
            tblPrueba.AddCell("Ods");
            tblPrueba.AddCell("Provincia");
            tblPrueba.AddCell("Orden");
            tblPrueba.AddCell("Oferta");

            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");

            procesosOrdenTaller proceso = new procesosOrdenTaller();
            lista = proceso.obtenerListaDesplazamientos(Convert.ToInt32(fechainicio.Value),
                                                     Convert.ToInt32(fechafin.Value), nombreProyecto.SelectedItem.ToString());

            if ((!inicio.Equals("") && !fin.Equals("")) &&
                (Convert.ToInt32(inicio) <= Convert.ToInt32(fin)) &&
                (lista.Count > 0))
            {
                foreach (clordentaller registro in lista)
                {
                    if (registro.Proyecto.Length > 8)
                    {
                        tblPrueba.AddCell(registro.Proyecto.Remove(8));
                    }
                    else tblPrueba.AddCell(registro.Proyecto);
                    if (registro.Cliente.Length > 25)
                    {
                        tblPrueba.AddCell(registro.Cliente.Remove(25));
                    }
                    else tblPrueba.AddCell(registro.Cliente);

                    tblPrueba.AddCell(registro.Actividad);
                    tblPrueba.AddCell(registro.Ods);
                    tblPrueba.AddCell(registro.Provincia);
                    tblPrueba.AddCell(registro.Orden);
                    tblPrueba.AddCell(registro.Oferta.ToString()); 
                }
            }
            doc.Add(tblPrueba);
            doc.Close();
            writer.Close();

            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        }
        #endregion

        protected void imprimirimporte_Click(object sender, EventArgs e)
        {
        }
    }
}