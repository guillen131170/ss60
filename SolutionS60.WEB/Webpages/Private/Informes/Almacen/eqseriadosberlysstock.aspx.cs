﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Informes.Almacen
{
    public partial class eqseriadosberlysstock : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }

        public int mostrarLista()
        {
            procesosEqBerlys proceso = new procesosEqBerlys();
            List<cleqseriadoberlys> lista = new List<cleqseriadoberlys>();
            try
            {
                lista = proceso.InformeBasicoRegistro();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("NombreMaterial");
                dt.Columns.Add("CodigoMaterial");
                dt.Columns.Add("CodSAP");
                dt.Columns.Add("CodAX");
                dt.Columns.Add("Origen");
                //dt.Columns.Add("ODSEntrada");
                foreach (cleqseriadoberlys registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.NombreMaterial;
                    dr[2] = registro.CodigoMaterial;
                    dr[3] = registro.CodSAP;
                    dr[4] = registro.CodAX;
                    dr[5] = registro.Origen;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosEqBerlys proceso = new procesosEqBerlys();
            cleqseriadoberlys registro = new cleqseriadoberlys();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            if (idProducto != -111)
            {
                try
                {

                    switch (e.CommandName)
                    {
                        case "Salida_Click":
                            if (!ods.Value.ToString().Equals("") && !destino.Value.ToString().Equals(""))
                                proceso.sacarEquipo(idProducto, ods.Value.ToString(), destino.Value.ToString());
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch (Exception ex)
                {
                    // recoge errores hacia el status text
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion
    }
}