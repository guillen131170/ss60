﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="infoportecnico.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Informes.Facturacion.infoportecnico" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">
    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">INFORME FACTURACION POR TÉCNICO</h3></div>
        <div class="panel-body">     
            
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:dropdownlist runat="server" id="estado" class="caja-md" >
                                        <asp:listitem text="ABIERTAS/CERRADAS" value="1"></asp:listitem>
                                        <asp:listitem text="ABIERTAS" value="2"></asp:listitem>
                                        <asp:listitem text="CERRADAS" value="1"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-5">
                                <asp:dropdownlist runat="server" id="nombreProyecto" class="caja-md" >
                                        <asp:listitem text="CAFENTO" value="1"></asp:listitem>
                                        <asp:listitem text="CANDELAS" value="2"></asp:listitem>
                                        <asp:listitem text="LATITUD" value="3"></asp:listitem>
                                        <asp:listitem text="DELTA" value="4"></asp:listitem>
                                        <asp:listitem text="BLACKZI" value="5"></asp:listitem>
                                        <asp:listitem text="DUNKIN" value="6"></asp:listitem>
                                        <asp:listitem text="PROINDE" value="7"></asp:listitem>
                                        <asp:listitem text="STARBUCKS CAFE" value="8"></asp:listitem>
                                        <asp:listitem text="STARBUCKS FRIO" value="9"></asp:listitem>
                                        <asp:listitem text="IKEA" value="10"></asp:listitem>
                                        <asp:listitem text="BERLYS" value="11"></asp:listitem>
                                        <asp:listitem text="DANONE WATERS" value="12"></asp:listitem>
                                        <asp:listitem text="DANONE VENDING" value="13"></asp:listitem>
                                        <asp:listitem text="PEPSI" value="14"></asp:listitem>
                                        <asp:listitem text="SCHWEPPES" value="15"></asp:listitem>
                                        <asp:listitem text="HEINEKEN STC" value="16"></asp:listitem>
                                        <asp:listitem text="HEINEKEN EVENTO" value="17"></asp:listitem>
                                        <asp:listitem text="HEINEKEN ALIMENTACIÓN" value="18"></asp:listitem>
                                        <asp:listitem text="EDEN SPRING" value="19"></asp:listitem>
                                        <asp:listitem text="SPECTANK" value="20"></asp:listitem>
                                        <asp:listitem text="GINOS FRIO" value="21"></asp:listitem>
                                        <asp:listitem text="LACREM" value="22"></asp:listitem>
                                        <asp:listitem text="OTROS" value="23"></asp:listitem>
                                        <asp:listitem text="TODO" value="24"></asp:listitem>
                                        <asp:listitem text="CAFE TODO" value="25"></asp:listitem>
                                        <asp:listitem text="OTROS TODO" value="26"></asp:listitem>
                                    </asp:dropdownlist>
                                    </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="actualizar" runat="server" Text="APLICAR FILTRO" class="btn btn-success btn-lg btn-block" 
                                        OnClick="actualizar_Click" />
                                </div>
                            </div>
                            <div class="row"><hr /></div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Label class="caja-pq" runat="server">Fecha Inicio</asp:Label>
                                    <input type="text" id="fechainicio" class="caja-pq" placeholder="Inicio" runat="server">
                                </div>
                                <div class="col-xs-3">
                                    <asp:Label class="caja-pq" runat="server">Fecha Fin</asp:Label>
                                    <input type="text" id="fechafin" class="caja-pq" placeholder="Fin" runat="server">
                                </div>
                                <div class="col-xs-3">
                                    <asp:Button ID="imprimir" runat="server" Text="IMPRIMIR TODO" class="btn btn-success btn-lg btn-block" 
                                        OnClick="imprimir_Click" />
                                </div>
                                <div class="col-xs-3">
                                    <asp:Button ID="informe" runat="server" Text="IMPRIMIR IMPORTE" class="btn btn-success btn-lg btn-block" 
                                        OnClick="imprimirimporte_Click" />
                                </div>
                            </div><br />

                <div class="row">
                <div class="col-xs-12">
                <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">TÉCNICOS</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Label ID="MEVA0001" class="caja-md" runat="server">MEVA0001</asp:Label>
                                    <asp:CheckBox ID="CheckMEVA0001" runat="server"/>
                                </div>
                                <div class="col-xs-3">
                                    <asp:Label ID="MESA0004" class="caja-md" runat="server">MESA0004</asp:Label>
                                    <asp:CheckBox ID="CheckMESA0004" runat="server"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Label ID="MEVA0002" class="caja-md" runat="server">MEVA0002</asp:Label>
                                    <asp:CheckBox ID="CheckMEVA0002" runat="server"/>
                                </div>
                                <div class="col-xs-3">
                                    <asp:Label ID="MESA0006" class="caja-md" runat="server">MESA0006</asp:Label>
                                    <asp:CheckBox ID="CheckMESA0006" runat="server"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Label ID="MEVA0010" class="caja-md" runat="server">MEVA0010</asp:Label>
                                    <asp:CheckBox ID="CheckMEVA0010" runat="server"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Label ID="MEVA0011" class="caja-md" runat="server">MEVA0011</asp:Label>
                                    <asp:CheckBox ID="CheckMEVA0011" runat="server"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Label ID="MEVA0013" class="caja-md" runat="server">MEVA0013</asp:Label>
                                    <asp:CheckBox ID="CheckMEVA0013" runat="server"/>
                                </div>
                            </div>
                    </div>
                </div>
             </div>
          </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<div id="divtitulo" runat="server">
        <h3><asp:Literal runat="server" ID="titulo"></asp:Literal></h3>
</div>  
<div class="table-responsive" style="margin-top: 30px;">      
    <asp:GridView runat="server" ID="tablaRegistros" class="table table-striped"
        Visible="false" AutoGenerateColumns="true" GridLines="None" BorderWidth="0" OnRowCommand="tablaRegistros_RowCommand">
    </asp:GridView>
</div>


</asp:Content>
