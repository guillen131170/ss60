﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Informes.Facturacion
{
    public partial class infoportecnico : System.Web.UI.Page
    {
        private int inicio;
        private int fin;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fin = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
                inicio = fin - (fin % 100) + 1;
                fechainicio.Value = inicio.ToString();
                fechafin.Value = fin.ToString();
            }
        }
        #endregion


        protected void ObtenerDatos()
        {
            int dd = ((Convert.ToInt32(inicio)) % 100);
            int mm = (((Convert.ToInt32(inicio)) / 100) % 100);
            int aa = ((Convert.ToInt32(inicio)) / 10000);
            int ddd = ((Convert.ToInt32(fin)) % 100);
            int mmm = (((Convert.ToInt32(fin)) / 100) % 100);
            int aaa = ((Convert.ToInt32(fin)) / 10000);
            titulo.Text = "PERIODO DE " + dd.ToString() + "/" + mm.ToString() + "/" + aa.ToString() + " A " +
                ddd.ToString() + "/" + mmm.ToString() + "/" + aaa.ToString();
            divtitulo.Attributes["class"] = "alert alert-sucess";
        }


        #region PULSAR BOTÓN ACTUALIZAR LISTA APLICANDO FILTRO
        protected void actualizar_Click(object sender, EventArgs e)
        {
            int totalMaterial = 0;
            if (CheckMEVA0001.Checked == true) totalMaterial++;
            if (CheckMEVA0002.Checked == true) totalMaterial++;
            if (CheckMEVA0010.Checked == true) totalMaterial++;
            if (CheckMEVA0011.Checked == true) totalMaterial++;
            if (CheckMEVA0013.Checked == true) totalMaterial++;
            if (CheckMESA0004.Checked == true) totalMaterial++;
            if (CheckMESA0006.Checked == true) totalMaterial++;

            string[] tecnicos = new string[totalMaterial];
            int i = 0;
            if (CheckMEVA0001.Checked == true) 
            {
                tecnicos[i] = "MEVA0001";
                i++;
            }
            if (CheckMEVA0002.Checked == true)
            {
                tecnicos[i] = "MEVA0002";
                i++;
            }
            if (CheckMEVA0010.Checked == true)
            {
                tecnicos[i] = "MEVA0010";
                i++;
            }
            if (CheckMEVA0011.Checked == true)
            {
                tecnicos[i] = "MEVA0011";
                i++;
            }
            if (CheckMEVA0013.Checked == true)
            {
                tecnicos[i] = "MEVA0013";
                i++;
            }
            if (CheckMESA0004.Checked == true)
            {
                tecnicos[i] = "MESA0004";
                i++;
            }
            if (CheckMESA0006.Checked == true)
            {
                tecnicos[i] = "MESA0006";
                i++;
            }

            procesosOrdenTaller proceso = new procesosOrdenTaller();
            //List<clordentaller> lista = new List<clordentaller>();

            if ((!inicio.Equals("") && !fin.Equals("")) &&
                (Convert.ToInt32(inicio) <= Convert.ToInt32(fin)) &&
                (totalMaterial > 0))
            {
                try
                {                  
                    DataTable dt = new DataTable();
                    dt.Columns.Add("TECNICO");
                    dt.Columns.Add("MANO DE OBRA");
                    dt.Columns.Add("REPUESTO");
                    dt.Columns.Add("DESPLAZAMIENTO");
                    dt.Columns.Add("OFERTA");
                    foreach (string tecnico in tecnicos)
                    {
                        float oferta = proceso.obtenerTotales(Convert.ToInt32(fechainicio.Value),
                                                     Convert.ToInt32(fechafin.Value),
                                                     estado.SelectedItem.ToString(), "OFERTA",
                                                     nombreProyecto.SelectedItem.ToString(), tecnico);
                        float repuesto = proceso.obtenerTotales(Convert.ToInt32(fechainicio.Value),
                                                                 Convert.ToInt32(fechafin.Value),
                                                                 estado.SelectedItem.ToString(), "REPUESTO",
                                                                 nombreProyecto.SelectedItem.ToString(), tecnico);
                        float desplazamiento = proceso.obtenerTotales(Convert.ToInt32(fechainicio.Value),
                                                                 Convert.ToInt32(fechafin.Value),
                                                                 estado.SelectedItem.ToString(), "DESPLAZAMIENTO",
                                                                 nombreProyecto.SelectedItem.ToString(), tecnico);
                        float mo = proceso.obtenerTotales(Convert.ToInt32(fechainicio.Value),
                                                                 Convert.ToInt32(fechafin.Value),
                                                                 estado.SelectedItem.ToString(), "MANO DE OBRA",
                                                                 nombreProyecto.SelectedItem.ToString(), tecnico);
                        DataRow dr = dt.NewRow();
                        dr[0] = tecnico;
                        dr[1] = mo;
                        dr[2] = repuesto;
                        dr[3] = desplazamiento;
                        dr[4] = oferta;
                        dt.Rows.Add(dr);
                    }
                    tablaRegistros.Visible = true;
                    tablaRegistros.DataSource = dt;
                    tablaRegistros.DataBind();
                }
                catch (Exception ex)
                {
                    // las excepciones al status y cual es el error
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }

            if (tecnicos.Length <= 0)
            {
                status.Text = "No se encontraron registros";
                statusdiv.Attributes["class"] = "alert alert-sucess";
            }
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        #region IMPRIMIR ORDEN DE TALLER EN DETALLE
        void imprimirDocumento(clordentaller i)//clordentaller registro)
        {
            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));

            // Le colocamos el título y el autor
            // **Nota: Esto no será visible en el documento
            doc.AddTitle("FACTURACION");
            doc.AddCreator("Fco Guillén");
            // Abrimos el archivo
            doc.Open();

            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph("ID " + i.Id));
            doc.Add(new Paragraph("TECNICO " + i.Tecnico + " " + i.Cod_Tecnico));
            doc.Add(new Paragraph("PROYECTO " + i.Proyecto + " " + i.Cod_proyecto));
            doc.Add(new Paragraph("CLIENTE " + i.Cliente + " " + i.Pds + " " + i.Provincia));
            doc.Add(new Paragraph("ODS " + i.Ods));
            doc.Add(new Paragraph("ACTIVIDAD " + i.Actividad + " " + i.Tipo));
            doc.Add(new Paragraph("ESTADO " + i.Estado + " " + i.Resultado));
            doc.Add(new Paragraph("FECHA " + i.F_trabajo + " " + i.F_repa));
            doc.Add(new Paragraph("EQUIPO " + i.Equipo));
            doc.Add(new Paragraph("MATERIAL " + i.Material + " " + i.Codmaterial));
            doc.Add(new Paragraph("NS " + i.Ax + " " + i.Sap));
            doc.Add(new Paragraph("ORDEN " + i.Orden + " " + i.Pedido));
            doc.Add(new Paragraph("OFERTA " + i.Oferta));
            doc.Add(new Paragraph("REPUESTO " + i.Repuestos + " DESPLAZAMIENTO " + i.Despl + " MO " + i.Mo));
            doc.Add(new Paragraph("DESCRIPCION " + i.Descripcion));
            doc.Add(new Paragraph("FICHERO " + i.Fichero));
            doc.Close();
            writer.Close();

            //string pdfPath = Path.Combine(Application.StartupPath, "archivo.pdf");
            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        }
        #endregion
        #endregion


        #region IMPRIMIR LISTA
        protected void imprimir_Click(object sender, EventArgs e)
        {
            int totalMaterial = 0;
            if (CheckMEVA0001.Checked == true) totalMaterial++;
            if (CheckMEVA0002.Checked == true) totalMaterial++;
            if (CheckMEVA0010.Checked == true) totalMaterial++;
            if (CheckMEVA0011.Checked == true) totalMaterial++;
            if (CheckMEVA0013.Checked == true) totalMaterial++;
            if (CheckMESA0004.Checked == true) totalMaterial++;
            if (CheckMESA0006.Checked == true) totalMaterial++;

            string[] tecnicos = new string[totalMaterial];
            int i = 0;
            if (CheckMEVA0001.Checked == true)
            {
                tecnicos[i] = "MEVA0001";
                i++;
            }
            if (CheckMEVA0002.Checked == true)
            {
                tecnicos[i] = "MEVA0002";
                i++;
            }
            if (CheckMEVA0010.Checked == true)
            {
                tecnicos[i] = "MEVA0010";
                i++;
            }
            if (CheckMEVA0011.Checked == true)
            {
                tecnicos[i] = "MEVA0011";
                i++;
            }
            if (CheckMEVA0013.Checked == true)
            {
                tecnicos[i] = "MEVA0013";
                i++;
            }
            if (CheckMESA0004.Checked == true)
            {
                tecnicos[i] = "MESA0004";
                i++;
            }
            if (CheckMESA0006.Checked == true)
            {
                tecnicos[i] = "MESA0006";
                i++;
            } 

            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
            doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            doc.Open();
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá el nombre, apellido y país
            // de nuestros visitante.
            //5, 5, 5, 10, 4, 4, 4, 59
            float[] columnWidths = { 15, 15, 15, 15, 15, 25 };
            PdfPTable tblPrueba = new PdfPTable(columnWidths);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 0;
            tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("OFERTA");
            tblPrueba.AddCell("REPUES");
            tblPrueba.AddCell("DESPLA");
            tblPrueba.AddCell("M.OBRA");

            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("------------");
            tblPrueba.AddCell("------------");
            tblPrueba.AddCell("------------");
            tblPrueba.AddCell("------------");

            procesosOrdenTaller proceso = new procesosOrdenTaller();

            if ((!inicio.Equals("") && !fin.Equals("")) &&
                (Convert.ToInt32(inicio) <= Convert.ToInt32(fin)) &&
                (totalMaterial > 0))
            {
                foreach (string tecnico in tecnicos)
                {
                    float oferta = proceso.obtenerTotales(Convert.ToInt32(fechainicio.Value),
                                                     Convert.ToInt32(fechafin.Value),
                                                     estado.SelectedItem.ToString(), "OFERTA",
                                                     nombreProyecto.SelectedItem.ToString(), tecnico);
                    float repuesto = proceso.obtenerTotales(Convert.ToInt32(fechainicio.Value),
                                                             Convert.ToInt32(fechafin.Value),
                                                             estado.SelectedItem.ToString(), "REPUESTO",
                                                             nombreProyecto.SelectedItem.ToString(), tecnico);
                    float desplazamiento = proceso.obtenerTotales(Convert.ToInt32(fechainicio.Value),
                                                             Convert.ToInt32(fechafin.Value),
                                                             estado.SelectedItem.ToString(), "DESPLAZAMIENTO",
                                                             nombreProyecto.SelectedItem.ToString(), tecnico);
                    float mo = proceso.obtenerTotales(Convert.ToInt32(fechainicio.Value),
                                                             Convert.ToInt32(fechafin.Value),
                                                             estado.SelectedItem.ToString(), "MANO DE OBRA",
                                                             nombreProyecto.SelectedItem.ToString(), tecnico);
                    tblPrueba.AddCell(tecnico);
                    tblPrueba.AddCell("");
                    tblPrueba.AddCell(oferta.ToString());
                    tblPrueba.AddCell(repuesto.ToString());
                    tblPrueba.AddCell(desplazamiento.ToString());
                    tblPrueba.AddCell(mo.ToString());                  
                }
            }
            doc.Add(tblPrueba);
            doc.Close();
            writer.Close();

            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        }
        #endregion

        protected void imprimirimporte_Click(object sender, EventArgs e)
        {
        }
    }
}