﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="repuestosusados.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Informes.Repuestos.repuestosusados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">
    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">GRAFICO DE REPUSTOS USADOS</h3></div>
        <div class="panel-body">     
            
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO 1</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Label class="caja-pq" runat="server">MES</asp:Label>
                                    <asp:dropdownlist runat="server" id="mes" class="caja-md"> 
                                        <asp:listitem text="01-31 ENERO" value="1"></asp:listitem>
                                        <asp:listitem text="02-28 FEBRERO" value="2"></asp:listitem>
                                        <asp:listitem text="03-31 MARZO" value="3"></asp:listitem>
                                        <asp:listitem text="04-30 ABRIL" value="4"></asp:listitem>
                                        <asp:listitem text="05-31 MAYO" value="5"></asp:listitem>
                                        <asp:listitem text="06-30 JUNIO" value="6"></asp:listitem>
                                        <asp:listitem text="07-31 JULIO" value="7"></asp:listitem>
                                        <asp:listitem text="08-31 AGOSTO" value="8"></asp:listitem>
                                        <asp:listitem text="09-30 SEPTIEMBRE" value="9"></asp:listitem>
                                        <asp:listitem text="10-31 OCTUBRE" value="10"></asp:listitem>
                                        <asp:listitem text="11-30 NOVIEMBRE" value="11"></asp:listitem>
                                        <asp:listitem text="12-31 DICIEMBRE" value="12"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-3">
                                    <asp:Label class="caja-pq" runat="server">AÑO</asp:Label>
                                    <asp:dropdownlist runat="server" id="año" class="caja-md"> 
                                        <asp:listitem text="2020" value="2020"></asp:listitem>
                                        <asp:listitem text="2021" value="2021"></asp:listitem>
                                        <asp:listitem text="2022" value="2022"></asp:listitem>
                                        <asp:listitem text="2023" value="2023"></asp:listitem>
                                        <asp:listitem text="2024" value="2024"></asp:listitem>
                                        <asp:listitem text="2025" value="2025"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-6">
                                    <asp:Label class="caja-pq" runat="server">SELECCIONA MES/AÑO</asp:Label>
                                    <asp:CheckBox ID="check" runat="server" OnCheckedChanged="check_CheckedChanged" autopostback="true"/>
                                </div>
                            </div>
                            <div class="row"><hr /></div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <asp:Label class="caja-pq" runat="server">Fecha Inicio</asp:Label>
                                    <input type="text" id="fechainicio" class="caja-pq" placeholder="Inicio" runat="server">
                                </div>
                                <div class="col-xs-2">
                                    <asp:Label class="caja-pq" runat="server">Fecha Fin</asp:Label>
                                    <input type="text" id="fechafin" class="caja-pq" placeholder="Fin" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label class="caja-pq" runat="server">Tecnico</asp:Label>
                                    <input type="text" id="tecnico" class="caja-pq" placeholder="MEVA0010" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="actualizar" runat="server" Text="APLICAR FILTRO" class="btn btn-success btn-lg btn-block" 
                                        OnClick="actualizar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO 2</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Button ID="seleccionar" runat="server" Text="SELECCONAR TODO" class="btn btn-info btn-lg btn-block" 
                                        OnClick="seleccionar_Click" />
                                </div>
                                <div class="col-xs-5">
                                    <asp:Button ID="deseleccionar" runat="server" Text="LIMPIAR TODO" class="btn btn-info btn-lg btn-block" 
                                        OnClick="deseleccionar_Click" />
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="imprimir" runat="server" Text="IMPRIMIR" class="btn btn-success btn-lg btn-block" 
                                        OnClick="imprimir_Click" />
                                </div>
                            </div><br /><br />
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:Label ID="Bomba" class="caja-md" runat="server">BOMBA ABRAZADERA</asp:Label>
                                    <asp:CheckBox ID="CheckBomba" runat="server"/>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Vibratoria" class="caja-md" runat="server">BOMBA VIBRATORIA</asp:Label>
                                    <asp:CheckBox ID="CheckVibratoria" runat="server"/>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Botonera" class="caja-md" runat="server">BOTONERA</asp:Label>
                                    <asp:CheckBox ID="CheckBotonera" runat="server"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:Label ID="Centralita" class="caja-md" runat="server">CENTRALITA</asp:Label>
                                    <asp:CheckBox ID="CheckCentralita" runat="server"/>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Electrovalvula" class="caja-md" runat="server">ELECTROVALVULA</asp:Label>
                                    <asp:CheckBox ID="CheckElectrovalvula" runat="server"/>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Grifovapor" class="caja-md" runat="server">GRIFO VAPOR</asp:Label>
                                    <asp:CheckBox ID="CheckGrifovapor" runat="server"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:Label ID="Grifoinfusion" class="caja-md" runat="server">GRIFO INFUSION</asp:Label>
                                    <asp:CheckBox ID="CheckGrifoinfusion" runat="server"/>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Autoexpulsorgrupo" class="caja-md" runat="server">AUTOEXPULSOR GRUPO</asp:Label>
                                    <asp:CheckBox ID="CheckAutoexpulsorgrupo" runat="server"/>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Autoexpulsorkit" class="caja-md" runat="server">AUTOEXPULSOR KIT</asp:Label>
                                    <asp:CheckBox ID="CheckAutoexpulsorkit" runat="server"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:Label ID="Lanza" class="caja-md" runat="server">LANZA VAPOR</asp:Label>
                                    <asp:CheckBox ID="CheckLanza" runat="server"/>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Presostato" class="caja-md" runat="server">PRESOSTATO</asp:Label>
                                    <asp:CheckBox ID="CheckPresostato" runat="server"/>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Sonda" class="caja-md" runat="server">SONDA NIVEL/TEMPERATURA</asp:Label>
                                    <asp:CheckBox ID="CheckSonda" runat="server"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:Label ID="Tapavolumetrico" class="caja-md" runat="server">TAPA VOLUMETRICO</asp:Label>
                                    <asp:CheckBox ID="CheckTapavolumetrico" runat="server"/>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Termostato" class="caja-md" runat="server">TERMOSTATO</asp:Label>
                                    <asp:CheckBox ID="CheckTermostato" runat="server"/>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Valvula" class="caja-md" runat="server">VALVULA SEGURIDAD/VACIO/RETENCION</asp:Label>
                                    <asp:CheckBox ID="CheckValvula" runat="server"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:Label ID="Volumetrico" class="caja-md" runat="server">VOLUMETRICO</asp:Label>
                                    <asp:CheckBox ID="CheckVolumetrico" runat="server"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="divrepuestos_tecnico" runat="server">
        <asp:Literal runat="server" ID="repuestos_tecnico"></asp:Literal>
</div>
<div id="divtitulo" runat="server">
        <h3><asp:Literal runat="server" ID="titulo"></asp:Literal></h3>
</div>   
       
    <asp:Chart ID="Chart1" runat="server">
        <Series>
            <asp:Series Name="Serie" ChartType="StackedBar" MarkerSize="1"></asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea"></asp:ChartArea>
        </ChartAreas>
    </asp:Chart>

</asp:Content>
