﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Informes.Repuestos
{
    public partial class listarepuestosusados : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            int fin = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
            int inicio = fin - (fin % 100) + 1;
            fechainicio.Value = inicio.ToString();
            fechafin.Value = fin.ToString();
            }
        }
        #endregion


        #region PULSAR BOTÓN ACTUALIZAR LISTA APLICANDO FILTRO
        protected void actualizar_Click(object sender, EventArgs e)
        {
            procesosOrdenTaller proceso = new procesosOrdenTaller();
            List<clordentaller> lista = new List<clordentaller>();
            try
            {
                lista = proceso.obtenerListaRegistroPorRepuestos(Convert.ToInt32(fechainicio.Value),
                                                     Convert.ToInt32(fechafin.Value),
                                                     repuesto.Value,
                                                     nombreProyecto.SelectedItem.ToString());
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Proyecto");
                dt.Columns.Add("Cliente");
                dt.Columns.Add("Actividad");
                dt.Columns.Add("Ods");
                dt.Columns.Add("Estado");
                dt.Columns.Add("Resultado");
                dt.Columns.Add("Orden");
                dt.Columns.Add("Oferta");
                foreach (clordentaller registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    if (registro.Proyecto.Length > 8)
                    {
                        dr[1] = registro.Proyecto.Remove(8);
                    }
                    else dr[1] = registro.Proyecto;
                    if (registro.Cliente.Length > 25)
                    {
                        dr[2] = registro.Cliente.Remove(25);
                    }
                    else dr[2] = registro.Cliente;
                    dr[3] = registro.Actividad;
                    dr[4] = registro.Ods;
                    dr[5] = registro.Estado;
                    dr[6] = registro.Resultado;
                    dr[7] = registro.Orden;
                    dr[8] = registro.Oferta;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }

            if (lista.Count <= 0)
            {
                status.Text = "No se encontraron registros";
                statusdiv.Attributes["class"] = "alert alert-sucess";
            }
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosOrdenTaller proceso = new procesosOrdenTaller();
            clordentaller registro = new clordentaller();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            registro = proceso.obtenerRegistroId(idProducto);

            string startFolder = Properties.Resource1.RutaListaOdsTaller2;
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(startFolder);
            string nombreFichero = "*" + registro.Ods + "*" + registro.Pds + "*" +
                registro.Ax + "*";
            //registro.Ax.Replace("/", "*").Replace("-", "*").Replace(".", "*") + " * ";

            IEnumerable <System.IO.FileInfo> fileList = dir.GetFiles(nombreFichero  + "*.*", System.IO.SearchOption.AllDirectories);

            IEnumerable<System.IO.FileInfo> fileQuery =
                from file in fileList
                where file.Extension == ".pdf"                 
                orderby file.Name
                select file;
           
            if (idProducto != -111 && fileQuery != null)
            {              
                try
                {
                    var newestFile =
               (from file in fileQuery
                orderby file.CreationTime
                select new { file.FullName, file.CreationTime })
               .Last();
                    switch (e.CommandName)
                    {
                        case "ODS_Click":
                            //Response.Redirect("~/WebPages/Public/DetalleProductos.aspx?id_producto=" + idProducto);
                            if (idProducto != -111 && fileQuery != null)
                            {
                                string pdfPath = newestFile.FullName;
                                Process.Start(pdfPath);
                            }
                            else
                            {
                                status.Text = "ARCHIVO NO ENCONTRADO";
                                statusdiv.Attributes["class"] = "alert alert-danger";
                            }
                            break;

                        case "Detalle_Click":
                            //Response.Redirect("~/WebPages/Private/ModificarProductos.aspx?id_producto=" + idProducto);
                            imprimirDocumento(registro);
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                }
                catch (Exception ex)
                {
                    // recoge errores hacia el status text
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }

        #region IMPRIMIR ORDEN DE TALLER EN DETALLE
        void imprimirDocumento(clordentaller i)//clordentaller registro)
        {
            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));

            // Le colocamos el título y el autor
            // **Nota: Esto no será visible en el documento
            doc.AddTitle("ORDEN DE TALLER");
            doc.AddCreator("Fco Guillén");
            // Abrimos el archivo
            doc.Open();

            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph("ID " + i.Id));
            doc.Add(new Paragraph("TECNICO " + i.Tecnico + " " + i.Cod_Tecnico));
            doc.Add(new Paragraph("PROYECTO " + i.Proyecto + " " + i.Cod_proyecto));
            doc.Add(new Paragraph("CLIENTE " + i.Cliente + " " + i.Pds + " " + i.Provincia));
            doc.Add(new Paragraph("ODS " + i.Ods));
            doc.Add(new Paragraph("ACTIVIDAD " + i.Actividad + " " + i.Tipo));
            doc.Add(new Paragraph("ESTADO " + i.Estado + " " + i.Resultado));
            doc.Add(new Paragraph("FECHA " + i.F_trabajo + " " + i.F_repa));
            doc.Add(new Paragraph("EQUIPO " + i.Equipo));
            doc.Add(new Paragraph("MATERIAL " + i.Material + " " + i.Codmaterial));
            doc.Add(new Paragraph("NS " + i.Ax + " " + i.Sap));
            doc.Add(new Paragraph("ORDEN " + i.Orden + " " + i.Pedido));
            doc.Add(new Paragraph("OFERTA " + i.Oferta));
            doc.Add(new Paragraph("REPUESTO " + i.Repuestos + " DESPLAZAMIENTO " + i.Despl + " MO " + i.Mo));
            doc.Add(new Paragraph("DESCRIPCION " + i.Descripcion));
            doc.Add(new Paragraph("FICHERO " + i.Fichero));
            doc.Close();
            writer.Close();

            //string pdfPath = Path.Combine(Application.StartupPath, "archivo.pdf");
            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        }
        #endregion
        #endregion


        #region IMPRIMIR LISTA
        protected void imprimir_Click(object sender, EventArgs e)
        {
            int actividades = 0;
            float auxiliar = 0;

            List<clordentaller> lista = new List<clordentaller>();
            procesosOrdenTaller proceso = new procesosOrdenTaller();
            lista = proceso.obtenerListaRegistroPorRepuestos(Convert.ToInt32(fechainicio.Value),
                                                     Convert.ToInt32(fechafin.Value),
                                                     repuesto.Value,
                                                     nombreProyecto.SelectedItem.ToString());

            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
            doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            doc.Open();
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá el nombre, apellido y país
            // de nuestros visitante.
            //5, 5, 5, 10, 4, 4, 4, 59
            float[] columnWidths = { 4, 20, 9, 8, 11, 7, 7, 7, 7, 20 };
            PdfPTable tblPrueba = new PdfPTable(columnWidths);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 0;
            tblPrueba.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

            tblPrueba.AddCell("NUM");
            //tblPrueba.AddCell("PROYECTO");
            tblPrueba.AddCell("TRABAJO");
            tblPrueba.AddCell("CLIENTE");
            tblPrueba.AddCell("FECHA");
            tblPrueba.AddCell("ORDEN");
            tblPrueba.AddCell("OFERTA");
            tblPrueba.AddCell("REPUES");
            tblPrueba.AddCell("DESPLA");
            tblPrueba.AddCell("M.OBRA");
            tblPrueba.AddCell("NOMBRE");

            int i = 1;
            int aux = 0;
            foreach (clordentaller o in lista)
            {
                if ((o.Oferta > 0 && !o.Estado.Equals("ANULADO")) || o.Cod_proyecto.Equals("007"))
                {
                    auxiliar = o.Oferta + o.Despl + o.Mo;
                    if (auxiliar > 0)
                        actividades++;
                    tblPrueba.AddCell(i++.ToString());
                    //tblPrueba.AddCell(o.Proyecto);
                    tblPrueba.AddCell(o.Actividad);
                    tblPrueba.AddCell(o.Proyecto);
                    tblPrueba.AddCell(o.F_trabajo.ToString());
                    if (o.Orden.Equals(""))
                        tblPrueba.AddCell("SIN RECAMBIO");
                    else
                        tblPrueba.AddCell(o.Orden);
                    tblPrueba.AddCell(String.Format("{0:000.00}", Math.Round(o.Oferta, 2)).ToString());
                    if ((o.Oferta - o.Despl - o.Mo) <= 0)
                        tblPrueba.AddCell("-----");
                    else
                        tblPrueba.AddCell(String.Format("{0:00.00}", Math.Round((o.Oferta - o.Despl - o.Mo), 2)).ToString());
                    if (o.Despl == 0)
                        tblPrueba.AddCell("-----");
                    else
                        tblPrueba.AddCell(String.Format("{0:00.00}", Math.Round(o.Despl, 2)).ToString());
                    tblPrueba.AddCell(String.Format("{0:00.00}", Math.Round(o.Mo, 2)).ToString());
                    if (o.Cliente.Length > 18)
                        tblPrueba.AddCell(o.Cliente.Substring(0, 18));
                    else
                        tblPrueba.AddCell(o.Cliente);
                    aux = o.F_trabajo;
                }
            }

            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");

            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");

            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("OFERTA");
            tblPrueba.AddCell("REPUES");
            tblPrueba.AddCell("DESPLA");
            tblPrueba.AddCell("M.OBRA");
            tblPrueba.AddCell("");

            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("---");
            tblPrueba.AddCell("");

            tblPrueba.AddCell("");
            tblPrueba.AddCell("TOTALES");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            tblPrueba.AddCell("");
            doc.Add(tblPrueba);

            doc.Close();
            writer.Close();

            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        } 
        #endregion
    }
}