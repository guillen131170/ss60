﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Informes.Pedidos
{
    public partial class repuestospedidos : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }

        public int mostrarLista()
        {
            procesosRepuestos proceso = new procesosRepuestos();
            procesosRepuestos proceso2 = new procesosRepuestos();
            List<clrepuestospedido> lista = new List<clrepuestospedido>();
            try
            {
                lista = proceso.obtenerPedidos();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Pedido");
                dt.Columns.Add("Precio");
                dt.Columns.Add("Portes");
                dt.Columns.Add("IVA");
                dt.Columns.Add("Importe");
                dt.Columns.Add("Fecha");
                dt.Columns.Add("Proveedor");
                //dt.Columns.Add("ODSEntrada");
                foreach (clrepuestospedido registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Pedido;
                    float importe = proceso2.obtenerImporte(registro.Pedido);
                    dr[2] = importe;
                    if (importe < 150)
                    {
                        if (registro.Fecha > 20220000)
                        {
                            dr[3] = 9;
                            importe += 9;
                        }
                        else
                        {
                            dr[3] = 7;
                            importe += 7;
                        }
                    }
                    else
                    {
                        dr[3] = 0;
                    }
                    dr[4] = (importe * 21) / 100;
                    dr[5] = importe + ((importe * 21)/100);
                    dr[6] = registro.Fecha;
                    dr[7] = registro.Proveedor;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosRepuestos proceso = new procesosRepuestos();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            clrepuestospedido registro = new clrepuestospedido();
            registro = proceso.obtenerRegistroPorId(idProducto);


            string startFolder = Properties.Resource1.RutaListaPedidos2;
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(startFolder);
            string nombreFichero = "*" + registro.Pedido + "*";
            IEnumerable<System.IO.FileInfo> fileList = dir.GetFiles(nombreFichero + "*.*", System.IO.SearchOption.AllDirectories);
            IEnumerable<System.IO.FileInfo> fileQuery =
                from file in fileList
                where file.Extension == ".pdf"
                orderby file.Name
                select file;

            if (idProducto != -111 && fileQuery != null)
            {
                try
                {
                    var newestFile =
                    (from file in fileQuery
                     orderby file.CreationTime
                     select new { file.FullName, file.CreationTime })
                    .Last();

                    switch (e.CommandName)
                    {
                        case "Ver_Click":
                            if (fileQuery.Count() > 0)
                            {
                                string pdfPath = newestFile.FullName;
                                Process.Start(pdfPath);
                            }
                            else
                            {
                                status.Text = "ARCHIVO NO ENCONTRADO";
                                statusdiv.Attributes["class"] = "alert alert-danger";
                            }
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch (Exception ex)
                {
                    // recoge errores hacia el status text
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion


        #region SACAR INFORME PDF
        public void informe(List<clrepuestospedido> lista, string pedido, string fecha, string proveedor)
        {
            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\" + pedido + ".pdf", FileMode.Create));
            //doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            doc.Open();
            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph("PEDIDO " + pedido + " " + fecha + " " + proveedor));
            doc.Add(new Paragraph("----------------------------------------------------------------------"));
            doc.Add(Chunk.NEWLINE);
            foreach (clrepuestospedido o in lista)
            {
                doc.Add(new Paragraph(o.Referencia + " " + o.Nombre));
                doc.Add(new Paragraph(o.Precio + " euros - " + o.Unidades + " uds. - " + o.Familia));
                doc.Add(Chunk.NEWLINE);
            }
            doc.Close();
            writer.Close();

            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        }
        #endregion


        protected void tablaRegistros_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}