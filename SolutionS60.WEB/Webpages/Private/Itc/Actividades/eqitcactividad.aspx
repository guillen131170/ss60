﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="eqitcactividad.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Itc.Actividades.eqitcactividad" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">

    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">INTERCAMBIADORES - CREAR NUEVA ACTIVIDAD DE EQUIPO ITC</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">REGISTRO</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" id="ns" class="caja-gr" placeholder="N SERIE" runat="server">
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" id="servicio" class="caja-gr" placeholder="SERVICIO" runat="server">
                                </div>
                            </div><br />

                            <div class="row">
                                <div class="col-xs-12">
                                    <textarea  id="problema" class="caja-gr" rows="3" style="resize:none" runat="server">
                                    </textarea>
                                </div>
                            </div><br />

                            <div class="row">
                                <div class="col-xs-12">
                                    <textarea  id="solucion" class="caja-gr" rows="3" style="resize:none" runat="server">
                                    </textarea>
                                </div>
                            </div><br />

                            <div class="row">
                                <div class="col-xs-5">
                                    <input type="text" id="autor" class="caja-gr" placeholder="AUTOR" runat="server">
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" id="fecha" class="caja-pq" placeholder="FECHA" runat="server">
                                </div>
                            </div><br />

                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-6">
                        <asp:Button ID="altaregistro" runat="server" Text="GUARDAR REGISTRO" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="altaregistro_Click" />
                    </div>
                    <div class="col-xs-2"></div>
                </div>
            </div>
            </div>
      </div>
</div>

</asp:Content>
