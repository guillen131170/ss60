﻿using SolutionS60.APPLICATION.ITC;
using SolutionS60.CORE.ITC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Itc.Actividades
{
    public partial class eqitcactividad : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        #region INSERTAR REGISTRO
        protected void altaregistro_Click(object sender, EventArgs e)
        {
            if (!erroresFormulario())
            {
                cleqItcActividad item = new cleqItcActividad();
                string nserie = ns.Value.TrimStart(' ').TrimEnd(' ');
                item.Nserie = nserie;
                if (servicio.Value.Equals("")) item.Servicio = "";
                else item.Servicio = servicio.Value.TrimStart(' ').TrimEnd(' ');
                if (problema.Equals("")) item.Problema = "";
                else item.Problema = problema.Value.TrimStart(' ').TrimEnd(' ');
                if (solucion.Equals("")) item.Solucion = "";
                else item.Solucion = solucion.Value.TrimStart(' ').TrimEnd(' ');
                item.Fecha = Convert.ToInt32(fecha.Value.TrimStart(' ').TrimEnd(' '));
                item.Autor = autor.Value.TrimStart(' ').TrimEnd(' ');

                procesosEqItc proceso = new procesosEqItc();
                if (!proceso.InsertaRegistro(item))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado";
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosEqItc consulta = new procesosEqItc();

            string nserie = ns.Value.TrimStart(' ').TrimEnd(' ');
            if (consulta.comprobarDuplicados(nserie))
            { error = true; }
            if (ns.Value.Equals(""))
            { error = true; }
            if (servicio.Value.Equals(""))
            { error = true; }
            if (problema.Value.Equals(""))
            { error = true; }
            if (solucion.Value.Equals(""))
            { error = true; }
            if (fecha.Value.Equals(""))
            { error = true; }
            if (autor.Value.Equals(""))
            { error = true; }

            return error;
        }


        #endregion
    }
}