﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="eqitcestado.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Itc.Estados.eqitcestado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">

    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">INTERCAMBIADORES - CREAR NUEVA UBICACION PARA EQUIPO ITC</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">REGISTRO</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-4">
                                    <input type="text" id="ns" class="caja-gr" placeholder="NUMERO SERIE" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" id="fecha" class="caja-pq" placeholder="FECHA" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" id="poblacion" class="caja-gr" placeholder="POBLACION" runat="server">
                                </div>
                            </div><br />

                            <div class="row">
                                <div class="col-xs-4">
                                    <input  type="text" id="nompds" class="caja-gr"  placeholder="PDS" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" id="codpds" class="caja-pq" placeholder="CODIGO PDS" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <input  type="text" id="direccionpds" class="caja-gr"  placeholder="DIRECCION PDS" runat="server">
                                </div>
                            </div><br />

                            <div class="row">
                                <div class="col-xs-4"></div>                               
                                <div class="col-xs-4">
                                    <input  type="text" id="contactopds" class="caja-gr"  placeholder="CONTACTO PDS" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" id="telefonopds" class="caja-pq" placeholder="TLFNO PDS" runat="server">
                                </div>
                            </div><br />

                            <div class="row">
                                <div class="col-xs-4">
                                    <input  type="text" id="stc" class="caja-gr"  placeholder="STC" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <input  type="text" id="contactostc" class="caja-gr"  placeholder="CONTACTO STC" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" id="telefonostc" class="caja-pq" placeholder="TLFNO STC" runat="server">
                                </div>
                            </div><br />

                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-6">
                        <asp:Button ID="altaregistro" runat="server" Text="GUARDAR REGISTRO" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="altaregistro_Click" />
                    </div>
                    <div class="col-xs-2"></div>
                </div>
            </div>
            </div>
      </div>
</div>

</asp:Content>
