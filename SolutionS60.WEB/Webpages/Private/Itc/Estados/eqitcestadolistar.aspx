﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="eqitcestadolistar.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Itc.Estados.eqitcestadolistar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

    <div class="form-horizontal-inline" role="form">
    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">VER UBICACIONES DE EQUIPOS ITC</h3></div>
        <div class="panel-body">     
            
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO</h3></div>
                        <div class="panel-body">  

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


    <div class="table-responsive" style="margin-top: 30px;">      
    <asp:GridView runat="server" ID="tablaRegistros" class="table table-striped"
        DataKeyNames="Id" Visible="false" AutoGenerateColumns="true" GridLines="None" BorderWidth="0" OnRowCommand="tablaRegistros_RowCommand">
            <Columns>
                <asp:ButtonField ButtonType="Link" HeaderText="Salida" Text="sacar" CommandName="Salida_Click"  />
            </Columns>
    </asp:GridView>
</div>

</asp:Content>
