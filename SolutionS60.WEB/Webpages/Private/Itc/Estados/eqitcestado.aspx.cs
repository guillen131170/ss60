﻿using SolutionS60.APPLICATION.ITC;
using SolutionS60.CORE.ITC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Itc.Estados
{
    public partial class eqitcestado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region GUARDAR REGISTRO
        protected void altaregistro_Click(object sender, EventArgs e)
        {
            if (!erroresFormulario())
            {
                cleqItcUbicacion item = new cleqItcUbicacion();
                string nserie = ns.Value.TrimStart(' ').TrimEnd(' ');
                item.Nserie = nserie;
                item.Fecha = Convert.ToInt32(fecha.Value.TrimStart(' ').TrimEnd(' '));
                if (nompds.Value.Equals("")) item.Nompds = "";
                else item.Nompds = nompds.Value.TrimStart(' ').TrimEnd(' ');
                if (codpds.Value.Equals("")) item.Codpds = "";
                else item.Codpds = codpds.Value.TrimStart(' ').TrimEnd(' ');
                if (direccionpds.Value.Equals("")) item.DireccionPds = "";
                else item.DireccionPds = direccionpds.Value.TrimStart(' ').TrimEnd(' ');
                if (poblacion.Value.Equals("")) item.Poblacion = "";
                else item.Poblacion = poblacion.Value.TrimStart(' ').TrimEnd(' ');
                if (contactopds.Value.Equals("")) item.ContactoPds = "";
                else item.ContactoPds = contactopds.Value.TrimStart(' ').TrimEnd(' ');
                if (telefonopds.Value.Equals("")) item.TelefonoPds = "";
                else item.TelefonoPds = telefonopds.Value.TrimStart(' ').TrimEnd(' ');
                if (stc.Value.Equals("")) item.Stc = "";
                else item.Stc = stc.Value.TrimStart(' ').TrimEnd(' ');
                if (contactostc.Value.Equals("")) item.ContactoStc = "";
                else item.ContactoStc = contactostc.Value.TrimStart(' ').TrimEnd(' ');
                if (telefonostc.Value.Equals("")) item.TelefonoStc = "";
                else item.TelefonoStc = telefonostc.Value.TrimStart(' ').TrimEnd(' ');

                procesosEqItc proceso = new procesosEqItc();
                if (!proceso.InsertaRegistro(item))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado";
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        } 
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosEqItc consulta = new procesosEqItc();

            string nserie = ns.Value.TrimStart(' ').TrimEnd(' ');
            if (!consulta.comprobarDuplicados(nserie))
            { error = true; }
            if (ns.Value.Equals(""))
            { error = true; }
            if (stc.Value.Equals(""))
            { error = true; }
            if (fecha.Value.Equals(""))
            { error = true; }
            if (poblacion.Value.Equals(""))
            { error = true; }

            return error;
        }


        #endregion
    }
}