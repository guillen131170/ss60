﻿using SolutionS60.APPLICATION.ITC;
using SolutionS60.CORE.ITC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Itc.Estados
{
    public partial class eqitcestadolistar : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros elementos";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
                else
                {
                    status.Text = "OK: " + lista.ToString() + " ELEMENTOS LEIDOS";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }

        public int mostrarLista()
        {
            procesosEqItc proceso = new procesosEqItc();
            List<cleqItcUbicacion> lista = new List<cleqItcUbicacion>();
            try
            {
                lista = proceso.obtenerListaUbicacion();
                DataTable dt = new DataTable();

                dt.Columns.Add("ID");
                dt.Columns.Add("Nº Serie");
                dt.Columns.Add("PDS");
                //dt.Columns.Add("Cod.PDS");
                dt.Columns.Add("Direccion");
                //dt.Columns.Add("Tlfno");
                //dt.Columns.Add("Contacto");
                dt.Columns.Add("STC");
                dt.Columns.Add("Tlfno");
                dt.Columns.Add("Contacto");
                dt.Columns.Add("Poblacion");
                dt.Columns.Add("Fecha");
                foreach (cleqItcUbicacion registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Nserie;
                    dr[2] = registro.Nompds;
                    //dr[3] = registro.Codpds;
                    dr[3] = registro.DireccionPds;
                    //dr[5] = registro.TelefonoPds;
                    //dr[6] = registro.ContactoPds;
                    dr[4] = registro.Stc;
                    dr[5] = registro.TelefonoStc;
                    dr[6] = registro.ContactoStc;    
                    dr[7] = registro.Poblacion;
                    dr[8] = registro.Fecha;                    
                    dt.Rows.Add(dr);
                }            
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosEqItc proceso = new procesosEqItc();
            cleqItcUbicacion registro = new cleqItcUbicacion();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            //registro = proceso.obtenerRegistroId(idProducto);
            if (idProducto != -111)
            {
                try
                {

                    switch (e.CommandName)
                    {
                        case "Salida_Click":
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch (Exception ex)
                {
                    // recoge errores hacia el status text
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion
    }
}