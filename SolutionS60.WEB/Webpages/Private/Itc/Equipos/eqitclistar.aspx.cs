﻿using SolutionS60.APPLICATION.ITC;
using SolutionS60.CORE.ITC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Itc.Equipos
{
    public partial class eqitclistar : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
                else
                {
                    status.Text = "ok " + lista.ToString();
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }

        public int mostrarLista()
        {
            procesosEqItc proceso = new procesosEqItc();
            List<cleqItc> lista = new List<cleqItc>();
            try
            {
                lista = proceso.obtenerLista();
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("N.SERIE");
                dt.Columns.Add("TAG");
                dt.Columns.Add("MODELO");
                dt.Columns.Add("FECHA");
                dt.Columns.Add("ESTADO");
                foreach (cleqItc registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Nserie;
                    dr[2] = registro.Tag;
                    dr[3] = registro.DNS;
                    dr[4] = registro.Fecha;
                    dr[5] = registro.Estado;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                /*status2.Text = "funcion";
                statusdiv2.Attributes["class"] = "alert alert-sucess";*/
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosEqItc proceso = new procesosEqItc();
            cleqItc registro = new cleqItc();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            //registro = proceso.obtenerRegistroId(idProducto);
            if (idProducto != -111)
            {
                try
                {

                    switch (e.CommandName)
                    {
                        case "Salida_Click":
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch (Exception ex)
                {
                    // recoge errores hacia el status text
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion
    }
}