﻿using SolutionS60.APPLICATION.ITC;
using SolutionS60.CORE.ITC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Itc.Equipos
{
    public partial class eqitc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        #region INSERTAR REGISTRO
        protected void altaregistro_Click(object sender, EventArgs e)
        {
            if (!erroresFormulario())
            {
                cleqItc item = new cleqItc();
                string nserie = ns.Value.TrimStart(' ').TrimEnd(' ');
                item.Nserie = nserie;
                if (tag.Value.Equals("")) item.Tag = "";
                else item.Tag = tag.Value.TrimStart(' ').TrimEnd(' ');
                string ip = ns.Value.TrimStart(' ').TrimEnd(' ');
                item.IP = ip;
                if (dns.Equals("")) item.DNS = "";
                else item.DNS = dns.Value.TrimStart(' ').TrimEnd(' ');
                item.Fecha = Convert.ToInt32(fecha.Value.TrimStart(' ').TrimEnd(' '));
                item.Autor = autor.Value.TrimStart(' ').TrimEnd(' ');
                item.Estado = "ACTIVO";

                procesosEqItc proceso = new procesosEqItc();
                if (!proceso.InsertaRegistro(item))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado";
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        } 
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosEqItc consulta = new procesosEqItc();

            string nserie = ns.Value.TrimStart(' ').TrimEnd(' ');
            if (consulta.comprobarDuplicados(nserie))
            { error = true; }
            if (ns.Value.Equals(""))
            { error = true; }
            if (ip.Value.Equals(""))
            { error = true; }
            if (fecha.Value.Equals(""))
            { error = true; }
            if (autor.Value.Equals(""))
            { error = true; }

            return error;
        }


        #endregion
    }
}