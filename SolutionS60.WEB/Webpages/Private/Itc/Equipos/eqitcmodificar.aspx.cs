﻿using SolutionS60.APPLICATION.ITC;
using SolutionS60.CORE.ITC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Itc.Equipos
{
    public partial class eqitcmodificar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        #region BUSCAR ORDEN DE TALLER
        protected void buscar_Click(object sender, EventArgs e)
        {
            cleqItc registro = new cleqItc();

            if (!id.Value.Equals(""))
            {
                if (comprobarSiExisteRegistro(ns.Value))
                {
                    registro = buscarRegistro(id.Value);
                    visualizarDatos(registro);
                }
            }
        }

        public void visualizarDatos(cleqItc registro)
        {
            //nombreTecnico.SelectedIndex = registro.Tecnico;
            ns.Value = registro.Nserie;
            tag.Value = registro.Tag;
            ip.Value = registro.IP;
            dns.Value = registro.DNS;
            fecha.Value = Convert.ToString(registro.Fecha);
            autor.Value = registro.Autor;
            estado.Value = registro.Estado;
        }

        public bool comprobarSiExisteRegistro(string ns)
        {
            procesosEqItc proceso = new procesosEqItc();
            bool busqueda = false;

            try
            {
                busqueda = proceso.comprobarDuplicados(ns);
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return busqueda;
        }

        public cleqItc buscarRegistro(string ns)
        {
            procesosEqItc proceso = new procesosEqItc();
            cleqItc registro = new cleqItc();

            try
            {
                registro = proceso.obtenerLRegistro(ns);
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return registro;
        }
        #endregion


        #region GUARDAR CAMBIOS
        protected void guardar_Click(object sender, EventArgs e)
        {
            if (erroresFormulario())
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            else
            {
                cleqItc orden = new cleqItc();
                orden.Nserie = ns.Value.TrimStart(' ').TrimEnd(' ');
                orden.Tag = tag.Value.TrimStart(' ').TrimEnd(' ');
                orden.IP = ip.Value.TrimStart(' ').TrimEnd(' ');
                orden.DNS = dns.Value.TrimStart(' ').TrimEnd(' ');
                orden.Fecha = Convert.ToInt32(fecha.Value.TrimStart(' ').TrimEnd(' '));
                orden.Autor = autor.Value.TrimStart(' ').TrimEnd(' ');
                orden.Estado = estado.Value.TrimStart(' ').TrimEnd(' ');

                procesosEqItc proceso1 = new procesosEqItc();
                if (proceso1.modificarRegistro(ns.Value, orden))
                {
                    status.Text = "Registro guardado";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
                else
                {
                    status.Text = "ERROR - NO SE HAN GUARDADO LOS CAMBIOS";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }

            }
        }

        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosEqItc consulta = new procesosEqItc();

            string nserie = ns.Value.TrimStart(' ').TrimEnd(' ');
            if (consulta.comprobarDuplicados(nserie))
            { error = true; }
            if (ns.Value.Equals(""))
            { error = true; }
            if (ip.Value.Equals(""))
            { error = true; }
            if (fecha.Value.Equals(""))
            { error = true; }
            if (autor.Value.Equals(""))
            { error = true; }

            return error;
        }


        #endregion
        #endregion
    }
}