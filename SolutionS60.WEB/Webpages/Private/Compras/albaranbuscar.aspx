﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="albaranbuscar.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Compras.albaranbuscar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
            </div>
                <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO DE BÚSQUEDA</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <br /><br />
                                <div class="col-xs-4">
                                    <input type="text" id="albaran" class="caja-md" placeholder="Nº ALBARÁN" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" id="referencia" class="caja-md" placeholder="REFERENCIA" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="buscar" runat="server" Text="BUSCAR" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="buscar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">LISTA DE ALBARANES</h3></div>
        <div class="panel-body">     
            <div class="table-responsive" style="margin-top: 30px;">      
                <asp:GridView runat="server" ID="tablaRegistros" class="table table-striped" DataKeyNames="Id" Visible="false" 
                    AutoGenerateColumns="true" GridLines="None" BorderWidth="0" OnRowCommand="tablaRegistros_RowCommand">
                    <Columns>
                        <asp:ButtonField ButtonType="Link" HeaderText="Detalle" Text="ver" CommandName="Ver_Click"  />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
