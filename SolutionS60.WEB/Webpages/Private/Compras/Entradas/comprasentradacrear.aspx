﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="comprasentradacrear.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Compras.Entradas.comprasentradacrear" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">

    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">CREAR ALBARÁN DE ENTRADA DE MERCANCIA</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">REGISTRO</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" id="proveedor" class="caja-md" placeholder="Proveedor" runat="server">
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" id="fecha" class="caja-pq" placeholder="Fecha" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" id="albaran" class="caja-md" placeholder="Nº albarán" runat="server">
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" id="referencia" class="caja-md" placeholder="Referencia" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <textarea  id="detalle" class="caja-gr" rows="10" style="resize:none" runat="server">
                                    </textarea>
                                </div>
                            </div><br />
                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-6">
                        <asp:Button ID="altaregistro" runat="server" Text="GUARDAR REGISTRO" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="altaregistro_Click1" />
                    </div>
                    <div class="col-xs-2"></div>
                </div>
            </div>
            </div>
      </div>
</div>
</asp:Content>
