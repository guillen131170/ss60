﻿using SolutionS60.APPLICATION.COMPRAS;
using SolutionS60.CORE.COMPRAS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Compras
{
    public partial class comprasinforme : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        } 
        #endregion


        #region MOSTRAR RESULTADOS
        public int mostrarLista()
        {
            procesosCompras proceso = new procesosCompras();
            List<clalbaranes> lista = new List<clalbaranes>();
            try
            {
                if (clase.SelectedItem.ToString().Equals("SALIDAS")) lista = proceso.listarRepuestosS();
                else if (clase.SelectedItem.ToString().Equals("ENTRADAS")) lista = proceso.listarRepuestosE();
                else lista = proceso.listarRepuestosC();

                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Proveedor");
                dt.Columns.Add("Albarán");
                dt.Columns.Add("Referencia");
                dt.Columns.Add("Fecha");

                foreach (clalbaranes registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Proveedor;
                    dr[2] = registro.Nalbaran;
                    dr[3] = registro.Referencia;
                    dr[4] = registro.Fecha;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }

        public int mostrarListaFecha(int i, int f)
        {
            procesosCompras proceso = new procesosCompras();
            List<clalbaranes> lista = new List<clalbaranes>();
            try
            {
                if (clase.SelectedItem.ToString().Equals("SALIDAS")) lista = proceso.listarRepuestosS(i, f);
                else if (clase.SelectedItem.ToString().Equals("ENTRADAS")) lista = proceso.listarRepuestosE(i, f);
                else lista = proceso.listarRepuestosC(i, f);

                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Proveedor");
                dt.Columns.Add("Albarán");
                dt.Columns.Add("Referencia");
                dt.Columns.Add("Fecha");

                foreach (clalbaranes registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Proveedor;
                    dr[2] = registro.Nalbaran;
                    dr[3] = registro.Referencia;
                    dr[4] = registro.Fecha;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        } 
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosCompras proceso = new procesosCompras();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            clalbaranes registro = new clalbaranes();
            if (clase.SelectedItem.ToString().Equals("SALIDAS")) registro = proceso.obtenerRegistroPorIdS(idProducto);
            else if (clase.SelectedItem.ToString().Equals("ENTRADAS")) registro = proceso.obtenerRegistroPorIdE(idProducto);
            else registro = proceso.obtenerRegistroPorIdC(idProducto);

            string startFolder = Properties.Resource1.RutaListaCompras2;
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(startFolder);
            string nombreFichero = "*" + registro.Nalbaran + "*";
            IEnumerable<System.IO.FileInfo> fileList = dir.GetFiles(nombreFichero + "*.*", System.IO.SearchOption.AllDirectories);
            IEnumerable<System.IO.FileInfo> fileQuery =
                from file in fileList
                where file.Extension == ".pdf"
                orderby file.Name
                select file;

            if (idProducto != -111 && fileQuery != null)
            {
                try
                {
                    var newestFile =
                    (from file in fileQuery
                     orderby file.CreationTime
                     select new { file.FullName, file.CreationTime })
                    .Last();
                    switch (e.CommandName)
                    {
                        case "Ver_Click":
                            if (fileQuery.Count() > 0)
                            {
                                string pdfPath = newestFile.FullName;
                                Process.Start(pdfPath);
                            }
                            else
                            {
                                status.Text = "ARCHIVO NO ENCONTRADO";
                                statusdiv.Attributes["class"] = "alert alert-danger";
                            }
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch
                {
                    // recoge errores hacia el status text
                    status.Text = "ARCHIVO NO ENCONTRADO";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion


        #region ACTUALIZAR O REFRESCAR
        protected void actualizar_Click(object sender, EventArgs e)
        {
            int lista = -111;
            int inicio;
            int fin;
            if (!fechainicio.Value.Equals("") && !fechafin.Value.Equals(""))
            {
                inicio = Convert.ToInt32(fechainicio.Value);
                fin = Convert.ToInt32(fechafin.Value);
                lista = mostrarListaFecha(inicio, fin);
            }
            else lista = mostrarLista();

            if (lista <= 0)
            {
                status.Text = "No se encontraron registros";
                statusdiv.Attributes["class"] = "alert alert-sucess";
            }
        }
        #endregion


        #region VER PDF
        protected void imprimir_Click(object sender, EventArgs e)
        {

        } 
        #endregion
    }
}