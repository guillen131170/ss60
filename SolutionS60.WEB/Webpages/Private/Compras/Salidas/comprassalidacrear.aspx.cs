﻿using SolutionS60.APPLICATION.COMPRAS;
using SolutionS60.CORE.COMPRAS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Compras.Salidas
{
    public partial class comprassalidacrear : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        //H
        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosCompras consulta = new procesosCompras();

            if (proveedor.Value.Equals(""))
            { error = true; }
            if (albaran.Value.Equals("")) { error = true; }
            else 
            {
                if (consulta.comprobarDuplicadosE(albaran.Value))
                { error = true; }
            }
            if (fecha.Value.Equals(""))
            { error = true; }

            return error;
        }


        #endregion


        #region INSERTAR REPUESTO NUEVO
        protected void altaregistro_Click1(object sender, EventArgs e)
        {
            if (!erroresFormulario())
            {
                clalbaranes item = new clalbaranes();
                item.Proveedor = proveedor.Value.TrimStart(' ').TrimEnd(' ');
                item.Nalbaran = albaran.Value.TrimStart(' ').TrimEnd(' ');
                if (!referencia.Value.Equals("")) item.Referencia = referencia.Value.TrimStart(' ').TrimEnd(' ');
                else item.Referencia = "";
                if (!detalle.Value.Equals("")) item.Descripcion = detalle.Value.TrimStart(' ').TrimEnd(' ');
                else item.Descripcion = "";
                item.Fecha = Convert.ToInt32(fecha.Value.TrimStart(' ').TrimEnd(' '));

                procesosCompras proceso = new procesosCompras();
                if (!proceso.InsertaRegistroS(item))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado" + " " + proveedor.Value + " " + albaran.Value + " " + referencia.Value;
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }
        #endregion


    }
}