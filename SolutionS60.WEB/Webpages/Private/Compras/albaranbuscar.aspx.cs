﻿using SolutionS60.APPLICATION;
using SolutionS60.APPLICATION.COMPRAS;
using SolutionS60.CORE;
using SolutionS60.CORE.COMPRAS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Compras
{
    public partial class albaranbuscar : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public int mostrarListaAlbaran()
        {
            procesosCompras proceso = new procesosCompras();
            List<clalbaranes> lista = new List<clalbaranes>();
            try
            {
                lista = proceso.listarAlbaranesPorAlbaran(albaran.Value);
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Proveedor");
                dt.Columns.Add("Nalbaran");
                dt.Columns.Add("Referencia");
                dt.Columns.Add("Descripcion");
                dt.Columns.Add("Fecha");
                foreach (clalbaranes registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Proveedor;
                    dr[2] = registro.Nalbaran;
                    dr[3] = registro.Referencia;
                    dr[4] = registro.Descripcion;
                    dr[5] = registro.Fecha;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }

        public int mostrarListaReferencia()
        {
            procesosCompras proceso = new procesosCompras();
            List<clalbaranes> lista = new List<clalbaranes>();
            try
            {
                lista = proceso.listarAlbaranesPorReferencia(referencia.Value);
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Proveedor");
                dt.Columns.Add("Nalbaran");
                dt.Columns.Add("Referencia");
                dt.Columns.Add("Descripcion");
                dt.Columns.Add("Fecha");
                foreach (clalbaranes registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Proveedor;
                    dr[2] = registro.Nalbaran;
                    dr[3] = registro.Referencia;
                    dr[4] = registro.Descripcion;
                    dr[5] = registro.Fecha;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion


        #region BUSCAR EQUIPO
        protected void buscar_Click(object sender, EventArgs e)
        {
            if (!referencia.Value.Equals("") && albaran.Value.Equals(""))
            {
                mostrarListaAlbaran();
            }
            else if (referencia.Value.Equals("") && !albaran.Value.Equals(""))
            {
                mostrarListaReferencia();
            }
            else
            {
                Response.Redirect("/Webpages/Private/Compras/albaranbuscar.aspx");
            }
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosCompras proceso = new procesosCompras();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            clalbaranes registro1 = new clalbaranes();
            clalbaranes registro2 = new clalbaranes();
            clalbaranes registro3 = new clalbaranes();
            registro1 = proceso.obtenerRegistroPorIdS(idProducto);
            registro2 = proceso.obtenerRegistroPorIdE(idProducto);
            registro3 = proceso.obtenerRegistroPorIdC(idProducto);

            string startFolder = Properties.Resource1.RutaListaCompras2;
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(startFolder);
            string nombreFichero1 = "*" + registro1.Nalbaran + "*";
            string nombreFichero2 = "*" + registro2.Nalbaran + "*";
            string nombreFichero3 = "*" + registro3.Nalbaran + "*";
            IEnumerable<System.IO.FileInfo> fileList = dir.GetFiles(nombreFichero1 + "*.*", System.IO.SearchOption.AllDirectories);
            IEnumerable<System.IO.FileInfo> fileQuery =
                from file in fileList
                where file.Extension == ".pdf"
                orderby file.Name
                select file;
            if (fileQuery == null)
            {
                fileList = dir.GetFiles(nombreFichero2 + "*.*", System.IO.SearchOption.AllDirectories);
                fileQuery =
                    from file in fileList
                    where file.Extension == ".pdf"
                    orderby file.Name
                    select file;
            }
            if (fileQuery == null)
            {
                fileList = dir.GetFiles(nombreFichero3 + "*.*", System.IO.SearchOption.AllDirectories);
                fileQuery =
                    from file in fileList
                    where file.Extension == ".pdf"
                    orderby file.Name
                    select file;
            }

            if (idProducto != -111 && fileQuery != null)
            {
                try
                {
                    var newestFile =
                    (from file in fileQuery
                     orderby file.CreationTime
                     select new { file.FullName, file.CreationTime })
                    .Last();
                    switch (e.CommandName)
                    {
                        case "Ver_Click":
                            if (fileQuery.Count() > 0)
                            {
                                string pdfPath = newestFile.FullName;
                                Process.Start(pdfPath);
                            }
                            else
                            {
                                status.Text = "ARCHIVO NO ENCONTRADO";
                                statusdiv.Attributes["class"] = "alert alert-danger";
                            }
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                }
                catch
                {
                    // recoge errores hacia el status text
                    status.Text = "ARCHIVO NO ENCONTRADO";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion
    }
}