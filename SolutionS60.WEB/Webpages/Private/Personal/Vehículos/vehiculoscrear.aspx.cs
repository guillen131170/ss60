﻿using SolutionS60.APPLICATION.PERSONAL.VEHICULOS;
using SolutionS60.CORE.PERSONAL.VEHICULOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Personal.Vehículos
{
    public partial class vehiculoscrear : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            if (!itvpasada.Value.Equals("")) item.Itvpasada = Convert.ToInt32(itvpasada.Value.TrimStart(' ').TrimEnd(' '));
            else item.Itvpasada = 0;
            if (!itvproxima.Value.Equals("")) item.Itvproxima = Convert.ToInt32(itvproxima.Value.TrimStart(' ').TrimEnd(' '));
            else item.Itvproxima = 0;
            */
            //conductor.SelectedIndex = 0;
            //matricula.SelectedIndex = conductor.SelectedIndex;
            /*
            procesosVehiculos proceso2 = new procesosVehiculos();
            clvehiculos ultimoRegistro2 = new clvehiculos();
            ultimoRegistro2 =
                proceso2.obtenerUltimoRegistroMatricula(matricula.SelectedItem.ToString().TrimStart(' ').TrimEnd(' '));
            if (ultimoRegistro2 != null)
            {
                itvpasada.Value = ultimoRegistro2.Itvpasada.ToString();
                itvproxima.Value = ultimoRegistro2.Itvproxima.ToString();
            }
            */
            //fecha.Value = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")).ToString();

            if (!IsPostBack)
            {
                matricula.SelectedIndex = conductor.SelectedIndex;
                conductor.SelectedIndex = matricula.SelectedIndex;
                fecha.Value = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")).ToString();
            }
            else
            {
                procesosVehiculos proceso = new procesosVehiculos();
                clvehiculos ultimoRegistro = new clvehiculos();
                ultimoRegistro =
                    proceso.obtenerUltimoRegistroMatricula(matricula.SelectedItem.ToString().TrimStart(' ').TrimEnd(' '));
                if (ultimoRegistro != null)
                {
                    itvpasada.Value = ultimoRegistro.Itvpasada.ToString();
                    itvproxima.Value = ultimoRegistro.Itvproxima.ToString();
                }
            }
        }

        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            if (itvpasada.Value.Equals("")) { error = true; }
            if (itvproxima.Value.Equals("")) { error = true; }
            if (modelo.Value.Equals("")) { error = true; }
            if (detalle.Value.Equals("")) { error = true; }
            if (fecha.Value.Equals("")) { error = true; }

            return error;
        }
        #endregion

        protected void altaregistro_Click(object sender, EventArgs e)
        {
            if (!erroresFormulario())
            {
                clvehiculos item = new clvehiculos();

                item.Conductor = conductor.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                item.Matricula = matricula.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                item.Tipo = tipo.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                item.Modelo = modelo.ToString().TrimStart(' ').TrimEnd(' ');
                item.Fecha = Convert.ToInt32(fecha.Value.TrimStart(' ').TrimEnd(' '));
                item.Detalle = detalle.Value.TrimStart(' ').TrimEnd(' ');
                if (!kms.Value.Equals("")) item.Kms = Convert.ToInt32(kms.Value.TrimStart(' ').TrimEnd(' '));
                else item.Kms = 0;
                if (!importe.Value.Equals("")) item.Importe = (float)Convert.ToDouble(importe.Value.TrimStart(' ').TrimEnd(' '));
                else item.Importe = (float)0;
                item.Itvpasada = Convert.ToInt32(itvpasada.Value.TrimStart(' ').TrimEnd(' '));
                item.Itvproxima = Convert.ToInt32(itvproxima.Value.TrimStart(' ').TrimEnd(' '));

                procesosVehiculos proceso = new procesosVehiculos();
                if (!proceso.InsertaRegistro(item))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado" + " " + fecha.Value + " " + matricula.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }

        protected void matricula_SelectedIndexChanged(object sender, EventArgs e)
        {
            conductor.SelectedIndex = matricula.SelectedIndex;
        }

        protected void conductor_SelectedIndexChanged(object sender, EventArgs e)
        {
            matricula.SelectedIndex = conductor.SelectedIndex;
        }
    }
}