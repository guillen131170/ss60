﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="vehiculoscrear.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Personal.Vehículos.vehiculoscrear" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">

    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">CREAR PARTE VEHÍCULO</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">REGISTRO</div>
                        <div class="panel-body">
                             <div class="row">
                                <div class="col-xs-8">
                                    <asp:dropdownlist runat="server" id="matricula" class="caja-gr" OnSelectedIndexChanged="matricula_SelectedIndexChanged" autopostback="true" >
                                        <asp:listitem text="8200KHR" value="1"></asp:listitem>
                                        <asp:listitem text="CARLOS" value="2"></asp:listitem>
                                        <asp:listitem text="JESUS" value="3"></asp:listitem>
                                        <asp:listitem text="JULIO" value="4"></asp:listitem>
                                        <asp:listitem text="FERRER" value="5"></asp:listitem>
                                        <asp:listitem text="1864KZK" value="6"></asp:listitem>
                                        <asp:listitem text="GONZALEZ" value="7"></asp:listitem>
                                        <asp:listitem text="NO TIENE" value="8"></asp:listitem>
                                        <asp:listitem text="JAIME" value="9"></asp:listitem>
                                        <asp:listitem text="MIGUEL" value="10"></asp:listitem>
                                        <asp:listitem text="8208KHR" value="11"></asp:listitem>
                                        <asp:listitem text="JOSE" value="12"></asp:listitem>
                                        <asp:listitem text="8159KHR" value="13"></asp:listitem>
                                        <asp:listitem text="3625JMD" value="14"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-4">
                                    <asp:dropdownlist runat="server" id="tipo" class="caja-pq" >
                                        <asp:listitem text="ITV" value="1"></asp:listitem>
                                        <asp:listitem text="REVISION" value="2"></asp:listitem>
                                        <asp:listitem text="TALLER" value="3"></asp:listitem>
                                        <asp:listitem text="MULTA" value="4"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                            </div><br />                            
                            <div class="row">
                                <div class="col-xs-8">
                                    <asp:dropdownlist runat="server" id="conductor" class="caja-gr" OnSelectedIndexChanged="conductor_SelectedIndexChanged" autopostback="true" >
                                        <asp:listitem text="HILARIO DUVAL" value="1"></asp:listitem>
                                        <asp:listitem text="CARLOS GARCIA" value="2"></asp:listitem>
                                        <asp:listitem text="JESUS CARNERO" value="3"></asp:listitem>
                                        <asp:listitem text="JULIO SERNA" value="4"></asp:listitem>
                                        <asp:listitem text="FERNANDO FERRER" value="5"></asp:listitem>
                                        <asp:listitem text="OSCAR CAMINO" value="6"></asp:listitem>
                                        <asp:listitem text="OSCAR GONZALEZ" value="7"></asp:listitem>
                                        <asp:listitem text="MANUEL GUILLEN" value="8"></asp:listitem>
                                        <asp:listitem text="JAIME GONZALEZ" value="9"></asp:listitem>
                                        <asp:listitem text="MIGUEL SANCHIDRIAN" value="10"></asp:listitem>
                                        <asp:listitem text="JULIAN PAIS" value="11"></asp:listitem>
                                        <asp:listitem text="JOSE ESTEVEZ" value="12"></asp:listitem>
                                        <asp:listitem text="FERNANDO SASTRE" value="13"></asp:listitem>
                                        <asp:listitem text="FRANCISCO GUILLEN" value="14"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" id="fecha" class="caja-pq" placeholder="Fecha" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" id="itvpasada" class="caja-md" placeholder="ITV anterior" runat="server">
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" id="itvproxima" class="caja-md" placeholder="Itv siguiente" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" id="kms" class="caja-md" placeholder="KMS" runat="server">
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" id="importe" class="caja-md" placeholder="Importe" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" id="modelo" class="caja-gr" placeholder="Modelo" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <textarea  id="detalle" class="caja-gr" rows="10" style="resize:none" runat="server">
                                    </textarea>
                                </div>
                            </div><br />
                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-6">
                        <asp:Button ID="altaregistro" runat="server" Text="GUARDAR REGISTRO" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="altaregistro_Click" />
                    </div>
                    <div class="col-xs-2"></div>
                </div>
            </div>
            </div>
      </div>
</div>
</asp:Content>
