﻿using SolutionS60.APPLICATION.PERSONAL.MEDICO;
using SolutionS60.CORE.PERSONAL.MEDICO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Personal.Partes_Médicos
{
    public partial class personalmedicocrear : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosMedico consulta = new procesosMedico();

            if (centro.Value.Equals("")) { error = true; }
            if (detalle.Value.Equals("")) { error = true; }
            if (fecha.Value.Equals("")) { error = true; }

            return error;
        }
        #endregion


        #region INSERTAR REPUESTO NUEVO
        protected void altaregistro_Click(object sender, EventArgs e)
        {
            if (!erroresFormulario())
            {
                clmedico item = new clmedico();
                if (!centro.Value.Equals("")) item.Centro = centro.Value.TrimStart(' ').TrimEnd(' ');
                else item.Centro = "";
                if (!referencia.Value.Equals("")) item.Referencia = referencia.Value.TrimStart(' ').TrimEnd(' ');
                else item.Referencia = "";
                item.Paciente = paciente.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                if (!detalle.Value.Equals("")) item.Descripcion = detalle.Value.TrimStart(' ').TrimEnd(' ');
                else item.Descripcion = "";
                item.Fecha = Convert.ToInt32(fecha.Value);

                procesosMedico proceso = new procesosMedico();
                if (!proceso.InsertaRegistro(item))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado" + " " + fecha.Value + " " + paciente.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }
        #endregion
    }
}