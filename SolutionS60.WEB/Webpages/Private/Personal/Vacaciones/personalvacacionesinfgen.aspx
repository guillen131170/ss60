﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="personalvacacionesinfgen.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Personal.Vacaciones.personalvacacionesinfgen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">INFORME DE VACACIONES</h3></div>
        <div class="panel-body">     
            <div class="table-responsive" style="margin-top: 30px;">      
                <asp:GridView runat="server" ID="tablaRegistros" class="table table-striped" DataKeyNames="Id" Visible="false" 
                    AutoGenerateColumns="true" GridLines="None" BorderWidth="0">
                </asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
