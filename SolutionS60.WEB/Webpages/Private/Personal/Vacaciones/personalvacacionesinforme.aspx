﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="personalvacacionesinforme.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Personal.Vacaciones.personalvacacionesinforme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

    <div class="form-horizontal-inline" role="form">
    <br />
    <br />
             <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:dropdownlist runat="server" id="nombre" class="caja-gr" >
                                        <asp:listitem text="HILARIO DUVAL" value="1"></asp:listitem>
                                        <asp:listitem text="CARLOS GARCIA" value="2"></asp:listitem>
                                        <asp:listitem text="JESUS CARNERO" value="3"></asp:listitem>
                                        <asp:listitem text="JULIO SERNA" value="4"></asp:listitem>
                                        <asp:listitem text="FERNANDO FERRER" value="5"></asp:listitem>
                                        <asp:listitem text="OSCAR CAMINO" value="6"></asp:listitem>
                                        <asp:listitem text="OSCAR GONZALEZ" value="7"></asp:listitem>
                                        <asp:listitem text="MANUEL GUILLEN" value="8"></asp:listitem>
                                        <asp:listitem text="JAIME GONZALEZ" value="9"></asp:listitem>
                                        <asp:listitem text="MIGUEL SANCHIDRIAN" value="10"></asp:listitem>
                                        <asp:listitem text="JULIAN PAIS" value="11"></asp:listitem>
                                        <asp:listitem text="JOSE ESTEVEZ" value="12"></asp:listitem>
                                        <asp:listitem text="FERNANDO SASTRE" value="13"></asp:listitem>
                                        <asp:listitem text="FRANCISCO GUILLEN" value="14"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="actualizar" runat="server" Text="APLICAR FILTRO" class="btn btn-success btn-lg btn-block" OnClick="actualizar_Click1" />
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="imprimir" runat="server" Text="PDF" class="btn btn-success btn-lg btn-block" />
                                </div>
                            </div><hr />
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Label class="caja-pq" runat="server">Fecha Inicio</asp:Label>
                                    <input type="text" id="fechainicio" class="caja-pq" placeholder="Inicio" runat="server">
                                </div>
                                <div class="col-xs-9">
                                    <asp:Label class="caja-pq" runat="server">Fecha Fin</asp:Label>
                                    <input type="text" id="fechafin" class="caja-pq" placeholder="Fin" runat="server">
                                </div>              
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">LISTA DE SOLICITUD DE VACACIONES</h3></div>
        <div class="panel-body">     
            <div class="table-responsive" style="margin-top: 30px;">      
                <asp:GridView runat="server" ID="tablaRegistros" class="table table-striped" DataKeyNames="Id" Visible="false" 
                    AutoGenerateColumns="true" GridLines="None" BorderWidth="0" OnRowCommand="tablaRegistros_RowCommand">
                    <Columns>
                        <asp:ButtonField ButtonType="Link" HeaderText="Detalle" Text="ver" CommandName="Ver_Click"  />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</div>
</asp:Content>
