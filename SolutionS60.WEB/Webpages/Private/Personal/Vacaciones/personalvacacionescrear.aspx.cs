﻿using SolutionS60.APPLICATION.PERSONAL.VACACIONES;
using SolutionS60.CORE.PERSONAL.VACACIONES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Personal.Vacaciones
{
    public partial class personalvacacionescrear : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosVacaciones consulta = new procesosVacaciones();

            if (abono.Value.Equals("") && cargo.Value.Equals("")) { error = true; }
            if (detalle.Value.Equals("")) { error = true; }
            if (fecha.Value.Equals("")) { error = true; }

            return error;
        }
        #endregion


        #region DEVUELVE EL ULTIMO REGISTRO
        private clvacaciones Datos()
        {
            procesosVacaciones proceso = new procesosVacaciones();
            return proceso.obtenerRegistroPorNombre(nombre.SelectedItem.ToString());
        } 
        #endregion


        #region INSERTAR REPUESTO NUEVO
        protected void altaregistro_Click(object sender, EventArgs e)
        {
            clvacaciones ultimoRegistro = new clvacaciones();
            ultimoRegistro = Datos();

            if (!erroresFormulario())
            {
                clvacaciones item = new clvacaciones();
                
                item.Nombre = nombre.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                if (!abono.Value.Equals("")) item.Abono = Convert.ToInt32(abono.Value.TrimStart(' ').TrimEnd(' '));
                else item.Abono = 0;
                if (!cargo.Value.Equals("")) item.Cargo = Convert.ToInt32(cargo.Value.TrimStart(' ').TrimEnd(' '));
                else item.Cargo = 0;
                if (!detalle.Value.Equals("")) item.Detalle = detalle.Value.TrimStart(' ').TrimEnd(' ');
                else item.Detalle = "";
                item.Fecha = Convert.ToInt32(fecha.Value.TrimStart(' ').TrimEnd(' ')); 
                if (ultimoRegistro!=null)
                {
                    item.Saldo = ultimoRegistro.Saldo - item.Cargo + item.Abono;
                }
                else item.Saldo = 0 - item.Cargo + item.Abono;

                procesosVacaciones proceso = new procesosVacaciones();
                if (!proceso.InsertaRegistro(item))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado" + " " + fecha.Value + " " + nombre.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }
        #endregion
    }
}