﻿using SolutionS60.APPLICATION.PERSONAL.VACACIONES;
using SolutionS60.CORE.PERSONAL.VACACIONES;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Personal.Vacaciones
{
    public partial class personalvacacionesinfgen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }

        public int mostrarLista()
        {
            procesosVacaciones proceso = new procesosVacaciones();
            List<clvacaciones> lista = new List<clvacaciones>();
            clvacaciones item = new clvacaciones();
            item = proceso.obtenerRegistroPorNombre("HILARIO DUVAL");
            if (item != null)
                lista.Add(item); 
            item = proceso.obtenerRegistroPorNombre("CARLOS GARCIA");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("JESUS CARNERO");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("JULIO SERNA");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("FERNANDO FERRER");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("OSCAR CAMINO");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("OSCAR GONZALEZ");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("MANUEL GUILLEN");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("JAIME GONZALEZ");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("MIGUEL SANCHIDRIAN");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("JULIAN PAIS");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("JOSE ESTEVEZ");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("FERNANDO SASTRE");
            if (item != null)
                lista.Add(item);
            item = proceso.obtenerRegistroPorNombre("FRANCISCO GUILLEN");
            if (item != null)
                lista.Add(item);

            try
            {

                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Nombre");
                dt.Columns.Add("Saldo");

                foreach (clvacaciones registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Nombre;
                    dr[2] = registro.Saldo;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
    }
}