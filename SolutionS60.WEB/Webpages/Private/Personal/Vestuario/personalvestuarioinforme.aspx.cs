﻿using SolutionS60.APPLICATION.PERSONAL.VESTUARIO;
using SolutionS60.CORE.PERSONAL.VESTUARIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Personal.Vestuario
{
    public partial class personalvestuarioinforme : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }
        #endregion


        #region MOSTRAR RESULTADOS
        public int mostrarLista()
        {
            procesosVestuario proceso = new procesosVestuario();
            List<clvestuario> lista = new List<clvestuario>();
            try
            {
                lista = proceso.obtenerTodosNombre(nombre.SelectedItem.ToString().TrimStart(' ').TrimEnd(' '));

                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Proveedor");
                dt.Columns.Add("Albarán");
                dt.Columns.Add("Destinatario");
                dt.Columns.Add("Fecha");

                foreach (clvestuario registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Proveedor;
                    dr[2] = registro.Nalbaran;
                    dr[3] = registro.Destinatario;
                    dr[4] = registro.Fecha;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }

        public int mostrarListaFecha(int i, int f)
        {
            procesosVestuario proceso = new procesosVestuario();
            List<clvestuario> lista = new List<clvestuario>();
            try
            {
                lista = proceso.obtenerTodosFechaNombre(i, f, nombre.SelectedItem.ToString().TrimStart(' ').TrimEnd(' '));

                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Proveedor");
                dt.Columns.Add("Albarán");
                dt.Columns.Add("Destinatario");
                dt.Columns.Add("Fecha");

                foreach (clvestuario registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Proveedor;
                    dr[2] = registro.Nalbaran;
                    dr[3] = registro.Destinatario;
                    dr[4] = registro.Fecha;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosVestuario proceso = new procesosVestuario();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            clvestuario registro = new clvestuario();
            registro = proceso.obtenerRegistroPorId(idProducto);

            string startFolder = Properties.Resource1.RutaListaPersonalVestuario2;
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(startFolder);
            string nombreFichero = "*" + registro.Fecha + "*" + registro.Destinatario + "*";
            IEnumerable<System.IO.FileInfo> fileList = dir.GetFiles(nombreFichero + "*.*", System.IO.SearchOption.AllDirectories);
            IEnumerable<System.IO.FileInfo> fileQuery =
                from file in fileList
                where file.Extension == ".pdf"
                orderby file.Name
                select file;

            if (idProducto != -111 && fileQuery != null)
            {
                try
                {
                    var newestFile =
                    (from file in fileQuery
                     orderby file.CreationTime
                     select new { file.FullName, file.CreationTime })
                    .Last();

                    switch (e.CommandName)
                    {
                        case "Ver_Click":
                            if (fileQuery.Count() > 0)
                            {
                                string pdfPath = newestFile.FullName;
                                Process.Start(pdfPath);
                            }
                            else
                            {
                                status.Text = "ARCHIVO NO ENCONTRADO";
                                statusdiv.Attributes["class"] = "alert alert-danger";
                            }
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch (Exception ex)
                {
                    // recoge errores hacia el status text
                    status.Text = "ARCHIVO NO ENCONTRADO";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion


        #region ACTUALIZAR O REFRESCAR
        protected void actualizar_Click1(object sender, EventArgs e)
        {
            int lista = -111;
            int inicio;
            int fin;
            if (!fechainicio.Value.Equals("") && !fechafin.Value.Equals(""))
            {
                inicio = Convert.ToInt32(fechainicio.Value);
                fin = Convert.ToInt32(fechafin.Value);
                lista = mostrarListaFecha(inicio, fin);
            }
            else lista = mostrarLista();

            if (lista <= 0)
            {
                status.Text = "No se encontraron registros";
                statusdiv.Attributes["class"] = "alert alert-sucess";
            }
        }
        #endregion
    }
}