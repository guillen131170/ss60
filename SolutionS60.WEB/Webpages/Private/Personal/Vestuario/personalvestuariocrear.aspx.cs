﻿using SolutionS60.APPLICATION.PERSONAL.VESTUARIO;
using SolutionS60.CORE.PERSONAL.VESTUARIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Personal.Vestuario
{
    public partial class personalvestuariocrear : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosVestuario consulta = new procesosVestuario();

            if (!albaran.Value.Equals(""))
            {
                if (consulta.comprobarDuplicados(albaran.Value)) { error = true; }
            }
            if (fecha.Value.Equals("")) { error = true; }

            return error;
        }
        #endregion


        #region INSERTAR REPUESTO NUEVO
        protected void altaregistro_Click(object sender, EventArgs e)
        {
            if (!erroresFormulario())
            {
                clvestuario item = new clvestuario();
                if (!proveedor.Value.Equals("")) item.Proveedor = proveedor.Value.TrimStart(' ').TrimEnd(' ');
                else item.Proveedor = "";
                if (!albaran.Value.Equals("")) item.Nalbaran = albaran.Value.TrimStart(' ').TrimEnd(' ');
                else item.Nalbaran = "";
                item.Destinatario = destinatario.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                if (!detalle.Value.Equals("")) item.Descripcion = detalle.Value.TrimStart(' ').TrimEnd(' ');
                else item.Descripcion = "";
                item.Fecha = Convert.ToInt32(fecha.Value);

                procesosVestuario proceso = new procesosVestuario();
                if (!proceso.InsertaRegistro(item))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado" + " " + fecha.Value + " " + destinatario.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }
        #endregion
    }
}