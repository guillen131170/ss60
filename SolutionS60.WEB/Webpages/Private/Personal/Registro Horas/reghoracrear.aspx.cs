﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Personal.Registro_Horas
{
    public partial class reghoracrear : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void enviar_Click(object sender, EventArgs e)
        {
            bool[] array = new bool[31];
            for (int i=0; i<31; i++) 
                array[0] = false;
            if (c1.Checked == true) array[0] = true;
            if (c2.Checked == true) array[1] = true;
            if (c3.Checked == true) array[2] = true;
            if (c4.Checked == true) array[3] = true;
            if (c5.Checked == true) array[4] = true;
            if (c6.Checked == true) array[5] = true;
            if (c7.Checked == true) array[6] = true;
            if (c8.Checked == true) array[7] = true;
            if (c9.Checked == true) array[8] = true;
            if (c10.Checked == true) array[9] = true;
            if (c11.Checked == true) array[10] = true;
            if (c12.Checked == true) array[11] = true;
            if (c13.Checked == true) array[12] = true;
            if (c14.Checked == true) array[13] = true;
            if (c15.Checked == true) array[14] = true;
            if (c16.Checked == true) array[15] = true;
            if (c17.Checked == true) array[16] = true;
            if (c18.Checked == true) array[17] = true;
            if (c19.Checked == true) array[18] = true;
            if (c20.Checked == true) array[19] = true;
            if (c21.Checked == true) array[20] = true;
            if (c22.Checked == true) array[21] = true;
            if (c23.Checked == true) array[22] = true;
            if (c24.Checked == true) array[23] = true;
            if (c25.Checked == true) array[24] = true;
            if (c26.Checked == true) array[25] = true;
            if (c27.Checked == true) array[26] = true;
            if (c28.Checked == true) array[27] = true;
            if (c29.Checked == true) array[28] = true;
            if (c30.Checked == true) array[29] = true;
            if (c31.Checked == true) array[30] = true;

            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\Basura\documento.pdf", FileMode.Create));

            // **Nota: Esto no será visible en el documento
            doc.AddTitle("REGISTRO HORAS");
            doc.AddCreator("Fco Guillén");
            // Abrimos el archivo
            doc.Open();
            // Escribimos el encabezamiento en el documento
            //doc.Add(new Paragraph(fecha.Value.TrimStart(' ').TrimEnd(' ') + " " + trabajador.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ')));
            //doc.Add(Chunk.NEWLINE);

            // Creamos una tabla
            PdfPTable tblPrueba = new PdfPTable(1);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 2;
            tblPrueba.AddCell("REGISTRO DIARIO DE JORNADA: TRABAJADORES A TIEMPO COMPLETO" + Chunk.NEWLINE);
            doc.Add(tblPrueba);

            tblPrueba = new PdfPTable(2);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 1;
            float[] values2 = new float[2];
            values2[0] = 50;
            values2[1] = 50;
            tblPrueba.SetWidths(values2);
            tblPrueba.AddCell("EMPRESA" + Chunk.NEWLINE);
            tblPrueba.AddCell("TRABAJADOR" + Chunk.NEWLINE);
            tblPrueba.AddCell("Razón Social:" + Chunk.NEWLINE + "" +
                "SERVICIOS SESENTA BURGOS SL" + Chunk.NEWLINE);
            tblPrueba.AddCell("Nombre:" + Chunk.NEWLINE +
                " " + trabajador.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ') + Chunk.NEWLINE);
            tblPrueba.AddCell("C.I.F.: B 09 224 528" + Chunk.NEWLINE);
            tblPrueba.AddCell("N.I.F.:" + dni.Value.ToString().TrimStart(' ').TrimEnd(' ') + Chunk.NEWLINE);
            tblPrueba.AddCell("C.C.C.: 47/1084544/67" + Chunk.NEWLINE);
            tblPrueba.AddCell("N.A.F.:" + Chunk.NEWLINE);
            tblPrueba.AddCell("Período de liquidación:" + Chunk.NEWLINE);
            tblPrueba.AddCell("Fecha:" + fecha.Value.ToString().TrimStart(' ').TrimEnd(' ') + Chunk.NEWLINE);
            doc.Add(tblPrueba);

            tblPrueba = new PdfPTable(6);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 2;
            float[] values4 = new float[6];
            values4[0] = 6;
            values4[1] = 24;
            values4[2] = 24;
            values4[3] = 12;
            values4[4] = 12;
            values4[5] = 22;
            tblPrueba.SetWidths(values4);
            tblPrueba.AddCell("Días" + Chunk.NEWLINE);
            tblPrueba.AddCell("Entrada" + Chunk.NEWLINE);
            tblPrueba.AddCell("Salida" + Chunk.NEWLINE);
            tblPrueba.AddCell("Descanso" + Chunk.NEWLINE);
            tblPrueba.AddCell("Total" + Chunk.NEWLINE);
            tblPrueba.AddCell("Firma" + Chunk.NEWLINE);
            doc.Add(tblPrueba);

            tblPrueba = new PdfPTable(8);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 2;
            float[] values3 = new float[8];
            values3[0] = 6;
            values3[1] = 12;
            values3[2] = 12;
            values3[3] = 12;
            values3[4] = 12;
            values3[5] = 12;
            values3[6] = 12;
            values3[7] = 22;
            tblPrueba.SetWidths(values3);
            tblPrueba.AddCell(" " + Chunk.NEWLINE);
            tblPrueba.AddCell("Mañana" + Chunk.NEWLINE);
            tblPrueba.AddCell("Tarde" + Chunk.NEWLINE);
            tblPrueba.AddCell("Mañana" + Chunk.NEWLINE);
            tblPrueba.AddCell("Tarde" + Chunk.NEWLINE);
            tblPrueba.AddCell("Mañana" + Chunk.NEWLINE);
            tblPrueba.AddCell(" " + Chunk.NEWLINE);
            tblPrueba.AddCell(" " + Chunk.NEWLINE);
            doc.Add(tblPrueba);

            tblPrueba = new PdfPTable(8);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 2;
            float[] values = new float[8];
            values[0] = 6;
            values[1] = 12;
            values[2] = 12;
            values[3] = 12;
            values[4] = 12;
            values[5] = 12;
            values[6] = 12;
            values[7] = 22;
            tblPrueba.SetWidths(values);
            for (int i = 0; i < 31; i++)
            {
                tblPrueba.AddCell((i+1) + "" + Chunk.NEWLINE);
                if (array[i])
                {
                    if (CheckJornada.Checked == false)
                    {
                        tblPrueba.AddCell("8:00" + Chunk.NEWLINE);
                        tblPrueba.AddCell("16:00" + Chunk.NEWLINE);
                        tblPrueba.AddCell("14:00" + Chunk.NEWLINE);
                        tblPrueba.AddCell("18:00" + Chunk.NEWLINE);
                        tblPrueba.AddCell("11:00" + Chunk.NEWLINE);
                        tblPrueba.AddCell("8 horas" + Chunk.NEWLINE);
                    }
                    else
                    {
                        tblPrueba.AddCell("8:00" + Chunk.NEWLINE);
                        tblPrueba.AddCell(" " + Chunk.NEWLINE);
                        tblPrueba.AddCell("12:00" + Chunk.NEWLINE);
                        tblPrueba.AddCell(" " + Chunk.NEWLINE);
                        tblPrueba.AddCell(" " + Chunk.NEWLINE);
                        tblPrueba.AddCell("4 horas" + Chunk.NEWLINE);
                    }
                }
                else
                {
                    tblPrueba.AddCell(" " + Chunk.NEWLINE);
                    tblPrueba.AddCell(" " + Chunk.NEWLINE);
                    tblPrueba.AddCell(" " + Chunk.NEWLINE);
                    tblPrueba.AddCell(" " + Chunk.NEWLINE);
                    tblPrueba.AddCell(" " + Chunk.NEWLINE);
                    tblPrueba.AddCell(" " + Chunk.NEWLINE);
                }
                tblPrueba.AddCell(" " + Chunk.NEWLINE);
            }
            doc.Add(tblPrueba);

            tblPrueba = new PdfPTable(4);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 2;
            float[] values5 = new float[4];
            values5[0] = 6;
            values5[1] = 36;
            values5[2] = 36;
            values5[3] = 22;
            tblPrueba.SetWidths(values5);
            tblPrueba.AddCell(" " + Chunk.NEWLINE);
            tblPrueba.AddCell("FIRMA EMPRESA" + Chunk.NEWLINE);
            tblPrueba.AddCell("FIRMA TRABAJADOR" + Chunk.NEWLINE);
            tblPrueba.AddCell(" " + Chunk.NEWLINE);
            tblPrueba.AddCell(" " + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE);
            tblPrueba.AddCell(" " + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE);
            tblPrueba.AddCell(" " + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE);
            tblPrueba.AddCell(" " + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE + Chunk.NEWLINE);
            doc.Add(tblPrueba);

            doc.Close();
            writer.Close();

            //string pdfPath = Path.Combine(Application.StartupPath, "archivo.pdf");
            string pdfPath = @"D:\Usuario\Desktop\Basura\documento.pdf";
            Process.Start(pdfPath);

            status.Text = fecha.Value.TrimStart(' ').TrimEnd(' ') + " " + trabajador.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
            statusdiv.Attributes["class"] = "alert alert-danger";

            Response.Clear();
        }
    }
}