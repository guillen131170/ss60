﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Personal.Registro_Horas
{
    public partial class reghoralista : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros" + lista;
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }


        #region MOSTRAR RESULTADOS
        public int mostrarLista()
        {
            string startFolder = Properties.Resource1.RutaListaPersonalRegHora2;
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(startFolder);

            string nombreFichero = trabajador.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
            IEnumerable<System.IO.FileInfo> fileList = dir.GetFiles("*"+ nombreFichero  + "*.*", System.IO.SearchOption.AllDirectories);
            IEnumerable<System.IO.FileInfo> fileQuery =
                from file in fileList
                where file.Extension == ".pdf"
                orderby file.Name
                select file;

            {
                try
                {
                    var newestFile =
                    (from file in fileQuery
                     orderby file.CreationTime
                     select new { file.FullName, file.CreationTime });
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Archivo");

                    foreach (var registro in newestFile)
                    {
                        DataRow dr = dt.NewRow();

                        string n = registro.FullName;
                        if (registro.FullName.Contains("REGISTRO HORAS"))
                        {
                            int index = registro.FullName.IndexOf("REGISTRO HORAS");
                            if (index >= 0) 
                                n = registro.FullName.Substring(index);
                        }

                        dr[0] = n;
                        dt.Rows.Add(dr);
                    }
                    tablaRegistros.Visible = true;
                    tablaRegistros.DataSource = dt;
                    tablaRegistros.DataBind();
                    return 99;//dir.GetFiles("*" + nombreFichero + "*.*").Length;//fileQuery.Count<System.IO.FileInfo>();//9999;
                }
                catch (Exception ex)
                {
                    // las excepciones al status y cual es el error
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
            return 0;
        }
        #endregion


        protected void enviar_Click(object sender, EventArgs e)
        {
            int lista = -111;
            lista = mostrarLista();

            if (lista <= 0)
            {
                status.Text = "No se encontraron registros" + lista;
                statusdiv.Attributes["class"] = "alert alert-sucess";
            }
        }

        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
           {
                try
                {
                    switch (e.CommandName)
                    {
                        case "Ver_Click":
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch
                {
                    // recoge errores hacia el status text
                    status.Text = "ARCHIVO NO ENCONTRADO";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
    }
}