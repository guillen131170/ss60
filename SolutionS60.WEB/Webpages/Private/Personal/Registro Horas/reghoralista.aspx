﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="reghoralista.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Personal.Registro_Horas.reghoralista" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
            </div>
                <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:dropdownlist runat="server" id="trabajador" class="caja-gr" >
                                        <asp:listitem text="HILARIO DUVAL" value="1"></asp:listitem>
                                        <asp:listitem text="CARLOS GARCIA" value="2"></asp:listitem>
                                        <asp:listitem text="JESUS CARNERO" value="3"></asp:listitem>
                                        <asp:listitem text="JULIO SERNA" value="4"></asp:listitem>
                                        <asp:listitem text="FERNANDO FERRER" value="5"></asp:listitem>
                                        <asp:listitem text="OSCAR CAMINO" value="6"></asp:listitem>
                                        <asp:listitem text="OSCAR GONZALEZ" value="7"></asp:listitem>
                                        <asp:listitem text="MANUEL GUILLEN" value="8"></asp:listitem>
                                        <asp:listitem text="JAIME GONZALEZ" value="9"></asp:listitem>
                                        <asp:listitem text="MIGUEL SANCHIDRIAN" value="10"></asp:listitem>
                                        <asp:listitem text="JULIAN PAIS" value="11"></asp:listitem>
                                        <asp:listitem text="JOSE ESTEVEZ" value="12"></asp:listitem>
                                        <asp:listitem text="FERNANDO SASTRE" value="13"></asp:listitem>
                                        <asp:listitem text="FRANCISCO GUILLEN" value="14"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                            </div>  
                            <div class="row">
                                <br /><br />
                                <div class="col-xs-4"></div>
                                <div class="col-xs-4"></div>
                                <div class="col-xs-4">
                                    <asp:Button ID="enviar" runat="server" Text="ENVIAR" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="enviar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">LISTA DE FIHAS</h3></div>
        <div class="panel-body">     
            <div class="table-responsive" style="margin-top: 30px;">      
                <asp:GridView runat="server" ID="tablaRegistros" class="table table-striped" Visible="false" 
                    AutoGenerateColumns="true" GridLines="None" BorderWidth="0" OnRowCommand="tablaRegistros_RowCommand">
                    <Columns>
                        <asp:ButtonField ButtonType="Link" HeaderText="Detalle" Text="ver" CommandName="Ver_Click"  />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
