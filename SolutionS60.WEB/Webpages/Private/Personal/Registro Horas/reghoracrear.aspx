﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="reghoracrear.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Personal.Registro_Horas.reghoracrear" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
            </div>
                <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:dropdownlist runat="server" id="trabajador" class="caja-gr" >
                                        <asp:listitem text="HILARIO DUVAL" value="1"></asp:listitem>
                                        <asp:listitem text="CARLOS GARCIA" value="2"></asp:listitem>
                                        <asp:listitem text="JESUS CARNERO" value="3"></asp:listitem>
                                        <asp:listitem text="JULIO SERNA" value="4"></asp:listitem>
                                        <asp:listitem text="FERNANDO FERRER" value="5"></asp:listitem>
                                        <asp:listitem text="OSCAR CAMINO" value="6"></asp:listitem>
                                        <asp:listitem text="OSCAR GONZALEZ" value="7"></asp:listitem>
                                        <asp:listitem text="MANUEL GUILLEN" value="8"></asp:listitem>
                                        <asp:listitem text="JAIME GONZALEZ" value="9"></asp:listitem>
                                        <asp:listitem text="MIGUEL SANCHIDRIAN" value="10"></asp:listitem>
                                        <asp:listitem text="JULIAN PAIS" value="11"></asp:listitem>
                                        <asp:listitem text="JOSE ESTEVEZ" value="12"></asp:listitem>
                                        <asp:listitem text="FERNANDO SASTRE" value="13"></asp:listitem>
                                        <asp:listitem text="FRANCISCO GUILLEN" value="14"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-3">
                                        <input type="Text" id="dni" class="caja-md" placeholder="DNI" runat="server">
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" id="fecha" class="caja-pq" placeholder="Fecha" runat="server">
                                </div>
                                <div class="col-xs-3">
                                    <asp:Label ID="jornada" class="caja-pq" runat="server">Media Jornada</asp:Label>
                                    <asp:CheckBox ID="CheckJornada" runat="server"/>
                                </div>
                            </div>  
                            <div class="row">
                                <br /><br />
                                <div class="col-xs-4"></div>
                                <div class="col-xs-4"></div>
                                <div class="col-xs-4">
                                    <asp:Button ID="enviar" runat="server" Text="ENVIAR" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="enviar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO 2</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-1">
                                    <asp:Label ID="Label1" class="caja-pq" runat="server">01</asp:Label>
                                    <asp:CheckBox ID="c1" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label2" class="caja-pq" runat="server">02</asp:Label>
                                    <asp:CheckBox ID="c2" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label3" class="caja-pq" runat="server">03</asp:Label>
                                    <asp:CheckBox ID="c3" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label4" class="caja-pq" runat="server">04</asp:Label>
                                    <asp:CheckBox ID="c4" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label5" class="caja-pq" runat="server">05</asp:Label>
                                    <asp:CheckBox ID="c5" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label6" class="caja-pq" runat="server">06</asp:Label>
                                    <asp:CheckBox ID="c6" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label7" class="caja-pq" runat="server">07</asp:Label>
                                    <asp:CheckBox ID="c7" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label8" class="caja-pq" runat="server">08</asp:Label>
                                    <asp:CheckBox ID="c8" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label9" class="caja-pq" runat="server">09</asp:Label>
                                    <asp:CheckBox ID="c9" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label10" class="caja-pq" runat="server">10</asp:Label>
                                    <asp:CheckBox ID="c10" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label11" class="caja-pq" runat="server">11</asp:Label>
                                    <asp:CheckBox ID="c11" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label12" class="caja-pq" runat="server">12</asp:Label>
                                    <asp:CheckBox ID="c12" runat="server"/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-1">
                                    <asp:Label ID="Label13" class="caja-pq" runat="server">13</asp:Label>
                                    <asp:CheckBox ID="c13" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label14" class="caja-pq" runat="server">14</asp:Label>
                                    <asp:CheckBox ID="c14" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label15" class="caja-pq" runat="server">15</asp:Label>
                                    <asp:CheckBox ID="c15" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label16" class="caja-pq" runat="server">16</asp:Label>
                                    <asp:CheckBox ID="c16" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label17" class="caja-pq" runat="server">17</asp:Label>
                                    <asp:CheckBox ID="c17" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label18" class="caja-pq" runat="server">18</asp:Label>
                                    <asp:CheckBox ID="c18" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label19" class="caja-pq" runat="server">19</asp:Label>
                                    <asp:CheckBox ID="c19" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label20" class="caja-pq" runat="server">20</asp:Label>
                                    <asp:CheckBox ID="c20" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label21" class="caja-pq" runat="server">21</asp:Label>
                                    <asp:CheckBox ID="c21" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label22" class="caja-pq" runat="server">22</asp:Label>
                                    <asp:CheckBox ID="c22" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label23" class="caja-pq" runat="server">23</asp:Label>
                                    <asp:CheckBox ID="c23" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label24" class="caja-pq" runat="server">24</asp:Label>
                                    <asp:CheckBox ID="c24" runat="server"/>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-1">
                                    <asp:Label ID="Label25" class="caja-pq" runat="server">25</asp:Label>
                                    <asp:CheckBox ID="c25" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label26" class="caja-pq" runat="server">26</asp:Label>
                                    <asp:CheckBox ID="c26" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label27" class="caja-pq" runat="server">27</asp:Label>
                                    <asp:CheckBox ID="c27" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label28" class="caja-pq" runat="server">28</asp:Label>
                                    <asp:CheckBox ID="c28" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label29" class="caja-pq" runat="server">29</asp:Label>
                                    <asp:CheckBox ID="c29" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label30" class="caja-pq" runat="server">30</asp:Label>
                                    <asp:CheckBox ID="c30" runat="server"/>
                                </div>
                                <div class="col-xs-1">
                                    <asp:Label ID="Label31" class="caja-pq" runat="server">31</asp:Label>
                                    <asp:CheckBox ID="c31" runat="server"/>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

</asp:Content>
