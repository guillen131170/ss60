﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ordentaller.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Ordentaller.ordentaller" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">

    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">CREAR ORDEN DE TALLER</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">TÉCNICO</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-7">
                                    <asp:dropdownlist runat="server" id="nombreTecnico" autopostback="true" class="caja-md" OnSelectedIndexChanged="nombreTecnico_SelectedIndexChanged"> 
                                        <asp:listitem text="EDUARDO CALVO" value="1"></asp:listitem>
                                        <asp:listitem text="DIEGO TOMÉ" value="2"></asp:listitem>
                                        <asp:listitem text="ROBERTO ESTÉBANEZ" value="3"></asp:listitem>
                                        <asp:listitem text="NACHO VELAZCO" value="4"></asp:listitem>
                                        <asp:listitem text="JAVIER" value="5"></asp:listitem>
                                        <asp:listitem text="SERGIO CALVO" value="6"></asp:listitem>
                                        <asp:listitem text="FELIPE" value="7"></asp:listitem>
                                        <asp:listitem text="JORGE HERRERO" value="8"></asp:listitem>
                                        <asp:listitem text="PACO CAMPO" value="9"></asp:listitem>
                                        <asp:listitem text="JUANJO TABERNERO" value="10"></asp:listitem>
                                        <asp:listitem text="JOSÉ BERMEJO" value="11"></asp:listitem>
                                        <asp:listitem text="JOSÉ VELAZCO " value="12"></asp:listitem>
                                        <asp:listitem text="ROBERTO TABARES " value="13"></asp:listitem>
                                        <asp:listitem text="DANIEL" value="14"></asp:listitem>
                                        <asp:listitem text="HILARIO DUVAL" value="15"></asp:listitem>
                                        <asp:listitem text="CARLOS GARCÍA" value="16"></asp:listitem>
                                        <asp:listitem text="JESÚS CARNERO" value="17"></asp:listitem>
                                        <asp:listitem text="JULIO SERNA" value="18"></asp:listitem>
                                        <asp:listitem text="FERNANDO FERRER" value="19"></asp:listitem>
                                        <asp:listitem text="ÓSCAR CAMINO" value="20"></asp:listitem>
                                        <asp:listitem text="ÓSCAR GONZÁLEZ" value="21"></asp:listitem>
                                        <asp:listitem text="MANUEL GUILLÉN" value="22"></asp:listitem>
                                        <asp:listitem text="JAIME GONZÁLEZ" value="23"></asp:listitem>
                                        <asp:listitem text="MIGUEL SANCHIDRIÁN" value="24"></asp:listitem>
                                        <asp:listitem text="JULIÁN PAÍS" value="25"></asp:listitem>
                                        <asp:listitem text="JOSÉ ESTÉVEZ" value="26"></asp:listitem>
                                        <asp:listitem text="FERNANDO SASTRE" value="27"></asp:listitem>
                                        <asp:listitem text="FRANCISCO GUILLÉN" value="28"></asp:listitem>
                                        <asp:listitem text="OTROS" value="29"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-5">
                                    <asp:TextBox id="codigoTecnico" class="caja-pq" placeholder="Código" runat="server" ReadOnly="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">PROYECTO</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-7">
                                    <asp:dropdownlist runat="server" id="nombreProyecto" autopostback="true" class="caja-md" 
                                        OnSelectedIndexChanged="nombreProyecto_SelectedIndexChanged"> 
                                        <asp:listitem text="CAFENTO" value="1"></asp:listitem>
                                        <asp:listitem text="CANDELAS" value="2"></asp:listitem>
                                        <asp:listitem text="LATITUD" value="3"></asp:listitem>
                                        <asp:listitem text="DELTA" value="4"></asp:listitem>
                                        <asp:listitem text="BLACKZI" value="5"></asp:listitem>
                                        <asp:listitem text="DUNKIN" value="6"></asp:listitem>
                                        <asp:listitem text="PROINDE" value="7"></asp:listitem>
                                        <asp:listitem text="STARBUCKS CAFE" value="8"></asp:listitem>
                                        <asp:listitem text="STARBUCKS FRIO" value="9"></asp:listitem>
                                        <asp:listitem text="IKEA" value="10"></asp:listitem>
                                        <asp:listitem text="BERLYS" value="11"></asp:listitem>
                                        <asp:listitem text="DANONE WATERS" value="12"></asp:listitem>
                                        <asp:listitem text="DANONE VENDING" value="13"></asp:listitem>
                                        <asp:listitem text="PEPSI" value="14"></asp:listitem>
                                        <asp:listitem text="SCHWEPPES" value="15"></asp:listitem>
                                        <asp:listitem text="HEINEKEN STC" value="16"></asp:listitem>
                                        <asp:listitem text="HEINEKEN EVENTO" value="17"></asp:listitem>
                                        <asp:listitem text="HEINEKEN ALIMENTACIÓN" value="18"></asp:listitem>
                                        <asp:listitem text="EDEN SPRING" value="19"></asp:listitem>
                                        <asp:listitem text="SPECTANK" value="20"></asp:listitem>
                                        <asp:listitem text="GINOS FRIO" value="21"></asp:listitem>
                                        <asp:listitem text="RED BULL" value="22"></asp:listitem>
                                        <asp:listitem text="LACREM" value="23"></asp:listitem>
                                        <asp:listitem text="OTROS" value="24"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-5">
                                    <asp:TextBox id="codigoProyecto" class="caja-pq" placeholder="Código" runat="server" ReadOnly="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">CLIENTE</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-7">
                                    <input type="text" id="nombrePDS" class="caja-md" placeholder="Nombre" runat="server">
                                </div>
                                <div class="col-xs-5">
                                    <input type="text" id="codigoPDS" class="caja-pq" placeholder="Código" runat="server">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">ACTIVIDAD</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-5">
                                    <input type="text" id="odsActividad" class="caja-md" placeholder="Orden" runat="server">
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" id="provincia" class="caja-md" placeholder="Provincia" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-7">
                                    <asp:dropdownlist runat="server" id="nombreActividad" class="caja-gr" autopostback="true" > 
                                        <asp:listitem text="AVERIA PDS" value="1"></asp:listitem>
                                        <asp:listitem text="LIMPIEZA PDS" value="2"></asp:listitem>
                                        <asp:listitem text="PREVENTIVO PDS" value="3"></asp:listitem>
                                        <asp:listitem text="PUESTA A PUNTO PDS" value="4"></asp:listitem>
                                        <asp:listitem text="REPARACIÓN TALLER" value="5"></asp:listitem>
                                        <asp:listitem text="PUESTA A PUNTO TALLER" value="6"></asp:listitem>
                                        <asp:listitem text="MONTAJE PDS" value="7"></asp:listitem>
                                        <asp:listitem text="DESMONTAJE PDS" value="8"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-5">
                                    <asp:dropdownlist runat="server" id="tipoActividad" class="caja-md" autopostback="true" > 
                                        <asp:listitem text="ORDEN CALLE" value="1"></asp:listitem>
                                        <asp:listitem text="ORDEN TALLER" value="2"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">ESTADO</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-5">
                                    <asp:dropdownlist runat="server" id="estado" class="caja-md" autopostback="true"> 
                                        <asp:listitem text="CERRADO" value="1"></asp:listitem>
                                        <asp:listitem text="ABIERTO" value="2"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-7">
                                    <asp:dropdownlist runat="server" id="resultado" class="caja-md" autopostback="true">
                                        <asp:listitem text="AUTORIZADO" value="1"></asp:listitem>
                                        <asp:listitem text="TRATAMIENTO" value="2"></asp:listitem>
                                        <asp:listitem text="RECHAZADO" value="3"></asp:listitem>
                                        <asp:listitem text="ANULADO" value="4"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-5">
                                    <input type="text" id="fechaODS" class="caja-pq" placeholder="Fecha trabajo" runat="server">
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" id="fechaTaller" class="caja-pq" placeholder="Fecha orden" runat="server">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">EQUIPO</div>
                <div class="panel-body">     
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="text" id="codigoEquipo" class="caja-gr" placeholder="Equipo" runat="server">
                        </div>
                        <div class="col-xs-4">
                            <input type="text" id="nombreMaterial" class="caja-gr" placeholder="Material" runat="server">
                        </div>
                        <div class="col-xs-4">
                            <input type="text" id="codigoMaterial" class="caja-gr" placeholder="Código" runat="server">
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4">
                            <input type="text" id="nsSAP" class="caja-gr" placeholder="SAP" runat="server">
                        </div>
                        <div class="col-xs-4">
                            <input type="text" id="nsAX" class="caja-gr" placeholder="AX" runat="server">
                        </div>
                    </div>
                 </div>
            </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">OFERTA</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" id="ordenTaller" class="caja-gr" placeholder="Orden taller" runat="server">
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" id="pedido" class="caja-gr" placeholder="Pedido" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                    <div class="col-xs-3">
                                        <input type="text" id="precioOferta" class="caja-pq" placeholder="Oferta" runat="server">
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" id="precioRepuesto" class="caja-pq" placeholder="Repuesto" runat="server">
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" id="precioMO" class="caja-pq" placeholder="MO" runat="server">
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" id="precioDesplazamiento" class="caja-pq" placeholder="Desplaz." runat="server">
                                    </div>
                                </div>
                        </div>
                    </div>

            <div class="row">
                <div class="col-xs-7">
                    <div class="panel panel-default">
                        <div class="panel-heading">DESCRIPCIÓN</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-10">
                                    <textarea  id="descripcion" class="caja-gr" rows="10" 
                                        style=" min-width:500px; max-width:100%;min-height:50px;height:100%;width:100%; resize:none" runat="server">
                                    </textarea>
                                </div>  
                                <div class="col-xs-2" id="resultdiv" runat="server">
                                    <asp:Literal runat="server" ID="result"></asp:Literal>
                                </div>  
                            </div><hr />
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-6">
                                        <asp:dropdownlist runat="server" id="objetoEncontardo" class="caja-gr" autopostback="true" OnSelectedIndexChanged="objetoEncontardo_SelectedIndexChanged">
                                        </asp:dropdownlist>
                                    </div>
                                    <div class="col-xs-4">
                                        <asp:dropdownlist runat="server" id="codigoEncontrado" class="caja-md" autopostback="true" OnSelectedIndexChanged="codigoEncontrado_SelectedIndexChanged">
                                        </asp:dropdownlist>
                                    </div>
                                    <div class="col-xs-2">
                                        <asp:dropdownlist runat="server" id="precioEncontrado" class="caja-pq" autopostback="true" OnSelectedIndexChanged="precioEncontrado_SelectedIndexChanged">

                                        </asp:dropdownlist>
                                    </div>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-5">
                                        <asp:TextBox id="objetoBuscado" class="caja-md" placeholder="Repuesto" runat="server" />
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" id="cantidad" class="caja-pq" placeholder="Uds" runat="server">
                                    </div>
                                    <div class="col-xs-2">
                                        <asp:Button ID="verrepuesto" runat="server" Text="VER" class="btn btn-primary" autopostback="true" 
                                            OnClick="verrepuesto_Click" />
                                    </div>
                                    <div class="col-xs-2">
                                        <asp:Button ID="añadirrepuesto" runat="server" Text="AÑADIR" class="btn btn-primary" autopostback="true" OnClick="añadirrepuesto_Click" />
                                    </div>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12">
                                        <asp:dropdownlist runat="server" id="tipolista" autopostback="true" class="caja-md"> 
                                            <asp:listitem text="LISTA CAFÉ" value="1"></asp:listitem>
                                            <asp:listitem text="LISTA GENERAL" value="2"></asp:listitem>
                                            <asp:listitem text="LISTA BERLYS" value="3"></asp:listitem>
                                            <asp:listitem text="LISTA STARBUCKS" value="4"></asp:listitem>
                                        </asp:dropdownlist>
                                    </div>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-2">
                                        <asp:Label class="caja-pq" runat="server">CAFE____</asp:Label>
                                    </div>
                                    <div class="col-xs-6">
                                        <asp:dropdownlist runat="server" id="listacafe" autopostback="true" class="caja-gr" OnTextChanged="listacafe_TextChanged"> 
                                        </asp:dropdownlist>
                                    </div>
                                    <div class="col-xs-4">
                                        <asp:dropdownlist runat="server" id="listacafecod" autopostback="true" class="caja-md" OnTextChanged="listacafecod_TextChanged"> 
                                        </asp:dropdownlist>
                                    </div>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-2">
                                        <asp:Label class="caja-pq" runat="server">GENERAL_</asp:Label>
                                    </div>
                                    <div class="col-xs-6">                                       
                                        <asp:dropdownlist runat="server" id="listageneral" autopostback="true" class="caja-gr" OnTextChanged="listageneral_TextChanged"> 
                                        </asp:dropdownlist>
                                    </div>
                                    <div class="col-xs-4">
                                        <asp:dropdownlist runat="server" id="listageneralcod" autopostback="true" class="caja-md" OnTextChanged="listageneralcod_TextChanged"> 
                                        </asp:dropdownlist>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5">
                        <div class="panel panel-default">
                            <div class="panel-heading">REPUESTOS</div>
                            <div class="panel-body">     
                                <div class="row">
                                    <div class="col-xs-6">
                                        <asp:ListBox id="repuestos" class="caja-gr" rows="19" style="resize:none" runat="server" autopostback="true">                                            
                                        </asp:ListBox>
                                    </div>
                                    <div class="col-xs-3">
                                        <asp:ListBox id="codRepuestos" class="caja-pq" rows="19" style="resize:none" runat="server" autopostback="true" >                                            
                                        </asp:ListBox>
                                    </div>
                                    <div class="col-xs-3">
                                        <asp:ListBox id="unidades" class="caja-pq" rows="19" style="resize:none" runat="server" autopostback="true">                                            
                                        </asp:ListBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <asp:Button ID="limpiarrepuesto" runat="server" Text="LIMPIAR" class="btn btn-primary" autopostback="true" OnClick="limpiarrepuesto_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-xs-3">
                    </div>
                    <div class="col-xs-3">
                        <asp:Button ID="imprimirregistro" runat="server" Text="IMPRIMIR REGISTRO" class="btn btn-link btn-lg btn-block" OnClick="imprimirregistro_Click" />
                    </div>

                    <div class="col-xs-3">
                        <asp:Button ID="altaregistro" runat="server" Text="GUARDAR REGISTRO" class="btn btn-success btn-lg btn-block" onclick="altaregistro_Click" />
                    </div>
                </div>
            </div>
            </div>
      </div>
</div>


</asp:Content>
