﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GraficoOrdenTaller01.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Ordentaller.GraficoOrdenTaller01" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">
    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">GRAFICO DE ORDENES DE TALLER</h3></div>
        <div class="panel-body">     
            
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Label class="caja-pq" runat="server">MES</asp:Label>
                                    <asp:dropdownlist runat="server" id="mes" class="caja-md"> 
                                        <asp:listitem text="01-31 ENERO" value="1"></asp:listitem>
                                        <asp:listitem text="02-28 FEBRERO" value="2"></asp:listitem>
                                        <asp:listitem text="03-31 MARZO" value="3"></asp:listitem>
                                        <asp:listitem text="04-30 ABRIL" value="4"></asp:listitem>
                                        <asp:listitem text="05-31 MAYO" value="5"></asp:listitem>
                                        <asp:listitem text="06-30 JUNIO" value="6"></asp:listitem>
                                        <asp:listitem text="07-31 JULIO" value="7"></asp:listitem>
                                        <asp:listitem text="08-31 AGOSTO" value="8"></asp:listitem>
                                        <asp:listitem text="09-30 SEPTIEMBRE" value="9"></asp:listitem>
                                        <asp:listitem text="10-31 OCTUBRE" value="10"></asp:listitem>
                                        <asp:listitem text="11-30 NOVIEMBRE" value="11"></asp:listitem>
                                        <asp:listitem text="12-31 DICIEMBRE" value="12"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-3">
                                    <asp:Label class="caja-pq" runat="server">AÑO</asp:Label>
                                    <asp:dropdownlist runat="server" id="año" class="caja-md"> 
                                        <asp:listitem text="2020" value="2020"></asp:listitem>
                                        <asp:listitem text="2021" value="2021"></asp:listitem>
                                        <asp:listitem text="2022" value="2022"></asp:listitem>
                                        <asp:listitem text="2023" value="2023"></asp:listitem>
                                        <asp:listitem text="2024" value="2024"></asp:listitem>
                                        <asp:listitem text="2025" value="2025"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-6">
                                    <asp:Label class="caja-pq" runat="server">SELECCIONA MES/AÑO</asp:Label>
                                    <asp:CheckBox ID="check" runat="server" OnCheckedChanged="check_CheckedChanged" autopostback="true"/>
                                </div>
                            </div>
                            <div class="row"><hr /></div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:Label class="caja-pq" runat="server">Fecha Inicio</asp:Label>
                                    <input type="text" id="fechainicio" class="caja-pq" placeholder="Inicio" runat="server">
                                </div>
                                <div class="col-xs-5">
                                    <asp:Label class="caja-pq" runat="server">Fecha Fin</asp:Label>
                                    <input type="text" id="fechafin" class="caja-pq" placeholder="Fin" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="actualizar" runat="server" Text="APLICAR FILTRO" class="btn btn-success btn-lg btn-block" 
                                        OnClick="actualizar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div id="divtitulo" runat="server">
        <h3><asp:Literal runat="server" ID="titulo"></asp:Literal></h3>
</div>   
       
    <asp:Chart ID="Chart1" runat="server">
        <Series>
            <asp:Series Name="Serie"></asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea"></asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</asp:Content>
