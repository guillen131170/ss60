﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ordentallertraspaso.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Ordentaller.ordentallertraspaso" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

    <br />
    <br />
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-12">
            <asp:Button ID="traspasoregistros" runat="server" Text="TRASPASO DE REGISTROS" class="btn btn-success btn-lg btn-block" OnClick="traspasoregistros_Click" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-12">
            <asp:Button ID="traspasoregistrosCopia" runat="server" Text="TRASPASO DE REGISTROS COPIA" class="btn btn-success btn-lg btn-block" OnClick="traspasoregistrosCopia_Click" />
        </div>
    </div>
</asp:Content>
