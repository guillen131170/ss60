﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Ordentaller
{
    public partial class ordentallerresolver : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int fin = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
                int inicio = fin - (fin % 100) + 1;
                fechainicio.Value = inicio.ToString();
                fechafin.Value = fin.ToString();
            }
        } 
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosOrdenTaller proceso = new procesosOrdenTaller();
            clordentaller registro = new clordentaller();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            //registro = proceso.obtenerRegistroId(idProducto);
            if (idProducto != -111)
            {
                try
                {

                    switch (e.CommandName)
                    {
                        case "AUTORIZAR_Click":
                            //Response.Redirect("~/WebPages/Public/DetalleProductos.aspx?id_producto=" + idProducto);
                            proceso.concluirOrden(idProducto, "CERRADO", "AUTORIZADO");
                            break;
                        case "RECHAZAR_Click":
                            //Response.Redirect("~/WebPages/Public/DetalleProductos.aspx?id_producto=" + idProducto);
                            proceso.concluirOrden(idProducto, "CERRADO", "RECHAZADO");
                            break;
                        case "ANULAR_Click":
                            //Response.Redirect("~/WebPages/Public/DetalleProductos.aspx?id_producto=" + idProducto);
                            proceso.concluirOrden(idProducto, "CERRADO", "ANULADO");
                            break;
                        case "ABRIR_Click":
                            //Response.Redirect("~/WebPages/Public/DetalleProductos.aspx?id_producto=" + idProducto);
                            proceso.concluirOrden(idProducto, "ABIERTO", "TRATAMIENTO");
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch (Exception ex)
                {
                    // recoge errores hacia el status text
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion


        #region PULSAR APLICAR FILTRO
        protected void actualizar_Click(object sender, EventArgs e)
        {
            int lista = mostrarLista();

            if (lista <= 0)
            {
                status.Text = "No se encontraron registros";
                statusdiv.Attributes["class"] = "alert alert-sucess";
            }
        }

        public int mostrarLista()
        {
            procesosOrdenTaller proceso = new procesosOrdenTaller();
            List<clordentaller> lista = new List<clordentaller>();
            try
            {
                lista = proceso.obtenerListaRegistro(Convert.ToInt32(fechainicio.Value),
                                                     Convert.ToInt32(fechafin.Value),
                                                     estado.SelectedItem.ToString(),
                                                     nombreProyecto.SelectedItem.ToString());
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Proyecto");
                dt.Columns.Add("Cliente");
                dt.Columns.Add("Actividad");
                dt.Columns.Add("Ods");
                dt.Columns.Add("Estado");
                dt.Columns.Add("Resultado");
                dt.Columns.Add("Orden");
                dt.Columns.Add("Oferta");
                foreach (clordentaller registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    if (registro.Proyecto.Length > 8)
                    {
                        dr[1] = registro.Proyecto.Remove(8);
                    }
                    else dr[1] = registro.Proyecto;
                    if (registro.Cliente.Length > 25)
                    {
                        dr[2] = registro.Cliente.Remove(25);
                    }
                    else dr[2] = registro.Cliente;
                    dr[3] = registro.Actividad;
                    dr[4] = registro.Ods;
                    dr[5] = registro.Estado;
                    dr[6] = registro.Resultado;
                    dr[7] = registro.Orden;
                    dr[8] = registro.Oferta;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion
    }
}