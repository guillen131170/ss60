﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ordentallerverlista.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Ordentaller.ordentallerverlista" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">
    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">VER LISTA DE ORDENES DE TALLER</h3></div>
        <div class="panel-body">     
            
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:dropdownlist runat="server" id="estado" class="caja-md" >
                                        <asp:listitem text="ABIERTAS/CERRADAS" value="1"></asp:listitem>
                                        <asp:listitem text="ABIERTAS" value="2"></asp:listitem>
                                        <asp:listitem text="CERRADAS" value="1"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                                <div class="col-xs-5">
                                <asp:dropdownlist runat="server" id="nombreProyecto" class="caja-md" >
                                        <asp:listitem text="CAFENTO" value="1"></asp:listitem>
                                        <asp:listitem text="CANDELAS" value="2"></asp:listitem>
                                        <asp:listitem text="LATITUD" value="3"></asp:listitem>
                                        <asp:listitem text="DELTA" value="4"></asp:listitem>
                                        <asp:listitem text="BLACKZI" value="5"></asp:listitem>
                                        <asp:listitem text="DUNKIN" value="6"></asp:listitem>
                                        <asp:listitem text="PROINDE" value="7"></asp:listitem>
                                        <asp:listitem text="STARBUCKS CAFE" value="8"></asp:listitem>
                                        <asp:listitem text="STARBUCKS FRIO" value="9"></asp:listitem>
                                        <asp:listitem text="IKEA" value="10"></asp:listitem>
                                        <asp:listitem text="BERLYS" value="11"></asp:listitem>
                                        <asp:listitem text="DANONE WATERS" value="12"></asp:listitem>
                                        <asp:listitem text="DANONE VENDING" value="13"></asp:listitem>
                                        <asp:listitem text="PEPSI" value="14"></asp:listitem>
                                        <asp:listitem text="SCHWEPPES" value="15"></asp:listitem>
                                        <asp:listitem text="HEINEKEN STC" value="16"></asp:listitem>
                                        <asp:listitem text="HEINEKEN EVENTO" value="17"></asp:listitem>
                                        <asp:listitem text="HEINEKEN ALIMENTACIÓN" value="18"></asp:listitem>
                                        <asp:listitem text="EDEN SPRING" value="19"></asp:listitem>
                                        <asp:listitem text="SPECTANK" value="20"></asp:listitem>
                                        <asp:listitem text="GINOS FRIO" value="21"></asp:listitem>
                                        <asp:listitem text="LACREM" value="22"></asp:listitem>
                                        <asp:listitem text="OTROS" value="23"></asp:listitem>
                                        <asp:listitem text="TODO" value="24"></asp:listitem>
                                        <asp:listitem text="CAFE TODO" value="25"></asp:listitem>
                                        <asp:listitem text="OTROS TODO" value="26"></asp:listitem>
                                    </asp:dropdownlist>
                                    </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="actualizar" runat="server" Text="APLICAR FILTRO" class="btn btn-success btn-lg btn-block" 
                                        OnClick="actualizar_Click" />
                                </div>
                            </div>
                            <div class="row"><hr /></div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <asp:Label class="caja-pq" runat="server">Fecha Inicio</asp:Label>
                                    <input type="text" id="fechainicio" class="caja-pq" placeholder="Inicio" runat="server">
                                </div>
                                <div class="col-xs-2">
                                    <asp:Label class="caja-pq" runat="server">Fecha Fin</asp:Label>
                                    <input type="text" id="fechafin" class="caja-pq" placeholder="Fin" runat="server">
                                </div>
                                <div class="col-xs-2">
                                    <asp:Label class="caja-pq" runat="server">Importe superior a </asp:Label>
                                    <input type="text" id="importe" class="caja-pq" placeholder="0" runat="server">
                                </div>
                                <div class="col-xs-3">
                                    <asp:Button ID="imprimir" runat="server" Text="IMPRIMIR TODO" class="btn btn-success btn-lg btn-block" 
                                        OnClick="imprimir_Click" />
                                </div>
                                <div class="col-xs-3">
                                    <asp:Button ID="informe" runat="server" Text="IMPRIMIR IMPORTE" class="btn btn-success btn-lg btn-block" 
                                        OnClick="imprimirimporte_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="table-responsive" style="margin-top: 30px;">      
    <asp:GridView runat="server" ID="tablaRegistros" class="table table-striped"
        DataKeyNames="Id" Visible="false" AutoGenerateColumns="true" GridLines="None" BorderWidth="0" OnRowCommand="tablaRegistros_RowCommand">
            <Columns>
                <asp:ButtonField ButtonType="Link" HeaderText="ODS" CommandName="ODS_Click" Text="ODS" />
            </Columns>
        <Columns>
                <asp:ButtonField ButtonType="Link" HeaderText="Detalle" CommandName="Detalle_Click" Text="Detalle" />
            </Columns>
    </asp:GridView>
</div>


</asp:Content>
