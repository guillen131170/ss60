﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Ordentaller
{
    public partial class ordentallermodificar : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion


        #region BUSCAR ORDEN DE TALLER
        protected void buscar_Click(object sender, EventArgs e)
        {
            clordentaller registro = new clordentaller();

            if (!id.Value.Equals(""))
            {
                if (comprobarSiExisteRegistro(Convert.ToInt32(id.Value)))
                {
                    registro = buscarRegistro(Convert.ToInt32(id.Value));
                    visualizarDatos(registro);
                }
            }
        }

        public void visualizarDatos(clordentaller registro)
        {
            //nombreTecnico.SelectedIndex = registro.Tecnico;
            codigoTecnico.Text = registro.Cod_Tecnico;
            //nombreProyecto.Value = registro.Proyecto;
            codigoProyecto.Text = registro.Cod_proyecto;
            nombrePDS.Value = registro.Cliente;
            codigoPDS.Value = registro.Pds;
            odsActividad.Value = registro.Ods;
            provincia.Value = registro.Provincia;
            //nombreActividad.Value = registro.Actividad;
            //tipoActividad.Value= registro.Tipo;
            //estado.Value = registro.Estado;
            //resultado.Value = registro.Resultado;
            fechaODS.Value = registro.F_trabajo.ToString();
            fechaTaller.Value = registro.F_repa.ToString();
            codigoEquipo.Value = registro.Equipo;
            nombreMaterial.Value= registro.Material;
            codigoMaterial.Value = registro.Codmaterial;
            nsAX.Value = registro.Ax;
            nsSAP.Value= registro.Sap;
            ordenTaller.Value = registro.Orden;
            pedido.Value = registro.Pedido;
            precioOferta.Value = registro.Oferta.ToString();
            precioRepuesto.Value = registro.Repuestos.ToString();
            precioDesplazamiento.Value = registro.Despl.ToString();
            precioMO.Value = registro.Mo.ToString();
            descripcion.Value = registro.Descripcion;
        }

        public bool comprobarSiExisteRegistro(int id)
        {
            procesosOrdenTaller proceso = new procesosOrdenTaller();
            bool busqueda = false;

            try
            {
                busqueda = proceso.comprobarDuplicados(id);
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return busqueda;
        }

        public clordentaller buscarRegistro(int id)
        {
            procesosOrdenTaller proceso = new procesosOrdenTaller();
            clordentaller registro = new clordentaller();

            try
            {
                registro = proceso.obtenerRegistroId(id);
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return registro;
        }
        #endregion


        #region SELECTED TEXT CHANGED
        protected void nombreTecnico_SelectedIndexChanged(object sender, EventArgs e)
        {
            estableceTecnico(nombreTecnico.SelectedIndex);
        }

        protected void nombreProyecto_SelectedIndexChanged(object sender, EventArgs e)
        {
            estableceCodProyecto(nombreProyecto.SelectedIndex);
        }

        #region DROPDOWNLIST TECNICO
        public void estableceTecnico(int i)
        {
            switch (i)
            {
                case 0:
                    codigoTecnico.Text = "MEBU0001";
                    break;
                case 1:
                    codigoTecnico.Text = "MEBU0002";
                    break;
                case 2:
                    codigoTecnico.Text = "MEBU0003";
                    break;
                case 3:
                    codigoTecnico.Text = "MEBU0004";
                    break;
                case 4:
                    codigoTecnico.Text = "MEBU0005";
                    break;
                case 5:
                    codigoTecnico.Text = "MEBU0006";
                    break;
                case 6:
                    codigoTecnico.Text = "RSTCBU01";
                    break;
                case 7:
                    codigoTecnico.Text = "MEPX0002";
                    break;
                case 8:
                    codigoTecnico.Text = "MEPX0003";
                    break;
                case 9:
                    codigoTecnico.Text = "MESA0001";
                    break;
                case 10:
                    codigoTecnico.Text = "MESA0002";
                    break;
                case 11:
                    codigoTecnico.Text = "MESA0003";
                    break;
                case 12:
                    codigoTecnico.Text = "MESA0004";
                    break;
                case 13:
                    codigoTecnico.Text = "MESA0006";
                    break;
                case 14:
                    codigoTecnico.Text = "MEVA0001";
                    break;
                case 15:
                    codigoTecnico.Text = "MEVA0002";
                    break;
                case 16:
                    codigoTecnico.Text = "MEVA0003";
                    break;
                case 17:
                    codigoTecnico.Text = "MEVA0004";
                    break;
                case 18:
                    codigoTecnico.Text = "MEVA0005";
                    break;
                case 19:
                    codigoTecnico.Text = "MEVA0006";
                    break;
                case 20:
                    codigoTecnico.Text = "MEVA0007";
                    break;
                case 21:
                    codigoTecnico.Text = "MEVA0008";
                    break;
                case 22:
                    codigoTecnico.Text = "MEVA0009";
                    break;
                case 23:
                    codigoTecnico.Text = "MEVA0010";
                    break;
                case 24:
                    codigoTecnico.Text = "MEVA0011";
                    break;
                case 25:
                    codigoTecnico.Text = "MEVA0012";
                    break;
                case 26:
                    codigoTecnico.Text = "MEVA0013";
                    break;
                case 27:
                    codigoTecnico.Text = "RSTCVA01";
                    break;
                case 28:
                    codigoTecnico.Text = "OTRO";
                    break;
                default:
                    codigoTecnico.Text = "OTRO";
                    break;
            }
        }
        #endregion

        #region DROPDOWNLIST PROYECTO
        public void estableceCodProyecto(int i)
        {
            switch (i)
            {
                case 0:
                    codigoProyecto.Text = "335";
                    break;
                case 1:
                    codigoProyecto.Text = "332";
                    break;
                case 2:
                    codigoProyecto.Text = "007";
                    break;
                case 3:
                    codigoProyecto.Text = "369";
                    break;
                case 4:
                    codigoProyecto.Text = "261";
                    break;
                case 5:
                    codigoProyecto.Text = "339";
                    break;
                case 6:
                    codigoProyecto.Text = "000";
                    break;
                case 7:
                    codigoProyecto.Text = "264";
                    break;
                case 8:
                    codigoProyecto.Text = "303";
                    break;
                case 9:
                    codigoProyecto.Text = "301";
                    break;
                case 10:
                    codigoProyecto.Text = "273";
                    break;
                case 11:
                    codigoProyecto.Text = "365";
                    break;
                case 12:
                    codigoProyecto.Text = "365";
                    break;
                case 13:
                    codigoProyecto.Text = "349";
                    break;
                case 14:
                    codigoProyecto.Text = "004";
                    break;
                case 15:
                    codigoProyecto.Text = "001";
                    break;
                case 16:
                    codigoProyecto.Text = "002";
                    break;
                case 17:
                    codigoProyecto.Text = "330";
                    break;
                case 18:
                    codigoProyecto.Text = "284";
                    break;
                case 19:
                    codigoProyecto.Text = "281";
                    break;
                case 20:
                    codigoProyecto.Text = "291";
                    break;
                case 21:
                    codigoProyecto.Text = "009";
                    break;
                case 22:
                    codigoProyecto.Text = "037";
                    break;
                default:
                    codigoProyecto.Text = "000";
                    break;
            }
        }
        #endregion
        #endregion


        #region GUARDAR CAMBIOS
        protected void guardar_Click(object sender, EventArgs e)
        {
            if (erroresFormulario())
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            else
            {
                clordentaller orden = new clordentaller();
                orden.Tecnico = nombreTecnico.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.Cod_Tecnico = codigoTecnico.Text.TrimStart(' ').TrimEnd(' ');
                orden.Proyecto = nombreProyecto.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.Cod_proyecto = codigoProyecto.Text.TrimStart(' ').TrimEnd(' ');
                orden.Cliente = nombrePDS.Value.TrimStart(' ').TrimEnd(' ');
                orden.Pds = codigoPDS.Value.TrimStart(' ').TrimEnd(' ');
                orden.Ods = odsActividad.Value.TrimStart(' ').TrimEnd(' ');
                orden.Provincia = provincia.Value.TrimStart(' ').TrimEnd(' ');
                orden.Actividad = nombreActividad.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.Tipo = tipoActividad.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.Estado = estado.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.Resultado = resultado.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.F_trabajo = Convert.ToInt32(fechaODS.Value.TrimStart(' ').TrimEnd(' '));
                orden.F_repa = Convert.ToInt32(fechaTaller.Value.TrimStart(' ').TrimEnd(' '));
                orden.Equipo = codigoEquipo.Value.TrimStart(' ').TrimEnd(' ');
                orden.Material = nombreMaterial.Value.TrimStart(' ').TrimEnd(' ');
                orden.Codmaterial = codigoMaterial.Value.TrimStart(' ').TrimEnd(' ');
                orden.Ax = nsAX.Value.TrimStart(' ').TrimEnd(' ');
                orden.Sap = nsSAP.Value.TrimStart(' ').TrimEnd(' ');
                orden.Orden = ordenTaller.Value.TrimStart(' ').TrimEnd(' ');
                orden.Pedido = pedido.Value.TrimStart(' ').TrimEnd(' ');
                orden.Oferta = (float)Convert.ToDouble(precioOferta.Value.TrimStart(' ').TrimEnd(' '));
                orden.Repuestos = (float)Convert.ToDouble(precioRepuesto.Value.TrimStart(' ').TrimEnd(' '));
                orden.Despl = (float)Convert.ToDouble(precioDesplazamiento.Value.TrimStart(' ').TrimEnd(' '));
                orden.Mo = (float)Convert.ToDouble(precioMO.Value.TrimStart(' ').TrimEnd(' '));
                orden.Descripcion = descripcion.Value.TrimStart(' ').TrimEnd(' ');
                orden.Fichero = orden.F_trabajo + " " + orden.Ods + " " +
                                orden.Pds + " " + orden.Ax + " " + orden.Cliente;

                procesosOrdenTaller proceso1 = new procesosOrdenTaller();
                if (proceso1.modificarOrden(Convert.ToInt32(id.Value), orden))
                {
                    status.Text = "Registro guardado";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
                else
                {
                    status.Text = "ERROR - NO SE HAN GUARDADO LOS CAMBIOS";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }

            }
        }

        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;

            if (odsActividad.Value.Equals(""))
            { error = true; }
            if (provincia.Value.Equals(""))
            { error = true; }
            if (fechaODS.Value.Equals(""))
            { error = true; }
            if (fechaTaller.Value.Equals(""))
            { error = true; }

            if (nombrePDS.Value.Equals(""))
            { error = true; }
            if (codigoPDS.Value.Equals(""))
            { error = true; }

            if (codigoEquipo.Value.Equals(""))
            { error = true; }
            if (nombreMaterial.Value.Equals(""))
            { error = true; }
            if (codigoMaterial.Value.Equals(""))
            { error = true; }
            if (nsSAP.Value.Equals(""))
            { error = true; }
            if (nsAX.Value.Equals(""))
            { error = true; }

            if (ordenTaller.Value.Equals(""))
            { error = true; }
            if (precioOferta.Value.Equals(""))
            { error = true; }
            if (precioMO.Value.Equals(""))
            { error = true; }
            if (precioRepuesto.Value.Equals(""))
            { error = true; }
            if (precioDesplazamiento.Value.Equals(""))
            { error = true; }

            if (descripcion.Value.Equals(""))
            { error = true; }

            return error;
        }
        #endregion
        #endregion
    }
}