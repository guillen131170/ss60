﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Ordentaller
{
    public partial class ordentaller : System.Web.UI.Page
    {
        #region CARGAR LA PÁGINA
        /// <summary>
        /// Carga la página
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            estableceCodProyecto(nombreProyecto.SelectedIndex);
            estableceTecnico(nombreTecnico.SelectedIndex);
            fechaTaller.Value = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")).ToString();
            //fechaODS.Value = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")).ToString();
            procesosRepuestoUsado proceso1 = new procesosRepuestoUsado();
            List<SAP_Repuestos_v001> lista1 = new List<SAP_Repuestos_v001>();
            procesosRepuestoUsado proceso2 = new procesosRepuestoUsado();
            List<SAP_Repuestos_General> lista2 = new List<SAP_Repuestos_General>();


            #region MyRegion
            /*
            #region DROPLIST CAFE
            procesosRepuestoUsado proceso1 = new procesosRepuestoUsado();
            List<SAP_Repuestos_v001> lista1 = new List<SAP_Repuestos_v001>();
            lista1 = proceso1.ObtenerRepuestosGeneralesV001PorNombre(objetoBuscado.Text);
            if (lista1.Count > 0)
            {
                listacafe.Items.Clear();
                listacafecod.Items.Clear();
                foreach (SAP_Repuestos_v001 o in lista1)
                {
                    listacafe.Items.Add(o.Name);
                    listacafecod.Items.Add(o.Code_sap);
                }
                listacafe.SelectedIndex = 0;
                listacafecod.SelectedIndex = 0;
            }
            else
            {
                listacafe.Items.Clear();
                listacafecod.Items.Clear();
            }
            #endregion

            #region DROPLIS GENERAL
            procesosRepuestoUsado proceso2 = new procesosRepuestoUsado();
            List<SAP_Repuestos_General> lista2 = new List<SAP_Repuestos_General>();
            lista2 = proceso2.ObtenerRepuestosGeneralesPorNombre(objetoBuscado.Text);
            if (lista2.Count > 0)
            {
                listageneral.Items.Clear();
                foreach (SAP_Repuestos_General o in lista2)
                {
                    listageneral.Items.Add(o.Name);
                }
                listageneral.SelectedIndex = 0;
            }
            else
            {
                listageneral.Items.Clear();
            }
            #endregion 
            */
            #endregion

            if (!IsPostBack)
            {
                HookOnFocus(this.Page as Control);

                #region MyRegion
                proceso1 = new procesosRepuestoUsado();
                lista1 = proceso1.ObtenerRepuestosGeneralesV001PorNombre(objetoBuscado.Text);
                if (lista1.Count > 0)
                {
                    listacafe.Items.Clear();
                    listacafecod.Items.Clear();
                    foreach (SAP_Repuestos_v001 o in lista1)
                    {
                        listacafe.Items.Add(o.Name);
                        listacafecod.Items.Add(o.Code_sap);
                    }
                    listacafe.SelectedIndex = 0;
                    listacafecod.SelectedIndex = 0;
                }
                else
                {
                    listacafe.Items.Clear();
                    listacafecod.Items.Clear();
                }

                lista2 = proceso2.ObtenerRepuestosGeneralesPorNombre(objetoBuscado.Text);
                if (lista2.Count > 0)
                {
                    listageneral.Items.Clear();
                    listageneralcod.Items.Clear();
                    foreach (SAP_Repuestos_General o in lista2)
                    {
                        listageneral.Items.Add(o.Name);
                        listageneralcod.Items.Add(o.Code_sap);
                    }
                    listageneral.SelectedIndex = 0;
                    listageneralcod.SelectedIndex = 0;
                }
                else
                {
                    listageneral.Items.Clear();
                    listageneralcod.Items.Clear();
                } 
                #endregion

            }
        }

        /// <span class="code-SummaryComment"><summary></span>
        /// This function goes recursively all child controls and sets 
        /// onfocus attribute if the control has one of defined types.
        /// <span class="code-SummaryComment"></summary></span>
        /// <span class="code-SummaryComment"><param name="CurrentControl">the control to hook.</param></span>
        private void HookOnFocus(Control CurrentControl)
        {
            //checks if control is one of TextBox, DropDownList, ListBox or Button
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add(
                   "onfocus",
                   "try{document.getElementById('__LASTFOCUS').value=this.id} catch (e) { }");
            //checks if the control has children
            if (CurrentControl.HasControls())
                //if yes do them all recursively
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }
        #endregion


        #region PULSAR BOTÓN GUARDAR REGISTRO
        /// <summary>
        /// Crea un registro nuevo
        /// </summary>
        protected void altaregistro_Click(object sender, EventArgs e)
        {
            if (erroresFormulario())
            {
                result.Text = "Revise los datos del formulario";
                resultdiv.Attributes["class"] = "alert alert-danger";
            }
            else
            {
                clordentaller orden = new clordentaller();
                orden.Tecnico = nombreTecnico.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.Cod_Tecnico = codigoTecnico.Text.TrimStart(' ').TrimEnd(' ');
                orden.Proyecto = nombreProyecto.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.Cod_proyecto = codigoProyecto.Text.TrimStart(' ').TrimEnd(' ');
                orden.Cliente = nombrePDS.Value.TrimStart(' ').TrimEnd(' ');
                orden.Pds = codigoPDS.Value.TrimStart(' ').TrimEnd(' ');
                orden.Ods = odsActividad.Value.TrimStart(' ').TrimEnd(' ');
                orden.Provincia = provincia.Value.TrimStart(' ').TrimEnd(' ');
                orden.Actividad = nombreActividad.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.Tipo = tipoActividad.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.Estado = estado.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.Resultado = resultado.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                orden.F_trabajo = Convert.ToInt32(fechaODS.Value.TrimStart(' ').TrimEnd(' '));
                orden.F_repa = Convert.ToInt32(fechaTaller.Value.TrimStart(' ').TrimEnd(' '));
                orden.Equipo = codigoEquipo.Value.TrimStart(' ').TrimEnd(' ');
                orden.Material = nombreMaterial.Value.TrimStart(' ').TrimEnd(' ');
                orden.Codmaterial = codigoMaterial.Value.TrimStart(' ').TrimEnd(' ');
                orden.Ax = nsAX.Value.TrimStart(' ').TrimEnd(' ');
                orden.Sap = nsSAP.Value.TrimStart(' ').TrimEnd(' ');
                orden.Orden = ordenTaller.Value.TrimStart(' ').TrimEnd(' ');
                orden.Pedido = pedido.Value.TrimStart(' ').TrimEnd(' ');
                orden.Oferta = (float)Convert.ToDouble(precioOferta.Value.TrimStart(' ').TrimEnd(' '));
                orden.Repuestos = (float)Convert.ToDouble(precioRepuesto.Value.TrimStart(' ').TrimEnd(' '));
                orden.Despl = (float)Convert.ToDouble(precioDesplazamiento.Value.TrimStart(' ').TrimEnd(' '));
                orden.Mo = (float)Convert.ToDouble(precioMO.Value.TrimStart(' ').TrimEnd(' '));
                orden.Descripcion = descripcion.Value.TrimStart(' ').TrimEnd(' ');
                orden.Fichero = orden.F_trabajo + " " + orden.Ods + " " +
                                orden.Pds + " " + orden.Ax + " " + orden.Cliente;

                procesosOrdenTaller proceso1 = new procesosOrdenTaller();
                if (!proceso1.InsertaRegistro(orden))
                {
                    result.Text = "Revise los datos del formulario";
                    resultdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    //obtiene el id de última orden taller
                    if (repuestos.Items.Count > 0 && codRepuestos.Items.Count > 0 && unidades.Items.Count > 0)
                    {
                        orden.Id = proceso1.obtenerUltimoRegistro().Id;
                        procesosRepuestoUsado proceso2 = new procesosRepuestoUsado();
                        List<clrepuestotaller> _repuestos = new List<clrepuestotaller>();
                        clrepuestotaller[] _repuesto = new clrepuestotaller[repuestos.Items.Count];
                        int i = 0;
                        for (i=0; i< repuestos.Items.Count; i++)
                        {
                            _repuesto[i] = new clrepuestotaller();
                            _repuesto[i].Id = orden.Id;
                        }
                        _repuestos.Clear();

                        #region AÑADE NOMBRE CODIGO Y CANTIDAD A CADA REPUESTO
                        i = 0;
                        foreach (Object campo1 in repuestos.Items)
                        {
                            _repuesto[i].Material = campo1.ToString();
                            i++;
                        }
                        i = 0;
                        foreach (Object campo2 in codRepuestos.Items)
                        {
                            _repuesto[i].Cod_pmaterial = campo2.ToString();
                            i++;
                        }
                        i = 0;
                        foreach (Object campo3 in unidades.Items)
                        {
                            _repuesto[i].Cantidad = Convert.ToInt32(campo3.ToString());
                            i++;
                        } 
                        #endregion

                        #region AÑADE LOS REPUESTOS A LA LISTA DE REPUESTOS
                        for (i = 0; i < repuestos.Items.Count; i++)
                        {
                            _repuestos.Add(_repuesto[i]);
                        } 
                        #endregion

                        foreach (clrepuestotaller registro in _repuestos)
                        {
                            if (false)//!proceso2.InsertaRegistro(registro))
                            {
                                result.Text = "Revise los datos del formulario";
                                resultdiv.Attributes["class"] = "alert alert-danger";
                            }
                            else
                            {
                                result.Text = "Registro guardado";
                                resultdiv.Attributes["class"] = "alert alert-success";
                            }
                        }
                    }
                    else
                    {
                        result.Text = "Registro guardado";
                        resultdiv.Attributes["class"] = "alert alert-sucess";
                    }
                }

            }
        }
        #endregion


        #region PULSAR BOTÓN IMPRIMIR - ORDEN TALLER
        protected void imprimirregistro_Click(object sender, EventArgs e)
        {
            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\Basura\" + nsAX.Value + "documento.pdf", FileMode.Create));

            // Le colocamos el título y el autor
            // **Nota: Esto no será visible en el documento
            doc.AddTitle("ORDEN DE TALLER");
            doc.AddCreator("Fco Guillén");
            // Abrimos el archivo
            doc.Open();

            string ods = odsActividad.Value;
            // Escribimos el encabezamiento en el documento
            doc.Add(new Paragraph(fechaODS.Value + " " + odsActividad.Value + " " +
                                codigoPDS.Value + " " + nsAX.Value + " " +
                                nombrePDS.Value));
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá el nombre, apellido y país
            // de nuestros visitante.
            PdfPTable tblPrueba = new PdfPTable(4);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 4;

            tblPrueba.AddCell("PROYECTO" + Chunk.NEWLINE + "" + nombreProyecto.SelectedItem.ToString());
            tblPrueba.AddCell("PDS" + Chunk.NEWLINE + "" + codigoPDS.Value);
            tblPrueba.AddCell("F.TRABAJO" + Chunk.NEWLINE + "" + fechaODS.Value);
            tblPrueba.AddCell("F.ORDEN" + Chunk.NEWLINE + "" + fechaTaller.Value);


            tblPrueba.AddCell("CLIENTE" + Chunk.NEWLINE + "" + nombrePDS.Value);
            tblPrueba.AddCell("");
            tblPrueba.AddCell("PROVINCIA" + Chunk.NEWLINE + "" + provincia.Value);
            tblPrueba.AddCell("");

            tblPrueba.AddCell("ODS" + Chunk.NEWLINE + "" + odsActividad.Value);
            tblPrueba.AddCell("");
            tblPrueba.AddCell("EQUIPO" + Chunk.NEWLINE + "" + codigoEquipo.Value);
            tblPrueba.AddCell("");
            doc.Add(tblPrueba);

            tblPrueba = new PdfPTable(2);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 4;
            tblPrueba.AddCell("MATERIAL" + Chunk.NEWLINE + "" + nombreMaterial.Value);
            tblPrueba.AddCell("COD.MATERIAL" + Chunk.NEWLINE + "" + codigoMaterial.Value);

            tblPrueba.AddCell("N.SERIE AX" + Chunk.NEWLINE + "" + nsAX.Value);
            tblPrueba.AddCell("N.SERIE SAP" + Chunk.NEWLINE + "" + nsSAP.Value);
            doc.Add(tblPrueba);

            tblPrueba = new PdfPTable(4);
            tblPrueba.WidthPercentage = 100;
            tblPrueba.DefaultCell.Padding = 4;
            tblPrueba.AddCell("ORDEN" + Chunk.NEWLINE + "" + ordenTaller.Value);
            tblPrueba.AddCell("PEDIDO" + Chunk.NEWLINE + "" + pedido.Value);
            tblPrueba.AddCell("ESTADO" + Chunk.NEWLINE + "" + estado.SelectedItem.ToString());
            tblPrueba.AddCell("ABIERTO/CERRADO" + Chunk.NEWLINE + "" + resultado.SelectedItem.ToString());

            tblPrueba.AddCell("OFERTA" + Chunk.NEWLINE + "" + precioOferta.Value);
            tblPrueba.AddCell("REPUESTO" + Chunk.NEWLINE + "" + precioRepuesto.Value);
            tblPrueba.AddCell("DESPLAZAMIENTO" + Chunk.NEWLINE + "" + precioDesplazamiento.Value);
            tblPrueba.AddCell("MANO DE OBRA" + Chunk.NEWLINE + "" + precioMO.Value);
            doc.Add(tblPrueba);

            // Escribimos el pie en el documento
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph("MANO DE OBRA, DESPLAZAMIENTO Y REPUESTOS"));
            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph(descripcion.Value));
            doc.Close();
            writer.Close();

            //string pdfPath = Path.Combine(Application.StartupPath, "archivo.pdf");
            string pdfPath = @"D:\Usuario\Desktop\Basura\" + nsAX.Value + "documento.pdf";
            Process.Start(pdfPath);

            Response.Clear();
        } 
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;

            if (odsActividad.Value.Equals(""))
            { error = true; }
            if (provincia.Value.Equals(""))
            { error = true; }
            if (fechaODS.Value.Equals(""))
            { error = true; }
            if (fechaTaller.Value.Equals(""))
            { error = true; }

            if (nombrePDS.Value.Equals(""))
            { error = true; }
            if (codigoPDS.Value.Equals(""))
            { error = true; }

            if (codigoEquipo.Value.Equals(""))
            { error = true; }
            if (nombreMaterial.Value.Equals(""))
            { error = true; }
            if (codigoMaterial.Value.Equals(""))
            { error = true; }
            if (nsSAP.Value.Equals(""))
            { error = true; }
            if (nsAX.Value.Equals(""))
            { error = true; }

            if (ordenTaller.Value.Equals(""))
            { error = true; }
            if (precioOferta.Value.Equals(""))
            { error = true; }
            if (precioMO.Value.Equals(""))
            { error = true; }
            if (precioRepuesto.Value.Equals(""))
            { error = true; }
            if (precioDesplazamiento.Value.Equals(""))
            { error = true; }

            if (descripcion.Value.Equals(""))
            { error = true; }

            return error;
        }
        #endregion


        #region GENERALES DROPDOWNLIST - TECNICO - PROYECTO
        #region SELECTED DROPDOWNLIST
        protected void nombreProyecto_SelectedIndexChanged(object sender, EventArgs e)
        {
            estableceCodProyecto(nombreProyecto.SelectedIndex);
        }

        protected void nombreTecnico_SelectedIndexChanged(object sender, EventArgs e)
        {
            estableceTecnico(nombreTecnico.SelectedIndex);
        }
        #endregion

        #region DROPDOWNLIST TECNICO
        public void estableceTecnico(int i)
        {
            switch (i)
            {
                case 0:
                    codigoTecnico.Text = "MEBU0001";
                    break;
                case 1:
                    codigoTecnico.Text = "MEBU0002";
                    break;
                case 2:
                    codigoTecnico.Text = "MEBU0003";
                    break;
                case 3:
                    codigoTecnico.Text = "MEBU0004";
                    break;
                case 4:
                    codigoTecnico.Text = "MEBU0005";
                    break;
                case 5:
                    codigoTecnico.Text = "MEBU0006";
                    break;
                case 6:
                    codigoTecnico.Text = "RSTCBU01";
                    break;
                case 7:
                    codigoTecnico.Text = "MEPX0002";
                    break;
                case 8:
                    codigoTecnico.Text = "MEPX0003";
                    break;
                case 9:
                    codigoTecnico.Text = "MESA0001";
                    break;
                case 10:
                    codigoTecnico.Text = "MESA0002";
                    break;
                case 11:
                    codigoTecnico.Text = "MESA0003";
                    break;
                case 12:
                    codigoTecnico.Text = "MESA0004";
                    break;
                case 13:
                    codigoTecnico.Text = "MESA0006";
                    break;
                case 14:
                    codigoTecnico.Text = "MEVA0001";
                    break;
                case 15:
                    codigoTecnico.Text = "MEVA0002";
                    break;
                case 16:
                    codigoTecnico.Text = "MEVA0003";
                    break;
                case 17:
                    codigoTecnico.Text = "MEVA0004";
                    break;
                case 18:
                    codigoTecnico.Text = "MEVA0005";
                    break;
                case 19:
                    codigoTecnico.Text = "MEVA0006";
                    break;
                case 20:
                    codigoTecnico.Text = "MEVA0007";
                    break;
                case 21:
                    codigoTecnico.Text = "MEVA0008";
                    break;
                case 22:
                    codigoTecnico.Text = "MEVA0009";
                    break;
                case 23:
                    codigoTecnico.Text = "MEVA0010";
                    break;
                case 24:
                    codigoTecnico.Text = "MEVA0011";
                    break;
                case 25:
                    codigoTecnico.Text = "MEVA0012";
                    break;
                case 26:
                    codigoTecnico.Text = "MEVA0013";
                    break;
                case 27:
                    codigoTecnico.Text = "RSTCVA01";
                    break;
                case 28:
                    codigoTecnico.Text = "OTRO";
                    break;
                default:
                    codigoTecnico.Text = "OTRO";
                    break;
            }
        }
        #endregion

        #region DROPDOWNLIST PROYECTO
        public void estableceCodProyecto(int i)
        {
            switch (i)
            {
                case 0:
                    codigoProyecto.Text = "335";
                    break;
                case 1:
                    codigoProyecto.Text = "332";
                    break;
                case 2:
                    codigoProyecto.Text = "007";
                    break;
                case 3:
                    codigoProyecto.Text = "369";
                    break;
                case 4:
                    codigoProyecto.Text = "261";
                    break;
                case 5:
                    codigoProyecto.Text = "339";
                    break;
                case 6:
                    codigoProyecto.Text = "000";
                    break;
                case 7:
                    codigoProyecto.Text = "264";
                    break;
                case 8:
                    codigoProyecto.Text = "303";
                    break;
                case 9:
                    codigoProyecto.Text = "301";
                    break;
                case 10:
                    codigoProyecto.Text = "273";
                    break;
                case 11:
                    codigoProyecto.Text = "365";
                    break;
                case 12:
                    codigoProyecto.Text = "365";
                    break;
                case 13:
                    codigoProyecto.Text = "349";
                    break;
                case 14:
                    codigoProyecto.Text = "004";
                    break;
                case 15:
                    codigoProyecto.Text = "001";
                    break;
                case 16:
                    codigoProyecto.Text = "002";
                    break;
                case 17:
                    codigoProyecto.Text = "330";
                    break;
                case 18:
                    codigoProyecto.Text = "284";
                    break;
                case 19:
                    codigoProyecto.Text = "281";
                    break;
                case 20:
                    codigoProyecto.Text = "291";
                    break;
                case 21:
                    codigoProyecto.Text = "009";
                    break;
                case 22:
                    codigoProyecto.Text = "037";
                    break;
                default:
                    codigoProyecto.Text = "000";
                    break;
            }
        }
        #endregion
        #endregion


        #region REPUESTOS
        #region AÑADIR REPUESTO A LISTBOX
        protected void añadirrepuesto_Click(object sender, EventArgs e)
        {
            /*
            if (!cantidad.Value.Equals("") && listacafe.Items.Count > 0 && listacafecod.Items.Count > 0)
            {
                descripcion.Value += listacafe.SelectedItem.ToString() + " " + listacafecod.SelectedItem.ToString() + " " +
                    cantidad.Value.ToString();
            }
            */

            if (!cantidad.Value.Equals("") && objetoEncontardo.Items.Count>0 && codigoEncontrado.Items.Count>0)
            {
                repuestos.Items.Add(objetoEncontardo.SelectedItem.ToString());
                codRepuestos.Items.Add(codigoEncontrado.SelectedItem.ToString());
                unidades.Items.Add(cantidad.Value.ToString());

                descripcion.Value += objetoEncontardo.SelectedItem.ToString() + " " + codigoEncontrado.SelectedItem.ToString() + " " + 
                    cantidad.Value.ToString();
            }
        }
        #endregion

        #region CAJA DE TEXTO BUSCAR REPUESTO
        protected void verrepuesto_Click(object sender, EventArgs e)
        {
            #region DROPLIST BUSCADOR
            procesosRepuestoUsado proceso = new procesosRepuestoUsado();
            List<SAP_Repuestos_v001> lista1 = new List<SAP_Repuestos_v001>();
            List<SAP_Repuestos_General> lista2 = new List<SAP_Repuestos_General>();
            List<SAP_Repuestos_v001> lista3 = new List<SAP_Repuestos_v001>();
            List<SAP_Repuestos_v001> lista4 = new List<SAP_Repuestos_v001>();
            if (tipolista.SelectedItem.ToString().Equals("LISTA GENERAL"))
            {
                if (!objetoBuscado.Text.Equals(""))
                {
                    lista2 = proceso.ObtenerRepuestosGeneralesPorNombre(objetoBuscado.Text);
                    if (lista2.Count > 0)
                    {
                        objetoEncontardo.Items.Clear();
                        codigoEncontrado.Items.Clear();
                        precioEncontrado.Items.Clear();
                        foreach (SAP_Repuestos_General o in lista2)
                        {
                            objetoEncontardo.Items.Add(o.Name);
                            codigoEncontrado.Items.Add(o.Code_sap);
                            precioEncontrado.Items.Add(o.Price.ToString());
                        }
                        objetoEncontardo.SelectedIndex = 0;
                        codigoEncontrado.SelectedIndex = 0;
                        precioEncontrado.SelectedIndex = 0;
                    }
                    else
                    {
                        objetoEncontardo.Items.Clear();
                        codigoEncontrado.Items.Clear();
                        precioEncontrado.Items.Clear();
                    }
                }
                else
                {
                    objetoEncontardo.Items.Clear();
                    codigoEncontrado.Items.Clear();
                    precioEncontrado.Items.Clear();
                }
            }
            else if (tipolista.SelectedItem.ToString().Equals("LISTA BERLYS"))
            {
                if (!objetoBuscado.Text.Equals(""))
                {
                    lista3 = proceso.ObtenerRepuestosBoomBlyPorNombre(objetoBuscado.Text);
                    if (lista3.Count > 0)
                    {
                        objetoEncontardo.Items.Clear();
                        codigoEncontrado.Items.Clear();
                        precioEncontrado.Items.Clear();
                        foreach (SAP_Repuestos_v001 o in lista3)
                        {
                            objetoEncontardo.Items.Add(o.Name);
                            codigoEncontrado.Items.Add(o.Code_sap);
                            precioEncontrado.Items.Add(o.Tipo.ToString());
                        }
                        objetoEncontardo.SelectedIndex = 0;
                        codigoEncontrado.SelectedIndex = 0;
                        precioEncontrado.SelectedIndex = 0;
                    }
                    else
                    {
                        objetoEncontardo.Items.Clear();
                        codigoEncontrado.Items.Clear();
                        precioEncontrado.Items.Clear();
                    }
                }
                else
                {
                    objetoEncontardo.Items.Clear();
                    codigoEncontrado.Items.Clear();
                    precioEncontrado.Items.Clear();
                }
            }
            else if (tipolista.SelectedItem.ToString().Equals("LISTA STARBUCKS"))
            {
                if (!objetoBuscado.Text.Equals(""))
                {
                    lista4 = proceso.ObtenerRepuestosBoomStarbucksPorNombre(objetoBuscado.Text);
                    if (lista4.Count > 0)
                    {
                        objetoEncontardo.Items.Clear();
                        codigoEncontrado.Items.Clear();
                        precioEncontrado.Items.Clear();
                        foreach (SAP_Repuestos_v001 o in lista4)
                        {
                            objetoEncontardo.Items.Add(o.Name);
                            codigoEncontrado.Items.Add(o.Code_sap);
                            precioEncontrado.Items.Add("");
                        }
                        objetoEncontardo.SelectedIndex = 0;
                        codigoEncontrado.SelectedIndex = 0;
                        precioEncontrado.SelectedIndex = 0;
                    }
                    else
                    {
                        objetoEncontardo.Items.Clear();
                        codigoEncontrado.Items.Clear();
                        precioEncontrado.Items.Clear();
                    }
                }
                else
                {
                    objetoEncontardo.Items.Clear();
                    codigoEncontrado.Items.Clear();
                    precioEncontrado.Items.Clear();
                }
            }            
            else
            {
                if (!objetoBuscado.Text.Equals(""))
                {
                    lista1 = proceso.ObtenerRepuestosGeneralesV001PorNombre(objetoBuscado.Text);
                    if (lista1.Count > 0)
                    {
                        objetoEncontardo.Items.Clear();
                        codigoEncontrado.Items.Clear();
                        precioEncontrado.Items.Clear();
                        foreach (SAP_Repuestos_v001 o in lista1)
                        {
                            objetoEncontardo.Items.Add(o.Name);
                            codigoEncontrado.Items.Add(o.Code_sap);
                            precioEncontrado.Items.Add(o.Price.ToString());
                        }
                        objetoEncontardo.SelectedIndex = 0;
                        codigoEncontrado.SelectedIndex = 0;
                        precioEncontrado.SelectedIndex = 0;
                    }
                    else
                    {
                        objetoEncontardo.Items.Clear();
                        codigoEncontrado.Items.Clear();
                        precioEncontrado.Items.Clear();
                    }
                }
                else
                {
                    objetoEncontardo.Items.Clear();
                    codigoEncontrado.Items.Clear();
                    precioEncontrado.Items.Clear();
                }
            }
            #endregion
        }
        #endregion

        #region LIMPIAR DROPDOWNLIST REPUESTOS
        protected void limpiarrepuesto_Click(object sender, EventArgs e)
        {
            repuestos.Items.Clear();
            codRepuestos.Items.Clear();
            unidades.Items.Clear();
        }
        #endregion

        protected void objetoEncontardo_SelectedIndexChanged(object sender, EventArgs e)
        {
            codigoEncontrado.SelectedIndex = objetoEncontardo.SelectedIndex;
            precioEncontrado.SelectedIndex = objetoEncontardo.SelectedIndex;
        }

        protected void precioEncontrado_SelectedIndexChanged(object sender, EventArgs e)
        {
            codigoEncontrado.SelectedIndex = precioEncontrado.SelectedIndex;
            objetoEncontardo.SelectedIndex = precioEncontrado.SelectedIndex;
        }

        protected void codigoEncontrado_SelectedIndexChanged(object sender, EventArgs e)
        {
            precioEncontrado.SelectedIndex = codigoEncontrado.SelectedIndex;
            objetoEncontardo.SelectedIndex = codigoEncontrado.SelectedIndex;
        }

        protected void listacafe_TextChanged(object sender, EventArgs e)
        {
            listacafecod.SelectedIndex = listacafe.SelectedIndex;
        }

        protected void listacafecod_TextChanged(object sender, EventArgs e)
        {
            listacafe.SelectedIndex = listacafecod.SelectedIndex;
        }

        protected void listageneral_TextChanged(object sender, EventArgs e)
        {
            listageneralcod.SelectedIndex = listageneral.SelectedIndex;
        }

        protected void listageneralcod_TextChanged(object sender, EventArgs e)
        {
            listageneral.SelectedIndex = listageneralcod.SelectedIndex;
        }
        #endregion


    }
}