﻿using SolutionS60.APPLICATION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Ordentaller
{
    public partial class GraficoOrdenTaller01 : System.Web.UI.Page
    {
        private int totalDias = 0;
        private string inicio;
        private string fin;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int fin = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
                int inicio = fin - (fin % 100) + 1;
                fechainicio.Value = inicio.ToString();
                fechafin.Value = fin.ToString();
            }

        }


        protected void ObtenerDatos(string[] nombres, int[] barras)
        {
            int dd = ((Convert.ToInt32(inicio)) % 100);
            int mm = (((Convert.ToInt32(inicio)) / 100) % 100);
            int aa = ((Convert.ToInt32(inicio)) / 10000);
            int ddd = ((Convert.ToInt32(fin)) % 100);
            int mmm = (((Convert.ToInt32(fin)) / 100) % 100);
            int aaa = ((Convert.ToInt32(fin)) / 10000);
            titulo.Text = "PERIODO DE " + dd.ToString() + "/" + mm.ToString() + "/" + aa.ToString() + " A " +
                ddd.ToString() + "/" + mmm.ToString() + "/" + aaa.ToString();
            divtitulo.Attributes["class"] = "alert alert-sucess";
            int valor = ((Convert.ToInt32(inicio)) % 100);
            if (valor > 30000)
            {
                Chart1.Width = 30000;
            }
            else Chart1.Width = 800 * ((Convert.ToInt32(inicio)) % 100);
            Chart1.Series["Serie"].Points.DataBindXY(nombres, barras);
        }


        protected void actualizar_Click(object sender, EventArgs e)
        {
            procesosOrdenTaller proceso = new procesosOrdenTaller();
            if (check.Checked == true)
            {
                inicio = año.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ') + mes.SelectedItem.ToString().Substring(0, 2) + 
                    "01";
                fin = año.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ') + mes.SelectedItem.ToString().Substring(0, 2) +
                    mes.SelectedItem.ToString().Substring(3, 2);
            }
            else
            {
                inicio = fechainicio.Value;
                fin = fechafin.Value;
            }

            if (!inicio.Equals("") && !fin.Equals("") && (Convert.ToInt32(inicio) <= Convert.ToInt32(fin)))
            {
                totalDias = Convert.ToInt32(fin) - Convert.ToInt32(inicio);
                int[] barras = new int[totalDias + 1];
                string[] nombres = new string[totalDias + 1];
                string[] fechas = new string[totalDias + 1];
                for (int i = 0; i <= totalDias; i++)
                {
                    nombres[i] = (((Convert.ToInt32(inicio))%100) + i).ToString();
                }

                for (int i = 0; i <= totalDias; i++)
                {
                    fechas[i] = (Convert.ToInt32(inicio) + i).ToString();
                }

                //var seed = Environment.TickCount;
                //var random = new Random(seed);
                //var value = random.Next(0, 50);
                int valor;
                for (int i=0; i<=totalDias; i++)
                {
                    valor = proceso.obtenerGrafico1(Convert.ToInt32(fechas[i]));
                    if (valor > 0)
                    {
                        barras[i] = valor;
                    }
                    else
                    {
                        barras[i] = 0;
                    }
                }

                ObtenerDatos(nombres, barras);
            }
        }


        protected void check_CheckedChanged(object sender, EventArgs e)
        {
            if (check.Checked == true)
            {
                fechainicio.Value = "";
                fechafin.Value = "";
            }
            else
            {
                int fin = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
                int inicio = fin - (fin % 100) + 1;
                fechainicio.Value = inicio.ToString();
                fechafin.Value = fin.ToString();
            }
        }
    }
}