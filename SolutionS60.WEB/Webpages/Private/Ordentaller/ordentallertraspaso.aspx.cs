﻿using CafeS60.CORE;
using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Ordentaller
{
    public partial class ordentallertraspaso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void traspasoregistros_Click(object sender, EventArgs e)
        {
            return;
            procesosOrdenTaller proceso1 = new procesosOrdenTaller();
            List<OBSOLETO_OTaller> lista1 = new List<OBSOLETO_OTaller>();
            lista1 = proceso1.obtenerListaAntigua();

            foreach (OBSOLETO_OTaller item in lista1)
            {
                clordentaller orden = new clordentaller();
                if (item.Tecnico.Contains("JOS"))
                {
                    orden.Tecnico = "JOSÉ ESTÉVEZ";
                    orden.Cod_Tecnico = "MEVA0012";
                }
                else if (item.Tecnico.Contains("MIGUE"))
                {
                    orden.Tecnico = "MIGUEL SANCHIDRIÁN";
                    orden.Cod_Tecnico = "MEVA0010";
                }
                else if (item.Tecnico.Contains("HILARIO"))
                {
                    orden.Tecnico = "HILARIO DUVAL";
                    orden.Cod_Tecnico = "MEVA0001";
                }
                else if (item.Tecnico.Contains("CARLOS"))
                {
                    orden.Tecnico = "CARLOS GARCÍA";
                    orden.Cod_Tecnico = "MEVA0002";
                }
                else
                {
                    orden.Tecnico = "FRANCISCO GUILLÉN";
                    orden.Cod_Tecnico = "RSTCVA01";
                }
                orden.Proyecto = item.Proyecto.TrimStart(' ').TrimEnd(' ');
                if (orden.Proyecto.Contains("CAFEN"))
                {
                    orden.Cod_proyecto = "335";
                }
                else if (orden.Proyecto.Contains("CAND"))
                {
                    orden.Cod_proyecto = "332";
                }
                else if (orden.Proyecto.Contains("SIH"))
                {
                    orden.Cod_proyecto = "007";
                }
                else if (orden.Proyecto.Contains("LATI"))
                {
                    orden.Cod_proyecto = "007";
                }
                else if (orden.Proyecto.Contains("BLACK"))
                {
                    orden.Cod_proyecto = "261";
                }
                else if (orden.Proyecto.Contains("DELT"))
                {
                    orden.Cod_proyecto = "369";
                }
                else if (orden.Proyecto.Contains("DUNK"))
                {
                    orden.Cod_proyecto = "339";
                }
                else orden.Cod_proyecto = "000";
                orden.Cliente = item.Cliente.TrimStart(' ').TrimEnd(' ');
                orden.Pds = item.Pds.TrimStart(' ').TrimEnd(' ');
                if (!item.Oaveria.Equals(""))
                {
                    orden.Ods = item.Oaveria.TrimStart(' ').TrimEnd(' ');
                }
                else if (!item.Odesmontaje.Equals(""))
                {
                    orden.Ods = item.Odesmontaje.TrimStart(' ').TrimEnd(' ');
                }
                else if (!item.Omontaje.Equals(""))
                {
                    orden.Ods = item.Omontaje.TrimStart(' ').TrimEnd(' ');
                } 
                else orden.Ods = "TALLER";
                orden.Provincia =item.Provincia.TrimStart(' ').TrimEnd(' ');
                orden.Actividad = item.Tipo.TrimStart(' ').TrimEnd(' ');
                orden.Tipo = "ORDEN CALLE";
                orden.Estado = item.Estado.TrimStart(' ').TrimEnd(' ');
                orden.Resultado = item.Resultado.TrimStart(' ').TrimEnd(' ');
                orden.F_trabajo = item.F_trabajo;
                orden.F_repa = item.F_repa;
                orden.Equipo = "";
                orden.Material = item.Material.TrimStart(' ').TrimEnd(' ');
                orden.Codmaterial = item.Codmaterial.TrimStart(' ').TrimEnd(' ');
                orden.Ax = item.Ax.TrimStart(' ').TrimEnd(' ');
                orden.Sap = item.Sap.TrimStart(' ').TrimEnd(' ');
                orden.Orden = item.Norden.TrimStart(' ').TrimEnd(' ');
                orden.Pedido = "";
                orden.Oferta = item.Oferta;
                orden.Despl = item.Despl;
                orden.Mo = item.Mo;
                orden.Repuestos = orden.Oferta - orden.Despl - orden.Mo;
                orden.Descripcion = item.Repuesto.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", "").TrimStart(' ').TrimEnd(' ');
                orden.Fichero = orden.F_trabajo + " " + orden.Ods + " " +
                                orden.Pds + " " + orden.Ax + " " + orden.Cliente;

                proceso1.InsertaRegistro(orden);
            }
        }


        protected void traspasoregistrosCopia_Click(object sender, EventArgs e)
        {
            return;
            procesosRepuestoUsado proceso = new procesosRepuestoUsado();
            proceso.traspasoRegistros2();

            /*
            procesosOrdenTaller proceso = new procesosOrdenTaller();
            List<clordentaller> listaCopia = new List<clordentaller>();
            listaCopia = proceso.obtenerListaCopia();

            foreach (clordentaller item in listaCopia)
            {
                proceso.modificarOrdenFechas(item.Id, item.F_trabajo, item.F_repa);
            }
            */
        }
    }
}