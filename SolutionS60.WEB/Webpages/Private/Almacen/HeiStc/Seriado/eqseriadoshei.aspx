﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="eqseriadoshei.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Almacen.HeiStc.Seriado.eqseriadoshei" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">

    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">CREAR ENTRADA DE EQUIPO SERIADO HEINEKEN</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">PROCEDENCIA</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="Text" id="procedencia" class="caja-gr" placeholder="Prodedencia" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">      
                                    <input type="text" id="odsprocedencia" class="caja-md" placeholder="ODS" runat="server">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">UBICACIÓN</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" id="almacen" class="caja-gr" placeholder="Almacén" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" id="estado" class="caja-md" placeholder="Estado" runat="server">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">DESTINO</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" id="destino" class="caja-gr" placeholder="Destino" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" id="odsdestino" class="caja-md" placeholder="ODS" runat="server">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">EQUIPO</div>
                <div class="panel-body">     
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="text" id="nombreMaterial" class="caja-gr" placeholder="Material" runat="server">
                        </div>
                        <div class="col-xs-8">
                            <input type="text" id="codigoMaterial" class="caja-md" placeholder="Código" runat="server">
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="text" id="nsSAP" class="caja-gr" placeholder="SAP" runat="server">
                        </div>
                        <div class="col-xs-8">
                            <input type="text" id="nsAX" class="caja-md" placeholder="AX" runat="server">
                        </div>
                    </div>
                 </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-6">
                        <asp:Button ID="altaregistro" runat="server" Text="GUARDAR REGISTRO" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="altaregistro_Click" />
                    </div>
                    <div class="col-xs-2"></div>
                </div>
            </div>
            </div>
      </div>
</div>

</asp:Content>
