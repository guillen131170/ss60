﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.HeiStc.Seriado
{
    public partial class eqseriadosheisalida : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;

            if (procedencia.Value.Equals(""))
            { error = true; }
            if (almacen.Value.Equals(""))
            { error = true; }
            if (estado.Value.Equals(""))
            { error = true; }
            if (nombreMaterial.Value.Equals(""))
            { error = true; }
            if (codigoMaterial.Value.Equals(""))
            { error = true; }
            if (nsAX.Value.Equals(""))
            { error = true; }

            return error;
        }
        #endregion


        #region PULSAR BOT{ON GUARDAR REGISTRO
        protected void altaregistro_Click(object sender, EventArgs e)
        {

            if (!erroresFormulario())
            {
                cleqseriadocafento equipo = new cleqseriadocafento();
                equipo.NombreMaterial = nombreMaterial.Value.TrimStart(' ').TrimEnd(' ');
                equipo.CodigoMaterial = codigoMaterial.Value.TrimStart(' ').TrimEnd(' ');
                equipo.CodSAP = nsSAP.Value.TrimStart(' ').TrimEnd(' ');
                equipo.CodAX = nsAX.Value.TrimStart(' ').TrimEnd(' ');
                equipo.Origen = procedencia.Value.TrimStart(' ').TrimEnd(' ');
                equipo.ODSEntrada = odsprocedencia.Value.TrimStart(' ').TrimEnd(' ');
                equipo.Ubicacion = almacen.Value.TrimStart(' ').TrimEnd(' ');
                equipo.Destino = destino.Value.TrimStart(' ').TrimEnd(' ');
                equipo.ODSSalida = odsdestino.Value.TrimStart(' ').TrimEnd(' ');
                equipo.Estado = estado.Value.TrimStart(' ').TrimEnd(' ');
                equipo.Stock = 1;

                procesosEqCafento proceso = new procesosEqCafento();
                if (!proceso.InsertaRegistro(equipo))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado";
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }

        }
        #endregion
    }
}