﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.Sch.NoSeriado
{
    public partial class eqnoseriadosch : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosEqHeiStc proceso = new procesosEqHeiStc();

            if (nombreMaterial.Value.Equals("")) { error = true; }
            else
            {
                if (proceso.buscaMaterial(nombreMaterial.Value) >0) error = true;
            }
            if (codigoMaterial.Value.Equals("")) { error = true; }
            else
            {
                if (proceso.buscaCodigo(codigoMaterial.Value) > 0) error = true;
            }

            return error;
        }
        #endregion


        #region PULSAR BOT{ON GUARDAR REGISTRO
        protected void altaregistro_Click(object sender, EventArgs e)
        {

            if (!erroresFormulario())
            {
                cleqnoseriadoheistd equipo = new cleqnoseriadoheistd();
                equipo.NombreMaterial = nombreMaterial.Value.TrimStart(' ').TrimEnd(' ');
                equipo.CodigoMaterial = codigoMaterial.Value.TrimStart(' ').TrimEnd(' ');
                if (stock.Value.Equals("")) { equipo.Stock = 0; }
                else equipo.Stock = Convert.ToInt32(stock.Value);

                procesosEqHeiStc proceso = new procesosEqHeiStc();
                if (!proceso.InsertaRegistro(equipo))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado";
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }

        }
        #endregion
    }
}