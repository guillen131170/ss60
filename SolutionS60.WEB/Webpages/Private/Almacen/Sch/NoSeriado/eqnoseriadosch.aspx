﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="eqnoseriadosch.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Almacen.Sch.NoSeriado.eqnoseriadosch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">

    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">CREAR EQUIPO NUEVO NO SERIADO DE SCHWEPPES EVENTO</h3>
        </div>
        <div class="panel-body">

            <div class="panel panel-default">
                <div class="panel-heading">EQUIPO</div>
                <div class="panel-body">     
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="text" id="nombreMaterial" class="caja-gr" placeholder="Material" runat="server">
                        </div>
                        <div class="col-xs-3">
                            <input type="text" id="codigoMaterial" class="caja-md" placeholder="Código" runat="server">
                        </div>
                        <div class="col-xs-5">
                            <input type="text" id="stock" class="caja-pq" placeholder="Stock" runat="server">
                        </div>
                    </div>
                 </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-6">
                        <asp:Button ID="altaregistro" runat="server" Text="GUARDAR REGISTRO" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="altaregistro_Click" />
                    </div>
                    <div class="col-xs-2"></div>
                </div>
            </div>
            </div>
      </div>
</div>

</asp:Content>
