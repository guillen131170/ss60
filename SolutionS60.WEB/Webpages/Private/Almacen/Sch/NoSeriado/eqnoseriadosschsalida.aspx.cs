﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.Sch.NoSeriado
{
    public partial class eqnoseriadosschsalida : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                procesosEqHeiStc proceso1 = new procesosEqHeiStc();
                List<cleqnoseriadoheistd> lista1 = new List<cleqnoseriadoheistd>();
                //HookOnFocus(this.Page as Control);

                #region MyRegion
                proceso1 = new procesosEqHeiStc();
                lista1 = proceso1.InformeBasicoRegistro();
                if (lista1.Count > 0)
                {
                    int i = 0;
                    listageneral.Items.Clear();
                    foreach (cleqnoseriadoheistd o in lista1)
                    {
                        listageneral.Items.Add(o.NombreMaterial);
                        listageneral.SelectedIndex = i;
                        listageneral.SelectedItem.Value = o.CodigoMaterial;
                        i++;
                    }
                    listageneral.SelectedIndex = 0;
                }
                else
                {
                    listageneral.Items.Clear();
                }
                #endregion
            }
        }
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosEqHeiStc proceso = new procesosEqHeiStc();

            if (proceso.buscaMaterial(listageneral.SelectedItem.Text) > 0) error = true;
            if (destino.Value.Equals("")) { error = true; }
            if (stock.Value.Equals("")) { error = true; }

            return error;
        }
        #endregion


        #region PULSAR BOT{ON GUARDAR REGISTRO
        protected void altaregistro_Click(object sender, EventArgs e)
        {

            if (!erroresFormulario())
            {
                cleqnoseriadoheistd equipo = new cleqnoseriadoheistd();
                equipo.NombreMaterial = listageneral.SelectedItem.Text.TrimStart(' ').TrimEnd(' ');
                equipo.CodigoMaterial = listageneral.SelectedItem.Value.TrimStart(' ').TrimEnd(' ');
                equipo.Origen = "";
                equipo.ODSEntrada = "";
                equipo.Destino = destino.Value.TrimStart(' ').TrimEnd(' ');
                equipo.ODSSalida = odsdestino.Value.TrimStart(' ').TrimEnd(' ');
                if (stock.Value.Equals("")) { equipo.Stock = 0; }
                else equipo.Stock = Convert.ToInt32(stock.Value);
                equipo.Fecha = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));

                procesosEqHeiStc proceso = new procesosEqHeiStc();
                if (!proceso.SalidaMovimiento(equipo) && !proceso.Salida(equipo.Id, equipo.Stock))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado";
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }

        }
        #endregion
    }
}