﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.Sch.Seriado
{
    public partial class eqseriadoschmodificar : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<cleqschseriado> lista = new List<cleqschseriado>();
                lista = obtenerLista();
                Session["lista"] = lista;
                Session["cabezal"] = 0;
                Session["numReg"] = lista.Count;
                mostrarRegistro((int)Session["cabezal"]);
                indice.Value = ((int)Session["cabezal"] + 1).ToString() + " de " + Session["numReg"].ToString();

                if (lista.Count <= 0)
                {
                    status.Text = "No se encontraron registros";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }

        public void mostrarRegistro(int cabezal)
        {
            nsSAP.Value = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].CodSAP;
            nsAX.Value = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].CodAX;
            procedencia.Value = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].Origen;
            odsprocedencia.Value = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].ODSEntrada;
            almacen.Value = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].Ubicacion;
            estado.Value = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].Estado;
            destino.Value = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].Destino;
            odsdestino.Value = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].ODSSalida;
            nombreMaterial.Value = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].NombreMaterial;
            codigoMaterial.Value = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].CodigoMaterial;
        }

        public List<cleqschseriado> obtenerLista()
        {
            procesosEqSch proceso = new procesosEqSch();
            List<cleqschseriado> lista = new List<cleqschseriado>();
            try
            {
                lista = proceso.InformeBasicoRegistroSeriado();
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return lista;
        }
        #endregion


        #region BOTONES ANTERIOR - SIGUIENTE
        protected void anterior_Click(object sender, EventArgs e)
        {
            if ((int)Session["cabezal"] > 0)
            {
                Session["cabezal"] = (int)Session["cabezal"] - 1;
                mostrarRegistro((int)Session["cabezal"]);
                indice.Value = ((int)Session["cabezal"] + 1).ToString() + " de " + Session["numReg"].ToString();
                status.Text = "OK";
                statusdiv.Attributes["class"] = "alert alert-sucess";
            }
            else
            {
                status.Text = "No hay más registros";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }

        protected void siguiente_Click(object sender, EventArgs e)
        {
            if ((int)Session["cabezal"] < (int)Session["numReg"] - 1)
            {
                Session["cabezal"] = (int)Session["cabezal"] + 1;
                mostrarRegistro((int)Session["cabezal"]);
                indice.Value = ((int)Session["cabezal"] + 1).ToString() + " de " + Session["numReg"].ToString();
                status.Text = "OK";
                statusdiv.Attributes["class"] = "alert alert-sucess";
            }
            else
            {
                status.Text = "No hay más registros";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }
        #endregion


        #region BOTON GUARDAR CAMBIOS
        protected void guardar_Click(object sender, EventArgs e)
        {
            if (!erroresFormulario())
            {
                cleqschseriado equipo = new cleqschseriado();
                equipo.NombreMaterial = nombreMaterial.Value;
                equipo.CodigoMaterial = codigoMaterial.Value;
                equipo.CodSAP = nsSAP.Value;
                equipo.CodAX = nsAX.Value;
                equipo.Origen = procedencia.Value;
                equipo.ODSEntrada = odsprocedencia.Value;
                equipo.Ubicacion = almacen.Value;
                equipo.Destino = destino.Value;
                equipo.ODSSalida = odsdestino.Value;
                equipo.Estado = estado.Value;
                equipo.Stock = 1;

                int id = ((List<cleqschseriado>)Session["lista"])[(int)Session["cabezal"]].Id;
                procesosEqSch proceso = new procesosEqSch();
                if (!proceso.modificarEquipo(id, equipo))
                {
                    status.Text = "Error al guardar los cambios";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro modificado";
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }
        #endregion


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;

            if (procedencia.Value.Equals(""))
            { error = true; }
            if (almacen.Value.Equals(""))
            { error = true; }
            if (estado.Value.Equals(""))
            { error = true; }
            if (nombreMaterial.Value.Equals(""))
            { error = true; }
            if (codigoMaterial.Value.Equals(""))
            { error = true; }
            if (nsAX.Value.Equals(""))
            { error = true; }

            return error;
        }
        #endregion
    }
}