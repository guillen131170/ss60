﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.Sch.Seriado
{
    public partial class eqseriadoschinfsalida : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                /*
                int fin = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
                int inicio = fin - (fin % 100) + 1;
                fechainicio.Value = inicio.ToString();
                fechafin.Value = fin.ToString();
                */
                //ods.Value = "";
                //destino.Value = "";
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }

        public int mostrarLista()
        {
            procesosEqSch proceso = new procesosEqSch();
            List<cleqschseriado> lista = new List<cleqschseriado>();
            try
            {
                lista = proceso.InformeBasicoRegistroSeriado();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("NombreMaterial");
                dt.Columns.Add("CodigoMaterial");
                dt.Columns.Add("CodSAP");
                dt.Columns.Add("CodAX");
                dt.Columns.Add("Origen");
                //dt.Columns.Add("ODSEntrada");
                foreach (cleqschseriado registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.NombreMaterial;
                    dr[2] = registro.CodigoMaterial;
                    dr[3] = registro.CodSAP;
                    dr[4] = registro.CodAX;
                    dr[5] = registro.Origen;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosEqSch proceso = new procesosEqSch();
            cleqschseriado registro = new cleqschseriado();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            //registro = proceso.obtenerRegistroId(idProducto);
            if (idProducto != -111)
            {
                try
                {

                    switch (e.CommandName)
                    {
                        case "Salida_Click":
                            //Response.Redirect("~/WebPages/Public/DetalleProductos.aspx?id_producto=" + idProducto);
                            //if (ods.Value.ToString().Equals("") || destino.Value.ToString().Equals("")) proceso.sacarEquipo(idProducto);
                            //else proceso.sacarEquipo(idProducto, ods.Value.ToString(), destino.Value.ToString());
                            if (!ods.Value.ToString().Equals("") && !destino.Value.ToString().Equals(""))
                                proceso.sacarEquipo(idProducto, ods.Value.ToString(), destino.Value.ToString());
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch (Exception ex)
                {
                    // recoge errores hacia el status text
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion


        #region SACAR INFORME PDF
        protected void informe_Click(object sender, EventArgs e)
        {
            List<cleqschseriado> lista = new List<cleqschseriado>();
            procesosEqSch proceso = new procesosEqSch();
            lista = proceso.InformeBasicoRegistroSeriado();

            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\documento.pdf", FileMode.Create));
            //doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            doc.Open();
            ///doc.Add(Chunk.NEWLINE);
            ///doc.Add(new Paragraph("LISTA DE EQUIPOS PARA SALIR"));
            foreach (cleqschseriado o in lista)
            {
                ///doc.Add(new Paragraph(o.NombreMaterial));
                ///doc.Add(new Paragraph(o.CodigoMaterial + " - " + o.CodSAP + " - " + o.CodAX));
                ///doc.Add(new Paragraph(o.Origen));
                ///doc.Add(Chunk.NEWLINE);         
                ///doc.Add(new Paragraph(o.NombreMaterial + " " + o.CodAX + " " + o.Origen));
                string c = o.Origen.Remove(0, 10);
                //c = c.Remove(c.Length-10,10);
                doc.Add(new Paragraph(o.CodAX + " " + o.NombreMaterial + " " + c));
            }
            doc.Close();
            writer.Close();

            string pdfPath = @"D:\Usuario\Desktop\documento.pdf";
            Process.Start(pdfPath);
        }
        #endregion


        #region ACTUALIZAR LISTA
        protected void actuaizar_Click(object sender, EventArgs e)
        {
            int lista = -111;
            lista = mostrarLista();

            if (lista <= 0)
            {
                status.Text = "No se encontraron registros";
                statusdiv.Attributes["class"] = "alert alert-sucess";
            }
        } 
        #endregion
    }
}