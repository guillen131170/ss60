﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.HeiEve
{
    public partial class eqnoseriadosheistock : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }

        public int mostrarLista()
        {
            procesosEqHeiEve proceso = new procesosEqHeiEve();
            List<cleqseriadoheieve> lista = new List<cleqseriadoheieve>();
            try
            {
                lista = proceso.InformeBasicoRegistro();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Nombre de Material");
                dt.Columns.Add("Código de Material");
                dt.Columns.Add("Stock");
                foreach (cleqseriadoheieve registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.NombreMaterial;
                    dr[2] = registro.CodigoMaterial;
                    dr[3] = registro.Stock;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosEqHeiEve proceso = new procesosEqHeiEve();
            cleqseriadoheieve registro = new cleqseriadoheieve();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            if (idProducto != -111)
            {
                try
                {

                    switch (e.CommandName)
                    {
                        case "Salida_Click":
                            /*if (!ods.Value.ToString().Equals("") && !destino.Value.ToString().Equals(""))
                                proceso.sacarEquipo(idProducto, ods.Value.ToString(), destino.Value.ToString());*/
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch (Exception ex)
                {
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion
    }
}