﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="eqnoseriadosheientrada.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Almacen.HeiEve.eqnoseriadosheientrada" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">

    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">CREAR ENTRADA DE EQUIPO NO SERIADO HEINEKEN - EVENTOS</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">ORIGEN DE LA ENTRADA</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="Text" id="procedencia" class="caja-gr" placeholder="Prodedencia" runat="server">
                                </div>
                                <div class="col-xs-6">      
                                    <input type="text" id="odsprocedencia" class="caja-md" placeholder="ODS" runat="server">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">EQUIPO</div>
                <div class="panel-body">     
                    <div class="row">
                        <div class="col-xs-7">                                       
                             <asp:dropdownlist runat="server" id="listageneral" autopostback="true" class="caja-gr" 
                                 style=" min-width:120px; max-width:100%;min-height:30px;height:60%;width:60%; resize:none"> 
                             </asp:dropdownlist>
                        </div>
                        <div class="col-xs-5">
                            <input type="text" id="stock" class="caja-pq" placeholder="Stock" runat="server">
                        </div>
                    </div>
                 </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-6">
                        <asp:Button ID="altaregistro" runat="server" Text="GUARDAR REGISTRO" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="altaregistro_Click" />
                    </div>
                    <div class="col-xs-2"></div>
                </div>
            </div>
            </div>
      </div>
</div>

</asp:Content>
