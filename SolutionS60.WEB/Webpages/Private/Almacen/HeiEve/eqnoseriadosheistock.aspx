﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="eqnoseriadosheistock.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Almacen.HeiEve.eqnoseriadosheistock" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

    <div class="table-responsive" style="margin-top: 30px;">      
        <asp:GridView runat="server" ID="tablaRegistros" class="table table-striped"
        DataKeyNames="Id" Visible="false" AutoGenerateColumns="true" GridLines="None" BorderWidth="0" OnRowCommand="tablaRegistros_RowCommand">
            <Columns>
                <%--<asp:ButtonField ButtonType="Link" HeaderText="Salida" Text="sacar" CommandName="Salida_Click" />--%>
                <asp:ButtonField ButtonType="Link" CommandName="Salida_Click"  />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
