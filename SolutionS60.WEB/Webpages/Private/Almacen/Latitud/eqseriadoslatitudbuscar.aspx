﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="eqseriadoslatitudbuscar.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Almacen.Latitud.eqseriadoslatitudbuscar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">BUSCAR EQUIPO SERIADO LATITUD POR NÚMERO DE SERIE</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">PROCEDENCIA</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="Text" id="procedencia" class="caja-gr" placeholder="Prodedencia" runat="server" readonly>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">      
                                    <input type="text" id="odsprocedencia" class="caja-md" placeholder="ODS" runat="server" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">UBICACIÓN</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" id="almacen" class="caja-gr" placeholder="Almacén" runat="server" readonly>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" id="estado" class="caja-md" placeholder="Estado" runat="server" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">DESTINO</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" id="destino" class="caja-gr" placeholder="Destino" runat="server" readonly>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" id="odsdestino" class="caja-md" placeholder="ODS" runat="server" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">EQUIPO</div>
                <div class="panel-body">     
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="text" id="nombreMaterial" class="caja-gr" placeholder="Material" runat="server" readonly>
                        </div>
                        <div class="col-xs-8">
                            <input type="text" id="codigoMaterial" class="caja-md" placeholder="Código" runat="server" readonly>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="text" id="nsSAP" class="caja-gr" placeholder="SAP" runat="server" readonly>
                        </div>
                        <div class="col-xs-8">
                            <input type="text" id="nsAX" class="caja-md" placeholder="AX" runat="server" readonly>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="text" id="stock" class="caja-gr" placeholder="" runat="server" readonly>
                        </div>
                        <div class="col-xs-8"></div>
                    </div>
                 </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <br /><br />
                    <div class="col-xs-2"></div>
                    <div class="col-xs-4">
                        <input type="text" id="ax" class="caja-md" placeholder="NS AX" runat="server">
                    </div>
                    <div class="col-xs-6">
                        <asp:Button ID="buscar" runat="server" Text="BUSCAR" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="buscar_Click" />
                    </div>
                </div>
            </div>
            </div>
      </div>   

</asp:Content>
