﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.Latitud
{
    public partial class eqseriadoslatitudbuscar : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion


        #region BUSCAR EQUIPO
        protected void buscar_Click(object sender, EventArgs e)
        {
            if (!ax.Value.Equals(""))
            {
                cleqseriadolatitud equipo = new cleqseriadolatitud();
                procesosEqLatitud proceso = new procesosEqLatitud();
                equipo = proceso.ultimoEstadoEquipo(ax.Value);

                if (equipo != null)
                {
                    nombreMaterial.Value = equipo.NombreMaterial;
                    codigoMaterial.Value = equipo.CodigoMaterial;
                    nsSAP.Value = equipo.CodSAP;
                    nsAX.Value = equipo.CodAX;
                    procedencia.Value = equipo.Origen;
                    odsprocedencia.Value = equipo.ODSEntrada;
                    almacen.Value = equipo.Ubicacion;
                    destino.Value = equipo.Destino;
                    odsdestino.Value = equipo.ODSSalida;
                    estado.Value = equipo.Estado;
                    if (equipo.Stock == 1)
                    {
                        stock.Value = "EQUIPO EN ALMACEN";
                    }
                    else
                    {
                        stock.Value = "EQUIPO FUERA DE SISTEMA";
                    }

                    status.Text = "OK";
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
                else
                {                    
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        } 
        #endregion
    }
}