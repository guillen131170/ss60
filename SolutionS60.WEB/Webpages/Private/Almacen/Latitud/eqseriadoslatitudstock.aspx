﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="eqseriadoslatitudstock.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Almacen.Latitud.eqseriadoslatitudstock" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
            </div>
                <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">FILTRO LATITUD - SIH</h3></div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-xs-6">
                                        <input type="Text" id="ods" class="caja-md" placeholder="ODS" runat="server">
                                </div>
                                <div class="col-xs-6">
                                        <input type="Text" id="destino" class="caja-gr" placeholder="DESTINO" runat="server">
                                </div>
                            </div>  
                            <div class="row">
                                <br /><br />
                                <div class="col-xs-4">
                                    <input type="text" id="modelo" class="caja-md" placeholder="Modelo" runat="server">
                                </div>
                                <div class="col-xs-4">
                                    <asp:Button ID="buscar" runat="server" Text="BUSCAR" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="buscar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <div class="table-responsive" style="margin-top: 30px;">      
        <asp:GridView runat="server" ID="tablaRegistros" class="table table-striped"
         DataKeyNames="Id" Visible="false" AutoGenerateColumns="true" GridLines="None" BorderWidth="0" OnRowCommand="tablaRegistros_RowCommand">
            <Columns>
                <asp:ButtonField ButtonType="Link" HeaderText="Salida" Text="sacar" CommandName="Salida_Click"  />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
