﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.Latitud
{
    public partial class eqseriadosbuscarlista : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public int mostrarListaAx()
        {
            procesosEqLatitud proceso = new procesosEqLatitud();
            List<cleqseriadolatitud> lista = new List<cleqseriadolatitud>();
            try
            {
                lista = proceso.movimientosAx(ax.Value);
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("NombreMaterial");
                dt.Columns.Add("CodigoMaterial");
                dt.Columns.Add("CodSAP");
                dt.Columns.Add("CodAX");
                dt.Columns.Add("Origen");
                dt.Columns.Add("Destino");
                dt.Columns.Add("Stock");
                //dt.Columns.Add("ODSEntrada");
                foreach (cleqseriadolatitud registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.NombreMaterial;
                    dr[2] = registro.CodigoMaterial;
                    dr[3] = registro.CodSAP;
                    dr[4] = registro.CodAX;
                    dr[5] = registro.Origen;
                    dr[6] = registro.Destino;
                    dr[7] = registro.Stock.ToString();
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }

        public int mostrarListaModelo()
        {
            procesosEqLatitud proceso = new procesosEqLatitud();
            List<cleqseriadolatitud> lista = new List<cleqseriadolatitud>();
            try
            {
                lista = proceso.movimientosModelo(modelo.Value);
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("NombreMaterial");
                dt.Columns.Add("CodigoMaterial");
                dt.Columns.Add("CodSAP");
                dt.Columns.Add("CodAX");
                dt.Columns.Add("Origen");
                dt.Columns.Add("Destino");
                dt.Columns.Add("Stock");
                //dt.Columns.Add("ODSEntrada");
                foreach (cleqseriadolatitud registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.NombreMaterial;
                    dr[2] = registro.CodigoMaterial;
                    dr[3] = registro.CodSAP;
                    dr[4] = registro.CodAX;
                    dr[5] = registro.Origen;
                    dr[6] = registro.Destino;
                    dr[7] = registro.Stock.ToString();
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion


        #region BUSCAR EQUIPO
        protected void buscar_Click(object sender, EventArgs e)
        {
            if (!ax.Value.Equals("") && modelo.Value.Equals(""))
            {
                mostrarListaAx();
            }
            else if (ax.Value.Equals("") && !modelo.Value.Equals(""))
            {
                mostrarListaModelo();
            }
            else
            {
                Response.Redirect("/Webpages/Private/Almacen/Latitud/eqseriadosbuscarlista.aspx");
            }
        }
        #endregion
    }
}