﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="repuestocrear.aspx.cs" Inherits="SolutionS60.WEB.Webpages.Private.Almacen.Repuestos.repuestocrear" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>

<div class="form-horizontal-inline" role="form">

    <br />
    <br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">CREAR NUEVO REPUESTO</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">REGISTRO</div>
                        <div class="panel-body">     
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" id="referencia" class="caja-md" placeholder="Referencia" runat="server">
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" id="precio" class="caja-pq" placeholder="Precio" runat="server">
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <textarea  id="nombre" class="caja-gr" rows="3" style="resize:none" runat="server">
                                    </textarea>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <textarea  id="descripcion" class="caja-gr" rows="3" style="resize:none" runat="server">
                                    </textarea>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:dropdownlist runat="server" id="proveedor" autopostback="true" class="caja-md"> 
                                            <asp:listitem text="EUNASA" value="1"></asp:listitem>
                                        </asp:dropdownlist>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:dropdownlist runat="server" id="familia" autopostback="true" class="caja-md"> 
                                            <asp:listitem text="CAFETERA" value="1"></asp:listitem>
                                            <asp:listitem text="MOLINO" value="1"></asp:listitem>
                                            <asp:listitem text="HORNO" value="1"></asp:listitem>
                                            <asp:listitem text="ARCÓN" value="1"></asp:listitem>
                                            <asp:listitem text="VITRINA" value="1"></asp:listitem>
                                    </asp:dropdownlist>
                                </div>
                            </div><br />
                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-6">
                        <asp:Button ID="altaregistro" runat="server" Text="GUARDAR REGISTRO" class="btn btn-success btn-lg btn-block" autopostback="true" OnClick="altaregistro_Click" />
                    </div>
                    <div class="col-xs-2"></div>
                </div>
            </div>
            </div>
      </div>
</div>

</asp:Content>
