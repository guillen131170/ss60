﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.Repuestos
{
    public partial class repuestocrear : System.Web.UI.Page
    {
        #region CARGAR PÁGINA
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        //H
        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosRepuestos consulta = new procesosRepuestos();

            if (consulta.comprobarDuplicados(referencia.Value))
            { error = true; }
            if (nombre.Value.Equals(""))
            { error = true; }
            if (referencia.Value.Equals(""))
            { error = true; }

            return error;
        }


        #endregion


        #region INSERTAR REPUESTO NUEVO
        protected void altaregistro_Click(object sender, EventArgs e)
        {
            /*procesosRepuestoUsado p = new procesosRepuestoUsado();
            p.traspasoRegistros();
            return;*/

            if (!erroresFormulario())
            {
                clrepuestos item = new clrepuestos();
                item.Precio = (float)Convert.ToDouble(precio.Value.TrimStart(' ').TrimEnd(' '));
                item.Nombre = nombre.Value.TrimStart(' ').TrimEnd(' ');
                item.Descripcion = descripcion.Value.TrimStart(' ').TrimEnd(' ');
                item.Proveedor = proveedor.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                item.Referencia = referencia.Value.TrimStart(' ').TrimEnd(' ');
                item.Familia = familia.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                item.Imagen = "";
                item.Stock = 0;

                procesosRepuestos proceso = new procesosRepuestos();
                if (!proceso.InsertaRegistro(item))
                {
                    status.Text = "Error al guardar los datos";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = "Registro guardado";
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        } 
        #endregion
    }
}