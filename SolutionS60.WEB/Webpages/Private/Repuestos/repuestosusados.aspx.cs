﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SolutionS60.APPLICATION;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Repuestos
{
    public partial class repuestosusados : System.Web.UI.Page
    {
        private int totalDias = 0;
        private string inicio;
        private string fin;

        string[] materialBombaAbrazadera = new string[] { "200006694", "200006946", "200007408", "200006475", "200014870" };
        string[] materialBombaVibratoria = new string[] { "200006086", "200014871" };
        string[] materialBotonera = new string[] { "200014873", "200006536", "200006053", "200006860" };
        string[] materialCentralita = new string[] { "200014877" };
        string[] materialElectrovalvula = new string[] { "200006131", "200006798", "200007946", "200008221", "200006582",
            "200008162", "200006016", "200005788", "200005879", "200001710", "200005945", "200014884", "200014885" };
        string[] materialGrifoVapor = new string[] { "200006349", "200006076", "200014898" };
        string[] materialGrifoInfusion = new string[] { "200014890" };
        string[] materialAutoexpulsorGrupo = new string[] { "200014876" };
        string[] materialAutoexpulsorKit = new string[] { "200005779", "200014875" };
        string[] materialLanza = new string[] { "200006596", "200014899" };
        string[] materialPresostato= new string[] { "200000516", "200002240", "200014881", "200014882" };
        string[] materialSonda = new string[] { "200006436", "200007667", "200014880" };
        string[] materialTapavolumetrico = new string[] { "200006786", "200014901" };
        string[] materialTermostato = new string[] { "200014883" };
        string[] materialValvula = new string[] { "200005936", "200006379", "200005936", "200006379", "200014900" };
        string[] materialVolumetrico = new string[] { "200006136", "200006161", "200007876", "200002212", "200014902" };

        /*
        200007052 RESISTENCIA 1G
        200009577 RESISTENCIA 2G
        */
        public string[] materialusado;
        public int[] cantidadusada;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                /*
                if (Session["Usuario"] != null)
                {

                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }*/
                int fin = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
                int inicio = fin - (fin % 100) + 1;
                fechainicio.Value = inicio.ToString();
                fechafin.Value = fin.ToString();
            }
        }


protected void ObtenerDatos(string[] nombres, int[] barras)
{
    int dd = ((Convert.ToInt32(inicio)) % 100);
    int mm = (((Convert.ToInt32(inicio)) / 100) % 100);
    int aa = ((Convert.ToInt32(inicio)) / 10000);
    int ddd = ((Convert.ToInt32(fin)) % 100);
    int mmm = (((Convert.ToInt32(fin)) / 100) % 100);
    int aaa = ((Convert.ToInt32(fin)) / 10000);
    titulo.Text = "PERIODO DE " + dd.ToString() + "/" + mm.ToString() + "/" + aa.ToString() + " A " +
        ddd.ToString() + "/" + mmm.ToString() + "/" + aaa.ToString();
    divtitulo.Attributes["class"] = "alert alert-sucess";
    int valor = ((Convert.ToInt32(inicio)) % 100);
    if (valor > 30000)
    {
        Chart1.Width = 30000;
    }
    else Chart1.Width = 800 * ((Convert.ToInt32(inicio)) % 100);
    //Chart1.Series["Serie"].BorderWidth = 5;
    Chart1.Series["Serie"].Points.DataBindXY(nombres, barras);
}


protected void actualizar_Click(object sender, EventArgs e)
{
    int totalMaterial = 0; 
            if (CheckBomba.Checked == true) totalMaterial++;
            if (CheckVibratoria.Checked == true) totalMaterial++;
            if (CheckBotonera.Checked == true) totalMaterial++;
            if (CheckCentralita.Checked == true) totalMaterial++;
    if (CheckElectrovalvula.Checked == true) totalMaterial++;
            if (CheckGrifovapor.Checked == true) totalMaterial++;
            if (CheckGrifoinfusion.Checked == true) totalMaterial++;
            if (CheckAutoexpulsorgrupo.Checked == true) totalMaterial++;
            if (CheckAutoexpulsorkit.Checked == true) totalMaterial++;
            if (CheckLanza.Checked == true) totalMaterial++;
            if (CheckPresostato.Checked == true) totalMaterial++;
            if (CheckSonda.Checked == true) totalMaterial++;
            if (CheckTapavolumetrico.Checked == true) totalMaterial++;
            if (CheckTermostato.Checked == true) totalMaterial++;
            if (CheckValvula.Checked == true) totalMaterial++;
            if (CheckVolumetrico.Checked == true) totalMaterial++;



    procesosOrdenTaller proceso = new procesosOrdenTaller();
    if (check.Checked == true)
    {
        inicio = año.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ') + mes.SelectedItem.ToString().Substring(0, 2) +
            "01";
        fin = año.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ') + mes.SelectedItem.ToString().Substring(0, 2) +
            mes.SelectedItem.ToString().Substring(3, 2);
    }
    else
    {
        inicio = fechainicio.Value;
        fin = fechafin.Value;
    }

            if ((!inicio.Equals("") && !fin.Equals("")) &&
                (Convert.ToInt32(inicio) <= Convert.ToInt32(fin)) &&
                (totalMaterial > 0))
            {
                if (tecnico.Value.Equals("")) tecnico.Value = "MEVA0010";
                materialusado = new string[totalMaterial];
                cantidadusada = new int[totalMaterial];

                int[] barras = new int[totalMaterial];
                string[] nombres = new string[totalMaterial];

                int i = 0;
                if (CheckBomba.Checked == true)
                {
                    nombres[i] = Bomba.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialBombaAbrazadera);
                    
                    materialusado[i] = Bomba.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialBombaAbrazadera, tecnico.Value);
                    i++;
                }
                if (CheckVibratoria.Checked == true)
                {
                    nombres[i] = Vibratoria.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialBombaVibratoria);
                    materialusado[i] = Vibratoria.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialBombaVibratoria, tecnico.Value);
                    i++;
                }
                if (CheckBotonera.Checked == true) 
                {
                    nombres[i] = Botonera.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialBotonera);
                    materialusado[i] = Botonera.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialBotonera, tecnico.Value);
                    i++;
                }
                if (CheckCentralita.Checked == true)
                {
                    nombres[i] = Centralita.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialCentralita);
                    materialusado[i] = Centralita.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialCentralita, tecnico.Value);
                    i++;
                }
                if (CheckElectrovalvula.Checked == true)
                {
                    nombres[i] = Electrovalvula.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialElectrovalvula);
                    materialusado[i] = Electrovalvula.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialElectrovalvula, tecnico.Value);
                    i++;
                }
                if (CheckGrifovapor.Checked == true)
                {
                    nombres[i] = Grifovapor.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialGrifoVapor);
                    materialusado[i] = Grifovapor.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialGrifoVapor, tecnico.Value);
                    i++;
                }
                if (CheckGrifoinfusion.Checked == true) 
                {
                    nombres[i] = Grifoinfusion.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialGrifoInfusion);
                    materialusado[i] = Grifoinfusion.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialGrifoInfusion, tecnico.Value);
                    i++;
                }
                if (CheckAutoexpulsorgrupo.Checked == true)
                {
                    nombres[i] = Autoexpulsorgrupo.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialAutoexpulsorGrupo);
                    materialusado[i] = Autoexpulsorgrupo.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialAutoexpulsorGrupo, tecnico.Value);
                    i++;
                }
                if (CheckAutoexpulsorkit.Checked == true)
                {
                    nombres[i] = Autoexpulsorkit.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialAutoexpulsorKit);
                    materialusado[i] = Autoexpulsorkit.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialAutoexpulsorKit, tecnico.Value);
                    i++;
                }
                if (CheckLanza.Checked == true)
                {
                    nombres[i] = Lanza.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialLanza);
                    materialusado[i] = Lanza.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialLanza, tecnico.Value);
                    i++;
                }
                if (CheckPresostato.Checked == true) 
                {
                    nombres[i] = Presostato.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialPresostato);
                    materialusado[i] = Presostato.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialPresostato, tecnico.Value);
                    i++;
                }
                if (CheckSonda.Checked == true)
                {
                    nombres[i] = Sonda.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialSonda);
                    materialusado[i] = Sonda.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialSonda, tecnico.Value);
                    i++;
                }
                if (CheckTapavolumetrico.Checked == true)
                {
                    nombres[i] = Tapavolumetrico.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialTapavolumetrico);
                    materialusado[i] = Tapavolumetrico.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialTapavolumetrico, tecnico.Value);
                    i++;
                }
                if (CheckTermostato.Checked == true) 
                {
                    nombres[i] = Termostato.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialLanza);
                    materialusado[i] = Termostato.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialLanza, tecnico.Value);
                    i++;
                }
                if (CheckValvula.Checked == true)
                {
                    nombres[i] = Valvula.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialValvula);
                    materialusado[i] = Valvula.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialValvula, tecnico.Value);
                    i++;
                }
                if (CheckVolumetrico.Checked == true)
                {
                    nombres[i] = Volumetrico.ID.ToString();
                    barras[i] = proceso.obtenerGrafico2(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialVolumetrico);
                    materialusado[i] = Volumetrico.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialVolumetrico, tecnico.Value);
                    i++;
                }

                repuestos_tecnico.Text = "TECNICO: " + tecnico.Value + "<br>";
                repuestos_tecnico.Text += "-----------------------------" + "<br>";
                repuestos_tecnico.Text += "<br>";
                for (int contador=0; contador<i; contador++)
                {
                    repuestos_tecnico.Text += cantidadusada[contador].ToString() + " " + materialusado[contador] + "s" + "<br>";
                }
                divrepuestos_tecnico.Attributes["class"] = "alert alert-primary";

                ObtenerDatos(nombres, barras);
            }
}


protected void check_CheckedChanged(object sender, EventArgs e)
{
    if (check.Checked == true)
    {
        fechainicio.Value = "";
        fechafin.Value = "";
    }
    else
    {
        int fin = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
        int inicio = fin - (fin % 100) + 1;
        fechainicio.Value = inicio.ToString();
        fechafin.Value = fin.ToString();
    }
}

        protected void seleccionar_Click(object sender, EventArgs e)
        {
            CheckBomba.Checked = true;
            CheckVibratoria.Checked = true;
            CheckBotonera.Checked = true;
            CheckCentralita.Checked = true;
            CheckElectrovalvula.Checked = true;
            CheckGrifovapor.Checked = true;
            CheckAutoexpulsorgrupo.Checked = true;
            CheckAutoexpulsorgrupo.Checked = true;
            CheckAutoexpulsorkit.Checked = true;
            CheckLanza.Checked = true;
            CheckPresostato.Checked = true;
            CheckSonda.Checked = true;
            CheckTapavolumetrico.Checked = true;
            CheckTermostato.Checked = true;
            CheckValvula.Checked = true;
            CheckVolumetrico.Checked = true;

        }

        protected void deseleccionar_Click(object sender, EventArgs e)
        {
            CheckBomba.Checked = false;
            CheckVibratoria.Checked = false;
            CheckBotonera.Checked = false;
            CheckCentralita.Checked = false;
            CheckElectrovalvula.Checked = false;
            CheckGrifovapor.Checked = false;
            CheckAutoexpulsorgrupo.Checked = false;
            CheckAutoexpulsorgrupo.Checked = false;
            CheckAutoexpulsorkit.Checked = false;
            CheckLanza.Checked = false;
            CheckPresostato.Checked = false;
            CheckSonda.Checked = false;
            CheckTapavolumetrico.Checked = false;
            CheckTermostato.Checked = false;
            CheckValvula.Checked = false;
            CheckVolumetrico.Checked = false;
        }

        protected void imprimir_Click(object sender, EventArgs e)
        {
            int totalMaterial = 0;
            if (CheckBomba.Checked == true) totalMaterial++;
            if (CheckVibratoria.Checked == true) totalMaterial++;
            if (CheckBotonera.Checked == true) totalMaterial++;
            if (CheckCentralita.Checked == true) totalMaterial++;
            if (CheckElectrovalvula.Checked == true) totalMaterial++;
            if (CheckGrifovapor.Checked == true) totalMaterial++;
            if (CheckGrifoinfusion.Checked == true) totalMaterial++;
            if (CheckAutoexpulsorgrupo.Checked == true) totalMaterial++;
            if (CheckAutoexpulsorkit.Checked == true) totalMaterial++;
            if (CheckLanza.Checked == true) totalMaterial++;
            if (CheckPresostato.Checked == true) totalMaterial++;
            if (CheckSonda.Checked == true) totalMaterial++;
            if (CheckTapavolumetrico.Checked == true) totalMaterial++;
            if (CheckTermostato.Checked == true) totalMaterial++;
            if (CheckValvula.Checked == true) totalMaterial++;
            if (CheckVolumetrico.Checked == true) totalMaterial++;

            if (check.Checked == true)
            {
                inicio = año.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ') + mes.SelectedItem.ToString().Substring(0, 2) +
                    "01";
                fin = año.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ') + mes.SelectedItem.ToString().Substring(0, 2) +
                    mes.SelectedItem.ToString().Substring(3, 2);
            }
            else
            {
                inicio = fechainicio.Value;
                fin = fechafin.Value;
            }

            procesosOrdenTaller proceso = new procesosOrdenTaller();
            if ((!inicio.Equals("") && !fin.Equals("")) &&
                (Convert.ToInt32(inicio) <= Convert.ToInt32(fin)) && (totalMaterial > 0))
            {
                materialusado = new string[totalMaterial];
                cantidadusada = new int[totalMaterial];

                int i = 0;
                if (CheckBomba.Checked == true)
                {
                    materialusado[i] = Bomba.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialBombaAbrazadera, tecnico.Value);
                    i++;
                }
                if (CheckVibratoria.Checked == true)
                {
                    materialusado[i] = Vibratoria.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialBombaVibratoria, tecnico.Value);
                    i++;
                }
                if (CheckBotonera.Checked == true)
                {
                    materialusado[i] = Botonera.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialBotonera, tecnico.Value);
                    i++;
                }
                if (CheckCentralita.Checked == true)
                {
                    materialusado[i] = Centralita.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialCentralita, tecnico.Value);
                    i++;
                }
                if (CheckElectrovalvula.Checked == true)
                {
                    materialusado[i] = Electrovalvula.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialElectrovalvula, tecnico.Value);
                    i++;
                }
                if (CheckGrifovapor.Checked == true)
                {
                    materialusado[i] = Grifovapor.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialGrifoVapor, tecnico.Value);
                    i++;
                }
                if (CheckGrifoinfusion.Checked == true)
                {
                    materialusado[i] = Grifoinfusion.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialGrifoInfusion, tecnico.Value);
                    i++;
                }
                if (CheckAutoexpulsorgrupo.Checked == true)
                {
                    materialusado[i] = Autoexpulsorgrupo.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialAutoexpulsorGrupo, tecnico.Value);
                    i++;
                }
                if (CheckAutoexpulsorkit.Checked == true)
                {
                    materialusado[i] = Autoexpulsorkit.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialAutoexpulsorKit, tecnico.Value);
                    i++;
                }
                if (CheckLanza.Checked == true)
                {
                    materialusado[i] = Lanza.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialLanza, tecnico.Value);
                    i++;
                }
                if (CheckPresostato.Checked == true)
                {
                    materialusado[i] = Presostato.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialPresostato, tecnico.Value);
                    i++;
                }
                if (CheckSonda.Checked == true)
                {
                    materialusado[i] = Sonda.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialSonda, tecnico.Value);
                    i++;
                }
                if (CheckTapavolumetrico.Checked == true)
                {
                    materialusado[i] = Tapavolumetrico.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialTapavolumetrico, tecnico.Value);
                    i++;
                }
                if (CheckTermostato.Checked == true)
                {
                    materialusado[i] = Termostato.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialLanza, tecnico.Value);
                    i++;
                }
                if (CheckValvula.Checked == true)
                {
                    materialusado[i] = Valvula.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialValvula, tecnico.Value);
                    i++;
                }
                if (CheckVolumetrico.Checked == true)
                {
                    materialusado[i] = Volumetrico.ID.ToString();
                    cantidadusada[i] = proceso.obtenerRepuestosTecnico(Convert.ToInt32(inicio), Convert.ToInt32(fin), materialVolumetrico, tecnico.Value);
                    i++;
                }
            }
                Document doc = new Document(PageSize.LETTER);
            var seed = Environment.TickCount;
            var random = new Random(seed);
            var value = random.Next(0, 999999);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(@"D:\Usuario\Desktop\" + value.ToString() + "documento.pdf", FileMode.Create));

            // Le colocamos el título y el autor
            // **Nota: Esto no será visible en el documento
            doc.AddTitle("REPUESTOS USADOS PARA TALLER");
            doc.AddCreator("Fco Guillén");
            // Abrimos el archivo
            doc.Open();

            // Escribimos el encabezamiento en el documento
            string _titulo = "REPUESTOS USADOS PARA TALLER - " + tecnico.Value;
            string _subrayado = "";
            for (int con = 0; con < (2*_titulo.Length); con++) _subrayado += "-";
            doc.Add(new Paragraph(_titulo));
            doc.Add(new Paragraph(_subrayado));
            doc.Add(Chunk.NEWLINE);

            for (int contador = 0; contador < totalMaterial; contador++)
            {
                doc.Add(new Paragraph(cantidadusada[contador].ToString() + " ### " + materialusado[contador]));
                doc.Add(Chunk.NEWLINE);
            }

            doc.Close();
            writer.Close();
            string pdfPath = @"D:\Usuario\Desktop\" + value.ToString() + "documento.pdf";
            Process.Start(pdfPath);

            Response.Clear();
        }
    }
}