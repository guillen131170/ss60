﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.Repuestos
{
    public partial class repuestoentrada : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        #region COMPROBAR ERRORES FORMULARIO
        /// <summary>
        /// Comprueba los errores de campos del formulario
        /// </summary>
        private Boolean erroresFormulario()
        {
            Boolean error = false;
            procesosRepuestos consulta = new procesosRepuestos();

            if (nombre.Value.Equals(""))
            { error = true; }
            if (referencia.Value.Equals(""))
            { error = true; }
            if (precio.Value.Equals(""))
            { error = true; }
            if (fecha.Value.Equals(""))
            { error = true; }

            return error;
        }


        #endregion


        #region INSERTAR REPUESTO NUEVO
        protected void altaregistro_Click(object sender, EventArgs e)
        {
            if (!erroresFormulario())
            {
                procesosRepuestos proceso = new procesosRepuestos();
                clrepuestos item = new clrepuestos();
                clrepuestospedido repuesto = new clrepuestospedido();

                string mensajeerror = "";
                if (!proceso.comprobarDuplicados(referencia.Value))
                {
                    mensajeerror += "no existe el artículo - ";
                    //item.Precio = (float)Convert.ToDouble(precio.Value);
                    item.Precio = (float)(Convert.ToDouble(pu.Value.TrimStart(' ').TrimEnd(' ')));
                    item.Nombre = nombre.Value.TrimStart(' ').TrimEnd(' ');
                    item.Descripcion = descripcion.Value.TrimStart(' ').TrimEnd(' ');
                    item.Proveedor = proveedor.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                    item.Referencia = referencia.Value.TrimStart(' ').TrimEnd(' ');
                    item.Familia = familia.SelectedItem.ToString().TrimStart(' ').TrimEnd(' ');
                    item.Imagen = "";
                    item.Stock = 0;

                    if (!proceso.InsertaRegistro(item))
                    {
                        mensajeerror += "error al crear artículo - ";
                        status.Text = mensajeerror;
                        statusdiv.Attributes["class"] = "alert alert-danger";
                    }
                    else
                    {
                        mensajeerror += "artículo creado - ";
                    }
                }

                repuesto.Referencia = referencia.Value;
                repuesto.Pedido = pedido.Value;
                repuesto.Unidades = Convert.ToInt32(unidades.Value);
                //repuesto.Precio = (float)(repuesto.Unidades * Convert.ToDouble(precio.Value));
                repuesto.Precio = (float)Convert.ToDouble(precio.Value);
                repuesto.Fecha = Convert.ToInt32(fecha.Value);
                repuesto.Nombre = nombre.Value;
                repuesto.Proveedor = proveedor.SelectedItem.ToString();
                repuesto.Familia = familia.SelectedItem.ToString();

                if (!proceso.InsertaRegistroPedido(repuesto))
                {
                    status.Text = mensajeerror + "Error al guardar pedido";
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
                else
                {
                    status.Text = mensajeerror + "Pedido guardado";
                    statusdiv.Attributes["class"] = "alert alert-success";
                }
            }
            else
            {
                status.Text = "Revise los datos del formulario";
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
        }
        #endregion
    }
}