﻿using SolutionS60.APPLICATION;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB.Webpages.Private.Almacen.Repuestos
{
    public partial class repuestoscatalogo : System.Web.UI.Page
    {
        #region CARGAR PAGINA
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                int lista = -111;
                lista = mostrarLista();

                if (lista <= 0)
                {
                    status.Text = "No se encontraron registros";
                    statusdiv.Attributes["class"] = "alert alert-sucess";
                }
            }
        }

        public int mostrarLista()
        {
            procesosRepuestos proceso = new procesosRepuestos();
            List<clrepuestos> lista = new List<clrepuestos>();
            try
            {
                lista = proceso.listarRepuestos();
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Referencia");
                dt.Columns.Add("Nombre");
                dt.Columns.Add("Precio");
                dt.Columns.Add("Familia");
                dt.Columns.Add("Proveedor");
                //dt.Columns.Add("ODSEntrada");
                foreach (clrepuestos registro in lista)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = registro.Id;
                    dr[1] = registro.Referencia;
                    dr[2] = registro.Nombre;
                    dr[3] = registro.Precio;
                    dr[4] = registro.Familia;
                    dr[5] = registro.Proveedor;
                    dt.Rows.Add(dr);
                }
                tablaRegistros.Visible = true;
                tablaRegistros.DataSource = dt;
                tablaRegistros.DataBind();
                return lista.Count;
            }
            catch (Exception ex)
            {
                // las excepciones al status y cual es el error
                status.Text = ex.Message;
                statusdiv.Attributes["class"] = "alert alert-danger";
            }
            return 0;
        }
        #endregion


        #region BOTONES DE LA TABLA DE REGISTROS
        protected void tablaRegistros_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            procesosRepuestos proceso = new procesosRepuestos();
            clrepuestos registro = new clrepuestos();
            int idProducto = -111;
            idProducto = Convert.ToInt32(tablaRegistros.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);
            //registro = proceso.obtenerRegistroId(idProducto);
            if (idProducto != -111)
            {
                try
                {

                    switch (e.CommandName)
                    {
                        case "Ver_Click":
                            break;

                        default:
                            throw new Exception("Operación desconocida");
                    }
                    mostrarLista();
                }
                catch (Exception ex)
                {
                    // recoge errores hacia el status text
                    status.Text = ex.Message;
                    statusdiv.Attributes["class"] = "alert alert-danger";
                }
            }
        }
        #endregion
    }
}