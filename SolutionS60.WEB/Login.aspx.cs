﻿using SolutionS60.APPLICATION.USUARIOS;
using SolutionS60.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session.Abandon();
            //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
        }


        protected void Unnamed6_Click(object sender, EventArgs e)
        {
            if (IsValid && (!Email.Text.Equals("") && !Password.Text.Equals("")))
            {
                procesosUsuarios proceso = new procesosUsuarios();
                if (proceso.Autentificado(Email.Text, Encrypt.GetSHA1(Password.Text)))
                {
                    Session.Timeout = 10;
                    Session["Usuario"] = Email.Text;
                    Response.Redirect("~/Default/Login");
                }
                else
                {
                    status.Text = "Invalid login attempt";
                    statusdiv.Visible = true;
                    Response.Redirect("~/Login");
                }                     
            }
        }
    }
}