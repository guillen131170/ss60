﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SolutionS60.WEB
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se verifica que no se esté realizando una doble solicitud
                if (!IsPostBack)
                {
                    //Si el usuario está autenticado muestra el mensaje y oculta el formulario
                    /*DESMARCAR PARA AUTENTIFICACION
                    if (Session["Usuario"] != null)
                    {
                        identificado.Visible = true;
                    }
                    else
                    {
                        identificado.Visible = false;
                    }*/
                }
            }
            catch (Exception ex)
            {
                statusMaster.Text = ex.Message;
                statusMasterdiv.Attributes["class"] = "alert alert-danger";
            }
        }


    }
}

