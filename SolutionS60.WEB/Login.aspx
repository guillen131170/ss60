﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SolutionS60.WEB.Login" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="statusdiv" runat="server">
        <asp:Literal runat="server" ID="status"></asp:Literal>
    </div>
<div class="jumbotron">
<h1>AUTENTIFICACIÓN DE ACCESO</h1>

    <div class="row">
        <div class="col-md-8">

<div class="panel panel-default">
    <div class="panel-heading">IDENTIFICACIÓN DE USUARIO</div>
        <div class="panel-body">  

            <section id="loginForm">

                <div class="form-horizontal" id="logeo" runat="server">
                    <h5><u>DATOS DE SOLICITUD</u></h5>
                    <hr />

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Correo Electrónico</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                CssClass="text-danger" ErrorMessage="The email field is required." />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Contraseña</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="The password field is required." />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <asp:Button runat="server" Text="Solicitud" CssClass="btn btn-default" OnClick="Unnamed6_Click" />
                        </div>
                    </div>
                </div>
                
                    <asp:HyperLink runat="server" ID="RegisterHyperLink" ViewStateMode="Disabled">
                    </asp:HyperLink>
                
            </section>

        </div>
    </div>
</div>

        </div>
    </div>
</div>
</asp:Content>
