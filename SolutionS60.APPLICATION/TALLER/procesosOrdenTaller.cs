﻿using CafeS60.CORE;
using SolutionS60.CORE;
using SolutionS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION
{
    public class procesosOrdenTaller
    {
        #region OBTENER TOTAL ORDENES DE TALLER GRAFICO 1
        public int obtenerGrafico1(int fecha)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.obtenerTotalOrdenesGrafico1(fecha);
        }
        #endregion


        #region OBTENER TOTAL REPUESTOS USADOS GRAFICO 2
        public int obtenerGrafico2(int inicio, int fin, string[] material)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.obtenerTotalRepuestoGrafico2(inicio, fin, material);
        }
        #endregion


        #region DEVUELVE UNA LISTA DE REGISTROS - ORDEN DE TALLER - DESPLAZAMIENTOS
        public List<clordentaller> obtenerListaDesplazamientos(int i, int f, string proyecto)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.listaDesplazamientos(i, f, proyecto);
        }
        #endregion 


        #region OBTENER TOTAL REPUESTOS USADOS TECNICOS
        public int obtenerRepuestosTecnico(int inicio, int fin, string[] material, string tecnico)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.obtenerTotalRepuestoTecnico(inicio, fin, material, tecnico);
        }
        #endregion


        #region COMPROBAR DUPLICADOS
        public bool comprobarDuplicados(int id)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.duplicado(id);
        }
        #endregion


        #region INSERTA UN REGISTRO ORDEN DE TALLER
        public bool InsertaRegistro(clordentaller registro)
        {
            bool resultado = false;
            SqlOrderTaller proceso = new SqlOrderTaller();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region DEVUELVE EL ULTIMO REGISTRO ORDEN DE TALLER
        public clordentaller obtenerUltimoRegistro()
        {
            clordentaller registro = new clordentaller();
            SqlOrderTaller proceso = new SqlOrderTaller();
            registro = proceso.obtenerUltimoRegistro();
            return registro;
        }
        #endregion


        #region DEVUELVE UN REGISTRO POR SU ID - ORDEN DE TALLER
        public clordentaller obtenerRegistroId(int id)
        {
            clordentaller registro = new clordentaller();
            SqlOrderTaller proceso = new SqlOrderTaller();
            registro = proceso.obtenerRegistroId(id);
            return registro;
        }
        #endregion


        #region DEVUELVE UNA LISTA DE REGISTROS POR REPUESTOS USADOS - ORDEN DE TALLER
        public List<clordentaller> obtenerListaRegistroPorRepuestos(int inicio, int fin, string repuesto, string proyecto)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.listarPorRepuesto(inicio, fin, repuesto, proyecto);
        }
        #endregion


        #region DEVUELVE UNA LISTA DE REGISTROS - ORDEN DE TALLER
        public List<clordentaller> obtenerListaRegistro(int inicio, int fin, string estado, string proyecto)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.lista(inicio, fin, estado, proyecto);
        }
        #endregion 


        #region DEVUELVE UNA LISTA DE REGISTROS - ORDEN DE TALLER
        public List<clordentaller> obtenerListaRegistro(int inicio, int fin, string estado, string proyecto, float importe)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.lista(inicio, fin, estado, proyecto, importe);
        }
        #endregion 


        #region TRASPASO 1 - ORDEN DE TALLER
        public List<OBSOLETO_OTaller> obtenerListaAntigua()
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.busca();
        }
        #endregion


        #region TRASPASO 2 - ORDEN DE TALLER
        public List<clordentaller> obtenerListaCopia()
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.buscaParaCopia();
        }
        #endregion


        #region MODIFICA UNA ORDEN DE TALLER
        public bool modificarOrden(int idObjeto, clordentaller objeto)
        {
            bool resultado = false;
            SqlOrderTaller proceso = new SqlOrderTaller();
            if (proceso.modificar(idObjeto, objeto) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region MODIFICA UNA ORDEN DE TALLER - FECHAS
        public void modificarOrdenFechas(int id, int f1, int f2)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            proceso.modificarFechas(id, f1, f2);
        }
        #endregion


        #region CONCLUYE UNA ORDEN DE TALLER EN SU ESTADO Y RESULTAFDO
        public void concluirOrden(int id, string estado, string resultado)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            proceso.concluir(id, estado, resultado);
        }
        #endregion


        #region OBTENER TOTALES - OFERTA - REPUESTO - MO - DESPLAZAMIENTO
        public float obtenerTotales(int fechainicio, int fechafin, string mecanico, string objeto, string proyecto, float importe)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.valorTotal(fechainicio, fechafin, mecanico, objeto, proyecto, importe);
        }
        #endregion


        #region OBTENER TOTALES - OFERTA - REPUESTO - MO - DESPLAZAMIENTO
        public float obtenerTotales(int fechainicio, int fechafin, string mecanico, string objeto, string proyecto)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.valorTotal(fechainicio, fechafin, mecanico, objeto, proyecto);
        }
        #endregion


        #region OBTENER TOTALES - OFERTA - REPUESTO - MO - DESPLAZAMIENTO
        public float obtenerTotales(int fechainicio, int fechafin, string mecanico, string objeto, string proyecto, string tecnico)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.valorTotal(fechainicio, fechafin, mecanico, objeto, proyecto, tecnico);
        }
        #endregion


        #region DEVUELVE UNA LISTA DE REGISTROS POR ODS- ORDEN DE TALLER
        public List<clordentaller> obtenerListaRegistroOds(int inicio, int fin, string estado, string proyecto, string ods)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.listaOds(inicio, fin, estado, proyecto, ods);
        }
        #endregion


        #region DEVUELVE UNA LISTA DE REGISTROS POR PDS- ORDEN DE TALLER
        public List<clordentaller> obtenerListaRegistroPds(int inicio, int fin, string estado, string proyecto, string pds)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.listaPds(inicio, fin, estado, proyecto, pds);
        }
        #endregion


        #region DEVUELVE UNA LISTA DE REGISTROS POR NOMBRE- ORDEN DE TALLER
        public List<clordentaller> obtenerListaRegistroNombre(int inicio, int fin, string estado, string proyecto, string nombre)
        {
            SqlOrderTaller proceso = new SqlOrderTaller();
            return proceso.listaNombre(inicio, fin, estado, proyecto, nombre);
        }
        #endregion 
    }
}
