﻿using SolutionS60.CORE.COMPRAS;
using SolutionS60.DAL.COMPRAS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION.COMPRAS
{
    public class procesosCompras
    {
        #region LISTAR ALBARANES
        public List<clalbaranes> listarRepuestosE()
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerTodosE();
        }

        public List<clalbaranes> listarRepuestosS()
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerTodosS();
        }

        public List<clalbaranes> listarRepuestosC()
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerTodosC();
        }
        #endregion


        #region LISTAR ALBARANES POR NUMERO DE ALBARAN
        public List<clalbaranes> listarAlbaranesPorAlbaran(string albaran)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerTodosAlbaran(albaran);
        }
        #endregion


        #region LISTAR ALBARANES POR REFERENCIA
        public List<clalbaranes> listarAlbaranesPorReferencia(string referencia)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerTodosReferencia(referencia);
        }
        #endregion


        #region LISTAR ALBARANES ENTRE FECHAS
        public List<clalbaranes> listarRepuestosE(int i, int f)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerTodosE(i, f);
        }

        public List<clalbaranes> listarRepuestosS(int i, int f)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerTodosS(i, f);
        }

        public List<clalbaranes> listarRepuestosC(int i, int f)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerTodosC(i, f);
        }
        #endregion


        #region OBTIENE ALBARAN POR ID
        public clalbaranes obtenerRegistroPorIdE(int id)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerRegistroIdE(id);
        }

        public clalbaranes obtenerRegistroPorIdS(int id)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerRegistroIdS(id);
        }

        public clalbaranes obtenerRegistroPorIdC(int id)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.obtenerRegistroIdC(id);
        }
        #endregion


        #region INSERTA UN ALBARAN
        public bool InsertaRegistroE(clalbaranes registro)
        {
            bool resultado = false;
            SqlCompras proceso = new SqlCompras();
            if (proceso.insertaE(registro) > 0) resultado = true;
            return resultado;
        }
        public bool InsertaRegistroS(clalbaranes registro)
        {
            bool resultado = false;
            SqlCompras proceso = new SqlCompras();
            if (proceso.insertaS(registro) > 0) resultado = true;
            return resultado;
        }

        public bool InsertaRegistroC(clalbaranes registro)
        {
            bool resultado = false;
            SqlCompras proceso = new SqlCompras();
            if (proceso.insertaC(registro) > 0) resultado = true;
            return resultado;
        }

        #endregion


        #region COMPROBAR DUPLICADOS
        public bool comprobarDuplicadosE(string referencia)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.duplicadoE(referencia);
        }

        public bool comprobarDuplicadosS(string referencia)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.duplicadoS(referencia);
        }

        public bool comprobarDuplicadosC(string referencia)
        {
            SqlCompras proceso = new SqlCompras();
            return proceso.duplicadoC(referencia);
        }
        #endregion
    }
}
