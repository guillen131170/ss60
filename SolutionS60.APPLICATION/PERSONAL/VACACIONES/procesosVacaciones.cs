﻿using SolutionS60.CORE.PERSONAL.VACACIONES;
using SolutionS60.DAL.PERSONAL.VACACIONES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION.PERSONAL.VACACIONES
{
    public class procesosVacaciones
    {
        #region LISTAR REGISTROS
        public List<clvacaciones> listarRepuestos()
        {
            SqlVacaciones proceso = new SqlVacaciones();
            return proceso.obtenerTodos();
        }
        #endregion


        #region LISTAR REGISTROS POR NOMBRE
        public List<clvacaciones> obtenerTodosNombre(string nombre)
        {
            SqlVacaciones proceso = new SqlVacaciones();
            return proceso.obtenerTodos(nombre);
        }
        #endregion


        #region LISTAR REGISTROS ENTRE FECHAS
        public List<clvacaciones> listarRepuestos(int i, int f)
        {
            SqlVacaciones proceso = new SqlVacaciones();
            return proceso.obtenerTodos(i, f);
        }
        #endregion


        #region LISTAR REGISTROS ENTRE FECHAS Y CON NOMBRE
        public List<clvacaciones> obtenerTodosFechaNombre(int i, int f, string nombre)
        {
            SqlVacaciones proceso = new SqlVacaciones();
            return proceso.obtenerTodos(i, f, nombre);
        }
        #endregion


        #region OBTIENE REGISTRO POR ID
        public clvacaciones obtenerRegistroPorId(int id)
        {
            SqlVacaciones proceso = new SqlVacaciones();
            return proceso.obtenerRegistroId(id);
        }
        #endregion


        #region OBTIENE REGISTRO POR NOMBRE
        public clvacaciones obtenerRegistroPorNombre(string nombre)
        {
            SqlVacaciones proceso = new SqlVacaciones();
            return proceso.obtenerRegistroNombre(nombre);
        }
        #endregion


        #region INSERTA REGISTRO
        public bool InsertaRegistro(clvacaciones registro)
        {
            bool resultado = false;
            SqlVacaciones proceso = new SqlVacaciones();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion
    }
}
