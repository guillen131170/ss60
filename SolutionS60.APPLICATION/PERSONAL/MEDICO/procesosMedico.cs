﻿using SolutionS60.CORE.PERSONAL.MEDICO;
using SolutionS60.DAL.PERSONAL.MEDICO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION.PERSONAL.MEDICO
{
    public class procesosMedico
    {
        #region LISTAR REGISTROS
        public List<clmedico> listarRepuestos()
        {
            SqlMedico proceso = new SqlMedico();
            return proceso.obtenerTodos();
        }
        #endregion


        #region LISTAR REGISTROS POR NOMBRE
        public List<clmedico> obtenerTodosNombre(string nombre)
        {
            SqlMedico proceso = new SqlMedico();
            return proceso.obtenerTodos(nombre);
        }
        #endregion


        #region LISTAR REGISTROS ENTRE FECHAS
        public List<clmedico> listarRepuestos(int i, int f)
        {
            SqlMedico proceso = new SqlMedico();
            return proceso.obtenerTodos(i, f);
        }
        #endregion


        #region LISTAR REGISTROS ENTRE FECHAS Y CON NOMBRE
        public List<clmedico> obtenerTodosFechaNombre(int i, int f, string nombre)
        {
            SqlMedico proceso = new SqlMedico();
            return proceso.obtenerTodos(i, f, nombre);
        }
        #endregion


        #region OBTIENE REGISTRO POR ID
        public clmedico obtenerRegistroPorId(int id)
        {
            SqlMedico proceso = new SqlMedico();
            return proceso.obtenerRegistroId(id);
        }
        #endregion


        #region INSERTA REGISTRO
        public bool InsertaRegistro(clmedico registro)
        {
            bool resultado = false;
            SqlMedico proceso = new SqlMedico();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion
    }
}
