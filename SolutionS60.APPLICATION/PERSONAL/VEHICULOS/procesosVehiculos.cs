﻿using SolutionS60.CORE.PERSONAL.VEHICULOS;
using SolutionS60.DAL.PERSONAL.VEHICULOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION.PERSONAL.VEHICULOS
{
    public class procesosVehiculos
    {
        #region LISTAR REGISTROS POR TIPO
        public clvehiculos obtenerUltimoRegistroMatricula(string matricula)
        {
            SqlVehiculos proceso = new SqlVehiculos();
            return proceso.obtenerRegistroMatricula(matricula);
        }
        #endregion


        #region LISTAR REGISTROS POR TIPO
        public clvehiculos obtenerUltimoRegistroNombre(string conductor)
        {
            SqlVehiculos proceso = new SqlVehiculos();
            return proceso.obtenerRegistroNombre(conductor);
        }
        #endregion


        #region LISTAR REGISTROS POR TIPO
        public List<clvehiculos> obtenerTodos(string tipo)
        {
            SqlVehiculos proceso = new SqlVehiculos();
            return proceso.obtenerTodos(tipo);
        }
        #endregion


        #region LISTAR REGISTROS POR TIPO Y POR CONDUCTOR
        public List<clvehiculos> obtenerTodos(string tipo, string conductor)
        {
            SqlVehiculos proceso = new SqlVehiculos();
            return proceso.obtenerTodos(conductor);
        }
        #endregion


        #region LISTAR REGISTROS POR TIPO Y POR CONDUCTOR  ENTRE FECHAS Y CON NOMBRE
        public List<clvehiculos> obtenerTodos(int i, int f, string tipo, string conductor)
        {
            SqlVehiculos proceso = new SqlVehiculos();
            return proceso.obtenerTodos(i, f, tipo, conductor);
        }
        #endregion


        #region OBTIENE REGISTRO POR ID
        public clvehiculos obtenerRegistroPorId(int id)
        {
            SqlVehiculos proceso = new SqlVehiculos();
            return proceso.obtenerRegistroId(id);
        }
        #endregion


        #region INSERTA REGISTRO
        public bool InsertaRegistro(clvehiculos registro)
        {
            bool resultado = false;
            SqlVehiculos proceso = new SqlVehiculos();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion
    }
}
