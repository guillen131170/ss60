﻿using SolutionS60.CORE.PERSONAL.VESTUARIO;
using SolutionS60.DAL.PERSONAL.VESTUARIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION.PERSONAL.VESTUARIO
{
    public class procesosVestuario
    {
        #region LISTAR ALBARANES
        public List<clvestuario> listarRepuestos()
        {
            SqlVestuario proceso = new SqlVestuario();
            return proceso.obtenerTodos();
        }
        #endregion


        #region LISTAR REGISTROS POR NOMBRE
        public List<clvestuario> obtenerTodosNombre(string nombre)
        {
            SqlVestuario proceso = new SqlVestuario();
            return proceso.obtenerTodosNombre(nombre);
        }
        #endregion


        #region LISTAR REGISTROS ENTRE FECHAS Y CON NOMBRE
        public List<clvestuario> obtenerTodosFechaNombre(int i, int f, string nombre)
        {
            SqlVestuario proceso = new SqlVestuario();
            return proceso.obtenerTodos(i, f, nombre);
        }
        #endregion


        #region LISTAR ALBARANES ENTRE FECHAS
        public List<clvestuario> listarRepuestos(int i, int f)
        {
            SqlVestuario proceso = new SqlVestuario();
            return proceso.obtenerTodos(i, f);
        }
        #endregion


        #region OBTIENE ALBARAN POR ID
        public clvestuario obtenerRegistroPorId(int id)
        {
            SqlVestuario proceso = new SqlVestuario();
            return proceso.obtenerRegistroId(id);
        }
        #endregion


        #region INSERTA UN ALBARAN
        public bool InsertaRegistro(clvestuario registro)
        {
            bool resultado = false;
            SqlVestuario proceso = new SqlVestuario();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region COMPROBAR DUPLICADOS
        public bool comprobarDuplicados(string referencia)
        {
            SqlVestuario proceso = new SqlVestuario();
            return proceso.duplicado(referencia);
        }
        #endregion
    }
}
