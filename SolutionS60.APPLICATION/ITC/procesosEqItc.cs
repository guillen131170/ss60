﻿using SolutionS60.CORE.ITC;
using SolutionS60.DAL.ITC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION.ITC
{
    public class procesosEqItc
    {
        #region COMPROBAR DUPLICADOS
        public bool comprobarDuplicados(string referencia)
        {
            SqlEqItc proceso = new SqlEqItc();
            return proceso.duplicado(referencia);
        }
        #endregion


        #region DEVUELVE UNA REGISTRO DE EQUIPO ITC POR NS
        public cleqItc obtenerLRegistro(string ns)
        {
            SqlEqItc proceso = new SqlEqItc();
            return proceso.obtenerRegistroNS(ns);
        }
        #endregion


        #region INSERTA UN REGISTRO ITC
        public bool InsertaRegistro(cleqItc registro)
        {
            bool resultado = false;
            SqlEqItc proceso = new SqlEqItc();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion

        #region INSERTA UN REGISTRO ITC ACTIVIDAD
        public bool InsertaRegistro(cleqItcActividad registro)
        {
            bool resultado = false;
            SqlEqItc proceso = new SqlEqItc();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion

        #region INSERTA UN REGISTRO ITC UBICACION
        public bool InsertaRegistro(cleqItcUbicacion registro)
        {
            bool resultado = false;
            SqlEqItc proceso = new SqlEqItc();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region DEVUELVE UNA LISTA DE REGISTROS ITC
        public List<cleqItc> obtenerLista()
        {
            SqlEqItc proceso = new SqlEqItc();
            return proceso.lista();
        }
        #endregion

        #region DEVUELVE UNA LISTA DE REGISTROS ACTIVIDAD
        public List<cleqItcActividad> obtenerListaActividad()
        {
            SqlEqItc proceso = new SqlEqItc();
            return proceso.listaActividad();
        }
        #endregion

        #region DEVUELVE UNA LISTA DE REGISTROS ESTADO
        public List<cleqItcUbicacion> obtenerListaUbicacion()
        {
            SqlEqItc proceso = new SqlEqItc();
            return proceso.listaUbicacion();
        }
        #endregion


        #region MODIFICA UNA REGISTRO ITC
        public bool modificarRegistro(string ns, cleqItc objeto)
        {
            bool resultado = false;
            SqlEqItc proceso = new SqlEqItc();
            if (proceso.modificar(ns, objeto) > 0) resultado = true;
            return resultado;
        }
        #endregion

        #region MODIFICA UNA REGISTRO ACTIVIDAD
        public void modificarRegistro(int idObjeto, cleqItcActividad objeto)
        {
            SqlEqItc proceso = new SqlEqItc();
            proceso.modificar(idObjeto, objeto);
        }
        #endregion

        #region MODIFICA UNA REGISTRO ESTADO
        public void modificarRegistro(string ns, cleqItcUbicacion objeto)
        {
            SqlEqItc proceso = new SqlEqItc();
            proceso.modificar(ns, objeto);
        }
        #endregion
    }
}
