﻿using SolutionS60.CORE;
using SolutionS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION
{
    public class procesosEqCafento
    {
        #region INSERTA UN REGISTRO EQUIPO SERIADO CAFENTO
        public bool InsertaRegistro(cleqseriadocafento registro)
        {
            bool resultado = false;
            SqlEqCafento proceso = new SqlEqCafento();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR ÚLTIMO ESTADO DE UN EQUIPO POR SU NS AX
        public cleqseriadocafento ultimoEstadoEquipo(string ax)
        {
            SqlEqCafento proceso = new SqlEqCafento();
            return proceso.obtenerUltimoEstadoEquipo(ax);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR NS AX
        public List<cleqseriadocafento> movimientosAx(string ax)
        {
            SqlEqCafento proceso = new SqlEqCafento();
            return proceso.obtenerMovimientosAx(ax);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR MODELO
        public List<cleqseriadocafento> movimientosModelo(string modelo)
        {
            SqlEqCafento proceso = new SqlEqCafento();
            return proceso.obtenerMovimientosModelo(modelo);
        }
        #endregion


        #region MODIFICAR EQUIPO
        public bool modificarEquipo(int id, cleqseriadocafento objeto)
        {
            bool resultado = false;
            SqlEqCafento proceso = new SqlEqCafento();
            if (proceso.modificar(id, objeto) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR EQUIPOS CAFENTO PREPARADOS PARA SALIR
        public List<cleqseriadocafento> InformeBasicoRegistro()
        {
            SqlEqCafento proceso = new SqlEqCafento();
            return proceso.busca();
        }
        #endregion


        #region SACAR EQUIPO DEL SISTEMA SIN ODS - PARA CAFENTO
        public void sacarEquipo(int id)
        {
            SqlEqCafento proceso = new SqlEqCafento();
            proceso.sacar(id);
        }
        #endregion


        #region SACAR EQUIPO DEL SISTEMA CON ODS - PARA MONTAR
        public void sacarEquipo(int id, string ods, string destino)
        {
            SqlEqCafento proceso = new SqlEqCafento();
            proceso.sacar(id, ods, destino);
        }
        #endregion
    }
}
