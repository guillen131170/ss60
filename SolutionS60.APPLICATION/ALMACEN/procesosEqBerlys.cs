﻿using SolutionS60.CORE;
using SolutionS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION
{
    public class procesosEqBerlys
    {
        #region INSERTA UN REGISTRO EQUIPO SERIADO BERLYS
        public bool InsertaRegistro(cleqseriadoberlys registro)
        {
            bool resultado = false;
            SqlEqBerlys proceso = new SqlEqBerlys();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR ÚLTIMO ESTADO DE UN EQUIPO POR SU NS AX
        public cleqseriadoberlys ultimoEstadoEquipo(string ax)
        {
            SqlEqBerlys proceso = new SqlEqBerlys();
            return proceso.obtenerUltimoEstadoEquipo(ax);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR NS AX
        public List<cleqseriadoberlys> movimientosAx(string ax)
        {
            SqlEqBerlys proceso = new SqlEqBerlys();
            return proceso.obtenerMovimientosAx(ax);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR MODELO
        public List<cleqseriadoberlys> movimientosModelo(string modelo)
        {
            SqlEqBerlys proceso = new SqlEqBerlys();
            return proceso.obtenerMovimientosModelo(modelo);
        }
        #endregion


        #region MODIFICAR EQUIPO
        public bool modificarEquipo(int id, cleqseriadoberlys objeto)
        {
            bool resultado = false;
            SqlEqBerlys proceso = new SqlEqBerlys();
            if (proceso.modificar(id, objeto) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region SACAR EQUIPO DEL SISTEMA CON ODS - PARA MONTAR
        public void sacarEquipo(int id, string ods, string destino)
        {
            SqlEqBerlys proceso = new SqlEqBerlys();
            proceso.sacar(id, ods, destino);
        }
        #endregion


        #region BUSCAR EQUIPOS BERLYS PREPARADOS PARA SALIR
        public List<cleqseriadoberlys> InformeBasicoRegistro()
        {
            SqlEqBerlys proceso = new SqlEqBerlys();
            return proceso.busca();
        }
        #endregion
    }
}
