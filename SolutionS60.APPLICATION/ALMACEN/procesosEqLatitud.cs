﻿using SolutionS60.CORE;
using SolutionS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION
{
    public class procesosEqLatitud
    {
        #region INSERTA UN REGISTRO EQUIPO SERIADO CAFENTO
        public bool InsertaRegistro(cleqseriadolatitud registro)
        {
            bool resultado = false;
            SqlEqLatitud proceso = new SqlEqLatitud();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR ÚLTIMO ESTADO DE UN EQUIPO POR SU NS AX
        public cleqseriadolatitud ultimoEstadoEquipo(string ax)
        {
            SqlEqLatitud proceso = new SqlEqLatitud();
            return proceso.obtenerUltimoEstadoEquipo(ax);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR NS AX
        public List<cleqseriadolatitud> movimientosAx(string ax)
        {
            SqlEqLatitud proceso = new SqlEqLatitud();
            return proceso.obtenerMovimientosAx(ax);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR MODELO
        public List<cleqseriadolatitud> movimientosModelo(string modelo)
        {
            SqlEqLatitud proceso = new SqlEqLatitud();
            return proceso.obtenerMovimientosModelo(modelo);
        }
        #endregion


        #region MODIFICAR EQUIPO
        public bool modificarEquipo(int id, cleqseriadolatitud objeto)
        {
            bool resultado = false;
            SqlEqLatitud proceso = new SqlEqLatitud();
            if (proceso.modificar(id, objeto) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region SACAR EQUIPO DEL SISTEMA CON ODS - PARA MONTAR
        public void sacarEquipo(int id, string ods, string destino)
        {
            SqlEqLatitud proceso = new SqlEqLatitud();
            proceso.sacar(id, ods, destino);
        }
        #endregion


        #region BUSCAR EQUIPOS CAFENTO PREPARADOS PARA SALIR
        public List<cleqseriadolatitud> InformeBasicoRegistro()
        {
            SqlEqLatitud proceso = new SqlEqLatitud();
            return proceso.busca();
        }
        #endregion


        #region BUSCAR EQUIPOS CAFENTO PREPARADOS PARA SALIR FILTRADO POR MODELO
        public List<cleqseriadolatitud> InformeBasicoRegistroModelo(string modelo)
        {
            SqlEqLatitud proceso = new SqlEqLatitud();
            return proceso.buscaModelo(modelo);
        }
        #endregion
    }
}
