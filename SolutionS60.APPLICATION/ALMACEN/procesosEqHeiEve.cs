﻿using SolutionS60.CORE;
using SolutionS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION
{
    public class procesosEqHeiEve
    {
        #region INSERTA UN REGISTRO EQUIPO
        public bool InsertaRegistro(cleqseriadoheieve registro)
        {
            bool resultado = false;
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region MODIFICA STOCK DE REGISTRO CON UNA ENTRADA
        public bool Entrada(int id, int numero)
        {
            bool resultado = false;
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            if (proceso.entrada(id, numero) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE ENTRADA
        public bool EntradaMovimiento(cleqseriadoheieve registro)
        {
            bool resultado = false;
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            if (proceso.entradaMovimiento(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR REGISTRO POR MATERIAL
        public int buscaMaterial(string material)
        {
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            return proceso.existeMaterial(material);
        }
        #endregion


        #region BUSCAR REGISTRO POR CODIGO MATERIAL
        public int buscaCodigo(string material)
        {
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            return proceso.existeCodigo(material);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR NOMBRE MATERIAL
        public List<cleqseriadoheieve> movimientosMaterial(string material)
        {
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            return proceso.obtenerMovimientosMaterial(material);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR CODIGO MATERIAL
        public List<cleqseriadoheieve> movimientosCodigo(string material)
        {
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            return proceso.obtenerMovimientosCodigo(material);
        }
        #endregion


        #region MODIFICAR EQUIPO
        public bool modificarEquipo(int id, cleqseriadoheieve objeto)
        {
            bool resultado = false;
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            if (proceso.modificar(id, objeto) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR TODOS LOS REGISTROS
        public List<cleqseriadoheieve> InformeBasicoRegistro()
        {
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            return proceso.busca();
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS
        public List<cleqseriadoheieve> InformeBasicoMovimientos()
        {
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            return proceso.buscaMovimientos();
        }
        #endregion


        #region MODIFICA STOCK DE REGISTRO CON UNA SALIDA
        public bool Salida(int id, int numero)
        {
            bool resultado = false;
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            if (proceso.salida(id, numero) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE SALIDA
        public bool SalidaMovimiento(cleqseriadoheieve registro)
        {
            bool resultado = false;
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            proceso.sacar(registro);
            return resultado;
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE SALIDA SIN ODS
        public void sacarEquipo(int id, int numero)
        {
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            proceso.sacar(id, numero);
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE SALIDA CON ODS
        public void sacarEquipo(int id, string ods, string destino, int numero)
        {
            SqlEqHeiEve proceso = new SqlEqHeiEve();
            proceso.sacar(id, ods, destino, numero);
        }
        #endregion
    }
}
