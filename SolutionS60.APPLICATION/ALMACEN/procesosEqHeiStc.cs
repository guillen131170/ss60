﻿using SolutionS60.CORE;
using SolutionS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION
{
    public class procesosEqHeiStc
    {
        #region EQUIPOS DE STD NO SERIADOS
        #region INSERTA UN REGISTRO EQUIPO
        public bool InsertaRegistro(cleqnoseriadoheistd registro)
        {
            bool resultado = false;
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region MODIFICA STOCK DE REGISTRO CON UNA ENTRADA
        public bool Entrada(int id, int numero)
        {
            bool resultado = false;
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            if (proceso.entrada(id, numero) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE ENTRADA
        public bool EntradaMovimiento(cleqnoseriadoheistd registro)
        {
            bool resultado = false;
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            if (proceso.entradaMovimiento(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR REGISTRO POR MATERIAL
        public int buscaMaterial(string material)
        {
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            return proceso.existeMaterial(material);
        }
        #endregion


        #region BUSCAR REGISTRO POR CODIGO MATERIAL
        public int buscaCodigo(string material)
        {
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            return proceso.existeCodigo(material);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR NOMBRE MATERIAL
        public List<cleqnoseriadoheistd> movimientosMaterial(string material)
        {
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            return proceso.obtenerMovimientosMaterial(material);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR CODIGO MATERIAL
        public List<cleqnoseriadoheistd> movimientosCodigo(string material)
        {
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            return proceso.obtenerMovimientosCodigo(material);
        }
        #endregion


        #region MODIFICAR EQUIPO
        public bool modificarEquipo(int id, cleqnoseriadoheistd objeto)
        {
            bool resultado = false;
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            if (proceso.modificar(id, objeto) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR TODOS LOS REGISTROS
        public List<cleqnoseriadoheistd> InformeBasicoRegistro()
        {
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            return proceso.busca();
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS
        public List<cleqnoseriadoheistd> InformeBasicoMovimientos()
        {
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            return proceso.buscaMovimientos();
        }
        #endregion


        #region MODIFICA STOCK DE REGISTRO CON UNA SALIDA
        public bool Salida(int id, int numero)
        {
            bool resultado = false;
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            if (proceso.salida(id, numero) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE SALIDA
        public bool SalidaMovimiento(cleqnoseriadoheistd registro)
        {
            bool resultado = false;
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            proceso.sacar(registro);
            return resultado;
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE SALIDA SIN ODS
        public void sacarEquipo(int id, int numero)
        {
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            proceso.sacar(id, numero);
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE SALIDA CON ODS
        public void sacarEquipo(int id, string ods, string destino, int numero)
        {
            SqlEqHeiStc proceso = new SqlEqHeiStc();
            proceso.sacar(id, ods, destino, numero);
        }
        #endregion
        #endregion


        #region EQUIPOS STD SERIADOS

        #endregion
    }
}
