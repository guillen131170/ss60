﻿using SolutionS60.CORE;
using SolutionS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION
{
    public class procesosEqSch
    {
        #region EQUIPOS DE SCH NO SERIADOS
        #region INSERTA UN REGISTRO EQUIPO
        public bool InsertaRegistro(cleqschnoseriado registro)
        {
            bool resultado = false;
            SqlEqSch proceso = new SqlEqSch();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region MODIFICA STOCK DE REGISTRO CON UNA ENTRADA
        public bool Entrada(int id, int numero)
        {
            bool resultado = false;
            SqlEqSch proceso = new SqlEqSch();
            if (proceso.entrada(id, numero) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE ENTRADA
        public bool EntradaMovimiento(cleqschnoseriado registro)
        {
            bool resultado = false;
            SqlEqSch proceso = new SqlEqSch();
            if (proceso.entradaMovimiento(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR REGISTRO POR MATERIAL
        public int buscaMaterial(string material)
        {
            SqlEqSch proceso = new SqlEqSch();
            return proceso.existeMaterial(material);
        }
        #endregion


        #region BUSCAR REGISTRO POR CODIGO MATERIAL
        public int buscaCodigo(string material)
        {
            SqlEqSch proceso = new SqlEqSch();
            return proceso.existeCodigo(material);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR NOMBRE MATERIAL
        public List<cleqschnoseriado> movimientosMaterial(string material)
        {
            SqlEqSch proceso = new SqlEqSch();
            return proceso.obtenerMovimientosMaterial(material);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR CODIGO MATERIAL
        public List<cleqschnoseriado> movimientosCodigo(string material)
        {
            SqlEqSch proceso = new SqlEqSch();
            return proceso.obtenerMovimientosCodigo(material);
        }
        #endregion


        #region MODIFICAR EQUIPO
        public bool modificarEquipo(int id, cleqschnoseriado objeto)
        {
            bool resultado = false;
            SqlEqSch proceso = new SqlEqSch();
            if (proceso.modificar(id, objeto) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR TODOS LOS REGISTROS
        public List<cleqschnoseriado> InformeBasicoRegistro()
        {
            SqlEqSch proceso = new SqlEqSch();
            return proceso.busca();
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS
        public List<cleqschnoseriado> InformeBasicoMovimientos()
        {
            SqlEqSch proceso = new SqlEqSch();
            return proceso.buscaMovimientos();
        }
        #endregion


        #region MODIFICA STOCK DE REGISTRO CON UNA SALIDA
        public bool Salida(int id, int numero)
        {
            bool resultado = false;
            SqlEqSch proceso = new SqlEqSch();
            if (proceso.salida(id, numero) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE SALIDA
        public bool SalidaMovimiento(cleqschnoseriado registro)
        {
            bool resultado = false;
            SqlEqSch proceso = new SqlEqSch();
            proceso.sacar(registro);
            return resultado;
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE SALIDA SIN ODS
        public void sacarEquipo(int id, int numero)
        {
            SqlEqSch proceso = new SqlEqSch();
            proceso.sacar(id, numero);
        }
        #endregion


        #region INSERTA UN REGISTRO MOVIMIENTO DE SALIDA CON ODS
        public void sacarEquipo(int id, string ods, string destino, int numero)
        {
            SqlEqSch proceso = new SqlEqSch();
            proceso.sacar(id, ods, destino, numero);
        }
        #endregion
        #endregion



        #region EQUIPOS DE SCH SERIADOS
        #region INSERTA UN REGISTRO EQUIPO SERIADO CAFENTO
        public bool InsertaRegistro(cleqschseriado registro)
        {
            bool resultado = false;
            SqlEqSch proceso = new SqlEqSch();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR ÚLTIMO ESTADO DE UN EQUIPO POR SU NS AX
        public cleqschseriado ultimoEstadoEquipo(string ax)
        {
            SqlEqSch proceso = new SqlEqSch();
            return proceso.obtenerUltimoEstadoEquipo(ax);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR NS AX
        public List<cleqschseriado> movimientosAx(string ax)
        {
            SqlEqSch proceso = new SqlEqSch();
            return proceso.obtenerMovimientosAx(ax);
        }
        #endregion


        #region BUSCAR TODOS LOS MOVIMIENTOS DE UN EQUIPO POR MODELO
        public List<cleqschseriado> movimientosModelo(string modelo)
        {
            SqlEqSch proceso = new SqlEqSch();
            return proceso.obtenerMovimientosModelo(modelo);
        }
        #endregion


        #region MODIFICAR EQUIPO
        public bool modificarEquipo(int id, cleqschseriado objeto)
        {
            bool resultado = false;
            SqlEqSch proceso = new SqlEqSch();
            if (proceso.modificar(id, objeto) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region BUSCAR EQUIPOS CAFENTO PREPARADOS PARA SALIR
        public List<cleqschseriado> InformeBasicoRegistroSeriado()
        {
            SqlEqSch proceso = new SqlEqSch();
            return proceso.buscaSriado();
        }
        #endregion


        #region SACAR EQUIPO DEL SISTEMA CON ODS - PARA MONTAR
        public void sacarEquipo(int id, string ods, string destino)
        {
            SqlEqSch proceso = new SqlEqSch();
            proceso.sacar(id, ods, destino);
        }
        #endregion 
        #endregion
    }
}
