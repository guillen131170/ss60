﻿using SolutionS60.CORE;
using SolutionS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION
{
    public class procesosRepuestos
    {
        #region LISTAR REPUESTOS
        public List<clrepuestos> listarRepuestos()
        {
            SqlRepuestos proceso = new SqlRepuestos();
            return proceso.obtenerTodos();
        }
        #endregion


        #region LISTAR PEDIDOS
        public List<clrepuestospedido> obtenerPedidos()
        {
            SqlRepuestos proceso = new SqlRepuestos();
            return proceso.obtenerTodosPedidos();
        }
        #endregion


        #region OBTIENE IMPORTE DE UN PEDIDO
        public float obtenerImporte(string referencia)
        {
            SqlRepuestos proceso = new SqlRepuestos();
            return proceso.importe(referencia);
        }
        #endregion


        #region OBTIENE REGISTRO POR ID
        public clrepuestospedido obtenerRegistroPorId(int id)
        {
            SqlRepuestos proceso = new SqlRepuestos();
            return proceso.obtenerRegistroId(id);
        }
        #endregion


        #region DETALLE PEDIDO POR REFERENCIA
        public List<clrepuestospedido> obtenerDetalle(string referencia)
        {
            SqlRepuestos proceso = new SqlRepuestos();
            return proceso.detallePedido(referencia);
        }
        #endregion


        #region INSERTA UN REGISTRO DE REPUESTO
        public bool InsertaRegistro(clrepuestos registro)
        {
            bool resultado = false;
            SqlRepuestos proceso = new SqlRepuestos();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion


        #region COMPROBAR DUPLICADOS
        public bool comprobarDuplicados(string referencia)
        {
            SqlRepuestos proceso = new SqlRepuestos();
            return proceso.duplicado(referencia);
        }
        #endregion


        #region INSERTA UN REGISTRO DE PEDIDO
        public bool InsertaRegistroPedido(clrepuestospedido registro)
        {
            bool resultado = false;
            SqlRepuestos proceso = new SqlRepuestos();
            if (proceso.insertaPedido(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion
    }
}
