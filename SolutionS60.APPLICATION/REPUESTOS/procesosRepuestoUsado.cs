﻿using SolutionS60.CORE;
using SolutionS60.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION
{
    public class procesosRepuestoUsado
    {
        #region ORDEN TALLER
        #region INSERTA UN REGISTRO ORDEN DE TALLER
        public bool InsertaRegistro(clrepuestotaller registro)
        {
            bool resultado = false;
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion
        #endregion


        #region REPUESTOS LISTA BOOM CAFE
        #region OBTIENE LOS REPUESTOS GENERALES POR COINCIDENCIA DE NOMBRE MATERIAL SAP
        public List<SAP_Repuestos_v001> ObtenerRepuestosGeneralesV001PorNombre(string nombre)
        {
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            return proceso.obtenerRepuestosV001PorNombre(nombre);
        }
        #endregion

        #region OBTIENE LOS REPUESTOS GENERALES POR COINCIDENCIA DE CÓDIGO MATERIAL SAP
        public List<SAP_Repuestos_v001> ObtenerRepuestosGeneralesV001PorCodigo(string codigo)
        {
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            return proceso.obtenerRepuestosV001PorCodigo(codigo);
        }
        #endregion
        #endregion


        #region REPUESTOS LISTA BOOM BERLYS
        #region OBTIENE LISTA BOOM BERLYS POR COINCIDENCIA DE NOMBRE MATERIAL SAP
        public List<SAP_Repuestos_v001> ObtenerRepuestosBoomBlyPorNombre(string nombre)
        {
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            return proceso.obtenerRepuestosBlyPorNombre(nombre);
        }
        #endregion

        #region OBTIENE LISTA BOOM BERLYS POR COINCIDENCIA DE CÓDIGO MATERIAL SAP
        public List<SAP_Repuestos_v001> ObtenerRepuestosBoomBlyPorCodigo(string codigo)
        {
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            return proceso.obtenerRepuestosBlyPorCodigo(codigo);
        }
        #endregion
        #endregion


        #region REPUESTOS LISTA BOOM STARBUCKS
        #region OBTIENE LISTA BOOM STARBUCKS POR COINCIDENCIA DE NOMBRE MATERIAL SAP
        public List<SAP_Repuestos_v001> ObtenerRepuestosBoomStarbucksPorNombre(string nombre)
        {
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            return proceso.obtenerRepuestosStarbucksPorNombre(nombre);
        }
        #endregion

        #region OBTIENE LISTA BOOM STARBUCKS POR COINCIDENCIA DE CÓDIGO MATERIAL SAP
        public List<SAP_Repuestos_v001> ObtenerRepuestosBoomStarbucksPorCodigo(string codigo)
        {
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            return proceso.obtenerRepuestosStarbucksPorCodigo(codigo);
        }
        #endregion 
        #endregion


        #region REPUESTOS GENERALES
        #region OBTIENE LOS REPUESTOS GENERALES POR COINCIDENCIA DE NOMBRE MATERIAL SAP
        public List<SAP_Repuestos_General> ObtenerRepuestosGeneralesPorNombre(string nombre)
        {
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            return proceso.obtenerRepuestosPorNombre(nombre);
        }
        #endregion

        #region OBTIENE LOS REPUESTOS GENERALES POR COINCIDENCIA DE CÓDIGO MATERIAL SAP
        public List<SAP_Repuestos_General> ObtenerRepuestosGeneralesPorCodigo(string codigo)
        {
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            return proceso.obtenerRepuestosPorCodigo(codigo);
        }
        #endregion 
        #endregion


        #region TRASPASO DE REGISTROS
        public void traspasoRegistros()
        {
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            proceso.traspasoDestino(proceso.traspasoOrigen());
        }

        public void traspasoRegistros2()
        {
            SqlRepuestosUsados proceso = new SqlRepuestosUsados();
            proceso.traspasoDestino2(proceso.traspasoOrigen2());
        }
        #endregion 
    }
}
