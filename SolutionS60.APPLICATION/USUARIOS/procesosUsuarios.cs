﻿using SolutionS60.CORE.USUARIOS;
using SolutionS60.DAL.USUARIOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionS60.APPLICATION.USUARIOS
{
    public class procesosUsuarios
    {
        #region AUTENTIFICACION
        public bool Autentificado(string mail, string pass)
        {
            SqlUsuarios proceso = new SqlUsuarios();
            return proceso.autentificacion(mail, pass);
        }
        #endregion


        #region COMPROBAR DUPLICADOS
        public bool Duplicado(string mail)
        {
            SqlUsuarios proceso = new SqlUsuarios();
            return proceso.duplicado(mail);
        }
        #endregion


        #region INSERTA UN REGISTRO ORDEN DE TALLER
        public bool Inserta(clusuarios registro)
        {
            bool resultado = false;
            SqlUsuarios proceso = new SqlUsuarios();
            if (proceso.inserta(registro) > 0) resultado = true;
            return resultado;
        }
        #endregion
    }
}
